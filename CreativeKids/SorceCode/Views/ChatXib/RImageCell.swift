//
//  RImageCell.swift
//  Smooshi
//
//  Created by Shubham Kaliyar on 07/09/20.
//  Copyright © 2020 fluper. All rights reserved.
//

import UIKit

class RImageCell: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imageSend: UIImageView!
    @IBOutlet weak var labelTime: UILabel!
    
    var message:MessageClass?{didSet{self.updateCell()}}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    func updateCell() {
        self.lblName.text = message?.senderName
        self.imageSend.contentMode = .scaleAspectFill
        self.imageSend.downlodeImage(serviceurl: String.getstring(self.message?.message), placeHolder: UIImage())
        self.labelTime.text = String.getTime(timeStamp:Double(message?.sendingTime ?? 0))
    }
    
    @IBAction func showImage(_ sender: UIButton) {
//        let nextvc = ImagePreview()
//        nextvc.modalPresentationStyle = .overFullScreen
//        nextvc.imageUrl = String.getstring(self.message?.message)
//        self.parentViewController?.present(nextvc,animated:true)
        
        let nextvc = ImagePreviewViewController.getController(storyboard: .TeacherDashboard)
        nextvc.cameFor = .Chat
            guard let imageUrl = self.message?.message else {return}
            nextvc.imageArr = [imageUrl]
        nextvc.headerName = "\(message?.senderName ?? "") on \(getDateTimeFromTimeStamp(timeStamp:  self.message?.sendingTime)),\(self.labelTime.text ?? "")"
        self.parentContainerViewController()?.navigationController?.pushViewController(nextvc, animated: true)
    }
}
