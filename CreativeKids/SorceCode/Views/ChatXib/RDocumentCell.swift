//
//  RDocumentCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 21/06/22.
//

import UIKit

class RDocumentCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var labelPdfName: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    
    @IBOutlet weak var imageDownload: UIImageView!
    var didTapCallBack:(()->Void)?
    var message:MessageClass?{didSet{ self.updateCell() }}
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateCell() {
//        viewMain.layer.cornerRadius = 5
        self.lblName.text = message?.senderName
        self.labelPdfName.text = message?.fileName
        self.labelTime.text = String.getTime(timeStamp:Double(message?.sendingTime ?? 0))
        checkForFile()
    }
    
    func checkForFile(){
//        let filePath = CommonUtils.getDocumentDirectoryPath() + "/\(message?.fileName)"
        let filePath = CommonUtils.getDocumentDirectoryPath()
//        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
            let url = NSURL(fileURLWithPath: filePath)
            if let pathComponent = url.appendingPathComponent("\(message?.fileName ?? "")") {
                let filePath = pathComponent.path
                let fileManager = FileManager.default
                if fileManager.fileExists(atPath: filePath) {
                    print("FILE AVAILABLE")
                    imageDownload.isHidden = true
                } else {
                    print("FILE NOT AVAILABLE")
                    imageDownload.isHidden = false
                }
            } else {
                print("FILE PATH NOT AVAILABLE")
                imageDownload.isHidden = false
            }
    }
    
    @IBAction func buttonPdfTapped(_ sender: UIButton) {
        didTapCallBack?()
    }
}
