//
//  VoiceViewXib.swift
//  CreativeKids
//
//  Created by Creative Kids on 22/09/21.
//

import UIKit
import Foundation

protocol MICDelegate{
    func animationStart()
    func animationStop()
}

class VoiceViewXib: UIView {

    @IBOutlet var containerView: UIView!
    @IBOutlet weak var viewInner: UIView!
    var navigationReferances:UINavigationController?
    var customDelegate:MICDelegate?
    var animation = false
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonUnit()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonUnit()
    }

    func commonUnit(){
        Bundle.main.loadNibNamed("VoiceViewXib", owner: self, options: [:])
        containerView.frame = self.bounds
        self.addSubview(containerView)
//        DispatchQueue.main.asyncAfter(deadline: .now()+2) {
//            self.layer.removeFromSuperlayer()
//        }
        
    }
    @IBAction func buttonMicTapped(_ sender: UIButton) {
//        let controller = VoiceViewController.getController(storyboard: .Home)
//        controller.modalTransitionStyle = .crossDissolve
//        controller.modalPresentationStyle = .overCurrentContext
//        self.navigationReferances?.present(controller, animated: true, completion: nil)
        sender.isUserInteractionEnabled = false
        self.addHeartBeatAnimation(status: true)
        self.viewInner.layer.cornerRadius =  self.viewInner.height/2
        customDelegate?.animationStart()
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            sender.isUserInteractionEnabled = true
            self.viewInner.layer.cornerRadius = 10
            self.addHeartBeatAnimation(status: false)
            self.customDelegate?.animationStop()
        }

    }
}

extension UIView {
    func addHeartBeatAnimation(status: Bool) {
        let heartBeatAnim: CAAnimationGroup = CAAnimationGroup()
        if status == true {
        let beatLong: CABasicAnimation = CABasicAnimation(keyPath: "transform.scale")
        beatLong.fromValue = NSValue(cgSize: CGSize(width: 1, height: 1))
            beatLong.toValue = NSValue(cgSize: CGSize(width: 0.7, height: 0.7))
        beatLong.autoreverses = true
        beatLong.duration = 0.5
        beatLong.beginTime = 0.0
        
        let beatShort: CABasicAnimation = CABasicAnimation(keyPath: "transform.scale")
        beatShort.fromValue = NSValue(cgSize: CGSize(width: 1, height: 1))
        beatShort.toValue = NSValue(cgSize: CGSize(width: 0.5, height: 0.5))
        beatShort.autoreverses = true
        beatShort.duration = 0.7
        beatShort.beginTime = beatLong.duration
        beatLong.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeIn)
        
        
        heartBeatAnim.animations = [beatLong, beatShort]
        heartBeatAnim.duration = beatShort.beginTime + beatShort.duration
        heartBeatAnim.fillMode = CAMediaTimingFillMode.forwards
        heartBeatAnim.isRemovedOnCompletion = false
        heartBeatAnim.repeatCount = .greatestFiniteMagnitude
        self.layer.add(heartBeatAnim, forKey: nil)
        
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = [UIColor.orange.cgColor, UIColor.white.cgColor, UIColor.green.cgColor]
        gradient.cornerRadius = gradient.frame.height/2
        gradient.type = .radial
        gradient.startPoint = CGPoint(x: 0.5, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        self.layer.insertSublayer(gradient, at: 0)
        } else {
            heartBeatAnim.isRemovedOnCompletion = true
            self.layer.removeAllAnimations()
        }
    }
}
 


