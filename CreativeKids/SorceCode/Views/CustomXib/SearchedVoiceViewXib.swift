//
//  SearchedVoiceViewXib.swift
//  CreativeKids
//
//  Created by Creative Kids on 28/09/21.
//

import UIKit
import Foundation
import IQKeyboardManagerSwift

class SearchedVoiceViewXib: UIView {
    @IBOutlet var containerView: UIView!
    @IBOutlet weak var textViewSearch: IQTextView!
    @IBOutlet weak var viewInner: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonUnit()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonUnit()
    }

    func commonUnit(){
        Bundle.main.loadNibNamed("SearchedVoiceViewXib", owner: self, options: [:])
        containerView.frame = self.bounds
        containerView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(containerView)
    }
}

extension SearchedVoiceViewXib{
    //MARK:- LEFT TOP
    func createLeftTopBezierPath() -> UIBezierPath {
        let arrowHeight:Int = 10
        let roundSize:Int = 5
        let wid:Int = Int(viewInner.frame.width)
        let hig:Int = Int(viewInner.frame.height)
        // create a new path
        let path = UIBezierPath()
        
        // initial point
        
        path.move(to: CGPoint(x: (roundSize * 3), y: arrowHeight))
        // Add line
        path.addLine(to: CGPoint(x: wid - roundSize, y: arrowHeight))
        // Add circle
        path.addArc(withCenter: CGPoint(x: wid - roundSize, y: arrowHeight + 5),
                                  radius:CGFloat(roundSize),
                    startAngle: CGFloat(3 * (Double.pi/2)), // straight up
                endAngle: CGFloat(0), // 0 radians = straight right
                clockwise: true)
        // Add line
        path.addLine(to: CGPoint(x: wid, y: hig - roundSize))
        // Add circle
        path.addArc(withCenter: CGPoint(x: wid - roundSize, y: hig - roundSize),
                                  radius: CGFloat(roundSize),
                    startAngle: CGFloat(0), // straight up
                endAngle: CGFloat((Double.pi/2)), // 0 radians = straight right
                clockwise: true)
        // Add line
        path.addLine(to: CGPoint(x: roundSize, y: hig))
        // Add circle
        path.addArc(withCenter: CGPoint(x: roundSize, y: hig - roundSize),
                                  radius: CGFloat(roundSize),
                    startAngle: CGFloat((Double.pi/2)), // straight up
                endAngle: CGFloat((Double.pi)), // 0 radians = straight right
                clockwise: true)
        // Add line
        path.addLine(to: CGPoint(x: (roundSize * 0), y: arrowHeight + roundSize))
        // Add circle
        path.addArc(withCenter: CGPoint(x: roundSize, y: arrowHeight + roundSize),
                                  radius: CGFloat(roundSize),
                    startAngle: CGFloat((Double.pi)), // straight up
                endAngle: CGFloat(3 * (Double.pi/2)), // 0 radians = straight right
                clockwise: true)
        path.addLine(to: CGPoint(x: (roundSize * 0), y: (roundSize * 0)))
        path.addLine(to: CGPoint(x: (roundSize * 3), y: arrowHeight))
        
        return path
    }
    
    //MARK:- LEFT BUTTOM
    func createLeftButtomBezierPath() -> UIBezierPath {
        let arrowHeight:Int = 10
        let roundSize:Int = 5
        let wid:Int = Int(viewInner.frame.width)
        let hig:Int = Int(viewInner.frame.height)
        // create a new path
        let path = UIBezierPath()
        
        // initial point
        path.move(to: CGPoint(x: (roundSize * 0) , y: roundSize))
        // Add circle
        path.addArc(withCenter: CGPoint(x: roundSize, y: roundSize),
                                  radius:CGFloat(roundSize),
                    startAngle: CGFloat((Double.pi)), // straight up
                endAngle: CGFloat(3 * (Double.pi/2)), // 0 radians = straight right
                clockwise: true)
        // Add line
        path.addLine(to: CGPoint(x: wid - roundSize, y: (roundSize * 0)))
        // Add circle
        path.addArc(withCenter: CGPoint(x: wid - roundSize, y: roundSize),
                                  radius:CGFloat(roundSize),
                    startAngle: CGFloat(3 * (Double.pi/2)), // straight up
                endAngle: CGFloat(0), // 0 radians = straight right
                clockwise: true)
        // Add line
        path.addLine(to: CGPoint(x: wid, y: hig - (arrowHeight + roundSize)))
        // Add circle
        path.addArc(withCenter: CGPoint(x: wid - roundSize, y: hig - (arrowHeight + roundSize)),
                                  radius: CGFloat(roundSize),
                    startAngle: CGFloat(0), // straight up
                endAngle: CGFloat((Double.pi/2)), // 0 radians = straight right
                clockwise: true)
        // Add line
        path.addLine(to: CGPoint(x: (roundSize * 3), y: hig - arrowHeight))
        path.addLine(to: CGPoint(x: (roundSize * 0), y: hig))
        path.addLine(to: CGPoint(x: roundSize, y: hig - arrowHeight))
        // Add circle
        path.addArc(withCenter: CGPoint(x: roundSize, y: hig - (arrowHeight + roundSize)),
                                  radius: CGFloat(roundSize),
                    startAngle: CGFloat((Double.pi/2)), // straight up
                endAngle: CGFloat((Double.pi)), // 0 radians = straight right
                clockwise: true)

        path.addLine(to: CGPoint(x: (roundSize * 0) , y: roundSize))
        
        return path
    }
    
    //MARK:- RIGHT TOP
    func createRightTopBezierPath() -> UIBezierPath {
        let arrowHeight:Int = 10
        let roundSize:Int = 5
        let wid:Int = Int(viewInner.frame.width)
        let hig:Int = Int(viewInner.frame.height)
        // create a new path
        let path = UIBezierPath()
        
        // initial point
        
        path.move(to: CGPoint(x: wid - roundSize, y: arrowHeight))
        // Add circle
        path.addArc(withCenter: CGPoint(x: wid - roundSize, y: arrowHeight + 5),
                                  radius:CGFloat(roundSize),
                    startAngle: CGFloat(3 * (Double.pi/2)), // straight up
                endAngle: CGFloat(0), // 0 radians = straight right
                clockwise: true)
        // Add line
        path.addLine(to: CGPoint(x: wid, y: hig - roundSize))
        // Add circle
        path.addArc(withCenter: CGPoint(x: wid - roundSize, y: hig - roundSize),
                                  radius: CGFloat(roundSize),
                    startAngle: CGFloat(0), // straight up
                endAngle: CGFloat((Double.pi/2)), // 0 radians = straight right
                clockwise: true)
        // Add line
        path.addLine(to: CGPoint(x: roundSize, y: hig))
        // Add circle
        path.addArc(withCenter: CGPoint(x: roundSize, y: hig - roundSize),
                                  radius: CGFloat(roundSize),
                    startAngle: CGFloat((Double.pi/2)), // straight up
                endAngle: CGFloat((Double.pi)), // 0 radians = straight right
                clockwise: true)
        // Add line
        path.addLine(to: CGPoint(x: (roundSize * 0), y: arrowHeight + roundSize))
        // Add circle
        path.addArc(withCenter: CGPoint(x: roundSize, y: arrowHeight + roundSize),
                                  radius: CGFloat(roundSize),
                    startAngle: CGFloat((Double.pi)), // straight up
                endAngle: CGFloat(3 * (Double.pi/2)), // 0 radians = straight right
                clockwise: true)
        // Add line
          path.addLine(to: CGPoint(x: wid - (roundSize * 3), y: arrowHeight))
        path.addLine(to: CGPoint(x: wid, y: (roundSize * 0)))
        path.addLine(to: CGPoint(x: wid - roundSize, y: arrowHeight))
        
        return path
    }
    
    //MARK:- LEFT BUTTOM
    func createRightButtomBezierPath() -> UIBezierPath {
        let arrowHeight:Int = 10
        let roundSize:Int = 5
        let wid:Int = Int(viewInner.frame.width)
        let hig:Int = Int(viewInner.frame.height)
        // create a new path
        let path = UIBezierPath()
        
        // initial point
        path.move(to: CGPoint(x: (roundSize * 0) , y: roundSize))
        // Add circle
        path.addArc(withCenter: CGPoint(x: roundSize, y: roundSize),
                                  radius:CGFloat(roundSize),
                    startAngle: CGFloat((Double.pi)), // straight up
                endAngle: CGFloat(3 * (Double.pi/2)), // 0 radians = straight right
                clockwise: true)
        // Add line
        path.addLine(to: CGPoint(x: wid - roundSize, y: (roundSize * 0)))
        // Add circle
        path.addArc(withCenter: CGPoint(x: wid - roundSize, y: roundSize),
                                  radius:CGFloat(roundSize),
                    startAngle: CGFloat(3 * (Double.pi/2)), // straight up
                endAngle: CGFloat(0), // 0 radians = straight right
                clockwise: true)
        // Add line
        path.addLine(to: CGPoint(x: wid, y: hig - (arrowHeight + roundSize)))
        // Add circle
        path.addArc(withCenter: CGPoint(x: wid - roundSize, y: hig - (arrowHeight + roundSize)),
                                  radius: CGFloat(roundSize),
                    startAngle: CGFloat(0), // straight up
                endAngle: CGFloat((Double.pi/2)), // 0 radians = straight right
                clockwise: true)
        // Add line
        path.addLine(to: CGPoint(x: wid, y: hig))
        path.addLine(to: CGPoint(x: wid - (roundSize * 3), y: hig - arrowHeight))
        path.addLine(to: CGPoint(x: roundSize, y: hig - arrowHeight))
        // Add circle
        path.addArc(withCenter: CGPoint(x: roundSize, y: hig - (arrowHeight + roundSize)),
                                  radius: CGFloat(roundSize),
                    startAngle: CGFloat((Double.pi/2)), // straight up
                endAngle: CGFloat((Double.pi)), // 0 radians = straight right
                clockwise: true)

        path.addLine(to: CGPoint(x: (roundSize * 0) , y: roundSize))
        
        return path
    }
    
    func leftTopSetUp() {
            // Create a CAShapeLayer
            let shapeLayer = CAShapeLayer()
            // The Bezier path that we made needs to be converted to
            // a CGPath before it can be used on a layer.
            shapeLayer.path = createLeftTopBezierPath().cgPath
            // apply other properties related to the path
            shapeLayer.strokeColor = UIColor.blue.cgColor
            shapeLayer.fillColor = UIColor.white.cgColor
            shapeLayer.lineWidth = 1.0
            shapeLayer.position = CGPoint(x: 0, y: 0)
            // add the new layer to our custom view
        self.viewInner.layer.addSublayer(shapeLayer)
        }
    func rightTopSetUp() {
            // Create a CAShapeLayer
            let shapeLayer = CAShapeLayer()
            // The Bezier path that we made needs to be converted to
            // a CGPath before it can be used on a layer.
            shapeLayer.path = createRightTopBezierPath().cgPath
            // apply other properties related to the path
            shapeLayer.strokeColor = UIColor.blue.cgColor
            shapeLayer.fillColor = UIColor.white.cgColor
            shapeLayer.lineWidth = 1.0
            shapeLayer.position = CGPoint(x: 0, y: 0)
            // add the new layer to our custom view
        self.viewInner.layer.addSublayer(shapeLayer)
        }
    func leftbottomSetUp() {
            // Create a CAShapeLayer
            let shapeLayer = CAShapeLayer()
            // The Bezier path that we made needs to be converted to
            // a CGPath before it can be used on a layer.
            shapeLayer.path = createLeftButtomBezierPath().cgPath
            // apply other properties related to the path
            shapeLayer.strokeColor = UIColor.blue.cgColor
            shapeLayer.fillColor = UIColor.white.cgColor
            shapeLayer.lineWidth = 1.0
            shapeLayer.position = CGPoint(x: 0, y: 0)
            // add the new layer to our custom view
        self.viewInner.layer.addSublayer(shapeLayer)
        }
    func rightbottomSetUp() {
            // Create a CAShapeLayer
            let shapeLayer = CAShapeLayer()
            // The Bezier path that we made needs to be converted to
            // a CGPath before it can be used on a layer.
            shapeLayer.path = createRightButtomBezierPath().cgPath
            // apply other properties related to the path
            shapeLayer.strokeColor = UIColor.blue.cgColor
            shapeLayer.fillColor = UIColor.white.cgColor
            shapeLayer.lineWidth = 1.0
            shapeLayer.position = CGPoint(x: 0, y: 0)
            // add the new layer to our custom view
        self.viewInner.layer.addSublayer(shapeLayer)
        }
}
