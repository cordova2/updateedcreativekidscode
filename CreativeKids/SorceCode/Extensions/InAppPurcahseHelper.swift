//
//  InAppPurcahseHelper.swift
//  CreativeKids
//
//  Created by Nitesh jha on 22/04/21.
//

import UIKit
import StoreKit
import Alamofire

enum ReceiptValidationError: Error {
    case receiptNotFound
    case jsonResponseIsNotValid(description: String)
    case notBought
    case expired
}

enum ProductType: String {
    
    //MARK:- NURSARY
    case NursaryRymes = "com.CreativeKids.RymesNursery"
    case NursaryStories = "com.CreativeKids.StoriesNursery"
    case NursaryEnglish = "com.CreativeKids.EnglishNursery"
    case NursaryMath = "com.CreativeKids.MathsNursery"
    
    //MARK:- LKG
    case LKGRymes = "com.CreativeKids.RymesLKG"
    case LKGStories = "com.CreativeKids.StoriesLKG"
    case LKGEnglish = "com.CreativeKids.EnglishLKG"
    case LKGMath = "com.CreativeKids.MathsLKG"
    
    //MARK:- UKG
    case UKGRymes = "com.CreativeKids.RymesUKG"
    case UKGStories = "com.CreativeKids.StoriesUKG"
    case UKGEnglish = "com.CreativeKids.EnglishUKG"
    case UKGMath = "com.CreativeKids.MathsUKG"
    
    //MARK:- CLASS 1
    case Class1Math = "com.CreativeKids.Mathematics1"
    case Class1English = "com.CreativeKids.English_1"
    case Class1Hindi_Vyalkaran = "com.CreativeKids.Hindi_Vyakaran_1"
    case Class1Computer = "com.CreativeKids.Computer_1"
    case Class1Science = "com.CreativeKids.Science_1"
    case Class1Social_Science = "com.CreativeKids.Social_Science_1"
    case Class1Hindi = "com.CreativeKids.Hindi_1"
    case Class1English_Grammer = "com.CreativeKids.EnglishGrammer_1"
    case Class1Evs = "com.CreativeKids.EVS1"
    case Class1SunShine_Term_1 = "com.CreativeKids.SunshineTerm1_1"
    case Class1SunShine_Term_2 = "com.CreativeKids.SunshineTerm2_1"
    
    //MARK:- CLASS 2
    case Class2Math = "com.CreativeKids.Mathematics_2"
    case Class2English = "com.CreativeKids.English_2"
    case Class2Hindi_Vyalkaran = "com.CreativeKids.Hindi_vyakaran_2"
    case Class2Computer = "com.CreativeKids.Computer_2"
    case Class2Science = "com.CreativeKids.Science_2"
    case Class2Social_Science = "com.CreativeKids.SocialScience_2"
    case Class2Hindi = "com.CreativeKids.Hindi_2"
    case Class2English_Grammer = "com.CreativeKids.EnglishGrammer_2"
    case Class2Evs = "com.CreativeKids.EVS_2"
    case Class2SunShine_Term_1 = "com.CreativeKids.SunShine_1_2"
    case Class2SunShine_Term_2 = "com.CreativeKids.SunShineTerms_2_2"
    
    //MARK:- CLASS 3
    case Class3Math = "com.CreativeKids.Mathematics_3"
    case Class3English = "com.CreativeKids.English_3"
    case Class3Hindi_Vyalkaran = "com.CreativeKids.HindiVyakaran_3"
    case Class3Computer = "com.CreativeKids.Computer_3"
    case Class3Science = "com.CreativeKids.Science_3"
    case Class3Social_Science = "com.CreativeKids.SocialScience_3"
    case Class3Hindi = "com.creativeKids.Hindi_3"
    case Class3English_Grammer = "com.CreativeKids.EnglishGrammer_3"
    case Class3Evs = "com.CreativeKids.EVS_3"
    case Class3SunShine_Term_1 = "com.CreativeKids.SunShineTerms_1_3"
    case Class3SunShine_Term_2 = "com.CreativeKids.SunshineTerms_2_3"
    
    //MARK:- CLASS 4
    case Class4Math = "com.CreativeKids.Mathematics_4"
    case Class4English = "com.CreativeKids.English_4"
    case Class4Hindi_Vyalkaran = "com.CreativeKids.HindiVyakaran_4"
    case Class4Computer = "com.CreativeKids.Computer_4"
    case Class4Science = "com.CreativeKids.Science_4"
    case Class4Social_Science = "com.CreativeKids.SocialScience_4"
    case Class4Hindi = "com.CreativeKids.Hindi_4"
    case Class4English_Grammer = "com.CreativeKids.EnglishGrammer_4"
    case Class4Evs = "com.CreativeKids.EVS_4"
    case Class4SunShine_Term_1 = "com.CreativeKids.SunShineTerms_1_4"
    case Class4SunShine_Term_2 = "com.CreativeKids.SunShineTerms_2_4"
    
    //MARK:- CLASS 5
    case Class5Math = "com.CreativeKids.Mathematics_5"
    case Class5English = "com.CreativeKids.English_5"
    case Class5Hindi_Vyalkaran = "com.CreativeKids.HindiVyakaran_5"
    case Class5Computer = "com.CreativeKids.Computer_5"
    case Class5Science = "com.CreativeKids.Science_5"
    case Class5Social_Science = "com.CreativeKids.SocialScience_5"
    case Class5Hindi = "com.CreativeKids.Hindi_5"
    case Class5English_Grammer = "com.CreativeKids.EnglishGrammer_5"
    case Class5Evs = "com.CreativeKids.EVS_5"
    case Class5Sanskrit = "com.CreativeKids.Sanskrit_5"
    case Class5SunShine_Term_1 = "com.CreativeKids.SunShineTerms_1_5"
    case Class5SunShine_Term_2 = "com.CreativeKids.SunShineTerms_2_5"
    
    //MARK:- CLASS 6
    case Class6Math = "com.CreativeKids.Mathematics_6"
    case Class6English = "com.CreativeKids.English_6"
    case Class6Hindi_Vyalkaran = "com.Creativekids.HindiVyakaran_6"
    case Class6Computer = "com.CreativeKids.Computer_6"
    case Class6Science = "com.CreativeKids.Science_6"
    case Class6Social_Science = "com.CreativeKids.SocialScience_6"
    case Class6Hindi = "com.CreativeKids.Hindi_6"
    case Class6English_Grammer = "com.CreativeKids.EnglishGrammer_6"
    case Class6Sanskrit = "com.CreativeKids.Sanskrit_6"

    // NCERT Subjects
    case Class6Math_NCERT = "com.CreativeKids.Mathematics_6_NCERT"
    case Class6English_NCERT = "com.Creativekids.English_6_NCERT"
    case Class6Hindi_Vyalkaran_NCERT = "com.CreativeKids.HindiVyakaran_6_NCERT"
    case Class6Science_NCERT = "com.CreativeKids.Science_6_NCERT"
    case Class6Social_Science_NCERT = "com.CreativeKids.SocialScience_6_NCERT"
    case Class6Hindi_NCERT = "com.CreativeKids.Hindi_6_NECERT"
    case Class6English_Grammer_NCERT = "com.CreativeKids.EnglishGrammer_6_NCERT"
    
    
    
    //MARK:- CLASS 7
    case Class7Math = "com.CreativeKids.Mathematics_7"
    case Class7English = "com.CreativeKids.English_7"
    case Class7Hindi_Vyalkaran = "com.CreativeKids.HindiVyakaran_7"
    case Class7Computer = "com.CreativeKids.Computer_7"
    case Class7Science = "com.CreativeKids.Science_7"
    case Class7Social_Science = "com.CreativeKids.SocialScience_7"
    case Class7Hindi = "com.CreariveKids.Hindi_7"
    case Class7English_Grammer = "com.CreativeKids.EnglishGrammer_7"
    case Class7Sanskrit = "com.CreativeKids.Sanskrit_7"
    
    // NCERT Subjects
    case Class7Math_NCERT = "com.CreativeKids.Mathematics_7_NCERT"
    case Class7English_NCERT = "com.CreativeKids.English_7_NCERT"
    case Class7Hindi_Vyalkaran_NCERT = "com.CreativeKids.HindiVyakaran_7_NCERT"
    case Class7Science_NCERT = "com.CreativeKids.Science_7_NCERT"
    case Class7Social_Science_NCERT = "com.CreativeKids.SocialScience_7_NCERT"
    case Class7Hindi_NCERT = "com.CreativeKids.HIndi_7_NCERT"
    case Class7English_Grammer_NCERT = "com.CreativeKids.EnglishGrammer_7_NCERT"
    
    //MARK:- CLASS 8
    case Class8Math = "com.CreativeKids.Mathematics_8"
    case Class8English = "com.CreartiveKids.English_8"
    case Class8Hindi_Vyalkaran = "com.CreativeKids.HindiVyakaran_8"
    case Class8Computer = "com.CreativeKids.Computer_8"
    case Class8Science = "com.CreativeKids.Science_8"
    case Class8Social_Science = "com.CreativeKids.SocialScience_8"
    case Class8Hindi = "com.CreativeKids.Hindi_8"
    case Class8English_Grammer = "com.CreativeKids.EnglishGrammer_8"
    case Class8Sanskrit = "com.CreativeKids.Sanskrit_8"
    
    // NCERT Subjects
    case Class8Math_NCERT = "com.CreativeKids.Mathematics_8_NCERT"
    case Class8English_NCERT = "com.CreativeKids.English_8_NCERT"
    case Class8Hindi_Vyalkaran_NCERT = "com.CreativeKids.HindiVyakaran_8_NCERT"
    case Class8Science_NCERT = "com.CreativeKids.Science_8_NCERT"
    case Class8Social_Science_NCERT = "com.CreativeKids.SocialScience_8_NCERT"
    case Class8Hindi_NCERT = "com.CreativeKids.Hindi_8_NCERT"
    case Class8English_Grammer_NCERT = "com.CreativeKids.EnglishGrammer_8_NCERT"
    
    //MARK:- CLASS 9
    case Class9Science = "com.CreativeKids.Science_9"
    case Class9Social_Science = "com.CreativeKids.SocialScience_9"
    
    // NCERT Subjects
    case Class9Math_NCERT = "com.CreativeKids.Mathematics_9_NCERT"
    case Class9English_NCERT = "com.CreativeKids.English_9_NCERT"
    case Class9Hindi_Vyalkaran_NCERT = "com.CreativeKids.HindiVyakaran_9_NCERT"
    case Class9Hindi_NCERT = "com.CreativeKids.Hindi_9_NCERT"
    case Class9English_Grammer_NCERT = "com.CreativeKids.EnglishGrammer_9_NCERT"
    
    //MARK:- CLASS 10
    case Class10Science = "com.CreativeKids.Science_10"
    case Class10Social_Science = "com.Creativekids.SocialScience_10"
    
    // NCERT Subjects
    case Class10Math_NCERT = "com.CreativeKids.Mathematics_10_NCERT"
    case Class10English_NCERT = "com.CreativeKids.English_10_NCERT"
    case Class10Hindi_Vyalkaran_NCERT = "com.CreativeKids.HindiVyakaran_10_NCERT"
    case Class10Hindi_NCERT = "com.CreativeKids.Hindi_10_NCERT"
    case Class10English_Grammer_NCERT = "com.CreativeKids.EnglishGrammer_10_NCERT"
    
    static var all: [ProductType] {
        return [ //CLASS LKG
                .NursaryRymes,
                .NursaryStories,
                .NursaryEnglish,
                .NursaryMath,
                //CLASS LKG
                .LKGRymes,
                .LKGStories,
                .LKGEnglish,
                .LKGMath,
                //CLASS UKG
                .UKGRymes,
                .UKGStories,
                .UKGEnglish,
                .UKGMath,
                //CLASS 1
                .Class1Math,
                .Class1English,
                .Class1Hindi_Vyalkaran,
                .Class1Computer,
                .Class1Science,
                .Class1Social_Science,
                .Class1Hindi,
                .Class1English_Grammer,
                .Class1Evs,
                .Class1SunShine_Term_1,
                .Class1SunShine_Term_2,
                //CLASS 2
                .Class2Math,
                .Class2English,
                .Class2Hindi_Vyalkaran,
                .Class2Computer,
                .Class2Science,
                .Class2Social_Science,
                .Class2Hindi,
                .Class2English_Grammer,
                .Class2Evs,
                .Class2SunShine_Term_1,
                .Class2SunShine_Term_2,
                //CLASS 3
                .Class3Math,
                .Class3English,
                .Class3Hindi_Vyalkaran,
                .Class3Computer,
                .Class3Science,
                .Class3Social_Science,
                .Class3Hindi,
                .Class3English_Grammer,
                .Class3Evs,
                .Class3SunShine_Term_1,
                .Class3SunShine_Term_2,
                //CLASS 4
                .Class4Math,
                .Class4English,
                .Class4Hindi_Vyalkaran,
                .Class4Computer,
                .Class4Science,
                .Class4Social_Science,
                .Class4Hindi,
                .Class4English_Grammer,
                .Class4Evs,
                .Class4SunShine_Term_1,
                .Class4SunShine_Term_2,
                //CLASS 5
                .Class5Math,
                .Class5English,
                .Class5Hindi_Vyalkaran,
                .Class5Computer,
                .Class5Science,
                .Class5Social_Science,
                .Class5Hindi,
                .Class5English_Grammer,
                .Class5Evs,
                .Class5SunShine_Term_1,
                .Class5SunShine_Term_2,
                .Class5Sanskrit,
                //CLASS 6
                .Class6Math,
                .Class6English,
                .Class6Hindi_Vyalkaran,
                .Class6Computer,
                .Class6Science,
                .Class6Social_Science,
                .Class6Hindi,
                .Class6English_Grammer,
                .Class6Sanskrit,
                //CLASS 6 NCERT
                .Class6Math_NCERT,
                .Class6English_NCERT,
                .Class6Hindi_Vyalkaran_NCERT,
                .Class6Science_NCERT,
                .Class6Social_Science_NCERT,
                .Class6Hindi_NCERT,
                .Class6English_Grammer_NCERT,
                //CLASS 7
                .Class7Math,
                .Class7English,
                .Class7Hindi_Vyalkaran,
                .Class7Computer,
                .Class7Science,
                .Class7Social_Science,
                .Class7Hindi,
                .Class7English_Grammer,
                .Class7Sanskrit,
                //CLASS 7 NCERT
                .Class7Math_NCERT,
                .Class7English_NCERT,
                .Class7Hindi_Vyalkaran_NCERT ,
                .Class7Science_NCERT,
                .Class7Social_Science_NCERT,
                .Class7Hindi_NCERT,
                .Class7English_Grammer_NCERT,
                //CLASS 8
                .Class8Math,
                .Class8Hindi_Vyalkaran,
                .Class8Computer,
                .Class8Science,
                .Class8Social_Science,
                .Class8Hindi,
                .Class8English_Grammer,
                .Class8Sanskrit,
                //CLASS 8 NCERT
                .Class8Math_NCERT,
                .Class8English_NCERT,
                .Class8Hindi_Vyalkaran_NCERT,
                .Class8Science_NCERT,
                .Class8Social_Science_NCERT,
                .Class8Hindi_NCERT,
                .Class8English_Grammer_NCERT,
                //CLASS 9
                .Class9Science,
                .Class9Social_Science,
                //CLASS 9 NCERT
                .Class9Math_NCERT,
                .Class9English_NCERT,
                .Class9Hindi_Vyalkaran_NCERT,
                .Class9Hindi_NCERT,
                .Class9English_Grammer_NCERT,
                //CLASS 10
                .Class10Science,
                .Class10Social_Science,
                //CLASS 10 NCERT
                .Class10Math_NCERT,
                .Class10English_NCERT,
                .Class10Hindi_Vyalkaran_NCERT,
                .Class10Hindi_NCERT,
                .Class10English_Grammer_NCERT]
    }
}

enum InAppErrors: Swift.Error {
    case noSubscriptionPurchased
    case noProductsAvailable
    
    var localizedDescription: String {
        switch self {
        case .noSubscriptionPurchased:
            return "No subscription purchased"
        case .noProductsAvailable:
            return "No products available"
        }
    }
}

protocol InAppManagerDelegate: AnyObject {
    func inAppLoadingStarted()
    func inAppLoadingSucceded(productType: ProductType)
    func inAppLoadingFailed(error: Swift.Error?)
    func subscriptionStatusUpdated(value: Bool)
}
protocol IAPSusscessfulDelegate: AnyObject {
    func paymentStatus(isSuccess: Bool)
}
protocol SubscribeDetailDelegate{
    func getSubScribedData(subscribeInfo:[SubscrptionDetails])
}


//Mark InAppPurchase Manager Class
class InAppManager: NSObject {
    static let shared = InAppManager()
    weak var delegate: InAppManagerDelegate?
    weak var statusDelegate: IAPSusscessfulDelegate? = nil
    var subscribeDataDelegate:SubscribeDetailDelegate? = nil
    var products: [SKProduct] = []
    let paymentQueue = SKPaymentQueue.default()
    var isTrialPurchased: Bool?
    var expirationDate: Date?
    var purchasedProduct: ProductType?
    var isSubscriptionAvailable: Bool = true
    {
        didSet(value) {
            self.delegate?.subscriptionStatusUpdated(value: value)
        }
    }
    
    func startMonitoring() {
        paymentQueue.add(self)
//        self.updateSubscriptionStatus()
    }
    
    func stopMonitoring() {
        paymentQueue.remove(self)
    }
    
    func loadProducts() {
        let productIdentifiers = Set<String>(ProductType.all.map({$0.rawValue}))
        let request = SKProductsRequest(productIdentifiers: productIdentifiers)
        request.delegate = self
        request.start()
        startMonitoring()
    }
    
    func purchaseProduct(productType: ProductType) {
        guard let productToPurchase = self.products.filter({$0.productIdentifier == productType.rawValue}).first else {
            self.delegate?.inAppLoadingFailed(error: InAppErrors.noProductsAvailable)
            return
        }
        print(productToPurchase)
        let payment = SKPayment(product: productToPurchase)
        paymentQueue.add(payment)
        self.delegate?.inAppLoadingStarted()
    }
    
//    func restoreSubscription() {
//        paymentQueue.add(self)
//        paymentQueue.restoreCompletedTransactions()
//        SKPaymentQueue.default().restoreCompletedTransactions()
//        self.delegate?.inAppLoadingStarted()
//    }
    //    func checkSubscriptionAvalability(transaction: SKPaymentTransaction,_ completionHandler: @escaping (Bool) -> Void){
    //
    //        let min  = getCurrentDate()
    //        var endDate: Double?
    //        let t: SKPaymentTransaction = transaction
    //        let prodID = t.payment.productIdentifier as String
    //        switch prodID {
    //        case  ProductType.monthly.rawValue:
    //             endDate = Date().timeIntervalSince1970
    //        case  ProductType.quarterly.rawValue:
    //            endDate = Date().timeIntervalSince1970
    //        case  ProductType.yearly.rawValue:
    //            endDate = Date().timeIntervalSince1970
    //        case  ProductType.lifetime.rawValue:
    //            endDate = Date().timeIntervalSince1970
    //        default:
    //            endDate = 0
    //        }
    //    }
    func checkSubscriptionAvailability(_ completionHandler: @escaping (Bool) -> Void) {
        if NetworkReachabilityManager()?.isReachable == true
        {
            CommonUtils.showHudWithNoInteraction(show: true)
            let passwd = "1a302394c7b5405ba18d7a2a972f90af"
            guard let appStoreReceiptURL = Bundle.main.appStoreReceiptURL, FileManager.default.fileExists(atPath: appStoreReceiptURL.path) else {
                self.getExpirationDateFromResponse([:])
                //kSharedUserDefaults.setSubcriptionStatus(status: false)
                //CommonUtils.showHudWithNoInteraction(show: false)
                completionHandler(false)
                return
            }
            print(appStoreReceiptURL)
            let receiptData = try! Data(contentsOf: appStoreReceiptURL, options: .alwaysMapped)
            let receiptString = receiptData.base64EncodedString()
            let requestContents: [String: Any] = [
                "receipt-data": receiptString,
                "password": passwd
            ]
            
            //let appleServer = appStoreReceiptURL.lastPathComponent == "sandboxReceipt" ? "sandbox" : "buy"
            //let stringURL = "https://\(appleServer).itunes.apple.com/verifyReceipt"
            #if DEBUG
            let stringUrl = URL(string: "https://sandbox.itunes.apple.com/verifyReceipt")!
            #else
            let stringUrl = URL(string: "https://buy.itunes.apple.com/verifyReceipt")!
            #endif
            print("Loading user receipt: \(stringUrl)...")
            
            Alamofire.request(stringUrl, method: .post, parameters: requestContents, encoding: JSONEncoding.default)
                .responseJSON { response in
//                    CommonUtils.showHudWithNoInteraction(show: false)
                    switch response.result
                    {
                    case .success(_):
                        if let value = response.result.value as? NSDictionary {
                            let subscriptionDetail = kSharedInstance.getDictionary(value)
                            let subscriptionDetailArray = kSharedInstance.getArray(withDictionary: subscriptionDetail["latest_receipt_info"])
                            let subscribedData = subscriptionDetailArray.map{SubscrptionDetails(data: $0)}
                            self.subscribeDataDelegate?.getSubScribedData(subscribeInfo: subscribedData)
//                            self.getExpirationDateFromResponse(value)
                        } else {
                            print("Receiving receipt from App Store failed: \(response.result)")
                            CommonUtils.showHudWithNoInteraction(show: false)
                        }
                        
                    case .failure(let error):
                        print_debug(items: "json error: \(error.localizedDescription)")
                        if error.localizedDescription == "cancelled"
                        {
                            showAlertMessage.alert(message: kDefaultErrorMsg)
                        }
                        else
                        {
                            showAlertMessage.alert(message: kDefaultErrorMsg)
                        }
                        CommonUtils.showHudWithNoInteraction(show: false)
                    }
                }
            
        }else {
            
            CommonUtils.showToast(message: kNoInternetMsg)
        }
        
        //        let _ = Router.User.sendReceipt(receipt: receipt).request(baseUrl: "https://sandbox.itunes.apple.com").responseObject { (response: DataResponse<RTSubscriptionResponse>) in
        //            switch response.result {
        //            case .success(let value):
        //                guard let expirationDate = value.expirationDate,
        //                    let productId = value.productId else {completionHandler(false); return}
        //                self.expirationDate = expirationDate
        //                self.isTrialPurchased = value.isTrial
        //                self.purchasedProduct = ProductType(rawValue: productId)
        //                completionHandler(Date().timeIntervalSince1970 < expirationDate.timeIntervalSince1970)
        //            case .failure(let error):
        //                completionHandler(false)
        //            }
        //        }
    }
    //    func validateReceipt() throws {
    //        guard let appStoreReceiptURL = Bundle.main.appStoreReceiptURL, FileManager.default.fileExists(atPath: appStoreReceiptURL.path) else {
    //            throw ReceiptValidationError.receiptNotFound
    //        }
    //
    //        let receiptData = try! Data(contentsOf: appStoreReceiptURL, options: .alwaysMapped)
    //        let receiptString = receiptData.base64EncodedString()
    //        let jsonObjectBody = ["receipt-data" : receiptString, "password" : "sadhfsah"]
    //
    //        #if DEBUG
    //        let url = URL(string: "https://sandbox.itunes.apple.com/verifyReceipt")!
    //        #else
    //        let url = URL(string: "https://buy.itunes.apple.com/verifyReceipt")!
    //        #endif
    //
    //        var request = URLRequest(url: url)
    //        request.httpMethod = "POST"
    //        request.httpBody = try! JSONSerialization.data(withJSONObject: jsonObjectBody, options: .prettyPrinted)
    //
    //        let semaphore = DispatchSemaphore(value: 0)
    //
    //        var validationError : ReceiptValidationError?
    //
    //        let task = URLSession.shared.dataTask(with: request) { data, response, error in
    //            guard let data = data, let httpResponse = response as? HTTPURLResponse, error == nil, httpResponse.statusCode == 200 else {
    //                validationError = ReceiptValidationError.jsonResponseIsNotValid(description: error?.localizedDescription ?? "")
    //                semaphore.signal()
    //                return
    //            }
    //            guard let jsonResponse = (try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [AnyHashable: Any] else {
    //                validationError = ReceiptValidationError.jsonResponseIsNotValid(description: "Unable to parse json")
    //                semaphore.signal()
    //                return
    //            }
    //            guard let expirationDate = self.expirationDate(jsonResponse: jsonResponse, forProductId: "dsg") else {
    //                validationError = ReceiptValidationError.notBought
    //                semaphore.signal()
    //                return
    //            }
    //
    //            let currentDate = Date()
    //            if currentDate > expirationDate {
    //                validationError = ReceiptValidationError.expired
    //            }
    //
    //            semaphore.signal()
    //        }
    //        task.resume()
    //
    //        semaphore.wait()
    //
    //        if let validationError = validationError {
    //            throw validationError
    //        }
    //    }
    //    func expirationDate(jsonResponse: [AnyHashable: Any], forProductId productId :String) -> Date? {
    //        guard let receiptInfo = (jsonResponse["latest_receipt_info"] as? [[AnyHashable: Any]]) else {
    //            return nil
    //        }
    //
    //        let filteredReceipts = receiptInfo.filter{ return ($0["product_id"] as? String) == productId }
    //
    //        guard let lastReceipt = filteredReceipts.last else {
    //            return nil
    //        }
    //
    //        let formatter = DateFormatter()
    //        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss VV"
    //
    //        if let expiresString = lastReceipt["expires_date"] as? String {
    //            print(expiresString)
    //            return formatter.date(from: expiresString)
    //        }
    //
    //        return nil
    //    }
    func getExpirationDateFromResponse(_ jsonResponse: NSDictionary) {
        
        guard let receiptInfo: NSArray = jsonResponse["latest_receipt_info"] as? NSArray else {
            //            guard let lifetimeReceiptInfo: NSDictionary = jsonResponse["receipt"] as? NSDictionary else {
            //                //paymentStatusApi(expDate: 0, strtDate: 0, status: 0, islifetime: 0)
            //                self.statusDelegate?.paymentStatus(isSuccess: false)
            //                return
            //            }
            //            if let iapLifetime: NSArray = lifetimeReceiptInfo["in_app"] as? NSArray{
            //                let lastReceipt = iapLifetime.lastObject as! NSDictionary
            //                guard let purchaseDate = lastReceipt["purchase_date_ms"] else {return}
            //                paymentStatusApi(expDate: 0, strtDate: Double.getDouble(purchaseDate), status: 1, islifetime: 1)
            //            }
            return
        }
        // print(receiptInfo)
        let lastReceipt = receiptInfo.lastObject as! NSDictionary
        // print(lastReceipt)
        guard let purDate = lastReceipt["purchase_date_ms"] else {return}
        guard let expiresDate = lastReceipt["expires_date_ms"]else{
            // paymentStatusApi(expDate: 0, strtDate: Double.getDouble(purDate), status: 1, islifetime: 1)
            return
        }
        print(purDate)
        print(expiresDate)
        self.statusDelegate?.paymentStatus(isSuccess: true)
        // paymentStatusApi(expDate: Double.getDouble(expiresDate), strtDate: Double.getDouble(purDate), status: 1, islifetime: 0)
        
    }
    //    func paymentStatusApi(expDate: Double,strtDate: Double,status: Int,islifetime: Int){
    //        //  CommonUtils.showHudWithNoInteraction(show: true)
    //        var params = [String: Any]()
    //
    //            params =                   ["start_date" : String.getString(strtDate),
    //                                        "end_date" : "",
    //                                        "payment_status": String.getString(status),
    //
    //            ]
    //
    //        TANetworkManager.sharedInstance.requestApi(withServiceName  : "user/paymentStatus",
    //                                                   requestMethod    : .POST,
    //                                                   requestParameters: params,
    //                                                   withProgressHUD  : false)
    //        {[weak self] (result: Any?, error: Error?, errorType: ErrorType, statusCode: Int?) in
    //            guard self != nil else { return }
    //            CommonUtils.showHudWithNoInteraction(show: false)
    //
    //            if errorType == .requestSuccess {
    //                let dicResponse = kSharedInstance.getDictionary(result)
    //
    //                switch Int.getInt(statusCode) {
    //                case 200,201,202:
    //                    self?.statusDelegate?.paymentStatus(isSuccess: true)
    //                    // print("payment successful")
    //                    //kSharedUserDefaults.setSubcriptionStatus(status: true)
    //                    //CommonUtils.showToast(message: String.getString(dicResponse["message"]))
    //
    //                default:
    //                    showAlertMessage.alert(message: String.getString(dicResponse["message"]))
    //                }
    //            }
    //            else if errorType == .noNetwork{
    //                showAlertMessage.alert(message: kNoInternetMsg)
    //            }
    //            else {
    //                showAlertMessage.alert(message: kDefaultErrorMsg)
    //            }
    //        }
    //    }
    func updateSubscriptionStatus() {
        self.checkSubscriptionAvailability({ [weak self] (isSubscribed) in
            self?.isSubscriptionAvailable = isSubscribed
        })
    }
}

extension InAppManager: SKPaymentTransactionObserver {
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            print(transaction.transactionState.rawValue,transaction.payment.productIdentifier)
            guard let productType = ProductType(rawValue: transaction.payment.productIdentifier) else {return}
            switch transaction.transactionState {
            case .purchasing:
                self.delegate?.inAppLoadingStarted()
            case .purchased:
                paymentQueue.finishTransaction(transaction)
                //kSharedUserDefaults.setSubcriptionStatus(status: true)
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                self.delegate?.inAppLoadingSucceded(productType: productType)
            case .failed:
                if let transactionError = transaction.error as NSError?,
                   transactionError.code != SKError.paymentCancelled.rawValue {
                    self.delegate?.inAppLoadingFailed(error: transaction.error)
                } else {
                    self.delegate?.inAppLoadingFailed(error: InAppErrors.noSubscriptionPurchased)
                }
                paymentQueue.finishTransaction(transaction)
            case .restored:
                paymentQueue.finishTransaction(transaction)
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                self.delegate?.inAppLoadingSucceded(productType: productType)
            case .deferred:
                self.delegate?.inAppLoadingSucceded(productType: productType)
            @unknown default:
                break
            }
        }
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Swift.Error) {
        // print(error.localizedDescription)
//        self.getExpirationDateFromResponse([:])
//        self.delegate?.inAppLoadingFailed(error: error)
    }
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        
        for transaction in queue.transactions {
            print(transaction.payment.productIdentifier)
            let t: SKPaymentTransaction = transaction
            let prodID = t.payment.productIdentifier as String
            
            switch prodID {
            //MARK:- NURSARY
            case ProductType.NursaryRymes.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.NursaryStories.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.NursaryEnglish.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.NursaryMath.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            //MARK:- LKG
            case ProductType.LKGRymes.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.LKGStories.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.LKGEnglish.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.LKGMath.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            //MARK:- UKG
            case ProductType.UKGRymes.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.UKGStories.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.UKGEnglish.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.UKGMath.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            //MARK:- CLASS 1
            case ProductType.Class1Math.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class1English.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class1Hindi_Vyalkaran.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class1Computer.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class1Science.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class1Social_Science.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class1Hindi.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class1English_Grammer.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class1Evs.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class1SunShine_Term_1.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class1SunShine_Term_2.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            //MARK:- CLASS 2
            case ProductType.Class2Math.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class2English.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class2Hindi_Vyalkaran.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class2Computer.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class2Science.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class2Social_Science.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class2Hindi.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class2English_Grammer.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class2Evs.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class2SunShine_Term_1.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class2SunShine_Term_2.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            //MARK:- CLASS 3
            case ProductType.Class3Math.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class3English.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class3Hindi_Vyalkaran.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class3Computer.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class3Science.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class3Social_Science.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class3Hindi.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class3English_Grammer.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class3Evs.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class3SunShine_Term_1.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class3SunShine_Term_2.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            //MARK:- CLASS 4
            case ProductType.Class4Math.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class4English.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class4Hindi_Vyalkaran.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class4Computer.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class4Science.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class4Social_Science.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class4Hindi.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class4English_Grammer.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class4Evs.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class4SunShine_Term_1.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class4SunShine_Term_2.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            //MARK:- CLASS 5
            case ProductType.Class5Math.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class5English.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class5Hindi_Vyalkaran.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class5Computer.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class5Science.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class5Social_Science.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class5Hindi.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class5English_Grammer.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class5Evs.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class5SunShine_Term_1.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class5SunShine_Term_2.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class5Sanskrit.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            //MARK:- CLASS 6
            case ProductType.Class6Math.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class6English.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class6Hindi_Vyalkaran.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class6Computer.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class6Science.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class6Social_Science.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class6Hindi.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class6English_Grammer.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class6Sanskrit.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
            
            //MARK:- CLASS 6 NCERT
            
            case ProductType.Class6Math_NCERT.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class6English_NCERT.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class6Hindi_Vyalkaran_NCERT.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class6Science_NCERT.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class6Social_Science_NCERT.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class6Hindi_NCERT.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class6English_Grammer_NCERT.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            //MARK:- CLASS 7
            case ProductType.Class7Math.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class7English.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class7Hindi_Vyalkaran.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class7Computer.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class7Science.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class7Social_Science.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class7Hindi.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class7English_Grammer.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class7Sanskrit.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            //MARK:- CLASS 7 NCERT
            
            case ProductType.Class7Math_NCERT.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class7English_NCERT.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class7Hindi_Vyalkaran_NCERT.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class7Science_NCERT.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class7Social_Science_NCERT.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class7Hindi_NCERT.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class7English_Grammer_NCERT.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            //MARK:- CLASS 8
            case ProductType.Class8Math.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class8English.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class8Hindi_Vyalkaran.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class8Computer.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class8Science.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class8Social_Science.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class8Hindi.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class8English_Grammer.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class8Sanskrit.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            //MARK:- CLASS 8 NCERT
            
            case ProductType.Class8Math_NCERT.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class8English_NCERT.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class8Hindi_Vyalkaran_NCERT.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class8Science_NCERT.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class8Social_Science_NCERT.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class8Hindi_NCERT.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class8English_Grammer_NCERT.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            //MARK:- CLASS 9
            case ProductType.Class9Science.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class9Social_Science.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            //MARK:- CLASS 9 NCERT
            
            case ProductType.Class9Math_NCERT.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class9English_NCERT.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class9Hindi_Vyalkaran_NCERT.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class9Hindi_NCERT.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class9English_Grammer_NCERT.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            //MARK:- CLASS 10
            case ProductType.Class10Science.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class10Social_Science.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            //MARK:- CLASS 10 NCERT
            
            case ProductType.Class10Math_NCERT.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class10English_NCERT.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class10Hindi_Vyalkaran_NCERT.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class10Hindi_NCERT.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
                
            case ProductType.Class10English_Grammer_NCERT.rawValue:
                self.updateSubscriptionStatus()
                self.isSubscriptionAvailable = true
            default:
                print("iap not found")
            }
        }
    }
}

//MARK: - SKProducatsRequestDelegate
extension InAppManager: SKProductsRequestDelegate {
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        print(response.products.count)
        for product in response.products {
            print(product.productIdentifier,product.localizedTitle)
        }
        guard response.products.count > 0 else {return}
        self.products = response.products
    }
}


