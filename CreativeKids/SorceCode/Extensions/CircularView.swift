//
//  CircularView.swift
//  CreativeKids(Updated)
//
//  Created by Creative Kids on 10/04/21.
//

import Foundation
import UIKit

class CircularProgressView: UIView {
    var progressLyr = CAShapeLayer()
    var trackLyr = CAShapeLayer()
    var percentLabel = UILabel()
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.makeCircularPath()
        }
    }
    var progressClr = UIColor.white {
        didSet {
            progressLyr.strokeColor = progressClr.cgColor
        }
    }
    var trackClr = UIColor.white {
        didSet {
            trackLyr.strokeColor = trackClr.cgColor
        }
    }
    
    var percentagetext = String() {
        didSet {
            percentLabel.text = percentagetext
        }
    }
    var  progressLyrlineWidth:CGFloat = 6
    
    func makeCircularPath() {
        self.backgroundColor = UIColor.clear
        self.layer.cornerRadius = self.frame.size.width/2
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: frame.size.width/2, y: frame.size.height/2), radius: (frame.size.width - 1.5)/2, startAngle: CGFloat(-0.5 * .pi), endAngle: CGFloat(1.5 * .pi), clockwise: true)
        trackLyr.path = circlePath.cgPath
        trackLyr.fillColor = UIColor.clear.cgColor
        trackLyr.strokeColor = trackClr.cgColor
        trackLyr.lineWidth = progressLyrlineWidth
        trackLyr.strokeEnd = 1.0
        layer.addSublayer(trackLyr)
        progressLyr.path = circlePath.cgPath
        progressLyr.fillColor = UIColor.clear.cgColor
        progressLyr.strokeColor = progressClr.cgColor
        progressLyr.lineWidth = progressLyrlineWidth
        progressLyr.strokeEnd = 0.0
        progressLyr.lineCap = .round
        trackLyr.addSublayer(progressLyr)
        percentLabel = UILabel(frame: CGRect(x: progressLyrlineWidth ,y: progressLyrlineWidth, width: (self.frame.width) - (progressLyrlineWidth * 2), height: (self.frame.height) - (progressLyrlineWidth * 2)))
        percentLabel.textAlignment = .center
//        percentLabel.text = percentagetext
        percentLabel.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        percentLabel.textColor = UIColor.white
        self.addSubview(percentLabel)
    }
    func setProgressWithAnimation(duration: TimeInterval, value: Float, percentageColor:UIColor = UIColor.white) {
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.duration = duration
        animation.fromValue = 0
        animation.toValue = value
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        progressLyr.strokeEnd = CGFloat(value)
        progressLyr.add(animation, forKey: "animateprogress")
        percentLabel.text = "\(Int(value * 100))%"
        percentLabel.textColor = percentageColor
    }
}

