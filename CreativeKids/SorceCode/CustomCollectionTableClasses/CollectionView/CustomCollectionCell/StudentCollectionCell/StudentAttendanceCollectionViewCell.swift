//
//  StudentAttendanceCollectionViewCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 27/06/22.
//

import UIKit

enum AttendanceStatus:String{
    case present = "P"
    case absent = "A"
    case leave = "L"
}

class StudentAttendanceCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var viewStatus: UIView!
    @IBOutlet weak var labelStatus: UILabel!
    @IBOutlet weak var viewMain: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configCell(data:Any){
        guard let kData = data as? StudentAttendanceDetailModel else {
            return
        }
        viewStatus.drawShadowwithCorner()
        viewMain.drawShadowwithCorner()
        manageStatusView(type:AttendanceStatus(rawValue: kData.status) ?? .absent)
        labelDate.text = String.convertDateString(dateString: kData.date, fromFormat: "yyyy-MM-dd'T'hh:mm:ss", toFormat: "MM/dd/yyyy")
        
    }
    
    func manageStatusView(type:AttendanceStatus){
        labelStatus.text = type.rawValue
        switch type{
        case .present:
            viewStatus.backgroundColor = UIColor.systemGreen
        case .absent:
            viewStatus.backgroundColor = UIColor.systemRed
        case .leave:
            viewStatus.backgroundColor = UIColor.systemIndigo
        }
    }
}
