//
//  UploadHomeworkCollectionCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 26/03/22.
//

import UIKit

class UploadHomeworkCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imageHomework: UIImageView!
    @IBOutlet weak var viewBackground: UIView! {
        didSet {
            viewBackground.drawShadowOnCell()
        }
    }
    @IBOutlet weak var buttonCross: UIButton!
    
    var removeImageCallBack: (()-> ())?
    
    @IBAction func buttonCrossTapped(_ sender: UIButton) {
        self.removeImageCallBack?()
    }
    
}
