//
//  SubjectCollectionViewCell.swift
//  CreativeKids(Updated)
//
//  Created by Creative Kids on 12/04/21.
//

import UIKit

class SubjectCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageSubject: UIImageView!
    @IBOutlet weak var labelSubjectName: UILabel!
   
    func cellConfig(object:Any,language:String){
        guard let kData = object as? SubjectModel else{return}
        DispatchQueue.main.async {
            self.imageSubject.sd_setImage(with: URL(string:kData.subjectImage ?? ""), placeholderImage: #imageLiteral(resourceName: "placeholder_btn"), options: .continueInBackground, completed: nil)
        }
        self.labelSubjectName.setfont(font: 13, fontFamily: futuraFont)
        self.labelSubjectName.text = kData.subjectName
    }
}
