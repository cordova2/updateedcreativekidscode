//
//  PopularVideosCollectionViewCell.swift
//  CreativeKids(Updated)
//
//  Created by Creative Kids on 12/04/21.
//

import UIKit

class PopularVideosCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageViewThumb: UIImageView!
    
    func cellConfig(object:Any){
        guard let kData = object as? YoutubeDataModel else{return}
        DispatchQueue.main.async {
            self.imageViewThumb.downloadImageFromURL(urlString: kData.videoThumb ?? "")
        }
    }
}
