//
//  OverAllRatingCollectionViewcellCollectionViewCell.swift
//  CreativeKids
//
//  Created by Rakesh jha on 07/05/21.
//

import UIKit
import AVFoundation

class OverAllRatingCollectionViewcell: UICollectionViewCell {

    @IBOutlet weak var labelSubjectName: UILabel!
    @IBOutlet weak var circularProgress: CircularProgressView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func cellConfig(object: Any){
        if let kData = object as? SubjectModel{
            labelSubjectName.text = kData.subjectName
        }
        setOverAllProgressView()
    }

    //MARK:- SET PROGRESS VIEW
    func setOverAllProgressView(){
        circularProgress.trackClr = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        circularProgress.progressClr = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        let progress = (1.00 * (80 / 100))
        
        if UIDevice.isPhone{
            circularProgress.progressLyrlineWidth = 8
            self.circularProgress.percentLabel.font = UIFont.systemFont(ofSize: 9, weight: .medium)
        }else{
            circularProgress.progressLyrlineWidth = 13
            self.circularProgress.percentLabel.font = UIFont.systemFont(ofSize: 17, weight: .medium)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.circularProgress.setProgressWithAnimation(duration: 1.0, value: Float(progress))
            self.circularProgress.percentLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            self.circularProgress.percentLabel.numberOfLines = 0
            self.circularProgress.percentLabel.adjustsFontSizeToFitWidth = true
            self.circularProgress.percentLabel.minimumScaleFactor = 0.5
            self.circularProgress.percentLabel.text = "80%"
           
        }
}
    
}
