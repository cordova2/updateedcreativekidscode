//
//  ImagePreviewCollectionViewCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 23/03/22.
//

import UIKit

class ImagePreviewCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imagePreview: UIImageView!
    @IBOutlet weak var scrollViewImage: UIScrollView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        scrollViewImage.delegate = self
        
        scrollViewImage.minimumZoomScale = 1.0
        scrollViewImage.maximumZoomScale = 10.0
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        imagePreview.image = nil
    }
}
extension ImagePreviewCollectionViewCell: UIScrollViewDelegate {
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imagePreview
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        if scrollView.zoomScale > scrollView.maximumZoomScale {
            scrollView.zoomScale = scrollView.maximumZoomScale
        } else if scrollView.zoomScale < scrollView.minimumZoomScale {
            scrollView.zoomScale = scrollView.minimumZoomScale
        }
    }
}
