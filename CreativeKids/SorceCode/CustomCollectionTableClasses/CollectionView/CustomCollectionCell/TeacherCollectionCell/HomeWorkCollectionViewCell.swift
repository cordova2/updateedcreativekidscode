//
//  HomeWorkCollectionViewCell.swift
//  CreativeKids
//
//  Created by Prashant Swain on 09/10/22.
//

import UIKit

class HomeWorkCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var imageView: UIImageView!
    var callBack:(()->Void)?
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    @IBAction func buttonSelectedTapped(_ sender: UIButton) {
        callBack?()
    }
    func configCell(data: Any){
        if let imageData = data as? String{
            imageView.downloadImageFromURL(urlString: imageData)
        }else{
            if let imageData = data as? UIImage{
                imageView.image = imageData
            }
        }
    }
}
