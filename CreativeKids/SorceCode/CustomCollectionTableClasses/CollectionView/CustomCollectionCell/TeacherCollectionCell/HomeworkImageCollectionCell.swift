//
//  HomeworkImageCollectionCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 16/03/22.
//

import UIKit

class HomeworkImageCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var viewBackground: UIView! {
        didSet {
            viewBackground.drawShadowOnCell()
        }
    }
    @IBOutlet weak var imageViewFile: UIImageView!
    @IBOutlet weak var buttonCross: UIButton!
    
    var removeImageButtonCallback: (() -> ())?
    
    @IBAction func buttonCrossTapped(_ sender: UIButton) {
        self.removeImageButtonCallback?()
    }
    
    func configCell(data:Any){
        guard let imageUrl = data as? String else{return}
        imageViewFile.downloadImageFromURL(urlString: imageUrl)
    }
}
