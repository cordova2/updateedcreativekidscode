//
//  PeriodsCollectionViewCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 23/06/22.
//

import UIKit

class PeriodsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var viewBackground: UIView!
    
    @IBOutlet weak var labelPeriodNumber: UILabel!
    @IBOutlet weak var labelClassName: UILabel!
    
    @IBOutlet weak var labelSubject: UILabel!
    @IBOutlet weak var viewPeriodStatus: UIView!
    
    @IBOutlet weak var imageThumb: UIImageView!
    @IBOutlet weak var labelDone: UILabel!
    
    func configCell(data:Any){
        viewBackground.layer.cornerRadius = 5
        guard let cellData = data as? PeriodModel else{return}
        if cellData.isOver || cellData.isAttandanceMarked {
            viewPeriodStatus.isHidden = false
            imageThumb.isHidden = !cellData.isAttandanceMarked
            labelDone.isHidden = !cellData.isAttandanceMarked
        }else{
            viewPeriodStatus.isHidden = true
            imageThumb.isHidden = !cellData.isAttandanceMarked
            labelDone.isHidden = !cellData.isAttandanceMarked
        }
        viewBackground.backgroundColor = cellData.isFree ? UIColor(hexString: "FBAD2B") : UIColor(hexString: "0500F5")
        labelClassName.text = "\(cellData.className ?? "") \(cellData.section ?? "")"
        labelSubject.text = cellData.subject ?? ""
        labelPeriodNumber.text = cellData.number ?? "Free"
        
        
    }
}
