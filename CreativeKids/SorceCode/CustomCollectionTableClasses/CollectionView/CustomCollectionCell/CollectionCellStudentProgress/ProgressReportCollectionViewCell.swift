//
//  ProgressReportCollectionViewCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 22/04/21.
//

import UIKit

class ProgressReportCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageViewSubjects: UIImageView!
    @IBOutlet weak var LabelSubjectName: UILabel!
    @IBOutlet weak var viewButtomLine: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func cellConfig(object: Any,index:Int){
        guard let kData = object as? SubjectModel else {return}
        imageViewSubjects.downlodeImage(serviceurl: kData.subjectImage, placeHolder: #imageLiteral(resourceName: "placeholder_btn"))
        self.LabelSubjectName.text = kData.subjectName
        viewButtomLine.isHidden = kData.isSelected ? false:true
        LabelSubjectName.textColor = kData.isSelected ? #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1):#colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        
    }
    
}
