//
//  SubjectPerformanceCollectionViewCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 26/04/21.
//

import UIKit

class SubjectPerformanceCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var circularProgress: CircularProgressView!
    @IBOutlet weak var chapterReadPercentage: UILabel!
    @IBOutlet weak var labelExerciseReadPercentage: UILabel!
    @IBOutlet weak var labelLtpReadPercentage: UILabel!
    
    @IBOutlet weak var progressChapterRead: UIProgressView!
    @IBOutlet weak var progressExerciseRead: UIProgressView!
    @IBOutlet weak var progressLtpRead: UIProgressView!
    
    @IBOutlet weak var labelTotalTime: UILabel!
    @IBOutlet weak var labelRightQuestion: UILabel!
    @IBOutlet weak var labelTotalMarks: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setOverAllProgressView()
        // Initialization code
    }
    
    func cellConfig(object: Any){
        guard let data = object as? (PerformanceModel,PerformanceModel) else {return}
        let chapterRead = Int.getInt(String.getString(data.0.chapterRead ?? ""))
        let exerciseDone = Int.getInt(String.getString(data.0.exerciseRead ?? ""))
        let LTPDone = Int.getInt(String.getString(data.0.ltpRead ?? ""))
        let overAllPerformance = Int.getInt(String.getString(data.0.performance ?? ""))
        let totalTime = Int.getInt(String.getString(data.1.totalTime ?? ""))
        let totalTimeConvert = String.secondsToHoursMinutesSeconds(seconds: totalTime)
        setOverAllProgressView(progressValue:Float(overAllPerformance)/100)
        chapterReadPercentage.text = "\(chapterRead)%"
        labelExerciseReadPercentage.text = "\(exerciseDone)%"
        labelLtpReadPercentage.text = "\(LTPDone)%"
        self.progressChapterRead.progress = Float(chapterRead)/100
        self.progressExerciseRead.progress = Float(exerciseDone)/100
        self.progressLtpRead.progress = Float(LTPDone)/100
        self.labelTotalTime.text = "\(totalTimeConvert.0):\(totalTimeConvert.1):\(totalTimeConvert.2)"
        self.labelRightQuestion.text = "\(data.1.rightAnswer ?? "")/\(data.1.totalQuestion ?? "")"
        self.labelTotalMarks.text = "\(data.1.obtainMarks ?? "")/\(data.1.totalMarks ?? "")"
    }
    
    //MARK:- SET PROGRESS VIEW
    func setOverAllProgressView(progressValue:Float = 0.0){
        circularProgress.trackClr = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0.3014964789)
        circularProgress.progressClr = #colorLiteral(red: 0.9607843161, green: 0.7058823705, blue: 0.200000003, alpha: 1)
        circularProgress.progressLyrlineWidth = 22
//        let progress = (1.00 * (80 / 100))
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.circularProgress.setProgressWithAnimation(duration: 1.0, value: Float(progressValue))
            self.circularProgress.percentLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            self.circularProgress.percentLabel.font = UIFont.systemFont(ofSize: 12, weight: .medium)
            self.circularProgress.percentLabel.numberOfLines = 0
            self.circularProgress.percentLabel.text = "\(Int(progressValue * 100))% Completed"
        }
    }
}
