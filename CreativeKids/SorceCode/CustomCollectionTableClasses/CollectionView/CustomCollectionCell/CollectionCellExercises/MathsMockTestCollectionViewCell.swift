//
//  MathsMockTestCollectionViewCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 11/10/21.
//

import UIKit

class MathsMockTestCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var labelQuestions: UILabel!
    @IBOutlet var labelOptions: [UILabel]!
    @IBOutlet var buttonOptions: [UIButton]!
    @IBOutlet weak var buttonConfirmNext: UIButton!
    @IBOutlet weak var optionImage: UIImageView!
    @IBOutlet var viewOptions: [UIView]!
    @IBOutlet weak var imageScrollTop: NSLayoutConstraint!
    @IBOutlet weak var imageQuestionTop: NSLayoutConstraint!
    @IBOutlet weak var questionImage: UIImageView!
    @IBOutlet var questionImageAspectRatio: NSLayoutConstraint!
    var nextCallback: (() -> ())?
    var selectedOptionCallback: (() -> ())?
    var checkAns = 0
    var titleChangeCount = 0
    var mockTestModel = [VSAQQuestionModel]()
    var mockTest: VSAQQuestionModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func configCell(modelData: VSAQQuestionModel?, indexPath: IndexPath) {
        self.mockTest = modelData
        self.titleChangeCount = indexPath.item
        self.viewOptions[0].isHidden = modelData?.option_A == "" ? true : false
        self.viewOptions[1].isHidden = modelData?.option_B == "" ? true : false
        self.viewOptions[2].isHidden = modelData?.option_C == "" ? true : false
        self.viewOptions[3].isHidden = modelData?.option_D == "" ? true : false
        
        if isHindiSubject(subejct: modelData?.subname ?? "") {
            self.labelQuestions.attributedText = makeNewNumericStringFor3And7(StringWithNumber:modelData?.Questions ?? "")
            self.labelOptions[0].attributedText =  makeNewNumericStringFor3And7(StringWithNumber:modelData?.option_A ?? "")
            self.labelOptions[1].attributedText =  makeNewNumericStringFor3And7(StringWithNumber:modelData?.option_B ?? "")
            self.labelOptions[2].attributedText =  makeNewNumericStringFor3And7(StringWithNumber:modelData?.option_C ?? "")
            self.labelOptions[3].attributedText =  makeNewNumericStringFor3And7(StringWithNumber:modelData?.option_D ?? "")
        } else {
            self.labelQuestions.setAttributedText(text: modelData?.Questions ?? "", colorText: .white, font: 18)
            self.questionImage.downlodeImage(serviceurl: modelData?.img, placeHolder: nil)
            self.labelOptions[0].setAttributedText(text: modelData?.option_A ?? "", colorText: .white, font: 16)
            self.labelOptions[1].setAttributedText(text: modelData?.option_B ?? "", colorText: .white, font: 16)
            self.labelOptions[3].setAttributedText(text: modelData?.option_D ?? "", colorText: .white, font: 16)
            self.labelOptions[2].setAttributedText(text: modelData?.option_C ?? "", colorText: .white, font: 16)
        }
        
        if imageTypeValue(question: modelData?.Questions?.lowercased() ?? "") {
            self.labelQuestions.isHidden = true
            self.imageScrollTop.isActive = true
            self.imageScrollTop.constant = 20
            self.imageQuestionTop.isActive = false
            DispatchQueue.main.async {
                self.questionImage.contentMode = UIDevice.isPad ? .scaleAspectFit : .scaleToFill
                self.questionImage.downlodeImage(serviceurl: modelData?.quesimg, placeHolder: nil)
                self.optionImage.downlodeImage(serviceurl: modelData?.img, placeHolder: nil)
            }
        } else {
            self.labelQuestions.isHidden = false
            self.imageQuestionTop.isActive = true
            self.imageQuestionTop.constant = 20
            self.imageScrollTop.isActive = false
            self.questionImage.isHidden = true
            DispatchQueue.main.async {
                self.optionImage.downlodeImage(serviceurl: modelData?.img, placeHolder: nil)
            }
        }
    }
    @IBAction func buttonOptionTapped(_ sender: UIButton) {
        viewChangeBacground(sender: sender)
    }
    @IBAction func nextBtnTapped(_ sender: UIButton) {
        // viewChangeBacground(sender: sender)
        self.checkOptionSelection()
        if sender.titleLabel?.text == "Confirm" {
            self.selectedOptionCallback?()
        } else {
            let selectedButton = buttonOptions.filter{$0.isSelected == true}
            mockTest?.userSelectedOption = selectedButton.first!.tag
            self.nextCallback?()
        }
    }
    //MARK: - Validation
    func checkOptionSelection() {
        if !buttonOptions[0].isSelected && !buttonOptions[1].isSelected && !buttonOptions[2].isSelected && !buttonOptions[3].isSelected {
            buttonConfirmNext.setTitle("Confirm", for: .normal)
            showAlertMessage.alert(message: "Please Select One Option")
            return
        } else {
            self.changeBackground()
        }
    }
    //MARK: - For Changing Selected Button Color According to answer
    func changeBackground() {
        buttonOptions[0].isUserInteractionEnabled = false
        buttonOptions[1].isUserInteractionEnabled = false
        buttonOptions[2].isUserInteractionEnabled = false
        buttonOptions[3].isUserInteractionEnabled = false
        if titleChangeCount == mockTestModel.count - 1 {
            self.buttonConfirmNext.setTitle("Done", for: .normal)
        } else {
            self.buttonConfirmNext.setTitle("Next", for: .normal)
        }
        self.buttonOptions[0].setImage(buttonOptions[0].isSelected == true ? (mockTest?.rightAns != buttonOptions[0].tag ? #imageLiteral(resourceName: "qb_red_btn") : #imageLiteral(resourceName: "qb_green_btn")):#imageLiteral(resourceName: "qb_blank_btn-1"), for: .normal)
        self.buttonOptions[1].setImage(buttonOptions[1].isSelected == true ? (mockTest?.rightAns != buttonOptions[1].tag ? #imageLiteral(resourceName: "qb_red_btn") : #imageLiteral(resourceName: "qb_green_btn")):#imageLiteral(resourceName: "qb_blank_btn-1"), for: .normal)
        self.buttonOptions[2].setImage(buttonOptions[2].isSelected == true ? (mockTest?.rightAns != buttonOptions[2].tag ? #imageLiteral(resourceName: "qb_red_btn") : #imageLiteral(resourceName: "qb_green_btn")):#imageLiteral(resourceName: "qb_blank_btn-1"), for: .normal)
        self.buttonOptions[3].setImage(buttonOptions[3].isSelected == true ? (mockTest?.rightAns != buttonOptions[3].tag ? #imageLiteral(resourceName: "qb_red_btn") : #imageLiteral(resourceName: "qb_green_btn")):#imageLiteral(resourceName: "qb_blank_btn-1"), for: .normal)
        
        switch mockTest?.rightAns {
        case 1:
            self.buttonOptions[0].setImage(#imageLiteral(resourceName: "qb_green_btn"), for: .normal)
        case 2:
            self.buttonOptions[1].setImage(#imageLiteral(resourceName: "qb_green_btn"), for: .normal)
        case 3:
            self.buttonOptions[2].setImage(#imageLiteral(resourceName: "qb_green_btn"), for: .normal)
        default:
            self.buttonOptions[3].setImage(#imageLiteral(resourceName: "qb_green_btn"), for: .normal)
        }
    }
    func viewChangeBacground(sender: UIButton) {
        self.buttonOptions[0].setImage(sender.tag == 1 ? #imageLiteral(resourceName: "qb_blue_btn"): #imageLiteral(resourceName: "qb_blank_btn-1"), for: .normal)
        self.buttonOptions[1].setImage(sender.tag == 2 ? #imageLiteral(resourceName: "qb_blue_btn"): #imageLiteral(resourceName: "qb_blank_btn-1"), for: .normal)
        self.buttonOptions[2].setImage(sender.tag == 3 ? #imageLiteral(resourceName: "qb_blue_btn"): #imageLiteral(resourceName: "qb_blank_btn-1"), for: .normal)
        self.buttonOptions[3].setImage(sender.tag == 4 ? #imageLiteral(resourceName: "qb_blue_btn"): #imageLiteral(resourceName: "qb_blank_btn-1"), for: .normal)
        
        self.buttonOptions[0].isSelected = sender.tag == 1 ? true : false
        self.buttonOptions[1].isSelected = sender.tag == 2 ? true : false
        self.buttonOptions[2].isSelected = sender.tag == 3 ? true : false
        self.buttonOptions[3].isSelected = sender.tag == 4 ? true : false
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        self.questionImage.isHidden = false
        self.optionImage.image = nil
        self.questionImage.image = nil
        self.buttonOptions.forEach{$0.setImage(#imageLiteral(resourceName: "qb_blank_btn-1"), for: .normal)}
        self.buttonOptions.forEach{$0.isUserInteractionEnabled = true}
        self.buttonOptions.forEach{$0.isSelected = false}
    }
}
