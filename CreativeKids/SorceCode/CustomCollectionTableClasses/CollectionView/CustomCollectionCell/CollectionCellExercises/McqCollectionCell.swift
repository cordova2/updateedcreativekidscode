//
//  McqCollectionCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 26/03/21.
//

import UIKit

class McqCollectionCell: UICollectionViewCell {
    
    //MARK: - Outlets
    @IBOutlet weak var labelQuestions: UILabel!
    @IBOutlet weak var buttonOptionsA: UIButton!
    @IBOutlet weak var buttonOptionsB: UIButton!
    @IBOutlet weak var buttonOptionsC: UIButton!
    @IBOutlet weak var buttonOptionsD: UIButton!
    @IBOutlet weak var viewOptionsA: UIView!
    @IBOutlet weak var viewOptionsB: UIView!
    @IBOutlet weak var viewOptionsC: UIView!
    @IBOutlet weak var viewOptionsD: UIView!
    @IBOutlet var labelOptions: [UILabel]!
    @IBOutlet weak var buttonConfirmNext: UIButton!
    @IBOutlet weak var labelQuestionMarks: UILabel!
    
    //MARK: - Callback
    var nextCallback: (() -> ())?
    var selectedOptionCallback: (() -> ())?
    var mcqModel: MCQQuestionModel?
    var selectedTopic:TopicModel?
    var mcqModelArr = [MCQQuestionModel]()
    
    var checkAns = 0
    var titleChangeCount = 0
    
    
    //MARK: - Button Action
    @IBAction func nextBtnTapped(_ sender: UIButton) {
        self.checkOptionSelection()
        if sender.titleLabel?.text == "Confirm" {
            self.selectedOptionCallback?()
        } else {
            self.nextCallback?()
        }
    }
    
    //MARK: - Configure Cell
    func configCollectionCell(questions: MCQQuestionModel, index:IndexPath) {
        self.titleChangeCount = index.item
        self.mcqModel = questions
        self.labelQuestionMarks.text = "Marks:\(String.getstring(questions.Marks))"
        self.viewOptionsA.isHidden = questions.option_A == "" ? true : false
        self.viewOptionsB.isHidden = questions.option_B == "" ? true : false
        self.viewOptionsC.isHidden = questions.option_C == "" ? true : false
        self.viewOptionsD.isHidden = questions.option_D == "" ? true : false
        
        if isHindiSubject(subejct:selectedTopic?.subjectName ?? ""){
            self.labelQuestions.setfont(font: 18, fontFamily: chanakyaFont)
            self.labelOptions.forEach{$0.setfont(font: 18, fontFamily: chanakyaFont)}
            if isContainNumber(checkString: questions.Questions ?? ""){
                self.labelQuestions.attributedText =  makeNewNumericString(StringWithNumber: questions.Questions?.stringByTrimmingWhiteSpaceAndNewLine() ?? "")
            }else{
                self.labelQuestions.text = questions.Questions?.stringByTrimmingWhiteSpaceAndNewLine()
            }
            self.labelOptions[0].attributedText = makeNewNumericString(StringWithNumber:questions.option_A ?? "")
            self.labelOptions[1].attributedText = makeNewNumericString(StringWithNumber:questions.option_B ?? "")
            self.labelOptions[2].attributedText = makeNewNumericString(StringWithNumber:questions.option_C ?? "")
            self.labelOptions[3].attributedText = makeNewNumericString(StringWithNumber:questions.option_D ?? "")
        } else {
            self.labelQuestions.setfont(font: 18, fontFamily: futuraFont)
            self.labelOptions.forEach{$0.setfont(font: 18, fontFamily: futuraFont)}
                self.labelQuestions.setAttributedText(text: questions.Questions?.stringByTrimmingWhiteSpaceAndNewLine() ?? "",colorText: .white, font: 16)
                self.labelOptions[0].setAttributedText(text: questions.option_A ?? "",colorText: .black, font: 14)
                self.labelOptions[1].setAttributedText(text: questions.option_B ?? "",colorText: .black, font: 14)
                self.labelOptions[2].setAttributedText(text: questions.option_C ?? "",colorText: .black, font: 14)
                self.labelOptions[3].setAttributedText(text: questions.option_D ?? "",colorText: .black, font: 14)
        }
    }
    //MARK: - Validation
    func checkOptionSelection() {
        if !buttonOptionsA.isSelected && !buttonOptionsB.isSelected && !buttonOptionsC.isSelected && !buttonOptionsD.isSelected {
            buttonConfirmNext.setTitle("Confirm", for: .normal)
            showAlertMessage.alert(message: "Please Select One Option")
            return
        } else {
            self.changeBackground()
        }
    }
    //MARK: - For Changing Selected Button Color According to answer
    func changeBackground() {
        buttonOptionsA.isUserInteractionEnabled = false
        buttonOptionsB.isUserInteractionEnabled = false
        buttonOptionsC.isUserInteractionEnabled = false
        buttonOptionsD.isUserInteractionEnabled = false
        if titleChangeCount == mcqModelArr.count - 1 {
            self.buttonConfirmNext.setTitle("Done", for: .normal)
        } else {
            self.buttonConfirmNext.setTitle("Next", for: .normal)
        }
        self.viewOptionsA.backgroundColor = buttonOptionsA.isSelected == true ? (mcqModel?.rightAns != buttonOptionsA.tag ? #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1) : #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)):#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.viewOptionsB.backgroundColor = buttonOptionsB.isSelected == true ? (mcqModel?.rightAns != buttonOptionsB.tag ? #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1) : #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)):#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.viewOptionsC.backgroundColor = buttonOptionsC.isSelected == true ? (mcqModel?.rightAns != buttonOptionsC.tag ? #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1) : #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)):#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.viewOptionsD.backgroundColor = buttonOptionsD.isSelected == true ? (mcqModel?.rightAns != buttonOptionsD.tag ? #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1) : #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)):#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        switch mcqModel?.rightAns {
        case 1:
            self.viewOptionsA.backgroundColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
        case 2:
            self.viewOptionsB.backgroundColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
        case 3:
            self.viewOptionsC.backgroundColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
        default:
            self.viewOptionsD.backgroundColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
        }
        //MARK: - Set Voice According to the Answer And Language
        if buttonConfirmNext.titleLabel?.text == "Confirm" {
            if  isHindiSubject(subejct:selectedTopic?.subjectName ?? ""){
                CommonUtils.greedVoice(voice: buttonOptionsA.isSelected == true ? (mcqModel?.rightAns != buttonOptionsA.tag ? "Galat Javaab" : "Sahi Javaab") : "")
                CommonUtils.greedVoice(voice: buttonOptionsB.isSelected == true ? (mcqModel?.rightAns != buttonOptionsB.tag ? "Galat Javaab" : "Sahi Javaab") : "")
                CommonUtils.greedVoice(voice: buttonOptionsC.isSelected == true ? (mcqModel?.rightAns != buttonOptionsC.tag ? "Galat Javaab" : "Sahi Javaab") : "")
                CommonUtils.greedVoice(voice: buttonOptionsD.isSelected == true ? (mcqModel?.rightAns != buttonOptionsD.tag ? "Galat Javaab" : "Sahi Javaab") : "")
            } else {
                CommonUtils.greedVoice(voice: buttonOptionsA.isSelected == true ? (mcqModel?.rightAns != buttonOptionsA.tag ? "Wrong Answer" : "Right Answer") : "")
                CommonUtils.greedVoice(voice: buttonOptionsB.isSelected == true ? (mcqModel?.rightAns != buttonOptionsB.tag ? "Wrong Answer" : "Right Answer") : "")
                CommonUtils.greedVoice(voice: buttonOptionsC.isSelected == true ? (mcqModel?.rightAns != buttonOptionsC.tag ? "Wrong Answer" : "Right Answer") : "")
                CommonUtils.greedVoice(voice: buttonOptionsD.isSelected == true ? (mcqModel?.rightAns != buttonOptionsD.tag ? "Wrong Answer" : "Right Answer") : "")
            }
        }else {
            CommonUtils.greedVoice(voice: "")
        }
    }
    //MARK: - Button Action
    @IBAction func buttonOptionATapped(_ sender: UIButton) {
        self.viewChangeBacground(sender: sender)
        
    }
    @IBAction func buttonOptionBTapped(_ sender: UIButton) {
        self.viewChangeBacground(sender: sender)
    }
    @IBAction func buttonOptionCTapped(_ sender: UIButton) {
        self.viewChangeBacground(sender: sender)
    }
    @IBAction func buttonOptionDTapped(_ sender: UIButton) {
        self.viewChangeBacground(sender: sender)
    }
    //MARK: - Change Color Of Button On Selection
    func viewChangeBacground(sender: UIButton) {
        self.viewOptionsA.backgroundColor = sender.tag == 1 ? #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1) :#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.viewOptionsB.backgroundColor = sender.tag == 2 ? #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1) :#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.viewOptionsC.backgroundColor = sender.tag == 3 ? #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1) :#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.viewOptionsD.backgroundColor = sender.tag == 4 ? #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1) :#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.buttonOptionsA.isSelected = sender.tag == 1 ? true : false
        self.buttonOptionsB.isSelected = sender.tag == 2 ? true : false
        self.buttonOptionsC.isSelected = sender.tag == 3 ? true : false
        self.buttonOptionsD.isSelected = sender.tag == 4 ? true : false
    }
    //MARK: - For Reuse Cell
    override func prepareForReuse() {
        super.prepareForReuse()
        self.buttonOptionsA.isUserInteractionEnabled = true
        self.buttonOptionsB.isUserInteractionEnabled = true
        self.buttonOptionsC.isUserInteractionEnabled = true
        self.buttonOptionsD.isUserInteractionEnabled = true
        self.buttonOptionsA.isSelected = false
        self.buttonOptionsB.isSelected = false
        self.buttonOptionsC.isSelected = false
        self.buttonOptionsD.isSelected = false
        self.viewOptionsA.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.viewOptionsB.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.viewOptionsC.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.viewOptionsD.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    }
}
//MARK: - Scroll Collection
extension UICollectionView {
    func scrollToNextItem() {
        let contentOffset = CGFloat(floor(self.contentOffset.x + self.bounds.size.width))
        print(self.bounds.size.width)
        self.moveToFrame(contentOffset: contentOffset)
    }
    func moveToFrame(contentOffset : CGFloat) {
        self.setContentOffset(CGPoint(x: contentOffset, y: self.contentOffset.y), animated: true)
    }
}

