//
//  ExcelExamTableViewCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 30/03/21.
//

import UIKit
import IQKeyboardManagerSwift

class ExcelExamCollectionViewCell: UICollectionViewCell {
    
    //MARK: - Outlets
    @IBOutlet weak var questionLable: UILabel!
    @IBOutlet weak var textViewAns: IQTextView!
    @IBOutlet weak var submitButton: UIButton!
    
    var callBackSubmit: (() -> ())?
    var getPdfSubmit: (() -> Void)?
    var selectedTopic:TopicSearchModel?
    
    //MARK: - Cell Configure
    func configCell(ExcelExamModel: ExcelExamQuestionModel) {
        
        let questions = ExcelExamModel.Questions?.replacingOccurrences(of: "<br>", with: "\n")
        
        if isHindiSubject(subejct:selectedTopic?.subjectName ?? ""){
            self.questionLable.attributedText = makeNewNumericString(StringWithNumber: questions ?? "")
            self.questionLable.setfont(font: 18, fontFamily: chanakyaFont)
            self.textViewAns.font = UIFont(name: chanakyaFont , size: 16)
            self.textViewAns.placeholder = "यहाँ उत्तर लिखें..."
        } else {
                self.questionLable.setAttributedText(text: questions ?? "",colorText: .black, font: 16)
                self.questionLable.setfont(font: 18, fontFamily: futuraFont)
                self.textViewAns.font = UIFont(name: futuraFont , size: 16)
                self.textViewAns.placeholder = "Write Answer Here..."
        }
    }
    
    //MARK: - Reuse Cell
    override func prepareForReuse() {
        super.prepareForReuse()
        self.textViewAns.text = ""
    }
    //MARK: - Button Action
    @IBAction func submitButtonTapped(_ sender: UIButton) {
        if textViewAns.text.isEmpty {
            showAlertMessage.alert(message: "Please Write Answer")
            return
        } else {
            self.callBackSubmit?()
        }
    }
    @IBAction func buttongetPDFTapped(_ sender: UIButton) {
        getPdfSubmit?()
    }
}
