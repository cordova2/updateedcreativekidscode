//
//  GroupMemberCollectionViewCell.swift
//  Chat Demo
//
//  Created by Creative Kids on 25/03/22.
//

import UIKit
import Foundation

class GroupMemberCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var buttonCancelTapped: UIButton!
    var deleteCallBACK:(()->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func buttonCancelTapped(_ sender: UIButton) {
        deleteCallBACK?()
    }
    func configureCell(data:Any){
        guard let kdata = data as? UsersState else{return}
        imageProfile.downlodeImage(serviceurl: String.getstring(kdata.profile_image), placeHolder: UIImage(systemName: "person.circle"))
        labelName.text = String.getstring(kdata.name)
    }
}
