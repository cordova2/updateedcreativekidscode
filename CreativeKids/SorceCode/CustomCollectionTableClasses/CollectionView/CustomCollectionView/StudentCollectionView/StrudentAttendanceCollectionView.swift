//
//  StrudentAttendanceCollectionView.swift
//  CreativeKids
//
//  Created by Creative Kids on 27/06/22.
//

import Foundation
import UIKit

class StudentAttendanceCollectionView: UICollectionView, UICollectionViewDelegateFlowLayout {
    
    var item = [Any]()
    var dataPassDelegate:DataSourcesCollectionDelegate?
    var customDataSourecs:CustomCollectionViewDataSources<StudentAttendanceCollectionViewCell,Any>!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.initialSetup()
    }
    func initialSetup() {
        self.register(UINib(nibName:  CollectionCellIdentifier.StudentAttendCell, bundle: nil), forCellWithReuseIdentifier:  CollectionCellIdentifier.StudentAttendCell)
        self.customDataSourecs = .init(cellIdentifier: CollectionCellIdentifier.StudentAttendCell, item: self.item, completion: { cell, data, index in
            cell.configCell(data: data)
        })
        
        DispatchQueue.main.async {
            self.delegate = self
            self.dataSource = self.customDataSourecs
            self.reloadData()
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.frame.width/4, height: self.frame.width/4)
    }
}
extension StudentAttendanceCollectionView: CollectionDataDelegate {
    func getData(cell: String, data: [Any]) {
        self.item = data
        self.initialSetup()
    }
}
