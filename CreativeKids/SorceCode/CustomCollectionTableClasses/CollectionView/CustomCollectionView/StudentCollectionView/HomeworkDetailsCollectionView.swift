//
//  HomeworkDetailsCollection.swift
//  CreativeKids
//
//  Created by Creative Kids on 29/04/22.
//

import UIKit

class HomeworkDetailsCollectionView: UICollectionView, UICollectionViewDelegate {
    
    var item = [Any]()
    var dataPassDelegate:DataSourcesCollectionDelegate?
    var customDataSourecs:CustomCollectionViewDataSources<HomeworkImageCollectionCell,Any>!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.initialSetup()
    }
    func initialSetup() {
        self.customDataSourecs = .init(cellIdentifier: CollectionCellIdentifier.homeworkDetails, item: self.item, completion: { cell, data, index in
            cell.configCell(data: data)
        })
        
        DispatchQueue.main.async {
            self.delegate = self
            self.dataSource = self.customDataSourecs
            self.reloadData()
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/4, height: collectionView.frame.width/4)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.dataPassDelegate?.collectionView(self, data: self.item[indexPath.item], selectedIndex: indexPath)
    }
}
extension HomeworkDetailsCollectionView: CollectionDataDelegate {
    func getData(cell: String, data: [Any]) {
        self.item = data
        self.initialSetup()
    }
}
