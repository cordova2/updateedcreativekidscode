//
//  HomeWorkCollectionView.swift
//  CreativeKids
//
//  Created by Prashant Swain on 09/10/22.
//

import Foundation
import UIKit

class HomeWorkCollectioView:UICollectionView,UICollectionViewDelegateFlowLayout{
    
    var item = [Any]()
    var dataPassDelegate:DataSourcesCollectionDelegate?
    var customDataSourecs:CustomCollectionViewDataSources<HomeWorkCollectionViewCell,Any>!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.initialSetup()
    }
    func initialSetup() {
        self.register(UINib(nibName: CollectionCellIdentifier.questionImageCell, bundle: nil), forCellWithReuseIdentifier: CollectionCellIdentifier.questionImageCell)
        dataPassDelegate?.collectionView(self, height: CGFloat((item.count % 4 == 0) ? item.count/4 : (item.count/4) + 1) * ((self.frame.width/4) + 20))
        self.customDataSourecs = .init(cellIdentifier: CollectionCellIdentifier.questionImageCell, item: self.item, completion: { cell, data, index in
            cell.configCell(data: data)
            cell.callBack = {
                self.dataPassDelegate?.collectionView(self,data:data, selectedIndex:index)
            }
        })
        
        DispatchQueue.main.async {
            self.delegate = self
            self.dataSource = self.customDataSourecs
            self.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/4, height: collectionView.frame.width/4)
    }
}
extension HomeWorkCollectioView: CollectionDataDelegate {
    func getData(cell: String, data: [Any]) {
        self.item = data
        self.initialSetup()
    }
}
