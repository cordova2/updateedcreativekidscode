//
//  SelectionCollectionView.swift
//  CreativeKids
//
//  Created by Creative Kids on 31/08/22.
//

import Foundation
import UIKit

class SelectionCollectionView: UICollectionView,UICollectionViewDelegate,UICollectionViewDataSource {
    var item = [SelectionDelegate]()
    var dataPassDelegate:DataSourcesCollectionDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        self.delegate = self
        self.dataSource = self
        self.initialSetup()
    }
    func initialSetup() {
        DispatchQueue.main.async {
            self.reloadData()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
                self.dataPassDelegate?.collectionView(self, height: self.contentSize.height)
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return item.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionCellIdentifier.selectedCell, for: indexPath) as! SelectedCell
        cell.configCell(data: item[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.dataPassDelegate?.collectionView(self, data: item[indexPath.item], selectedIndex: indexPath)
    }
}
extension SelectionCollectionView: CollectionDataDelegate {
    func getData(cell: String, data: [Any]) {
        guard let kData = data as? [SelectionDelegate] else{return}
        self.item = kData
        self.initialSetup()
    }
}

//MARK:
class SelectedCell:UICollectionViewCell{
    @IBOutlet weak var viewOuter: UIView!
    @IBOutlet weak var labelClass: UILabel!
    
    func configCell<C>(data:C){
        guard let kdata = data as? SelectionDelegate else{return}
        viewOuter.layer.cornerRadius = viewOuter.frame.height/2
        labelClass.text = kdata.name
    }
    
}
