//
//  progressShowCollectionView.swift
//  CreativeKids
//
//  Created by Rakesh jha on 27/04/21.
//

import UIKit

class ProgressShowCollectionView: UICollectionView {
    override func awakeFromNib() {
        super.awakeFromNib()
        setDelegate()
    }
    var item = [Any]()
    var dataSourcesCollection:DataSourcesCollectionDelegate?
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    //MARK: - Set delegate,dataSources
    private func setDelegate() {
//        self.register(UINib(nibName: CollectionCellIdentifier.allSubjectPerformance, bundle: nil), forCellWithReuseIdentifier: CollectionCellIdentifier.allSubjectPerformance)
        self.register(UINib(nibName: CollectionCellIdentifier.subjectPerformance, bundle: nil), forCellWithReuseIdentifier: CollectionCellIdentifier.subjectPerformance)
        
        self.delegate = self
        self.dataSource = self
    }
}

//MARK: - UICollectionViewDelegateFlowLayout, UICollectionViewDataSource
extension ProgressShowCollectionView: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return item.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        if indexPath.item == 0 {
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionCellIdentifier.allSubjectPerformance, for: indexPath) as! AllSubjectPerformanceCollectionViewCell
//            return cell
//        }else{
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionCellIdentifier.subjectPerformance, for: indexPath) as! SubjectPerformanceCollectionViewCell
//            return cell
//        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionCellIdentifier.subjectPerformance, for: indexPath) as! SubjectPerformanceCollectionViewCell
        cell.cellConfig(object: item[indexPath.item])
        return cell
       
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.width, height: self.height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        dataSourcesCollection?.collectionView(self, data: item[indexPath.item])
    }
}

extension ProgressShowCollectionView: CollectionDataDelegate{
    func getData(cell: String, data: [Any]) {
        self.item = data
        self.reloadData()
    }


}
