//
//  CustomCollectionViewDataSources.swift
//  CreativeKids
//
//  Created by Creative Kids on 10/07/21.
//

import Foundation
import UIKit

class CustomCollectionViewDataSources<CELL:UICollectionViewCell,T>:NSObject,UICollectionViewDataSource{
    
    var cellIdentifier:String?
    var item = [T]()
    var configCell:((CELL,T,Int)->())
    
    init(cellIdentifier:String,item:[T],completion: @escaping ((CELL,T,Int)->())) {
        self.cellIdentifier = cellIdentifier
        self.item = item
        self.configCell = completion
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
           return collectionView.getNumberOfCell(numberofCell: item.count, message: "", messageColor: #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1))
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier ?? "", for: indexPath) as! CELL
        configCell(cell,item[indexPath.item],indexPath.item)
        return cell
    }

}
