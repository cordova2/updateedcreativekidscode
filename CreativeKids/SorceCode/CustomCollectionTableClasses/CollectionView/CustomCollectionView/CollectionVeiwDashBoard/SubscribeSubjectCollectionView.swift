//
//  SubscribeSubjectCollectionView.swift
//  CreativeKids
//
//  Created by Creative Kids on 02/06/21.
//

import UIKit
import Foundation

class SubscribeSubjectCollectionView: UICollectionView {

    var item = [Any]()
    var exerciseLanguage:String?
    var dataSourcesCollection:DataSourcesCollectionDelegate?
    var forCollectionViewHeight:Int{
        get{
            let count = item.count
            return (count % 3) == 0 ? count/3 : (count/3) + 1
        }
         
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        setDelegate()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    //MARK: - Set delegate,dataSources
    private func setDelegate() {
        self.register(UINib(nibName: CollectionCellIdentifier.subscribeSubject, bundle: nil), forCellWithReuseIdentifier: CollectionCellIdentifier.subscribeSubject)
            self.delegate = self
            self.dataSource = self
        DispatchQueue.main.async {
            self.reloadData()
        }
    }
}

//MARK: - UICollectionViewDelegateFlowLayout, UICollectionViewDataSource
extension SubscribeSubjectCollectionView: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        dataSourcesCollection?.collectionView(self, height: CGFloat(forCollectionViewHeight * Int(self.frame.width) / 3))
        return item.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionCellIdentifier.subscribeSubject, for: indexPath) as! SubScribeSubjectCollectionViewCell
        cell.cellConfig(object:item[indexPath.item],language: self.exerciseLanguage ?? "English")
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: self.frame.width / 3, height: 130)
        return CGSize(width: self.frame.width / 3, height: self.frame.width / 3)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.dataSourcesCollection?.collectionView(self, data: item[indexPath.item])
    }
    
}
extension SubscribeSubjectCollectionView: CollectionDataDelegate{
    func getData(cell: String, data: [Any], language: String) {
        self.item = data
        self.exerciseLanguage = language
        setDelegate()
    }
}
