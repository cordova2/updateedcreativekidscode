//
//  SubjectCollectionView.swift
//  CreativeKids
//
//  Created by Creative Kids on 14/04/21.
//

import UIKit
import Foundation

class SubjectCollectionView: UICollectionView {

    var item = [Any]()
    var exerciseLanguage:String?
    var dataSourcesCollection:DataSourcesCollectionDelegate?
    var forCollectionViewHeight:Int{
        get{
            let count = item.count
            return (count % 3) == 0 ? count/3 : (count/3) + 1
        }
         
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        setDelegate()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    //MARK: - Set delegate,dataSources
    private func setDelegate() {
            self.delegate = self
            self.dataSource = self
        DispatchQueue.main.async {
            self.reloadData()
        }
    }
}

//MARK: - UICollectionViewDelegateFlowLayout, UICollectionViewDataSource
extension SubjectCollectionView: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        dataSourcesCollection?.collectionView(self, height: CGFloat(forCollectionViewHeight * (Int(self.frame.width) / 3 + 15)))
        return self.getNumberOfCell(numberofCell: item.count, message: "All subjects already subscribed! ")
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionCellIdentifier.subject, for: indexPath) as! SubjectCollectionViewCell
        if indexPath.item == 0{ 
        self.dataSourcesCollection?.collectionView(self, cell: cell)
        }
        cell.cellConfig(object:item[indexPath.item],language: self.exerciseLanguage ?? "English")
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.frame.width / 3, height: (self.frame.width / 3) + 15)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.dataSourcesCollection?.collectionView(self, data: item[indexPath.item])
    }
    
}
extension SubjectCollectionView: CollectionDataDelegate{
    func getData(cell: String, data: [Any], language: String) {
        self.item = data
        self.exerciseLanguage = language
        setDelegate()
    }
}

