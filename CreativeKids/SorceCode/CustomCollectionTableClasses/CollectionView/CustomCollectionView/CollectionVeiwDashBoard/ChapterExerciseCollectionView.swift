//
//  ChapterExerciseCollectionView.swift
//  CreativeKids
//
//  Created by Creative Kids on 12/07/21.
//

import UIKit
import Foundation

class ChapterExerciseCollectionView: UICollectionView, UICollectionViewDelegate {
    var item = [Any]()
    var exerciseLanguage:String?
    var cellIdentifier:String?
    var customDataSourecs:CustomCollectionViewDataSources<ChapterExerciseCollectionViewCell,Any>!
    var dataSourcesCollection:DataSourcesCollectionDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setDelegate()
    }
    
    //MARK: - Set delegate,dataSources
    private func setDelegate() {
            self.register(UINib(nibName: CollectionCellIdentifier.chapterExercise, bundle: nil), forCellWithReuseIdentifier: CollectionCellIdentifier.chapterExercise)
            customDataSourecs = .init(cellIdentifier: CollectionCellIdentifier.chapterExercise, item: item){ (cell, data, index) in
                cell.configCell(cellData: data,index: index,language:self.exerciseLanguage ?? "")
            }
            DispatchQueue.main.async {
                self.dataSource = self.customDataSourecs
                self.delegate = self
                self.reloadData()
            }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.dataSourcesCollection?.collectionView(self, data: item[indexPath.item], selectedIndex: indexPath)
    }
}

extension ChapterExerciseCollectionView: CollectionDataDelegate{
    func getData(cell: String, data: [Any], language: String) {
        self.cellIdentifier = cell
        self.item = data
        self.exerciseLanguage = language
        setDelegate()
    }
}
