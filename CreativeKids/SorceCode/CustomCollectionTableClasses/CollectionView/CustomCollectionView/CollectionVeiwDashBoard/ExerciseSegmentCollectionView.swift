//
//  ExerciseSegmentCollectionView.swift
//  CreativeKids
//
//  Created by Creative Kids on 12/07/21.
//

import UIKit
import Foundation

class ExerciseSegmentCollectionView: UICollectionView, UICollectionViewDelegate {
    var item = [Any]()
    var exerciseLanguage:String?
    var cellIdentifier:String?
    var customDataSourecs:CustomCollectionViewDataSources<ExerciseSegmentCollectionViewCell,Any>!
    var dataSourcesCollection:DataSourcesCollectionDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setDelegate()
    }
    
    //MARK: - Set delegate,dataSources
    private func setDelegate() {
            self.register(UINib(nibName: "ExerciseSegmentCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ExerciseSegmentCollectionViewCell")
            customDataSourecs = .init(cellIdentifier: "ExerciseSegmentCollectionViewCell", item: item){ (cell, data, index) in
                cell.configCell(cellData: data,index: index)
            }
            DispatchQueue.main.async {
                self.dataSource = self.customDataSourecs
                self.delegate = self
                self.reloadData()
            }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let _ = self.item.map{($0 as! SegmentModel).isSelected = false}
        (self.item[indexPath.item] as! SegmentModel).isSelected = true
        self.dataSourcesCollection?.collectionView(self, data: item[indexPath.item], selectedIndex: indexPath)
    }
}

extension ExerciseSegmentCollectionView: CollectionDataDelegate{
    func getData(cell: String, data: [Any], language: String) {
        self.cellIdentifier = cell
        self.item = data
        self.exerciseLanguage = language
        setDelegate()
    }
}


