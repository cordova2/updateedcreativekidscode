//
//  NewChatTableViewCell.swift
//  Chat Demo
//
//  Created by Creative Kids on 24/03/22.
//

import UIKit

class NewChatTableViewCell: UITableViewCell {

    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var suspendView: UIView!
    var selectCallBack:(()->())?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func buttonSelectedTapped(_ sender: UIButton) {
        selectCallBack?()
    }
    
    func configCell(object:Any){
         guard let kdata = object as? UsersState else{return}
        self.labelName.text = String.getstring(kdata.name)
        self.imageProfile.downlodeImage(serviceurl: String.getstring(kdata.profile_image), placeHolder: UIImage(named:"individual_chat 1"))
    }
}
