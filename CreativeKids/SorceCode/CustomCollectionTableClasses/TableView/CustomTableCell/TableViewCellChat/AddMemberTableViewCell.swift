//
//  AddMemberTableViewCell.swift
//  Chat Demo
//
//  Created by Creative Kids on 25/03/22.
//

import UIKit

class AddMemberTableViewCell: UITableViewCell {

    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var imageCheck: UIImageView!
    var selectedCallBack:(()->())?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configCell(object:Any){
         guard let kdata = object as? UsersState else{return}
         self.labelName.text = String.getstring(kdata.name)
         self.imageCheck.image = UIImage(systemName: kdata.isSelected ? "checkmark.circle.fill" : "circle")
    }
    
    @IBAction func buttonTapOnCellTapped(_ sender: UIButton) {
        self.selectedCallBack?()
    }
}
