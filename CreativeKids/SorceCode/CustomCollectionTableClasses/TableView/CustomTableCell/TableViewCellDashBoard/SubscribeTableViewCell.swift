//
//  SubscribeTableViewCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 02/06/21.
//

import UIKit
import Foundation

class SubscribeTableViewCell: UITableViewCell {

    
    @IBOutlet weak var subjectCollectionView: SubscribeSubjectCollectionView!
    @IBOutlet weak var subjectCollectionHeight: NSLayoutConstraint!
    @IBOutlet weak var labelClassName: UILabel!
    
    var navigationReference:UINavigationController?
    override func awakeFromNib() {
        super.awakeFromNib()
        subjectCollectionView.dataSourcesCollection = self
    }
    func configCell(data: Any){
    guard let subscribedData = data as? SubscribeSubjectModel else{return}
        self.labelClassName.text = subscribedData.className ?? ""
        self.subjectCollectionView.getData(cell: CollectionCellIdentifier.subscribeSubject, data: subscribedData.subjectSubscribe, language: "English")
    }
}

// MARK:- CollectionViewViewDataSources
extension SubscribeTableViewCell:DataSourcesCollectionDelegate{
    
    func collectionView(_ collectionView: UICollectionView, data: Any?) {
        guard let selectedSubject = data as? SubjectModel else{return}
        let controller = ChapterViewController.getController(storyboard: .Home)
        controller.subjectSelected = selectedSubject
        self.navigationReference?.pushViewController(controller, animated: true)
    
    }
}
