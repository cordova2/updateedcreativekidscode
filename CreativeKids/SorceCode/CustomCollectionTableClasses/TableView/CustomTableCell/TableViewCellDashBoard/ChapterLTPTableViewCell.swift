//
//  ChapterLTPTableViewCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 13/07/21.
//

import UIKit

class ChapterLTPTableViewCell: UITableViewCell {

    @IBOutlet weak var labelLtpNumber: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configCell(object: Any,index:Int){
        guard let data = object as? ExerciseModel else{return}
        labelLtpNumber.setfont(font: 15, fontFamily: futuraFont)
        labelLtpNumber.text = "\(data.exercise ?? "")"
    }
    
}
