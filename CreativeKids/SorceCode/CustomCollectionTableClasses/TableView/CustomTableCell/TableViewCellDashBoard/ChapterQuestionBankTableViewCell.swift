//
//  ChapterQuestionBankTableViewCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 13/07/21.
//

import UIKit

class ChapterQuestionBankTableViewCell: UITableViewCell {

    @IBOutlet weak var labelSetNumber: UILabel!
    @IBOutlet weak var labelTestNumber: UILabel!
    @IBOutlet weak var buttonGetPdf: UIButton!
    
    var getPDFCallback: (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    func configCell(object: Any,index:Int){
//        labelSetNumber.setfont(font: 15, fontFamily: futuraFont)
//        labelSetNumber.text = "Set \(index + 1)"
        labelTestNumber.text = "Paper - \(index + 1)"
    }
    
    @IBAction func getPdfTapped(_ sender: UIButton) {
        self.getPDFCallback?()
    }
}
