//
//  ChapterExerciseTableViewCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 12/07/21.
//

import UIKit

class ChapterExerciseTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTopicName: UILabel!
    @IBOutlet weak var exerciseCollectionView: ChapterExerciseCollectionView!
    var exerciseCallBackBack:((_ data:Any, _ index:Int)->Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configCell(object: Any,language:String, index:Int){
        exerciseCollectionView.dataSourcesCollection = self
        guard let data = object as? TopicModel else {return}
        setAccordingLangauge(topic:data,language:language, index:index)
        exerciseCollectionView.getData(cell: CollectionCellIdentifier.chapterExercise, data: data.exercise, language: language)
    }
    
    func setAccordingLangauge(topic:TopicModel,language:String, index:Int){
        if isHindiSubject(subejct:language){
            self.labelTopicName.setfont(font: 15, fontFamily: chanakyaFont)
            if isContainNumber(checkString: topic.topic ?? ""){
                self.labelTopicName.text = "\(ConstantHindiText.bhaag) \(index + 1)"
            }else{
                self.labelTopicName.text = topic.topic
            }
        }else{
            self.labelTopicName.setfont(font: 15, fontFamily: futuraFont)
            self.labelTopicName.text = topic.topic
        }
    }
}

extension ChapterExerciseTableViewCell:DataSourcesCollectionDelegate{
    func collectionView(_ collectionView: UICollectionView, data: Any?, selectedIndex: IndexPath) {
        guard let collectionData = data as? ExerciseModel else{return}
        self.exerciseCallBackBack?(collectionData, selectedIndex.item)
    }
}

