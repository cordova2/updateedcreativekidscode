//
//  SideMenuTableViewCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 18/03/21.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var imageViewSideContent: UIImageView!
    @IBOutlet weak var labelSideContent: UILabel!
    
    //MARK: - cell Config Method
    func setConfig(object:SideMenuDataModel,index:Int){
        self.labelSideContent.setfont(font: 15, fontFamily: futuraFont)
        if index == 1{
            if kSharedUserDefaults.isUserLoggedIn(){
                if !kUserData.subscriptionData.isEmpty{
                    self.labelSideContent.text = (object.title ?? "") + " (\(kUserData.subscriptionData.count))"
                    self.imageViewSideContent.image = object.sideImage
                }else{
                    self.labelSideContent.text = object.title
                    self.imageViewSideContent.image = object.sideImage
                }
            }else{
                self.labelSideContent.text = object.title
                self.imageViewSideContent.image = object.sideImage
            }
        }else if index == 3{
            if kSharedUserDefaults.isUserLoggedIn(){
                self.labelSideContent.text = "Logout"
                self.imageViewSideContent.image = object.sideImage
            }else{
                self.labelSideContent.text = object.title
                self.imageViewSideContent.image = object.sideImage
            }
        }else{
            self.labelSideContent.text = object.title
            self.imageViewSideContent.image = object.sideImage
        }
    }
}

