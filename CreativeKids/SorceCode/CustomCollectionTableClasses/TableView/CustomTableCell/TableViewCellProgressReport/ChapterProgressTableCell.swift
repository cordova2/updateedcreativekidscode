//
//  ChapterProgress.swift
//  CreativeKids
//
//  Created by Rakesh jha on 07/05/21.
//

import UIKit

class ChapterProgressTableCell: UITableViewCell {

    @IBOutlet weak var labelSubjectName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func configCell(object: Any){
        if let kData = object as? SubjectModel{
            labelSubjectName.text = kData.subjectName
        }
    }
}
