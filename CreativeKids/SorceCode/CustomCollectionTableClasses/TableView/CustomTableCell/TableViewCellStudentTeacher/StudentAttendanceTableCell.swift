//
//  StudentAttendanceTableCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 05/03/22.
//

import UIKit

class StudentAttendanceTableCell: UITableViewCell {
    
    @IBOutlet weak var viewBackground: UIView! {
        didSet {
            viewBackground.drawShadowOnCell()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
