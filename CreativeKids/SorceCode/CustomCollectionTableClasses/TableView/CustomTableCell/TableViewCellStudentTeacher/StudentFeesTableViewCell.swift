//
//  StudentFeesTableViewCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 09/03/22.
//

import UIKit

class StudentFeesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var labelTotalFee: UILabel!
    @IBOutlet weak var labelTutionFee: UILabel!
    @IBOutlet weak var labelTransportationFee: UILabel!
    @IBOutlet weak var labelActivityFee: UILabel!
    @IBOutlet weak var labelAdmissionFees: UILabel!
    @IBOutlet weak var labelAnnualCharges: UILabel!
    @IBOutlet weak var labelLateFee: UILabel!
    @IBOutlet weak var labelDueAmount: UILabel!
    @IBOutlet weak var imageViewDropDown: UIImageView!
    @IBOutlet weak var labelCautionFee: UILabel!
    @IBOutlet weak var labelProspectusFee: UILabel!
    
    
    @IBOutlet weak var labelReceivedBy: UILabel!
    @IBOutlet weak var labelMonth: UILabel!
    @IBOutlet weak var labelFeesPaidDate: UILabel!
    @IBOutlet weak var labelPaymentMode: UILabel!
    @IBOutlet weak var labelBalanceAmount: UILabel!
    @IBOutlet weak var viewBackground: UIView! {
        didSet {
            viewBackground.drawShadowOnCell()
        }
    }
    @IBOutlet weak var stackTotalFee: UIStackView!
    @IBOutlet weak var stackTotalFeeInside: UIStackView!
    
    var tableViewReload: UITableView!
    var feeModel: FeesModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configCell(object:Any, feeTableView: UITableView){
        self.tableViewReload = feeTableView
        guard let kdata = object as? FeesModel else{return}
        self.feeModel = kdata
        labelTotalFee.text = kdata.amountToPay.isEmpty ? "__" : kdata.amountToPay
        labelTutionFee.text = kdata.tutionFee.isEmpty ? "__" : kdata.tutionFee
        labelTransportationFee.text = kdata.transportChagres.isEmpty ? "__" : kdata.transportChagres
        labelActivityFee.text = kdata.activityChagres.isEmpty ? "__" : kdata.activityChagres
        labelAdmissionFees.text = kdata.addmissionFee.isEmpty ? "__" : kdata.addmissionFee
        labelAnnualCharges.text = kdata.annualChagres.isEmpty ? "__" : kdata.annualChagres
        labelCautionFee.text = kdata.cautionAmount.isEmpty ? "__" : kdata.cautionAmount
        labelProspectusFee.text = kdata.prospectusAmount.isEmpty ? "__" : kdata.prospectusAmount
        labelLateFee.text = kdata.fineAmount.isEmpty ? "__" : kdata.fineAmount
        labelDueAmount.text = kdata.dueAmount.isEmpty ? "__" : kdata.dueAmount
        labelMonth.text = kdata.month.isEmpty ? "__" : kdata.month
        labelReceivedBy.text = kdata.receivedBy.isEmpty ? "__" : kdata.receivedBy
        labelFeesPaidDate.text = String.convertDateString(dateString: kdata.paidDate, fromFormat: "yyyy-MM-dd'T'HH:mm:ss", toFormat: "dd-MM-yyyy")
        labelPaymentMode.text = kdata.paymentMode.isEmpty ? "__" : kdata.paymentMode
        self.stackTotalFeeInside.isHidden = !kdata.isSelected
        self.imageViewDropDown.image = UIImage(systemName: kdata.isSelected ? "arrowtriangle.up.circle": "arrowtriangle.down.circle")
    }
    @IBAction func buttonTotalFeeDrop(_ sender: UIButton) {
        sender.isSelected = !self.feeModel.isSelected
        self.feeModel.isSelected = !self.feeModel.isSelected
        self.tableViewReload.reloadData()
    }
}
