//
//  HomeWorkAccDateTableViewCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 15/09/22.
//

import UIKit

class HomeWorkAccDateTableViewCell: UITableViewCell {

    enum Status:String{
        case Unchecked
        case Checked
    }
    var addHomeworkCallback: (()->())?
    var checkedCallback: (()->())?
    @IBOutlet weak var viewBackground: UIView!{
    didSet {
        viewBackground.drawShadowOnCell()
    }
    }
    @IBOutlet weak var labelClass: UILabel!
    @IBOutlet weak var labelSubject: UILabel!
    @IBOutlet weak var buttonAssignedWork: UIButton!
    @IBOutlet weak var labelStatus: UILabel!
    @IBOutlet weak var labelPeriodNumber: UILabel!
    @IBOutlet weak var buttonOnCell: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configCell(data:Any){
        guard let kData = data as? HomeWorkModel else{return}
        labelPeriodNumber.attributedText = self.makeAttrubutesTextforDate(firstString: "Period: ", lastString: "\(kData.periodNo)")
        labelClass.attributedText = self.makeAttrubutesTextforDate(firstString: "Class: ", lastString: "\(kData.className) \(kData.section)")
        labelSubject.attributedText = self.makeAttrubutesTextforDate(firstString: "Subject: ", lastString: kData.subjectName)
        if kData.canAssignWork{
            buttonAssignedWork.isHidden = false
            buttonAssignedWork.setImage(UIImage(named: kData.isWorkedAssigned ? "Update_2" : "Add homework"), for: .normal)
            buttonAssignedWork.tag = kData.isWorkedAssigned ? 1 : 0
            if kData.isWorkedAssigned{
                self.buttonOnCell.isUserInteractionEnabled = true
                labelStatus.attributedText = self.makeAttrubutesTextforStatus(status: kData.isWorkedChecked ? .Checked : .Unchecked)
            }else{
                self.buttonOnCell.isUserInteractionEnabled = false
                labelStatus.text = "  "
            }
        }else{
            labelStatus.attributedText = self.makeAttrubutesTextforStatus(status: kData.isWorkedChecked ? .Checked : .Unchecked)
            self.buttonOnCell.isUserInteractionEnabled = true
            buttonAssignedWork.isHidden = true
        }
    }

    @IBAction func buttonAssignedWorkTapped(_ sender: UIButton) {
        self.addHomeworkCallback?()
    }
    
    @IBAction func buttonOnCellTapped(_ sender: UIButton) {
        self.checkedCallback?()
    }
    
    
    func makeAttrubutesTextforDate(firstString:String, lastString:String)->NSAttributedString{
        let firstTextAttributed = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16, weight: .regular)]
        let secondTextAttributed = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18, weight: .semibold)]
        let firstAttributedString = NSAttributedString(string: firstString, attributes: firstTextAttributed)
        let secondAttributedString = NSAttributedString(string: lastString, attributes: secondTextAttributed)
        return firstAttributedString + secondAttributedString
    }
    
    func makeAttrubutesTextforStatus(status:Status = .Unchecked)->NSAttributedString{
        let firstTextAttributed = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16, weight: .regular),NSAttributedString.Key.foregroundColor: UIColor.black]
        let secondTextAttributed = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18, weight: .semibold), NSAttributedString.Key.foregroundColor: status == .Unchecked ? UIColor.red : UIColor.systemGreen]
        let firstAttributedString = NSAttributedString(string: "Status: ", attributes: firstTextAttributed)
        let secondAttributedString = NSAttributedString(string: status.rawValue, attributes: secondTextAttributed)
        return firstAttributedString + secondAttributedString
    }
    
}
