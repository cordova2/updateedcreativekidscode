//
//  DateHomeworkTableViewCell.swift
//  CreativeKids
//
//  Created by creativekids solutions on 01/09/22.
//

import UIKit

class DateHomeworkTableViewCell: UITableViewCell {
    enum Status:String{
        case Pending
        case Checked
    }
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelStatus: UILabel!
    @IBOutlet weak var viewBackground: UIView! {
        didSet {
            viewBackground.drawShadowOnCell()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func configCell(data:Any){
        guard let datesObject = data as? HomeWorkDate else{return}
        labelDate.attributedText = self.makeAttrubutesTextforDate(date: datesObject.date)
        labelStatus.attributedText = self.makeAttrubutesTextforStatus(status: DateHomeworkTableViewCell.Status(rawValue: datesObject.status) ?? .Pending)
    }
    
    func makeAttrubutesTextforDate(date:String = "24/43/54")->NSAttributedString{
        let firstTextAttributed = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16, weight: .regular)]
        let secondTextAttributed = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18, weight: .semibold)]
        let firstAttributedString = NSAttributedString(string: "Date: ", attributes: firstTextAttributed)
        let secondAttributedString = NSAttributedString(string: date, attributes: secondTextAttributed)
        return firstAttributedString + secondAttributedString
    }
    
    func makeAttrubutesTextforStatus(status:Status = .Checked)->NSAttributedString{
        let firstTextAttributed = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16, weight: .regular),NSAttributedString.Key.foregroundColor: UIColor.black]
        let secondTextAttributed = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18, weight: .semibold), NSAttributedString.Key.foregroundColor: status == .Pending ? UIColor.red : UIColor.systemGreen]
        let firstAttributedString = NSAttributedString(string: "Status: ", attributes: firstTextAttributed)
        let secondAttributedString = NSAttributedString(string: status.rawValue, attributes: secondTextAttributed)
        return firstAttributedString + secondAttributedString
    }
}
