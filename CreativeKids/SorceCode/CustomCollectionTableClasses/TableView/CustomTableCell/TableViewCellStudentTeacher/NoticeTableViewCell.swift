//
//  NoticeTableViewCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 08/07/22.
//

import UIKit

class NoticeTableViewCell: UITableViewCell {

    @IBOutlet weak var labelNameDesignation: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelNoticeMessage: UILabel!
    @IBOutlet weak var viewMain: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func cellConfig(data:Any){
        viewMain.drawShadowOnCell()
        guard let kData = data as? NoticeModel else{return}
        labelDate.text = String.convertDateString(dateString: kData.date, fromFormat: "yyyy-MM-dd'T'hh:mm:ss", toFormat: "dd/MM/yyyy")
        labelNameDesignation.attributedText = createAttributedText(firstText: kData.name, secondText: " (\(kData.designation))")
        labelNoticeMessage.text = kData.message
    }
}
 
