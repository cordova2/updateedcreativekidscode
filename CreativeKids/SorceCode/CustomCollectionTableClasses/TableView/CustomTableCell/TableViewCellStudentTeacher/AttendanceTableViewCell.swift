//
//  AttendanceTableViewCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 15/02/22.
//

import UIKit

protocol ReloadAnyViewDelegate{
    func reload()
}

class AttendanceTableViewCell: UITableViewCell {
    
    //https://downloadly.net/2021/18/42012/05/ios-swift-chat-application-like-whatsapp-viber-telegram/21/?#/42012-udemy-102202020016.html
    
    @IBOutlet weak var viewBackground: UIView! {
        didSet {
            viewBackground.drawShadowOnCell()
        }
    }
    @IBOutlet weak var labelStudentName: UILabel!
    @IBOutlet var attendenceTypeButton: [UIButton]!
    var studentDetail:StudentAttendance?
    var reloadDelegate:ReloadAnyViewDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        initialSetUp()
    }
    
    func cellConfigure(data:Any){
        guard let kdata = data as? StudentAttendance else{return}
        studentDetail = kdata
        labelStudentName.text = kdata.studentName
        manageInitialView(data:kdata)

    }
    func manageInitialView(data:StudentAttendance){
        if data.isOnLeave {
            changeColorButton(sender: attendenceTypeButton[2])
        }else{
            if !(data.attendanceType.isEmpty){
                if data.isPresent{
                    changeColorButton(sender: attendenceTypeButton[0])
                }else{
                    changeColorButton(sender: attendenceTypeButton[1])
                }
            }
        }
        
    }
    
    @IBAction func attendanceTypeButtonTapped(_ sender: UIButton) {
        attendenceTypeButton.forEach {$0.isSelected = false}
        changeColorButton(sender: sender)
        reloadDelegate?.reload()
    }
    func changeColorButton(sender: UIButton) {
        switch sender.tag {
        case 0:
            sender.isSelected = true
            self.attendenceTypeButton[sender.tag].backgroundColor = UIColor.systemGreen
            self.attendenceTypeButton[sender.tag].titleLabel?.textColor = UIColor.white
            self.attendenceTypeButton[1].backgroundColor = UIColor.white
            self.attendenceTypeButton[2].backgroundColor = UIColor.white
            self.attendenceTypeButton[1].titleLabel?.textColor = UIColor.systemRed
            self.attendenceTypeButton[2].titleLabel?.textColor = UIColor.systemIndigo
            studentDetail?.attendanceType = "Need to reload Cell"
            studentDetail?.isPresent = true
            studentDetail?.isOnLeave = false
        case 1:
            sender.isSelected = true
            self.attendenceTypeButton[sender.tag].backgroundColor = UIColor.systemRed
            self.attendenceTypeButton[sender.tag].titleLabel?.textColor = UIColor.white
            self.attendenceTypeButton[0].backgroundColor = UIColor.white
            self.attendenceTypeButton[2].backgroundColor = UIColor.white
            self.attendenceTypeButton[0].titleLabel?.textColor = UIColor.systemGreen
            self.attendenceTypeButton[2].titleLabel?.textColor = UIColor.systemIndigo
            studentDetail?.attendanceType = "Need to reload Cell"
            studentDetail?.isPresent = false
            studentDetail?.isOnLeave = false
        case 2:
//            sender.isSelected = true
            self.attendenceTypeButton[sender.tag].backgroundColor = UIColor.systemIndigo
            self.attendenceTypeButton[sender.tag].titleLabel?.textColor = UIColor.white
            self.attendenceTypeButton[1].backgroundColor = UIColor.white
            self.attendenceTypeButton[0].backgroundColor = UIColor.white
            self.attendenceTypeButton[1].titleLabel?.textColor = UIColor.systemRed
            self.attendenceTypeButton[0].titleLabel?.textColor = UIColor.systemGreen
            studentDetail?.attendanceType = "Need to reload Cell"
//          studentDetail?.isPresent = false
//          studentDetail?.isOnLeave = true
        default:
            return
        }
        studentDetail?.isSelected = true
    }
    
    func initialSetUp(){
        self.attendenceTypeButton.forEach{
            $0.backgroundColor = UIColor.white
            $0.isSelected = false
        }
        self.attendenceTypeButton[0].titleLabel?.textColor = UIColor.systemGreen
        self.attendenceTypeButton[1].titleLabel?.textColor = UIColor.systemRed
        self.attendenceTypeButton[2].titleLabel?.textColor = UIColor.systemIndigo
    }
}
