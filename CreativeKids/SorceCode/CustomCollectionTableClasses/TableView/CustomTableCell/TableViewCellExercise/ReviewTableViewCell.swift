//
//  ReviewTableViewCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 27/03/21.
//

import UIKit

class ReviewTableViewCell: UITableViewCell {
    
    //MARK: - Outlets
    @IBOutlet weak var labelAns: UILabel!
    @IBOutlet weak var labelcorrectAns: UILabel!
    @IBOutlet weak var labelQuestion: UILabel!
    @IBOutlet weak var wrongView: UIView!
    @IBOutlet weak var ansStack: UIStackView!
    @IBOutlet weak var imageWrong: UIImageView!
    
    var subjectSelected:SubjectModel?
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    //MARK: - Configure Cell
    func configTableCell(questionCheck: VSAQQuestionModel?,index:IndexPath) {
        
        let wrongAnswer = questionCheck?.userSelectedOption
        if wrongAnswer == questionCheck?.rightAns {
            self.wrongView.isHidden = true
//            self.ansStack.layoutIfNeeded()
        } else {
            self.wrongView.isHidden = false
        }
        
        if isHindiSubject(subejct:subjectSelected?.subjectName ?? ""){
            self.labelQuestion.setfont(font: 18, fontFamily: chanakyaFont)
            self.labelcorrectAns.setfont(font: 18, fontFamily: chanakyaFont)
            self.labelAns.setfont(font: 18, fontFamily: chanakyaFont)
//            let myQuestionNumber = "\(index.row + 1)"
//            let myNumberAttribute = [ NSAttributedString.Key.font: UIFont(name: futuraFont, size: 13.0)!]
//            let myAttrNumberString = NSAttributedString(string: myQuestionNumber, attributes: myNumberAttribute)
//            let removedNumber = removeNumberFromString(stringWithNumber: questionCheck?.Questions ?? "")
//            let myAttribute = [ NSAttributedString.Key.font: UIFont(name: chanakyaFont, size: 18.0)!]
//            let myAttrQuestionString = NSAttributedString(string: removedNumber, attributes: myAttribute)
//            self.labelQuestion.attributedText =  myAttrNumberString + myAttrQuestionString
            self.labelQuestion.attributedText =  makeNewNumericStringFor3And7(StringWithNumber:questionCheck?.Questions ?? "")
        } else {
            self.labelQuestion.font = UIFont.systemFont(ofSize: 17, weight: .heavy)
            self.labelcorrectAns.font =  UIFont.systemFont(ofSize: 16, weight: .bold)
            self.labelAns.font =  UIFont.systemFont(ofSize: 16, weight: .bold)
            self.labelQuestion.setAttributedText(text: questionCheck?.Questions?.replacingOccurrences(of: "<br>", with: "\n") ?? "", colorText: .white, font: 15)
        }
        if isHindiSubject(subejct:subjectSelected?.subjectName ?? ""){
            switch questionCheck?.rightAns {
                
            case 1:
                self.labelcorrectAns.setAttributedText(text: "mRRkj & \(questionCheck?.option_A ?? "")", colorText: .white, font: 16, fontFamily: chanakyaFont)
            case 2:
                self.labelcorrectAns.setAttributedText(text: "mRRkj & \(questionCheck?.option_B ?? "")", colorText: .white, font: 16, fontFamily: chanakyaFont)
            case 3:
                self.labelcorrectAns.setAttributedText(text: "mRRkj & \(questionCheck?.option_C ?? "")", colorText: .white, font: 16, fontFamily: chanakyaFont)
            case 4:
                self.labelcorrectAns.setAttributedText(text: "mRRkj & \(questionCheck?.option_D ?? "")", colorText: .white, font: 16, fontFamily: chanakyaFont)
            default:
                break
            }
        }else{
            switch questionCheck?.rightAns {
                
            case 1:
                self.labelcorrectAns.setAttributedText(text: "Ans: \(questionCheck?.option_A ?? "")", colorText: .white, font: 13)
            case 2:
                self.labelcorrectAns.setAttributedText(text: "Ans: \(questionCheck?.option_B ?? "")", colorText: .white, font: 13)
            case 3:
                self.labelcorrectAns.setAttributedText(text: "Ans: \(questionCheck?.option_C ?? "")", colorText: .white, font: 13)
            case 4:
                self.labelcorrectAns.setAttributedText(text: "Ans: \(questionCheck?.option_D ?? "")", colorText: .white, font: 13)
            default:
                break
            }
        }
        if isHindiSubject(subejct: subjectSelected?.subjectName ?? "") {
            switch wrongAnswer {
            case 1:
                self.labelAns.setAttributedText(text: "\(questionCheck?.option_A ?? "")", colorText: .white, font: 16, fontFamily: chanakyaFont)
            case 2:
                self.labelAns.setAttributedText(text: "\(questionCheck?.option_B ?? "")", colorText: .white, font: 16, fontFamily: chanakyaFont)
            case 3:
                self.labelAns.setAttributedText(text: "\(questionCheck?.option_C ?? "")", colorText: .white, font: 16, fontFamily: chanakyaFont)
            case 4:
                self.labelAns.setAttributedText(text: "\(questionCheck?.option_D ?? "")", colorText: .white, font: 16, fontFamily: chanakyaFont)
            default:
                break
            }
        } else {
            switch wrongAnswer {
            case 1:
                self.labelAns.setAttributedText(text: "\(questionCheck?.option_A ?? "")", colorText: .white, font: 13)
            case 2:
                self.labelAns.setAttributedText(text: "\(questionCheck?.option_B ?? "")", colorText: .white, font: 13)
            case 3:
                self.labelAns.setAttributedText(text: "\(questionCheck?.option_C ?? "")", colorText: .white, font: 13)
            case 4:
                self.labelAns.setAttributedText(text: "\(questionCheck?.option_D ?? "")", colorText: .white, font: 13)
            default:
                break
            }
        }
    }
}

