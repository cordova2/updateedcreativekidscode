//
//  VideoTableViewCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 31/03/21.
//

import UIKit

class VideoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var videoImageView: UIImageView!
    @IBOutlet weak var labelVideoName: UILabel!
    @IBOutlet weak var ViewWholeCell: UIView!
    @IBOutlet weak var labelBhaagNumber: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configCell(data: Any,index: Int, language:String){
        if let cellData = data as? TopicModel{
            DispatchQueue.main.async {
                if isHindiSubject(subejct:language){
                    self.labelVideoName.setfont(font: 15, fontFamily: chanakyaFont)
                    if isContainNumber(checkString: cellData.topic ?? ""){
                        self.labelVideoName.text = ConstantHindiText.bhaag
                        self.labelBhaagNumber.text = "- \(index + 1)"
                    }else{
                        self.labelVideoName.text = cellData.topic
                    }
                }else{
                    self.labelVideoName.setfont(font: 15, fontFamily: futuraFont)
                    self.labelVideoName.text = cellData.topic
                }
                // self.videoImageView.sd_setImage(with: URL(string: cellData.videoThumb ?? ""), placeholderImage: #imageLiteral(resourceName: "videoPlaceHolder"), options: .continueInBackground, completed: nil)
                self.videoImageView.downloadImageFromURL(urlString: cellData.videoThumb ?? "")
                self.ViewWholeCell.isHidden = cellData.isSelected ? false : true
                self.ViewWholeCell.alpha = cellData.isSelected ? 0.8 : 1.0
                self.isUserInteractionEnabled = cellData.isSelected ? false : true
            }
        }else{
            let cellData = data as? DemoModel
            self.labelVideoName.setfont(font: 15, fontFamily: futuraFont)
            self.videoImageView.sd_setImage(with: URL(string: cellData?.videoThumb ?? ""), placeholderImage: #imageLiteral(resourceName: "videoPlaceHolder"), options: .continueInBackground, completed: nil)
            self.labelVideoName.text = cellData?.name ?? ""
            self.ViewWholeCell.isHidden = false
            self.ViewWholeCell.alpha = 0.8
            self.isUserInteractionEnabled = false
        }
    }
}



