//
//  ChapterTableViewCell.swift
//  CreativeKids
//
//  Created by Creative Kids on 24/03/21.
// अध्याय

import UIKit

class ChapterTableViewCell: UITableViewCell {
    enum CellType{
        case normal,assign
    }
    @IBOutlet weak var labelChapterName: UILabel!
    @IBOutlet weak var labelchapterNumber: UILabel!
    @IBOutlet weak var labelModuleExeLbt: UILabel!
    @IBOutlet weak var cellBgImage: UIImageView!
    @IBOutlet weak var ViewWholeCell: UIView!
    @IBOutlet weak var labelModuleCount: UILabel!
    @IBOutlet weak var labelExerciseCount: UILabel!
    @IBOutlet weak var labelLtPcount: UILabel!
    @IBOutlet weak var labelLTP: UILabel!
    @IBOutlet weak var labelExercise: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    
    
    @IBOutlet weak var widthModuleCons: NSLayoutConstraint!
    @IBOutlet weak var widthExerciseCons: NSLayoutConstraint!
    @IBOutlet weak var widthModuleCount: NSLayoutConstraint!
    @IBOutlet weak var widthExerciseCount: NSLayoutConstraint!
    @IBOutlet weak var imageViewChapterRead: UIImageView!
    
    @IBOutlet weak var buttonAssignChapter: UIButton!
    var cellType:CellType = .normal
    var showPopUpCallBack:(()->Void)?
    var assignChapterCallBack:(()->Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configCell(data: Any,index:Int){
        manageAssignButton(type:cellType)
        guard let kdata = data as? TopicSearchModel else{return}
        ckeckSetData(object:kdata,index: index + 1)
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        labelModuleExeLbt.text = ""
        labelModuleCount.text = ""
        labelExerciseCount.text = ""
        labelLtPcount.text = ""
        labelLTP.text = ""
        labelExercise.text = ""
        manageAssignButton(type:cellType)
    }
    @IBAction func buttonShowPopScreen(_ sender: UIButton) {
        showPopUpCallBack?()
    }
    @IBAction func buttonAssignedChapter(_ sender: UIButton) {
        assignChapterCallBack?()
    }
    // MARK: Method for ManageAssign Button
    
    func manageAssignButton(type:CellType){
        switch type {
        case .normal:
            buttonAssignChapter.isHidden = true
            buttonAssignChapter.isUserInteractionEnabled = false
            buttonAssignChapter.sendSubviewToBack(ViewWholeCell)
        case .assign:
            buttonAssignChapter.isHidden = false
            buttonAssignChapter.isUserInteractionEnabled = true
            buttonAssignChapter.bringSubviewToFront(ViewWholeCell)
        }
    }
    func ckeckSetData(object:TopicSearchModel,index:Int){
        self.ViewWholeCell.isHidden = true
        self.imageViewChapterRead.image = object.isChapterRead ? #imageLiteral(resourceName: "read_2"):#imageLiteral(resourceName: "read_1")
        cellBgImage.image = GetSubjectColor(subject: object.subjectName ?? "").chapterImage
        if isHindiSubject(subejct:object.subjectName ?? ""){
            forTextHindi(object:object,index:index)
        }else{
            forTextEnglish(object:object,index:index)
        }
        if object.chapterEnableCount != 0{
            if let enableCount = object.chapterEnableCount{
                self.ViewWholeCell.isHidden = index > enableCount ? false : true
                self.ViewWholeCell.alpha = index > enableCount ? 0.8 : 1.0
//                self.isUserInteractionEnabled = index > enableCount ? false : true
                
            }
        }
    }
    
    //MARK:- FOR HINDI TEXT
    func forTextHindi(object:TopicSearchModel,index:Int){
        self.labelChapterName.setfont(font: 20, fontFamily: chanakyaFont)
        //        self.labelModuleExeLbt.font = UIFont(name: "WalkmanChanakya901Normal", size: 13)
        labelchapterNumber.text = "\(index)."
        // self.labelChapterName.text = "\(object.chapterName ?? "")"
        self.labelChapterName.attributedText = makeNewNumericStringFor3And7(StringWithNumber:object.chapterName ?? "")
        if String.getString(object.topicCount) != "0"{
            widthModuleCons.constant = 60
            widthModuleCount.constant = 20
            labelModuleCount.text = "\(String.getString(object.topicCount))"
            labelModuleExeLbt.text = "Modules"
        }else{
            widthModuleCons.constant = 0
            widthModuleCount.constant = 0
        }
        if String.getString(object.exerciseCount) != "0"{
            widthExerciseCons.constant = 60
            widthExerciseCount.constant = 30
            labelExerciseCount.text = " | \(String.getString(object.exerciseCount))"
            labelExercise.text = " Exercises"
        }else{
            widthExerciseCons.constant = 0
            widthExerciseCount.constant = 0
        }
        if String.getString(object.lptCount) != "0"{
            labelLtPcount.text = " | \(String.getString(object.lptCount))"
            labelLTP.text = " LTP"
            
        }
        stackView.layoutIfNeeded()
        //        if moduleExeText == ""{
        //            labelModuleExeLbt.isHidden = true
        //        }
        //        labelModuleExeLbt.text = moduleExeText
    }
    
    //MARK:- FOR ENGLISH TEXT
    func forTextEnglish(object:TopicSearchModel,index:Int){
        //        var moduleExeText = ""
        //        self.labelChapterName.font = UIFont(name: "Roboto-Regular", size: 17)
        ////        self.labelModuleExeLbt.font = UIFont(name: "Roboto-Medium", size: 13)
        //        labelchapterNumber.text = "\(index)."
        //        self.labelChapterName.text = "\(object.chapterName ?? "")"
        //        if String.getString(object.topicCount) != "0"{
        //            moduleExeText += "\(String.getString(object.topicCount)) Modules"
        //        }
        //        if String.getString(object.exerciseCount) != "0"{
        //            moduleExeText += "| \(String.getString(object.exerciseCount)) Exercises"
        //        }
        //        if String.getString(object.lptCount) != "0"{
        //            moduleExeText += "| \(String.getString(object.lptCount)) LTP"
        //        }
        //        if moduleExeText == ""{
        //            labelModuleExeLbt.isHidden = true
        //        }
        //        labelModuleExeLbt.text = moduleExeText
        //        stackView.layoutIfNeeded()
        
        
        self.labelChapterName.font = UIFont(name: "Roboto-Regular", size: 16)
        //        self.labelModuleExeLbt.font = UIFont(name: "WalkmanChanakya901Normal", size: 13)
        labelchapterNumber.text = "\(index)."
        self.labelChapterName.text = "\(object.chapterName ?? "")"
        if String.getString(object.topicCount) != "0"{
            widthModuleCons.constant = 60
            widthModuleCount.constant = 20
            labelModuleCount.text = "\(String.getString(object.topicCount))"
            labelModuleExeLbt.text = "Modules"
        }else{
            widthModuleCons.constant = 0
            widthModuleCount.constant = 0
        }
        if String.getString(object.exerciseCount) != "0"{
            widthExerciseCons.constant = 60
            widthExerciseCount.constant = 30
            labelExerciseCount.text = " | \(String.getString(object.exerciseCount))"
            labelExercise.text = " Exercises"
        }else{
            widthExerciseCons.constant = 0
            widthExerciseCount.constant = 0
        }
        if String.getString(object.lptCount) != "0"{
            labelLtPcount.text = " | \(String.getString(object.lptCount))"
            labelLTP.text = " LTP"
            
        }
        stackView.layoutIfNeeded()
    }
    
}
