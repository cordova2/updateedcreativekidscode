//
//  TableViewChatContact.swift
//  CreativeKids
//
//  Created by Creative Kids on 07/03/22.
//


import Foundation
import UIKit

class TableViewChatContact:UITableView,UITableViewDelegate {
    var tableCellId:String?
    var item = [Any]()
    var dataPassDelegate:DataSourcesDelegate?
    var customDataSourecs:CustomTableViewDataSources<TableViewCellChatContact,Any>!
    override func awakeFromNib() {
        super.awakeFromNib()
        initialSet()
    }
    
    func initialSet(){
        self.register(UINib(nibName: "TableViewCellChatContact", bundle: nil), forCellReuseIdentifier: "TableViewCellChatContact")
        DispatchQueue.main.async {
            CommonUtils.showHud(show: true)
            ChatHalper.shared.receiveResentUsers { users in
                    self.item = users ?? []
                    self.customDataSourecs = .init(cellIdentifier: "TableViewCellChatContact", item: self.item,message:"No Chats Available", completion: { (cell, data, index) in
                        self.separatorStyle = .singleLine
                        cell.configCell(object: data)
                    })
                    DispatchQueue.main.async {
                        self.dataSource = self.customDataSourecs
                        self.delegate = self
                        self.reloadData()
                        CommonUtils.showHud(show: false)
                    }
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = self.cellForRow(at: indexPath) as? TableViewCellChatContact
        cell?.viewUnReadCount.isHidden = true
        self.dataPassDelegate?.tableView(self, selectedIndex: indexPath.row, data: self.item[indexPath.row])
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
extension TableViewChatContact:TableDataDelegate {
    func getData(cell:String,data:[Any]){
        self.tableCellId = cell
        self.item = data
        initialSet()
    }
}
