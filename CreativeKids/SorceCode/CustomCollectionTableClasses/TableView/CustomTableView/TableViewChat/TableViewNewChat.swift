//
//  TableViewNewChat.swift
//  Chat Demo
//
//  Created by Creative Kids on 24/03/22.
//

import Foundation
import UIKit

class TableViewNewChat:UITableView,UITableViewDelegate {
    var tableCellId:String?
    var item = [Any]()
    var dataPassDelegate:DataSourcesDelegate?
    var customDataSourecs:CustomTableViewDataSources<NewChatTableViewCell,Any>!
    override func awakeFromNib() {
        super.awakeFromNib()
        initialSet()
    }
    
    func initialSet(){
        self.register(UINib(nibName: "NewChatTableViewCell", bundle: nil), forCellReuseIdentifier: "NewChatTableViewCell")
        DispatchQueue.main.async {
            ChatHalper.shared.retrivedAllUser { [weak self] users in
                self?.item = users
                self?.customDataSourecs = .init(cellIdentifier: "NewChatTableViewCell", item: self?.item ?? [],message:"User not found", completion: { (cell, data, index) in
                    self?.separatorStyle = .singleLine
                    cell.configCell(object: data)
                    cell.selectCallBack = {
                        self?.dataPassDelegate?.tableView(self!, selectedIndex: index, data: data)
                    }
                })
                DispatchQueue.main.async {
                    self?.dataSource = self?.customDataSourecs
                    self?.delegate = self
                    self?.reloadData()
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
extension TableViewNewChat:TableDataDelegate {
    func filterDataUsingText(searchBy: String) {
        let searedText = String.getstring(searchBy)
        if searedText.isEmpty{
            self.customDataSourecs.item = self.item
            self.reloadData()
        }else{
            let filteredItem = self.item.filter{String.getstring(($0 as? UsersState)?.name).lowercased().contains(searchBy)}
            self.customDataSourecs.item = filteredItem
            self.reloadData()
        }
    }
}
