//
//  PeriodListTableView.swift
//  CreativeKids
//
//  Created by creativekids solutions on 02/09/22.
//

import UIKit

class PeriodListTableView: UITableView, UITableViewDelegate {

    var item = [Any]()
    var dataPassDelegate:DataSourcesDelegate?
    var customDataSourecs:CustomTableViewDataSources<HomeWorkAccDateTableViewCell,Any>!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.initialSetup()
    }
    
    func initialSetup() {
        self.register(UINib(nibName: TableCellIdentifier.homeWorkAccPeriod, bundle: nil), forCellReuseIdentifier: TableCellIdentifier.homeWorkAccPeriod)
        self.customDataSourecs = .init(cellIdentifier: TableCellIdentifier.homeWorkAccPeriod, item: self.item, message: "No Homework", completion: { cell, data, index in
            cell.configCell(data:data)
            cell.addHomeworkCallback = {
                self.dataPassDelegate?.tableViewCallBack(self, selectedIndex: index, data: data, cell: cell)
            }
            cell.checkedCallback = {
                self.dataPassDelegate?.tableView(self, selectedIndex: index, data: data)
            }
        })
        DispatchQueue.main.async {
            self.delegate = self
            self.dataSource = self.customDataSourecs
            self.reloadData()
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
extension PeriodListTableView: TableDataDelegate {
    func getData(cell:String,data:[Any]) {
        self.item = data
        self.initialSetup()
    }
}
