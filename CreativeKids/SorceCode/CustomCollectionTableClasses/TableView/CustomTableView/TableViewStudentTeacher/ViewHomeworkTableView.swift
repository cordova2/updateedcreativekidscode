//
//  ViewHomeworkTableView.swift
//  CreativeKids
//
//  Created by Creative Kids on 19/05/22.
//

import UIKit

class ViewHomeworkTableView: UITableView, UITableViewDelegate {
    var item = [Any]()
    var dataPassDelegate:DataSourcesDelegate?
    var customDataSourecs:CustomTableViewDataSources<ViewHomeworkTableViewCell,Any>!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialSetup()
    }
    
    func initialSetup() {
        self.customDataSourecs = .init(cellIdentifier: TableCellIdentifier.viewHomework, item: self.item, completion: { cell, data, index in
            
        })
        DispatchQueue.main.async {
            self.delegate = self
            self.dataSource = self.customDataSourecs
            self.reloadData()
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dataPassDelegate?.tableView(self, selectedIndex: indexPath.row, data: self.item[indexPath.row])
    }
    
}

extension ViewHomeworkTableView: TableDataDelegate {
    func getData(cell: String, data: [Any]) {
        self.item = data
        self.initialSetup()
    }
}
