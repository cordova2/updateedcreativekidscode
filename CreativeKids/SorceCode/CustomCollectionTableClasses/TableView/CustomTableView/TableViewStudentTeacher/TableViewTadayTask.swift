//
//  TableViewTadayTask.swift
//  CreativeKids
//
//  Created by Creative Kids on 22/02/22.
//

import Foundation
import UIKit

class TeacherTodayTaskTableView:UITableView,UITableViewDelegate {
    var item = [Any]()
    var dataPassDelegate:DataSourcesDelegate?
    var customDataSourecs:CustomTableViewDataSources<TableViewCellTodayClasses,Any>!
    override func awakeFromNib() {
        super.awakeFromNib()
        initialSet()
    }
    
    func initialSet(){
        self.register(UINib(nibName: TableCellIdentifier.teacherTodayTaskCell, bundle: nil), forCellReuseIdentifier: TableCellIdentifier.teacherTodayTaskCell)
        self.customDataSourecs = .init(cellIdentifier: TableCellIdentifier.teacherTodayTaskCell, item: self.item, completion: { (cell, data, index) in
            self.separatorStyle = .none
            cell.configCell(object: data)
        })
        DispatchQueue.main.async {
            self.dataSource = self.customDataSourecs
            self.delegate = self
            self.reloadData()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dataPassDelegate?.tableView(self, selectedIndex: indexPath.row, data: self.item[indexPath.row])
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
extension TeacherTodayTaskTableView:TableDataDelegate {
    func getData(cell:String,data:[Any]){
        self.item = data
        initialSet()
    }
}
