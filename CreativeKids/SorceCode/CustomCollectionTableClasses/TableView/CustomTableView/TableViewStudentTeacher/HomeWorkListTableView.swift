//
//  HomeWorkListTableView.swift
//  CreativeKids
//
//  Created by Creative Kids on 22/02/22.
//

import UIKit

class HomeWorkListTableView: UITableView, UITableViewDelegate {
    
    var navigate: UINavigationController?
    var item = [Any]()
    var dataPassDelegate:DataSourcesDelegate?
    var customDataSourecs:CustomTableViewDataSources<HomeWorkListTableCell,Any>!
    var viewModel:HomeWorkViewModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialSetup()
        
    }
    func initialSetup() {
        self.register(UINib(nibName: TableCellIdentifier.studentHomework, bundle: nil), forCellReuseIdentifier: TableCellIdentifier.studentHomework)
        self.customDataSourecs = .init(cellIdentifier: TableCellIdentifier.studentHomework, item: self.item, completion: { cell, data, index in
            cell.configCell(data: data)
            cell.studentButtonCallback = { type in
                switch type{
                case .Add:
                    self.dataPassDelegate?.tableViewCallBackMultipleAction(self, selectedIndex: index, data: data, cell: cell,actionFor:0)
                case .Update:
                    self.dataPassDelegate?.tableViewCallBackMultipleAction(self, selectedIndex: index, data: data, cell: cell,actionFor:1)
                }
            }
            
            cell.cellTapped = {
                guard let object = data as? HomeWork else{return}
                self.viewModel = HomeWorkViewModel()
                self.viewModel?.dataPassDelegate = self
                self.viewModel?.submittedWorkApi(assignmentId: object.assignmentId ?? "")
            }
            
        })
        DispatchQueue.main.async {
            self.dataSource = self.customDataSourecs
            self.delegate = self
            self.reloadData()
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
extension HomeWorkListTableView:TableDataDelegate {
    func getData(cell:String,data:[Any]){
        self.item = data
        initialSetup()
    }
}

extension HomeWorkListTableView:HomeWorkDelegate{
    func onSuccess(_ data: Any) {
        guard let assignmentData = data as? AssignmentModel else{return}
        
        let controller = HomeWorkWithQuestionViewController.getController(storyboard: .TeacherDashboard)
        controller.submittedWorkForStudent = assignmentData
        controller.cameFor = .checkSubmittedWork
        self.navigate?.pushViewController(controller, animated: true)
        
    }
}
