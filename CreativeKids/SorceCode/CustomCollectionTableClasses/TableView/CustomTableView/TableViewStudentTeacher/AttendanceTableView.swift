//
//  AttendanceTableView.swift
//  CreativeKids
//
//  Created by Creative Kids on 15/02/22.
//

import UIKit

class AttendanceTableView: UITableView, UITableViewDelegate,UIScrollViewDelegate {
    
    var item = [Any]()
    var tableDelegate:TableDataDelegate?
    var dataPassDelegate:DataSourcesDelegate?
    var customDataSourecs:CustomTableViewDataSources<AttendanceTableViewCell,Any>!
    var message = "Select Class"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.initialSetup()
    }
    func initialSetup() {
        self.register(UINib(nibName: TableCellIdentifier.attendance, bundle: nil), forCellReuseIdentifier: TableCellIdentifier.attendance)
        self.customDataSourecs = .init(cellIdentifier: TableCellIdentifier.attendance, item: self.item, message: message, completion: { cell, data, index in
            cell.reloadDelegate = self
            cell.cellConfigure(data: data)
        })
        DispatchQueue.main.async {
            self.delegate = self
            self.dataSource = self.customDataSourecs
            self.reloadData()
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let isReachingEnd = scrollView.contentOffset.y >= 0
             && scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height) - 20
        self.tableDelegate?.notifyWhenScrollToButtom(status: isReachingEnd)
            
        }
    
}
extension AttendanceTableView: TableDataDelegate {
    func getData(cell: String, data: [Any]) {
        self.item = data
        self.initialSetup()
    }
}

extension AttendanceTableView:ReloadAnyViewDelegate{
    func reload() {
        self.reloadData()
    }
}
