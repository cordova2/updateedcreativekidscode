//
//  LeaveListTableView.swift
//  CreativeKids
//
//  Created by Creative Kids on 29/04/22.
//

import UIKit

class LeaveListTableView: UITableView, UITableViewDelegate {
    
    var item = [Any]()
    var dataPassDelegate:DataSourcesDelegate?
    var customDataSourecs:CustomTableViewDataSources<LeaveListTableViewCell,Any>!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.initialSetup()
    }
    
    func initialSetup() {
        self.register(UINib(nibName: TableCellIdentifier.leaveList, bundle: nil), forCellReuseIdentifier: TableCellIdentifier.leaveList)
        self.customDataSourecs = .init(cellIdentifier: TableCellIdentifier.leaveList, item: self.item, completion: { cell, data, index in
            cell.configCell(data: data)
        })
        DispatchQueue.main.async {
            self.delegate = self
            self.dataSource = self.customDataSourecs
            self.reloadData()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dataPassDelegate?.tableView(self, selectedIndex: indexPath.row, data: self.item[indexPath.row])
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
extension LeaveListTableView: TableDataDelegate {
    func getData(cell:String,data:[Any]) {
        self.item = data
        self.initialSetup()
    }
}
