//
//  StudentFeesTableView.swift
//  CreativeKids
//
//  Created by Creative Kids on 09/03/22.
//

import UIKit

class StudentFeesTableView: UITableView, UITableViewDelegate {
    
    var item = [Any]()
    var dataPassDelegate:DataSourcesDelegate?
    var customDataSourecs:CustomTableViewDataSources<StudentFeesTableViewCell,Any>!
    override func awakeFromNib() {
        super.awakeFromNib()
        initialSet()
    }
    
    func initialSet(){
        self.register(UINib(nibName: TableCellIdentifier.studentFeesCell, bundle: nil), forCellReuseIdentifier: TableCellIdentifier.studentFeesCell)
        self.customDataSourecs = .init(cellIdentifier: TableCellIdentifier.studentFeesCell, item: self.item, completion: { (cell, data, index) in
            self.separatorStyle = .none
            cell.configCell(object: data, feeTableView: self)
        })
        DispatchQueue.main.async {
            self.dataSource = self.customDataSourecs
            self.delegate = self
            self.reloadData()
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
extension StudentFeesTableView:TableDataDelegate {
    func getData(cell:String,data:[Any]){
        self.item = data
        initialSet()
    }
}
