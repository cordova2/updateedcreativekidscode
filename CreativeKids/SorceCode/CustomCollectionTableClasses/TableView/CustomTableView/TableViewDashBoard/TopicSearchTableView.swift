//
//  TopicSearchTableView.swift
//  CreativeKids
//
//  Created by Creative Kids on 15/04/21.
//

import Foundation
import UIKit

class TopicSeachTableView:UITableView,UITableViewDelegate {
    var tableCellId:String?
    var item = [Any]()
    var dataPassDelegate:DataSourcesDelegate?
    var customDataSourecs:CustomTableViewDataSources<TopicSearchTableViewCell,Any>!
    override func awakeFromNib() {
        super.awakeFromNib()
        initialSet()
    }
    
    func initialSet(){
        self.register(UINib(nibName: TableCellIdentifier.topicSearch, bundle: nil), forCellReuseIdentifier: TableCellIdentifier.topicSearch)
        self.customDataSourecs = .init(cellIdentifier: TableCellIdentifier.topicSearch, item: self.item, completion: { (cell, data, index) in
            self.separatorStyle = .singleLine
            cell.configCell(object: data)
//            self.dataPassDelegate?.tableView!(self, height:CGFloat(self.item.count * 100) + 40)
        })
        DispatchQueue.main.async {
            self.dataSource = self.customDataSourecs
            self.delegate = self
            self.reloadData()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dataPassDelegate?.tableView(self, selectedIndex: indexPath.row, data: self.item[indexPath.row])
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
}
extension TopicSeachTableView:TableDataDelegate {
    func getData(cell: String, data: [Any], language: String) {
        self.tableCellId = cell
        self.item = data
        initialSet()
    }
}
