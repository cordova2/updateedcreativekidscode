//
//  ContinueLearningTableView.swift
//  CreativeKids
//
//  Created by Creative Kids on 19/04/21.
//

import UIKit
import Foundation

class ContinueLearningTableView: UITableView,UITableViewDelegate {

    var tableCellId:String?
    var item = [Any]()
    var dataPassDelegate:DataSourcesDelegate?
    var customDataSourecs:CustomTableViewDataSources<ContinueLearningTableVeiwCell,Any>!
    override func awakeFromNib() {
        super.awakeFromNib()
        initialSet()
    }
    
    func initialSet(){
        self.register(UINib(nibName: TableCellIdentifier.continueLearning, bundle: nil), forCellReuseIdentifier: TableCellIdentifier.continueLearning)
        self.customDataSourecs = .init(cellIdentifier: TableCellIdentifier.continueLearning, item: self.item, completion: { (cell, data, index) in
            cell.configCell(object: data)
        })
        if UIDevice.isPhone{
            self.dataPassDelegate?.tableView(self, height:CGFloat(self.item.count * 60))
        }else{
            self.dataPassDelegate?.tableView(self, height:CGFloat(self.item.count * 80))
        }
        DispatchQueue.main.async {
            self.dataSource = self.customDataSourecs
            self.delegate = self
            self.reloadData()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dataPassDelegate?.tableView(self, selectedIndex: indexPath.row, data: self.item[indexPath.row])
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice.isPhone{
            return 60
        }else{
            return 80
        }
    }
    
}
extension ContinueLearningTableView:TableDataDelegate {
    func getData(cell: String, data: [Any], language: String) {
        self.tableCellId = cell
        self.item = data
        initialSet()
    }
}
