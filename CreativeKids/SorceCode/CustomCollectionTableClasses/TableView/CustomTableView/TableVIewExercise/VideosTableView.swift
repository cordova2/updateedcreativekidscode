//
//  VideosTableView.swift
//  CreativeKids
//
//  Created by Creative Kids on 01/04/21.
//

import Foundation
import UIKit

class VideoTableView:UITableView,UITableViewDelegate {
    var tableCellId:String?
    var item = [Any]()
    var videoLanguage:String?
    var dataPassDelegate:DataSourcesDelegate?
    var noDataMessage = "Loading..."
    var customDataSourecs:CustomTableViewDataSources<VideoTableViewCell,Any>!
    override func awakeFromNib() {
        super.awakeFromNib()
        initialSet()
    }
    
    func initialSet(){
        self.register(UINib(nibName: TableCellIdentifier.video, bundle: nil), forCellReuseIdentifier: TableCellIdentifier.video)
        self.dataPassDelegate?.tableView(self, height:CGFloat(self.item.count * 100))
        self.customDataSourecs = .init(cellIdentifier: TableCellIdentifier.video, item: self.item,message: noDataMessage, completion: { (cell, data, index) in
            cell.configCell(data: data,index: index,language: self.videoLanguage ?? "English")
        })
        DispatchQueue.main.async {
            self.dataSource = self.customDataSourecs
            self.delegate = self
            self.reloadData()
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dataPassDelegate?.tableView(self, selectedIndex: indexPath.row, data: self.item[indexPath.row])
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}
extension VideoTableView:TableDataDelegate {
    func getData(cell: String, data: [Any], language: String) {
        self.tableCellId = cell
        self.item = data
        self.videoLanguage = language
        initialSet()
    }
}
