//
//  ChapterTableView.swift
//  CreativeKids
//
//  Created by Creative Kids on 24/03/21.
//

import Foundation
import UIKit

class ChapterTableView:UITableView,UITableViewDelegate {
    var tableCellId:String?
    var item = [Any]()
    var message:String{
            if String.getstring(kUserData.role) == DashboardType.Student.userType &&
                String.getstring(kUserData.role) == DashboardType.Teacher.userType{
                return "No chapter has assigen"
            }else{
                return "No data found"
            }
    }
    var dataPassDelegate:DataSourcesDelegate?
    var customDataSourecs:CustomTableViewDataSources<ChapterTableViewCell,Any>!
    override func awakeFromNib() {
        super.awakeFromNib()
        //   initialSet()
    }
    
    func initialSet(){
        self.separatorStyle = .none
        self.register(UINib(nibName: TableCellIdentifier.chapter, bundle: nil), forCellReuseIdentifier: TableCellIdentifier.chapter)
        self.customDataSourecs = .init(cellIdentifier: TableCellIdentifier.chapter, item: self.item,message:message, completion: { (cell, data, index) in
            self.dataPassDelegate?.tableViewCallBack(self, selectedIndex: index, data: data,cell: cell)
            cell.configCell(data: data,index: index)
        })
        if UIDevice.isPhone{
            self.dataPassDelegate?.tableView(self, height:CGFloat(self.item.count * 70))
        }else{
            self.dataPassDelegate?.tableView(self, height:CGFloat(self.item.count * 100))
        }
        DispatchQueue.main.async {
            self.dataSource = self.customDataSourecs
            self.delegate = self
            self.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dataPassDelegate?.tableView(self, selectedIndex: indexPath.row, data: self.item[indexPath.row])
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice.isPhone{
            return 70
        }else{
            return 100
        }
    }
    
}
extension ChapterTableView:TableDataDelegate {
    func getData(cell: String, data: [Any]) {
        self.tableCellId = cell
        self.item = data
        initialSet()
    }
}
