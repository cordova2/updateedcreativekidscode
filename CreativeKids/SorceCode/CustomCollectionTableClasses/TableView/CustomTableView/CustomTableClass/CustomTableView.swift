//
//  DashBoardTableView.swift
//  CreativeKids
//
//  Created by Creative Kids on 18/03/21.
//

import Foundation
import UIKit
 protocol TableDataDelegate {
      func getData(cell:String,data:[Any])
      func getData(cell:String,data:[Any],language:String)
      func getDataWithAdditionalData(cell:String,data:[Any],language:String,otherData:[Any])
     func notifyWhenScrollToButtom(status:Bool)
     
}
 protocol DataSourcesDelegate{
     func tableView(_ tableView:UITableView, data:Any,cell:UITableViewCell)
     func tableView(_ tableView:UITableView,data:Any?)
     func tableView(_ tableView:UITableView,height:CGFloat)
     func tableView(_ tableView:UITableView, selectedIndex:Int, data:Any?)
     func tableViewCallBack(_ tableView:UITableView, selectedIndex:Int, data:Any?,cell:UITableViewCell)
     func tableViewCallBackMultipleAction(_ tableView:UITableView, selectedIndex:Int, data:Any?,cell:UITableViewCell, actionFor:Any)
}

extension TableDataDelegate{
    func getData(cell:String,data:[Any]){}
    func getData(cell:String,data:[Any],language:String){}
    func getDataWithAdditionalData(cell:String,data:[Any],language:String,otherData:[Any]){}
    func notifyWhenScrollToButtom(status:Bool){}
}

extension DataSourcesDelegate{
    func tableView(_ tableView:UITableView, data:Any,cell:UITableViewCell){}
    func tableView(_ tableView:UITableView,data:Any?){}
    func tableView(_ tableView:UITableView,height:CGFloat){}
    func tableView(_ tableView:UITableView, selectedIndex:Int, data:Any?){}
    func tableViewCallBack(_ tableView:UITableView, selectedIndex:Int, data:Any?,cell:UITableViewCell){}
    func tableViewCallBackMultipleAction(_ tableView:UITableView, selectedIndex:Int, data:Any?,cell:UITableViewCell, actionFor:Any){}
}


extension UITableView {
    func getNumberOfRow(numberofRow count:Int? ,message messages:String? , messageColor:UIColor = .black) -> Int {
        var noDataLbl : UILabel?
        noDataLbl = UILabel(frame: self.frame)
        noDataLbl?.textAlignment = .center
        noDataLbl?.font = UIFont.boldSystemFont(ofSize: 18)
        noDataLbl?.numberOfLines = 0
        noDataLbl?.center = self.center
        noDataLbl?.textColor = messageColor
        noDataLbl?.lineBreakMode = .byTruncatingTail
        self.backgroundView = noDataLbl
        self.separatorStyle = .none
        if Int.getInt(count) == 0 {
            noDataLbl?.text = messages
        }else{
            noDataLbl?.text = ""
        }
        return Int.getInt(count)
    }
}

