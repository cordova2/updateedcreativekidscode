//
//  TemAndConditionsViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 06/04/21.
//

import UIKit
import WebKit

class TemAndConditionsViewController: UIViewController {
    
    @IBOutlet weak var webView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        statusBarColor(headerColor: UIColor.blue)
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let urlRequest = URLRequest(url: URL(string: "https://creativekidssolutions.com/Home/tnc")!)
        self.webView.load(urlRequest)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppUtility.lockOrientation(.portrait)
    }
    @IBAction func buttonBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
