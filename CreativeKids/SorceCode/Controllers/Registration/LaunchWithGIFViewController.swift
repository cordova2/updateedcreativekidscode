//
//  LaunchWithGIFViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 24/06/21.
//

import UIKit

class LaunchWithGIFViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppUtility.lockOrientation(.portrait)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2){
            let jeremyGif = UIImage.gifImageWithName("launch_screen_new")
            let imageView = UIImageView(image: jeremyGif)
            imageView.frame = self.view.bounds
            self.view.addSubview(imageView)
            
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 4){
            kSharedAppManager.moveToWlkThrough()
        }
    }
}
