//
//  ForgotPasswordViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 19/03/21.
//

import UIKit

class ForgotPasswordViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var textFieldEmailID: UITextField!
    @IBOutlet weak var headerImageHeight: NSLayoutConstraint!
    @IBOutlet weak var topConsBoyImage: NSLayoutConstraint!
    @IBOutlet weak var viewEmailID: UIView!
    @IBOutlet weak var buttonSubmitted: UIButton!
    @IBOutlet weak var heightButtonSubmitted: NSLayoutConstraint!
    
    //MARK: - Variables
    var forGotModel:ForgotViewModel?
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldEmailID.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        statusBarColor(headerColor: UIColor.blue)
        AppUtility.lockOrientation(.portrait)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        heightButtonSubmitted.constant = kScreenHeight * 0.045
        buttonSubmitted.cornerRadius = (kScreenHeight * 0.045)/2
        self.headerImageHeight.constant = kScreenHeight * 0.30
        self.topConsBoyImage.constant = -(kScreenHeight * 0.30)/2
    }
    
    //MARK: - BActions
    @IBAction func buttonSubmitTapped(_ sender: UIButton) {
        self.forGotModel = ForgotViewModel(viewController: self)
    }
    
    @IBAction func buttonBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension ForgotPasswordViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.viewEmailID.layer.borderColor = textFieldEmailID.isFirstResponder ? #colorLiteral(red: 0.01680417731, green: 0.1983509958, blue: 1, alpha: 0.6013814156): #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        self.viewEmailID.layer.borderWidth = 0.9
    }
}
