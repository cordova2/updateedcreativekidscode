//
//  ClassSelectionViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 17/04/21.
// https://apps.apple.com/us/app/creative-kids-home-tutor/id1563219383

import UIKit

class ClassSelectionViewController: UIViewController {
    enum CameFor{
        case classSelected,classChanged,classSelectForSubScribe,classSelectionForSignUp
    }
    var cameFor:CameFor = .classChanged
    var changeClassCallBack:(()->Void)?
    var demoClassCallBack:(()->Void)?
    var selectClassCallBack:((String,String)->Void)?
    var registerClassCallBack:((String)->Void)?
    @IBOutlet weak var viewClasses: UIView!
    @IBOutlet weak var viewNCERTClass: UIView!
    @IBOutlet weak var labelCordovaClass: UILabel!
    @IBOutlet weak var labelPrimaryClasses: UILabel!
    @IBOutlet weak var viewForTopSpace: UIView!
    @IBOutlet weak var viewForDemo: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialView()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        viewClasses.roundCorner([.topLeft,.topRight], radius: 20)
    }
    
    @IBAction func buttonClassSelectedTapped(_ sender: UIButton) {
        switch cameFor {
        case .classSelected:
            classSelcted(sender: sender)
        case .classChanged:
            classChanged(sender: sender)
        case .classSelectForSubScribe:
            classSelctedForSubscribe(sender: sender)
        case .classSelectionForSignUp:
            classSelctedForSignUp(sender: sender)
        }
    }
    
    func initialView(){
        switch cameFor {
        case .classSelected:
            viewForTopSpace.isHidden = false
            viewForDemo.isHidden = true
            viewNCERTClass.isHidden = true
            labelPrimaryClasses.text = ""
            labelCordovaClass.text = ""
        case .classChanged:
//            viewForTopSpace.isHidden = true
//            viewForDemo.isHidden = false
            viewNCERTClass.isHidden = false
            labelCordovaClass.text = "Cordova - Classes"
            demoStatusApi()
        case .classSelectForSubScribe:
            viewForTopSpace.isHidden = false
            viewForDemo.isHidden = true
            viewNCERTClass.isHidden = false
//            labelPrimaryClasses.text = ""
            labelCordovaClass.text = "Cordova - Classes"
        case  .classSelectionForSignUp:
            viewForTopSpace.isHidden = false
            viewForDemo.isHidden = true
            viewNCERTClass.isHidden = true
            labelPrimaryClasses.text = ""
            labelCordovaClass.text = ""
        }
        
    }
    
    func classChanged(sender:UIButton){
        switch sender.tag {
        case 20:
            self.dismiss(animated: true){
                kSharedUserDefaults.setUserClass(userClass: ["className":ClassName.demo,"classType":ClassType.cordova])
                self.demoClassCallBack?()
            }
        case 0:
            self.dismiss(animated: true){
                kSharedUserDefaults.setUserClass(userClass: ["className":ClassName.nursery,"classType":ClassType.cordova])
                self.changeClassCallBack?()
            }
        case 1:
            self.dismiss(animated: true){
                kSharedUserDefaults.setUserClass(userClass: ["className":ClassName.lkg,"classType":ClassType.cordova])
                self.changeClassCallBack?()
            }
        case 2:
            self.dismiss(animated: true){
                kSharedUserDefaults.setUserClass(userClass: ["className":ClassName.ukg,"classType":ClassType.cordova])
                self.changeClassCallBack?()
            }
        case 3:
            self.dismiss(animated: true){
                kSharedUserDefaults.setUserClass(userClass: ["className":ClassName.class1,"classType":ClassType.cordova])
                self.changeClassCallBack?()
            }
        case 4:
            self.dismiss(animated: true){
                kSharedUserDefaults.setUserClass(userClass: ["className":ClassName.class2,"classType":ClassType.cordova])
                self.changeClassCallBack?()
            }
        case 5:
            self.dismiss(animated: true){
                kSharedUserDefaults.setUserClass(userClass: ["className":ClassName.class3,"classType":ClassType.cordova])
                self.changeClassCallBack?()
            }
        case 6:
            self.dismiss(animated: true){
                kSharedUserDefaults.setUserClass(userClass: ["className":ClassName.class4,"classType":ClassType.cordova])
                self.changeClassCallBack?()
            }
        case 7:
            self.dismiss(animated: true){
                kSharedUserDefaults.setUserClass(userClass: ["className":ClassName.class5,"classType":ClassType.cordova])
                self.changeClassCallBack?()
            }
        case 8:
            self.dismiss(animated: true){
                kSharedUserDefaults.setUserClass(userClass: ["className":ClassName.class6,"classType":ClassType.cordova])
                self.changeClassCallBack?()
            }
        case 9:
            self.dismiss(animated: true){
                kSharedUserDefaults.setUserClass(userClass: ["className":ClassName.class7,"classType":ClassType.cordova])
                self.changeClassCallBack?()
            }
        case 10:
            self.dismiss(animated: true){
                kSharedUserDefaults.setUserClass(userClass: ["className":ClassName.class8,"classType":ClassType.cordova])
                self.changeClassCallBack?()
            }
        case 11:
            self.dismiss(animated: true){
                kSharedUserDefaults.setUserClass(userClass: ["className":ClassName.class6,"classType":ClassType.ncert])
                self.changeClassCallBack?()
            }
        case 12:
            self.dismiss(animated: true){
                kSharedUserDefaults.setUserClass(userClass: ["className":ClassName.class7,"classType":ClassType.ncert])
                self.changeClassCallBack?()
            }
        case 13:
            self.dismiss(animated: true){
                kSharedUserDefaults.setUserClass(userClass: ["className":ClassName.class8,"classType":ClassType.ncert])
                self.changeClassCallBack?()
            }
        case 14:
            self.dismiss(animated: true){
                kSharedUserDefaults.setUserClass(userClass: ["className":ClassName.class9,"classType":ClassType.ncert])
                self.changeClassCallBack?()
            }
        case 15:
            self.dismiss(animated: true){
                kSharedUserDefaults.setUserClass(userClass: ["className":ClassName.class10,"classType":ClassType.ncert])
                self.changeClassCallBack?()
            }
        default:
            return
        }
    }
    
    func classSelcted(sender:UIButton){
        switch sender.tag {
        case 0:
            self.dismiss(animated: true){
                kSharedUserDefaults.setUserClass(userClass: ["className":ClassName.nursery,"classType":ClassType.cordova])
            }
        case 1:
            self.dismiss(animated: true){
                kSharedUserDefaults.setUserClass(userClass: ["className":ClassName.lkg,"classType":ClassType.cordova])
            }
        case 2:
            self.dismiss(animated: true){
                kSharedUserDefaults.setUserClass(userClass: ["className":ClassName.ukg,"classType":ClassType.cordova])
            }
        case 3:
            self.dismiss(animated: true){
                kSharedUserDefaults.setUserClass(userClass: ["className":ClassName.class1,"classType":ClassType.cordova])
            }
        case 4:
            self.dismiss(animated: true){
                kSharedUserDefaults.setUserClass(userClass: ["className":ClassName.class2,"classType":ClassType.cordova])
            }
        case 5:
            self.dismiss(animated: true){
                kSharedUserDefaults.setUserClass(userClass: ["className":ClassName.class3,"classType":ClassType.cordova])
            }
        case 6:
            self.dismiss(animated: true){
                kSharedUserDefaults.setUserClass(userClass: ["className":ClassName.class4,"classType":ClassType.cordova])
            }
        case 7:
            self.dismiss(animated: true){
                kSharedUserDefaults.setUserClass(userClass: ["className":ClassName.class5,"classType":ClassType.cordova])
            }
        case 8:
            self.dismiss(animated: true){
                kSharedUserDefaults.setUserClass(userClass: ["className":ClassName.class6,"classType":ClassType.cordova])
            }
        case 9:
            self.dismiss(animated: true){
                kSharedUserDefaults.setUserClass(userClass: ["className":ClassName.class7,"classType":ClassType.cordova])
            }
        case 10:
            self.dismiss(animated: true){
                kSharedUserDefaults.setUserClass(userClass: ["className":ClassName.class8,"classType":ClassType.cordova])
            }
        case 11:
            self.dismiss(animated: true){
                kSharedUserDefaults.setUserClass(userClass: ["className":ClassName.class6,"classType":ClassType.ncert])
            }
        case 12:
            self.dismiss(animated: true){
                kSharedUserDefaults.setUserClass(userClass: ["className":ClassName.class7,"classType":ClassType.ncert])
            }
        case 13:
            self.dismiss(animated: true){
                kSharedUserDefaults.setUserClass(userClass: ["className":ClassName.class8,"classType":ClassType.ncert])
            }
        case 14:
            self.dismiss(animated: true){
                kSharedUserDefaults.setUserClass(userClass: ["className":ClassName.class9,"classType":ClassType.ncert])
            }
        case 15:
            self.dismiss(animated: true){
                kSharedUserDefaults.setUserClass(userClass: ["className":ClassName.class10,"classType":ClassType.ncert])
            }
        default:
            return
        }
    }
    
    func classSelctedForSubscribe(sender:UIButton){
        switch sender.tag {
        case 0:
            self.dismiss(animated: true){
                self.selectClassCallBack?(ClassName.nursery,ClassType.cordova)
            }
        case 1:
            self.dismiss(animated: true){
                self.selectClassCallBack?(ClassName.lkg,ClassType.cordova)
            }
        case 2:
            self.dismiss(animated: true){
                self.selectClassCallBack?(ClassName.ukg,ClassType.cordova)
            }
        case 3:
            self.dismiss(animated: true){
                self.selectClassCallBack?(ClassName.class1,ClassType.cordova)
            }
        case 4:
            self.dismiss(animated: true){
                self.selectClassCallBack?(ClassName.class2,ClassType.cordova)
            }
        case 5:
            self.dismiss(animated: true){
                self.selectClassCallBack?(ClassName.class3,ClassType.cordova)
            }
        case 6:
            self.dismiss(animated: true){
                self.selectClassCallBack?(ClassName.class4,ClassType.cordova)
            }
        case 7:
            self.dismiss(animated: true){
                self.selectClassCallBack?(ClassName.class5,ClassType.cordova)
            }
        case 8:
            self.dismiss(animated: true){
                self.selectClassCallBack?(ClassName.class6,ClassType.cordova)
            }
        case 9:
            self.dismiss(animated: true){
                self.selectClassCallBack?(ClassName.class7,ClassType.cordova)
            }
        case 10:
            self.dismiss(animated: true){
                self.selectClassCallBack?(ClassName.class8,ClassType.cordova)
            }
        case 11:
            self.dismiss(animated: true){
                self.selectClassCallBack?(ClassName.class6,ClassType.ncert)
            }
        case 12:
            self.dismiss(animated: true){
                self.selectClassCallBack?(ClassName.class7,ClassType.ncert)
            }
        case 13:
            self.dismiss(animated: true){
                self.selectClassCallBack?(ClassName.class8,ClassType.ncert)
            }
        case 14:
            self.dismiss(animated: true){
                self.selectClassCallBack?(ClassName.class9,ClassType.ncert)
            }
        case 15:
            self.dismiss(animated: true){
                self.selectClassCallBack?(ClassName.class10,ClassType.ncert)
            }
        default:
            return
        }
    }
    
    
    func classSelctedForSignUp(sender:UIButton){
        switch sender.tag {
        case 0:
            self.dismiss(animated: true){
                self.registerClassCallBack?(ClassName.nursery)
            }
        case 1:
            self.dismiss(animated: true){
                self.registerClassCallBack?(ClassName.lkg)
            }
        case 2:
            self.dismiss(animated: true){
                self.registerClassCallBack?(ClassName.ukg)
            }
        case 3:
            self.dismiss(animated: true){
                self.registerClassCallBack?(ClassName.class1)
            }
        case 4:
            self.dismiss(animated: true){
                self.registerClassCallBack?(ClassName.class2)
            }
        case 5:
            self.dismiss(animated: true){
                self.registerClassCallBack?(ClassName.class3)
            }
        case 6:
            self.dismiss(animated: true){
                self.registerClassCallBack?(ClassName.class4)
            }
        case 7:
            self.dismiss(animated: true){
                self.registerClassCallBack?(ClassName.class5)
            }
        case 8:
            self.dismiss(animated: true){
                self.registerClassCallBack?(ClassName.class6)
            }
        case 9:
            self.dismiss(animated: true){
                self.registerClassCallBack?(ClassName.class7)
            }
        case 10:
            self.dismiss(animated: true){
                self.registerClassCallBack?(ClassName.class8)
            }
        case 11:
            self.dismiss(animated: true){
                self.registerClassCallBack?(ClassName.class6)
            }
        case 12:
            self.dismiss(animated: true){
                self.registerClassCallBack?(ClassName.class7)
            }
        case 13:
            self.dismiss(animated: true){
                self.registerClassCallBack?(ClassName.class8)
            }
        case 14:
            self.dismiss(animated: true){
                self.registerClassCallBack?(ClassName.class9)
            }
        case 15:
            self.dismiss(animated: true){
                self.registerClassCallBack?(ClassName.class10)
            }
        default:
            return
        }
    }
    
    
    //MARK: - Get Demo Status Api
    func demoStatusApi(){
        let serviceName = "?Demo"
        BaseController.shared.postToServerAPI(url: serviceName, params: [:], type: .GET,showHud: false) { (response, statusCode) in
            if statusCode == 200 {
                let kresponce = kSharedInstance.getArray(withDictionary: response[getResponce])
                print(kresponce)
                let demoDict = kSharedInstance.getDictionary(kresponce[0])
                
                self.viewForTopSpace.isHidden = String.getString(demoDict["subjdemo"]) == "true"
                self.viewForDemo.isHidden = String.getString(demoDict["subjdemo"]) != "true"
                print("response: ------\(response)")
            } else if statusCode == 404{
                self.viewForTopSpace.isHidden = false
                self.viewForDemo.isHidden = true
            }else {
                showAlertMessage.alert(message: "Invalid Credential")
            }
        }
    }
}
