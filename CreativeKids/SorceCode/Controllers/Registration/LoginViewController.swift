//
//  LoginViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 17/03/21.
// https://creativekidssolutions.com/Home/PrivacyPolicy

import UIKit

class LoginViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var headerImageHeight: NSLayoutConstraint!
    @IBOutlet weak var topConsBoyImage: NSLayoutConstraint!
    @IBOutlet weak var textFieldMailID: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var viewMailID: UIView!
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var buttonLogin: UIButton!
    @IBOutlet weak var heightLoginButton: NSLayoutConstraint!
    
    //MARK: - Variables
    var loginModel:LoginViewModel?
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldPassword.delegate = self
        textFieldMailID.delegate = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        heightLoginButton.constant = kScreenHeight * 0.045
        buttonLogin.cornerRadius = (kScreenHeight * 0.045)/2
        self.headerImageHeight.constant = kScreenHeight * 0.30
        self.topConsBoyImage.constant = -(kScreenHeight * 0.30/2)
        statusBarColor(headerColor: UIColor.blue)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppUtility.lockOrientation(.portrait)
    }
    
    //MARK: - IBActions
    @IBAction func buttonLoginTapped(_ sender: UIButton) {
        loginModel = LoginViewModel(viewController: self)
        
    }
    
    
    @IBAction func buttonForgotPasswordTapped(_ sender: UIButton) {
        let controller = ForgotPasswordViewController.getController(storyboard: .Registration)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func buttonNewUserTapped(_ sender: UIButton) {
        let controller = SignupViewController.getController(storyboard: .Registration)
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
extension LoginViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.viewMailID.layer.borderColor = textFieldMailID.isFirstResponder ? #colorLiteral(red: 0.01680417731, green: 0.1983509958, blue: 1, alpha: 0.6013814156): #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        self.viewMailID.layer.borderWidth = 0.9
        self.viewPassword.layer.borderColor = textFieldPassword.isFirstResponder ? #colorLiteral(red: 0.01680417731, green: 0.1983509958, blue: 1, alpha: 0.6013814156): #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        self.viewPassword.layer.borderWidth = 0.9
    }
}
