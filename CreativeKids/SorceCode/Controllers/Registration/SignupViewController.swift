//
//  SignupViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 18/03/21.
//

import UIKit

class SignupViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var headerImageHeight: NSLayoutConstraint!
    @IBOutlet weak var textFieldName: UITextField!
    @IBOutlet weak var textFieldMobileNumber: UITextField!
    @IBOutlet weak var textFieldEmailID: UITextField!
    @IBOutlet weak var textFieldGuardianEmailId: UITextField!
    @IBOutlet weak var textFieldTeacherEmailId: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var textFieldSchoolName: UITextField!
    @IBOutlet weak var textFieldCity: UITextField!
    @IBOutlet weak var textFieldClass: UITextField!
    @IBOutlet weak var labelTermAndCondition: UILabel!
    @IBOutlet weak var btnTermsCondition: UIButton!
    @IBOutlet weak var topConsBoyImage: NSLayoutConstraint!
    @IBOutlet weak var viewName: UIView!
    @IBOutlet weak var viewMobileNumber: UIView!
    @IBOutlet weak var viewEmailID: UIView!
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var viewSchoolName: UIView!
    @IBOutlet weak var viewCity: UIView!
    @IBOutlet weak var viewClass: UIView!
    @IBOutlet weak var buttonContinue: UIButton!
    @IBOutlet weak var heightbuttonContinue: NSLayoutConstraint!
    
    var stateName = ""
    var cityName = ""
    //MARK: - Variables
    var signupModel : SignupViewModel?
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    func initialSetup() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(termAndCondition(gesture:)))
        self.labelTermAndCondition.isUserInteractionEnabled = true
        self.labelTermAndCondition.addGestureRecognizer(gesture)
        self.textFieldClass.text = String.getString(kSharedUserDefaults.getUserClass()["className"])
        textFieldName.delegate = self
        textFieldMobileNumber.delegate = self
        textFieldEmailID.delegate = self
        textFieldPassword.delegate = self
        textFieldSchoolName.delegate = self
        textFieldCity.delegate = self
        textFieldClass.delegate = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        heightbuttonContinue.constant = kScreenHeight * 0.045
        buttonContinue.cornerRadius = (kScreenHeight * 0.045)/2
        self.headerImageHeight.constant = kScreenHeight * 0.30
        self.topConsBoyImage.constant = -((kScreenHeight * 0.30)/2)
        statusBarColor(headerColor: UIColor.blue)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppUtility.lockOrientation(.portrait)
    }
    
    //MARK: - IBActions
    @IBAction func buttonBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonSignUpTapped(_ sender: UIButton) {
        signupModel = SignupViewModel.init(viewController: self)
    }
    @IBAction func buttontermsTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func buttonSignInTapped(_ sender: UIButton) {
        kSharedAppManager.moveToLogin()
    }
    @IBAction func buttonSelectClassTapped(_ sender: UIButton) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            let controller = ClassSelectionViewController.getController(storyboard: .Registration)
            controller.cameFor = .classSelectionForSignUp
            controller.registerClassCallBack = { selectedClass in
                self.textFieldClass.text = selectedClass
            }
            self.present(controller, animated: true, completion: nil)
        })
    }
    @objc func termAndCondition(gesture:UITapGestureRecognizer){
        let controller = TemAndConditionsViewController.getController(storyboard: .Registration)
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
extension SignupViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.viewName.layer.borderColor = textFieldName.isFirstResponder ? #colorLiteral(red: 0.01680417731, green: 0.1983509958, blue: 1, alpha: 0.6013814156): #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        self.viewName.layer.borderWidth = 0.9
        self.viewMobileNumber.layer.borderColor = textFieldMobileNumber.isFirstResponder ? #colorLiteral(red: 0.01680417731, green: 0.1983509958, blue: 1, alpha: 0.6013814156): #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        self.viewMobileNumber.layer.borderWidth = 0.9
        self.viewEmailID.layer.borderColor = textFieldEmailID.isFirstResponder ? #colorLiteral(red: 0.01680417731, green: 0.1983509958, blue: 1, alpha: 0.6013814156): #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        self.viewEmailID.layer.borderWidth = 0.9
        self.viewPassword.layer.borderColor = textFieldPassword.isFirstResponder ? #colorLiteral(red: 0.01680417731, green: 0.1983509958, blue: 1, alpha: 0.6013814156): #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        self.viewPassword.layer.borderWidth = 0.9
        self.viewSchoolName.layer.borderColor = textFieldSchoolName.isFirstResponder ? #colorLiteral(red: 0.01680417731, green: 0.1983509958, blue: 1, alpha: 0.6013814156): #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        self.viewSchoolName.layer.borderWidth = 0.9
        self.viewCity.layer.borderColor = textFieldCity.isFirstResponder ? #colorLiteral(red: 0.01680417731, green: 0.1983509958, blue: 1, alpha: 0.6013814156): #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        self.viewCity.layer.borderWidth = 0.9
        self.viewCity.layer.borderColor = textFieldCity.isFirstResponder ? #colorLiteral(red: 0.01680417731, green: 0.1983509958, blue: 1, alpha: 0.6013814156): #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        self.viewCity.layer.borderWidth = 0.9
        if textField == textFieldCity{
            textFieldCity.resignFirstResponder()
            let controller = SeachLocationViewController.getController(storyboard: .Registration)
            controller.locationPassCallBack = { [unowned self] (city1, city2, address) in
                self.textFieldCity.text = address
                self.cityName = city1
                self.stateName = city2
            }
            self.present(controller, animated: true, completion: nil)
        }
    }
}
