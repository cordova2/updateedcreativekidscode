//
//  HomeNavigationController.swift
//  CreativeKids
//
//  Created by Creative Kids on 21/09/21.
//

import UIKit
import Speech

class HomeNavigationController: UINavigationController {

    enum Side{
        case topLeft,topRight,top,buttomLeft,buttomRight,buttom,left,right
    }
    
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private let audioEngine = AVAudioEngine()
    
    let speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-IN"))  //1
    
    var voiceView:VoiceViewXib?
    var searchTextView:SearchedVoiceViewXib?
    var searchText = ""
    override func viewDidLoad() {
        super.viewDidLoad()
//        addView()
//        speechRecognizer?.delegate = self
        // Do any additional setup after loading the view.
    }
    
    func addView(){
        self.voiceView = VoiceViewXib(frame: CGRect(x: self.view.frame.width - 80, y: 100, width: 80, height: 80))
        voiceView?.customDelegate = self
        self.voiceView?.navigationReferances = self
        self.view.addSubview(voiceView ?? UIView())
        self.voiceView?.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handler)))
        self.searchTextView = SearchedVoiceViewXib()
        self.searchTextView?.isHidden = true
        self.view.addSubview(searchTextView ?? UIView())
            searchTextView?.translatesAutoresizingMaskIntoConstraints = false
        let topConstraint = NSLayoutConstraint(item: searchTextView as Any, attribute: NSLayoutConstraint.Attribute.topMargin, relatedBy: NSLayoutConstraint.Relation.equal, toItem: voiceView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 20)
        let trailingConstraint = NSLayoutConstraint(item: searchTextView as Any, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: voiceView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)
        let widthConstraint = NSLayoutConstraint(item: searchTextView as Any, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.width, multiplier: 0.5, constant: 0)
        let heightConstraint = NSLayoutConstraint(item: searchTextView as Any, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.greaterThanOrEqual, toItem: voiceView, attribute: NSLayoutConstraint.Attribute.height, multiplier: 0.1, constant: 20)
            view.addConstraints([topConstraint, trailingConstraint,heightConstraint,widthConstraint])
    }
}
extension HomeNavigationController:SFSpeechRecognizerDelegate{
    func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
//            buttonMic.isEnabled = true
            } else {
//            buttonMic.isEnabled = false
            }
    }
    
    // ***************************************************************************
    
//    func checkAuthentication(){
//        SFSpeechRecognizer.requestAuthorization { (authStatus) in  //4
//
//                var isButtonEnabled = false
//
//                switch authStatus {  //5
//                case .authorized:
//                    isButtonEnabled = true
//
//                case .denied:
//                    isButtonEnabled = false
//                    print("User denied access to speech recognition")
//
//                case .restricted:
//                    isButtonEnabled = false
//                    print("Speech recognition restricted on this device")
//
//                case .notDetermined:
//                    isButtonEnabled = false
//                    print("Speech recognition not yet authorized")
//                default:
//                    print("unKnown error")
//                }
//
//                OperationQueue.main.addOperation() {
//                    self.buttonMic.isEnabled = isButtonEnabled
//            }
//        }
//    }
    
    func startRecording() {
        self.searchTextView?.isHidden = true
        self.searchTextView?.textViewSearch.text = ""
        
        if recognitionTask != nil {
            recognitionTask?.cancel()
            recognitionTask = nil
        }
        
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSession.Category.record)
            try audioSession.setMode(AVAudioSession.Mode.measurement)
            try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
        } catch {
            print("audioSession properties weren't set because of an error.")
        }
        
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        
        let inputNode = audioEngine.inputNode
        
        guard let recognitionRequest = recognitionRequest else {
            fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
        }
        
        recognitionRequest.shouldReportPartialResults = true
        
        recognitionTask = speechRecognizer?.recognitionTask(with: recognitionRequest, resultHandler: { (result, error) in
            
            var isFinal = false
            
//            if result != nil {
//
//                self.textFieldVoiceText.text = result?.bestTranscription.formattedString
//                isFinal = (result?.isFinal)!
//            }
//
//            if error != nil || isFinal {
//                self.audioEngine.stop()
//                inputNode.removeTap(onBus: 0)
//
//                self.recognitionRequest = nil
//                self.recognitionTask = nil
//
//                self.buttonMic.isEnabled = true
//            }
                if result != nil {
                    
                    let bestString = result!.bestTranscription.formattedString
                    var lastString: String = ""
                    for segment in result!.bestTranscription.segments {
                        let indexTo = bestString.index(bestString.startIndex, offsetBy: segment.substringRange.location)
                        lastString = String(bestString[indexTo...])
                        print(lastString)
                    }
                        if bestString != ""{
                            self.searchText = bestString
                            self.searchTextView?.isHidden = false
                            self.searchTextView?.textViewSearch.text = bestString
                        }
                    isFinal = (result!.isFinal)
//                    self.textFieldVoiceText.text = bestString
//                    self.checkForColorsSaid(resultString: lastString)
                } else{
                if let error = error {
//                    self.sendAlert(title: "Speech Recognizer Error", message: "There has been a speech recognition error.")
                    print(error)
                }
                    
                   if error != nil || isFinal {
                   self.audioEngine.stop()
                   inputNode.removeTap(onBus: 0)
                    
                   self.recognitionRequest = nil
                   self.recognitionTask = nil
                    
//                   self.buttonMic.isEnabled = true
                    }
                }
            
        })
        inputNode.removeTap(onBus: 0)
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
            self.recognitionRequest?.append(buffer)
        }
        
        audioEngine.prepare()
        
        do {
            try audioEngine.start()
        } catch {
            print("audioEngine couldn't start because of an error.")
        }
//        textFieldVoiceText.text = "Say something, I'm listening!"
    }
}
extension HomeNavigationController:MICDelegate{
    func animationStart() {
        startRecording()
    }
    func animationStop() {
        self.audioEngine.stop()
        self.recognitionRequest?.endAudio()
//        self.buttonMic.isEnabled = true
//        self.buttonMic.setTitle("Start Recording", for: .normal)
    }
}






extension HomeNavigationController{
    @objc func handler(gesture: UIPanGestureRecognizer){
        let location = gesture.location(in: self.view)
        let draggedView = gesture.view
        draggedView?.center = location
        guard let voiceView = self.voiceView else{return}
        let dragableFrameHeightMidPoint = voiceView.frame.height/2
        let dragableFrameWidthMidPoint = voiceView.frame.height/2
        
        if gesture.state == .changed || gesture.state == .began{
            if searchTextView != nil{
               searchTextView?.removeFromSuperview()
            }
        }
        
        if gesture.state == .ended {
            // Manage Top Y Axis
            if voiceView.frame.midY - dragableFrameHeightMidPoint <= voiceView.frame.height{
                // Manage Left Top Corner
                if voiceView.frame.midX - dragableFrameWidthMidPoint <= voiceView.frame.width{
                    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
                        voiceView.center.y = dragableFrameHeightMidPoint
                        voiceView.center.x = dragableFrameWidthMidPoint
                    }, completion: nil)
//                    self.searchTextView?.center.x = dragableFrameWidthMidPoint + voiceView.frame.width
//                    self.searchTextView?.center.y = dragableFrameHeightMidPoint + voiceView.frame.height
                    manageSearchTextView(side:.topLeft)
                    return
                }
                // Manage Right Top Corner
                else if voiceView.frame.midX + dragableFrameWidthMidPoint >= (self.view.layer.frame.width - voiceView.frame.width){
                    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
                        voiceView.center.y = dragableFrameHeightMidPoint
                        voiceView.center.x = self.view.layer.frame.width - dragableFrameWidthMidPoint
                    }, completion: nil)
//                    self.searchTextView?.center.x = (self.view.layer.frame.width - (searchTextView!.frame.width/2))
//                    self.searchTextView?.center.y = dragableFrameHeightMidPoint + voiceView.frame.height
                    manageSearchTextView(side:.topRight)
                    return
                }
                // Manage top Y axis
                else{
                    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
                        voiceView.center.y = dragableFrameHeightMidPoint
                    }, completion: nil)
                    manageSearchTextView(side:.top)
                    return
                }
            }
            
            // Manage Buttom Y Axis
            if voiceView.frame.midY + dragableFrameHeightMidPoint >= (self.view.layer.frame.height - voiceView.frame.height){
                // Manage left buttom Corner
                if voiceView.frame.midX - dragableFrameWidthMidPoint <= voiceView.frame.width{
                    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
                        voiceView.center.y = self.view.layer.frame.height - dragableFrameHeightMidPoint
                        voiceView.center.x = dragableFrameWidthMidPoint
                    }, completion: nil)
//                    self.searchTextView?.center.x = dragableFrameWidthMidPoint + voiceView.frame.width
//                    self.searchTextView?.center.y = dragableFrameHeightMidPoint - voiceView.frame.height
                    manageSearchTextView(side:.buttomLeft)
                    return
                }
                // Manage Right buttom Corner
                else if voiceView.frame.midX + dragableFrameWidthMidPoint >= (view.layer.frame.width - voiceView.frame.width){
                    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
                        voiceView.center.y = self.view.layer.frame.height - dragableFrameHeightMidPoint
                        voiceView.center.x = self.view.layer.frame.width - dragableFrameWidthMidPoint
                    }, completion: nil)
//                    self.searchTextView?.center.x = dragableFrameWidthMidPoint - searchTextView!.frame.width
//                    self.searchTextView?.center.y = dragableFrameHeightMidPoint - voiceView.frame.height
                    manageSearchTextView(side:.buttomRight)
                    return
                }
                else{
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
                    voiceView.center.y = self.view.layer.frame.height - dragableFrameHeightMidPoint
                }, completion: nil)
                    manageSearchTextView(side:.buttom)
                return
                }
            }
            
            // Manage Buttom x Axis Right
            if voiceView.frame.midX >= self.view.layer.frame.width / 2 {
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
                    voiceView.center.x = self.view.layer.frame.width - dragableFrameWidthMidPoint
                }, completion: nil)
                manageSearchTextView(side:.right)
                return
                
            }
            // Manage Buttom x Axis Left
            else{
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
                    voiceView.center.x = dragableFrameWidthMidPoint
                }, completion: nil)
                manageSearchTextView(side:.left)
                return
            }
        }
    }
    func manageSearchTextView(side:Side){
        switch side{
        case .topLeft:
            topLeftShift()
        case .topRight:
            topRightShift()
        case .top:
            top()
        case .buttomLeft:
            buttomLeftShift()
        case .buttomRight:
            buttomRightShift()
        case .buttom:
            buttom()
        case .left:
            left()
        case .right:
            right()
        }
    }
    
    
    func topLeftShift(){
        
        self.searchTextView = SearchedVoiceViewXib()
        self.view.addSubview(searchTextView ?? UIView())
            searchTextView?.translatesAutoresizingMaskIntoConstraints = false
        let topConstraint = NSLayoutConstraint(item: searchTextView as Any, attribute: NSLayoutConstraint.Attribute.topMargin, relatedBy: NSLayoutConstraint.Relation.equal, toItem: voiceView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 20)
        let leadingConstraint = NSLayoutConstraint(item: searchTextView as Any, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: voiceView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
        let widthConstraint = NSLayoutConstraint(item: searchTextView as Any, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.width, multiplier: 0.5, constant: 0)
        let heightConstraint = NSLayoutConstraint(item: searchTextView as Any, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.greaterThanOrEqual, toItem: voiceView, attribute: NSLayoutConstraint.Attribute.height, multiplier: 0.1, constant: 0)
            view.addConstraints([topConstraint, leadingConstraint,heightConstraint,widthConstraint])
        self.searchTextView?.isHidden = self.searchText == "" ? true : false
        
    }
    func topRightShift(){
        self.searchTextView = SearchedVoiceViewXib()
        self.view.addSubview(searchTextView ?? UIView())
            searchTextView?.translatesAutoresizingMaskIntoConstraints = false
        let topConstraint = NSLayoutConstraint(item: searchTextView as Any, attribute: NSLayoutConstraint.Attribute.topMargin, relatedBy: NSLayoutConstraint.Relation.equal, toItem: voiceView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 20)
        let trailingConstraint = NSLayoutConstraint(item: searchTextView as Any, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: voiceView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)
        let widthConstraint = NSLayoutConstraint(item: searchTextView as Any, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.width, multiplier: 0.5, constant: 0)
        let heightConstraint = NSLayoutConstraint(item: searchTextView as Any, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.greaterThanOrEqual, toItem: voiceView, attribute: NSLayoutConstraint.Attribute.height, multiplier: 0.1, constant: 20)
            view.addConstraints([topConstraint, trailingConstraint,heightConstraint,widthConstraint])
        self.searchTextView?.isHidden = self.searchText == "" ? true : false
    }
    func top(){
        if voiceView?.frame.midX < self.view.width/2{
            topLeftShift()
        }else{
            topRightShift()
        }
    }
    func buttomLeftShift(){
        self.searchTextView = SearchedVoiceViewXib()
        self.view.addSubview(searchTextView ?? UIView())
            searchTextView?.translatesAutoresizingMaskIntoConstraints = false
        let topConstraint = NSLayoutConstraint(item: searchTextView as Any, attribute: NSLayoutConstraint.Attribute.bottomMargin, relatedBy: NSLayoutConstraint.Relation.equal, toItem: voiceView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: -20)
        let leadingConstraint = NSLayoutConstraint(item: searchTextView as Any, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: voiceView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
        let widthConstraint = NSLayoutConstraint(item: searchTextView as Any, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.width, multiplier: 0.5, constant: 0)
        let heightConstraint = NSLayoutConstraint(item: searchTextView as Any, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.greaterThanOrEqual, toItem: voiceView, attribute: NSLayoutConstraint.Attribute.height, multiplier: 0.1, constant: 20)
            view.addConstraints([topConstraint, leadingConstraint,heightConstraint,widthConstraint])
        self.searchTextView?.isHidden = self.searchText == "" ? true : false
    }
    func buttomRightShift(){
        self.searchTextView = SearchedVoiceViewXib()
        self.view.addSubview(searchTextView ?? UIView())
            searchTextView?.translatesAutoresizingMaskIntoConstraints = false
        let topConstraint = NSLayoutConstraint(item: searchTextView as Any, attribute: NSLayoutConstraint.Attribute.bottomMargin, relatedBy: NSLayoutConstraint.Relation.equal, toItem: voiceView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: -20)
        let trailingConstraint = NSLayoutConstraint(item: searchTextView as Any, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: voiceView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)
        let widthConstraint = NSLayoutConstraint(item: searchTextView as Any, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.width, multiplier: 0.5, constant: 0)
        let heightConstraint = NSLayoutConstraint(item: searchTextView as Any, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.greaterThanOrEqual, toItem: voiceView, attribute: NSLayoutConstraint.Attribute.height, multiplier: 0.1, constant: 20)
            view.addConstraints([topConstraint, trailingConstraint,heightConstraint,widthConstraint])
        self.searchTextView?.isHidden = self.searchText == "" ? true : false
    }
    func buttom(){
        if voiceView?.frame.midX < self.view.width/2{
            buttomLeftShift()
        }else{
            buttomRightShift()
        }
    }
    func left(){
        if voiceView?.frame.midY < kScreenHeight/2{
            topLeftShift()
        }else{
            buttomLeftShift()
        }
    }
    func right(){
        if voiceView?.frame.midY < kScreenHeight/2{
            topRightShift()
        }else{
            buttomRightShift()
        }
    }
}
