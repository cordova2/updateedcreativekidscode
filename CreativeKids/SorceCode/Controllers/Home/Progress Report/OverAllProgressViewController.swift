//
//  OverAllProgressViewController.swift
//  CreativeKids
//
//  Created by Rakesh jha on 06/05/21.
//

import UIKit
import CoreCharts
enum UserPerformance{
    case normalUser
    case student
}

class OverAllProgressViewController: UIViewController {
    
    
    
    @IBOutlet weak var circularProgress: CircularProgressView!
    @IBOutlet weak var barChart: HCoreBarChart!
    @IBOutlet weak var collectionViewOverAllRating: OverAllRatingCollectionVew!
    
    @IBOutlet weak var chapterProgressTableView: ChapterLearnTableView!
    
    
    @IBOutlet weak var viewOverAllChapterPrecentage: UIView!
    @IBOutlet weak var viewOverAllLTPPercentage: UIView!
    @IBOutlet weak var viewOverAllChapPercentage: UIView!
    @IBOutlet weak var viewOverAllLtpPercentage: UIView!
    
    @IBOutlet weak var LiveTestProgressTableView: LiveTestPaperTableView!
    
    @IBOutlet weak var heightchapterProgressTableView: NSLayoutConstraint!
    @IBOutlet weak var heightLiveTestProgressTableView: NSLayoutConstraint!
    @IBOutlet weak var heightOverAllRatingCollectionView: NSLayoutConstraint!
    @IBOutlet weak var heightOverAllChapterCons: NSLayoutConstraint!
    @IBOutlet weak var heightOverAllLTPCons: NSLayoutConstraint!
    
    @IBOutlet weak var labelOverAllChapter: UILabel!
    @IBOutlet weak var labelOverAllLTP: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var buttonGetPdf: UIButton!
    @IBOutlet weak var pdfView: UIView!
    @IBOutlet weak var shareView: UIView!
    var subjectListArray = [SubjectModel]()
    var performanceFor:UserPerformance = .normalUser
    override func viewDidLoad() {
        super.viewDidLoad()
        setDelegate()
        initialView()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        statusBarColor(headerColor: UIColor.blue)
    }
    
    func initialView(){
        collectionViewOverAllRating.getData(cell: CollectionCellIdentifier.overAllRating, data: subjectListArray)
        chapterProgressTableView.getData(cell: TableCellIdentifier.chapterProgress, data: subjectListArray, language: "English")
        LiveTestProgressTableView.getData(cell: TableCellIdentifier.chapterProgress, data:subjectListArray, language: "English")
        setOverAllProgressView()
        setTestPaperProgressView(percentage: 0.7)
        setChapterLearnProgressView(percentage: 0.2)
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.setTestPaperProgressView(percentage: 0.2)
            self.setChapterLearnProgressView(percentage: 0.7)
        }
        profileImage.layer.cornerRadius = profileImage.height/2
        buttonGetPdf.layer.cornerRadius = buttonGetPdf.height/2
    }
    func setDelegate(){
        collectionViewOverAllRating.dataSourcesCollection = self
        chapterProgressTableView.dataPassDelegate = self
        LiveTestProgressTableView.dataPassDelegate = self
        barChart.dataSource = self
        self.profileImage.downlodeImage(serviceurl: kUserData.profileImage, placeHolder: #imageLiteral(resourceName: "kid_icon"))
    }
    
    @IBAction func buttonGetPdfViewTapped(_ sender: UIButton) {
        //        let controller = pdfViewController.getController(storyboard: .Home)
        //        controller.path = pdfView.exportAsPdfFromView()
        //        self.navigationController?.pushViewController(controller, animated: true)
        //        print(pdfView.exportAsPdfFromView())
        shareApp(appUrl: pdfView.exportAsPdfFromView(), sorceView: shareView, sender: sender)
    }
    @IBAction func backTappes(_ sender: UIButton) {
        //        AppManager.shared.moveToHome()
        
        switch performanceFor{
        case .normalUser:
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: DashBoradInstructionViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        case .student:
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: StudentTabBarController.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }
        
    }
}
extension OverAllProgressViewController:DataSourcesDelegate{
    func tableView(_ tableView: UITableView, height: CGFloat) {
        switch tableView {
        case chapterProgressTableView:
            if height < 50{
                self.heightLiveTestProgressTableView.constant = 50
            }else{
                self.heightchapterProgressTableView.constant = height
            }
        case LiveTestProgressTableView:
            if height < 50{
                self.heightLiveTestProgressTableView.constant = 50
            }else{
                self.heightLiveTestProgressTableView.constant = height
            }
        default:
            return
        }
    }
}

extension OverAllProgressViewController:DataSourcesCollectionDelegate{
    func collectionView(_ collectionView: UICollectionView, height: CGFloat) {
        heightOverAllRatingCollectionView.constant = height
    }
}

extension OverAllProgressViewController:CoreChartViewDataSource{
    func loadCoreChartData() -> [CoreChartEntry] {
        getStudentPerformanceList()
    }
    
    func getStudentPerformanceList()->[CoreChartEntry] {
        var allCityData = [CoreChartEntry]()
        let cityNames = ["35%","5%","60%"]
        let plateNumber = [35.0,5.0,60.0]
        
        for index in 0..<cityNames.count {
            let newEntry = CoreChartEntry(id: "", barTitle: cityNames[index], barHeight: CGFloat(plateNumber[index]), barColor: rainbowColor())
            allCityData.append(newEntry)
        }
        barChart.displayConfig.titleFontSize = 5.0
        barChart.displayConfig.barWidth = CGFloat(barChart.height/CGFloat(allCityData.count)) - 10
        return allCityData
    }
}

extension OverAllProgressViewController {
    //MARK:- SET PROGRESS VIEW
    func setOverAllProgressView(){
        circularProgress.trackClr = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        circularProgress.progressClr = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        circularProgress.progressLyrlineWidth = 8
        let progress = (1.00 * (80 / 100))
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.circularProgress.setProgressWithAnimation(duration: 1.0, value: Float(progress))
            self.circularProgress.percentLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            self.circularProgress.percentLabel.numberOfLines = 0
            self.circularProgress.percentLabel.adjustsFontSizeToFitWidth = true
            self.circularProgress.percentLabel.minimumScaleFactor = 0.5
//            self.circularProgress.percentLabel.font = UIFont.systemFont(ofSize: 9, weight: .medium)
            
            self.circularProgress.percentLabel.text = "80%"
        }
    }
    
    //MARK:- SET PROGRESS VIEW FOR CHAPTER LEARN
    func setChapterLearnProgressView(percentage:CGFloat){
        heightOverAllChapterCons = heightOverAllChapterCons.setMultiplier(multiplier: percentage)
        labelOverAllChapter.text = "\(Int(percentage * 100)) %"
    }
    //MARK:- SET PROGRESS VIEW
    func setTestPaperProgressView(percentage:CGFloat){
        heightOverAllLTPCons = heightOverAllLTPCons.setMultiplier(multiplier: percentage)
        labelOverAllLTP.text = "\(Int(percentage * 100)) %"
    }
}


// 408-1867002-4205927
