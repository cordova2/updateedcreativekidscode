//
//  ProgressReportViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 22/04/21.
//

import UIKit

class ProgressReportViewController: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var collectionVeiwSubjectWiseReport: ProgressReportCollectionView!
    @IBOutlet weak var progressShowCollectionView: ProgressShowCollectionView!
    @IBOutlet weak var labelselectedSubject: UILabel!
    @IBOutlet weak var heightSubjectWiseCollectionView: NSLayoutConstraint!
    //MARK:- Variables
    var performanceArray:(PerformanceModel,PerformanceModel)?
    var subjectListArray = [SubjectModel]()
    var progressViewModel:SubjectProgressViewModel?
    var allSubjectObject:Dict = ["CLASS":String.getString(kSharedUserDefaults.getUserClass()["className"]),"titlename":"All","img":"all"]
    var performanceFor:UserPerformance = .normalUser
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initialLoad()
        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        statusBarColor(headerColor: UIColor.blue)
    }
    
    func initialLoad(){
        collectionVeiwSubjectWiseReport.dataSourcesCollection = self
//        subjectListArray.insert(SubjectModel(data: self.allSubjectObject), at: 0)
        subjectListArray.first?.isSelected = true
        collectionVeiwSubjectWiseReport.getData(cell: "", data: subjectListArray)
        labelselectedSubject.text = subjectListArray.first?.subjectName
        subjectSelectedApi()
    }
    @IBAction func buttonBackTapped(_ sender: UIButton) {
        //        self.navigationController?.popViewController(animated: true)
        let controller = OverAllProgressViewController.getController(storyboard: .Home)
        controller.performanceFor = self.performanceFor
        controller.subjectListArray = subjectListArray
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    func subjectSelectedApi(){
        progressViewModel = SubjectProgressViewModel.init(viewController: self) { arrayDict in
            self.performanceArray = arrayDict
            self.progressShowCollectionView.getData(cell: CollectionCellIdentifier.subjectPerformance, data: [self.performanceArray as Any])
        }
    }
    
}

//MARK:- DataSourcesCollectionDelegate
extension ProgressReportViewController:DataSourcesCollectionDelegate{
    
    func collectionView(_ collectionView: UICollectionView, data: Any?, selectedIndex: IndexPath) {
        switch collectionView {
        case collectionVeiwSubjectWiseReport:
            guard let Kdata = data as? SubjectModel  else {return}
            let _ = subjectListArray.map{$0.isSelected = false}
            Kdata.isSelected = true
            labelselectedSubject.text = Kdata.subjectName
            collectionVeiwSubjectWiseReport.reloadData()
            subjectSelectedApi()
        default:
            return
        }
    }
    func collectionView(_ collectionView: UICollectionView, height: CGFloat) {
        self.heightSubjectWiseCollectionView.constant = height
    }
    
}
