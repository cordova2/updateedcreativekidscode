//
//  videoCheckViewController.swift
//  demo1
//
//  Created by Rakesh jha on 28/03/21.
//  https://creativekidssolutions.com/Content/video/Learning_Science-7/Class-7/9/vid/1.mp4
//  https://creativekidssolutions.com/Content/video/Learning_Science-7/Class-7/10/vid/1.mp4

import UIKit
import AVFoundation
import AVKit

enum CameFrom{
    case demoVideo,classVideo
}

class VideoPlayerViewController: UIViewController {
    
    //MARK:- IBOutlets
    @IBOutlet weak var playerView: UIView!
    @IBOutlet weak var viewAddPanGesture: UIView!
    @IBOutlet weak var viewPlayerButtom: UIView!
    @IBOutlet weak var viewBrightNessIndicater: UIView!
    @IBOutlet weak var viewAddPanGestureBrightNess: UIView!
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var progress: UIProgressView!
    @IBOutlet weak var timeSlider: UISlider!
    
    @IBOutlet weak var buttonNextSong: UIButton!
    @IBOutlet weak var buttonPreviousSong: UIButton!
    @IBOutlet weak var buttonOnWholeVideoPlayer: UIButton!
    @IBOutlet weak var buttonPlayerView: UIButton!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var buttonPlayButtom: UIButton!
    @IBOutlet weak var buttonNextVideo: UIButton!
    @IBOutlet weak var buttonPreviousVideo: UIButton!
    @IBOutlet weak var buttonVideoForword: UIButton!
    @IBOutlet weak var buttonVideoBackword: UIButton!
    @IBOutlet weak var buttonReplayVideo: UIButton!
    @IBOutlet weak var buttonQuality: UIButton!
    
    @IBOutlet weak var labelShowVolumePercentage: UILabel!
    @IBOutlet weak var labelShowBrightNessPercentage: UILabel!
    @IBOutlet weak var labelCurrentTime: UILabel!
    @IBOutlet weak var labelDurationTime: UILabel!
    @IBOutlet weak var videoNameLabel: UILabel!
    @IBOutlet weak var labelChapterName: UILabel!
    @IBOutlet weak var labelVideoName: UILabel!
    @IBOutlet weak var labelBhaagNumber: UILabel!
    @IBOutlet weak var labelVideoBhaagNumber: UILabel!
    
    @IBOutlet weak var topVideoPlayer: NSLayoutConstraint!
    @IBOutlet weak var aspectHeightWidth: NSLayoutConstraint!
    @IBOutlet var viewEbbokTop: NSLayoutConstraint!
    
    @IBOutlet weak var heightEbookPdfView: NSLayoutConstraint!
    @IBOutlet weak var leadingConsPreviewImage: NSLayoutConstraint!
    
    @IBOutlet weak var exerciseSegmentCollectionView: ExerciseSegmentCollectionView!
    
    @IBOutlet weak var tableViewPlaybackSpeed: PlaybackSpeedTableView!
    
    @IBOutlet weak var viewExcelInExam: UIView!
    @IBOutlet weak var viewEbook: UIView!
    @IBOutlet weak var imageViewForCheck: UIImageView!
    
    @IBOutlet weak var containerViewVideos: UIView!
    @IBOutlet weak var containerViewExercise: UIView!
    @IBOutlet weak var containerViewLTP: UIView!
    @IBOutlet weak var containerViewQuestionBank: UIView!
    @IBOutlet weak var viewSamplePaper: UIView!
    @IBOutlet weak var viewVideoRate: UIView!
    
    //MARK:- Variables
    var innerView = UIView()
    var statusView = UIView()
    var statusHeight = 0.0
    var isPlaying = true
    var showFilterMenu = true
    var player:AVPlayer!
    var playerItem:AVPlayerItem!
    var playerlayer:AVPlayerLayer!
    var videoPlayingforLongTime = false
    var initialVideoLoaded = false
    var currentPlayingVideo = 0
    var videoUrls = ["https://","https://"]
    var segmentDataArray = [SegmentModel]()
    var selectedTopic:TopicSearchModel?
    var subjectSelected:SubjectModel?
    var videoViewModel:VideoViewModel?
    var videoDataArray = [TopicModel]()
    var excelInExam = [ExerciseModel]()
    var ebook = [ExerciseModel]()
    var practicePaperPdf = [ExerciseModel]()
    var ltpExam = [ExerciseModel]()
    var exercises = [ExerciseModel]()
    var mockTest = [ExerciseModel]()
    var cameFromChapterScreen = true
    var cameFrom:CameFrom = .classVideo
    var videoPlayAgain = false
    var speedType:SpeedType = .normal
    var speedModelArray = [PlayerSpeedModel]()
    
    var passDataToVideoController:PassDataToChapterVideoViewController?
    var passDataToExerciseController:PassDataToChapterExerciseViewController?
    var passDataToLTPController:PassDataToChapterLTPViewController?
    var passDataToQuestionBankController:PassDataToQuestionBankViewController?
    
    var previousValue:Int = 0
    var currentVideoTotalTimeDuration = 0
    
    var readchapter:((Bool)->())?
    //MARK:- ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(notifyWithVideoSeen), name: Notification.Name(videoSeen), object: nil)
        didLoad()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        playerlayer.frame = playerView.layer.bounds
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        willAppear()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        didDisappearView()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        prepareSegue(segue:segue)
    }
    
    
    //MARK:- Actions
    @IBAction func buttonBackTapped(_ sender: UIButton) {
        self.cameFromChapterScreen = true
        readchapter?(self.selectedTopic?.isChapterRead ?? false)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonViewTapped(_ sender: UIButton) {
        viewVideoRate.superview?.sendSubviewToBack(viewVideoRate)
        buttonQuality.isSelected = false
        DispatchQueue.main.async {
            self.fadePreviousVideoPlayButton()
            self.fadeNextVideoPlayButton()
        }
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(fadeCenterPlayButton), object: sender)
        self.perform(#selector(fadeCenterPlayButton), with: sender, afterDelay: 0.2)
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(fadePlayerButtomView), object: sender)
        self.perform(#selector(fadePlayerButtomView), with: sender, afterDelay: 0.2)
    }
    
    @IBAction func buttonPreviousVideoTapped(_ sender: UIButton) {
        videoChange(sender:sender, isNext: false)
    }
    
    @IBAction func buttonNextVideoTapped(_ sender: UIButton) {
        videoChange(sender:sender, isNext: true)
    }
    
    @IBAction func buttonPlay(_ sender: UIButton) {
        if !self.buttonReplayVideo.isHidden{
            let time:CMTime = CMTimeMake(value: Int64(0.0*1000), timescale: 1000)
            self.player.seek(to: time)
            player.playImmediately(atRate: self.speedType.rawValue)
            playButton.isHidden = false
            playButton.isSelected = false
            buttonPlayButtom.isSelected = false
            self.isPlaying = true
            buttonReplayVideo.isHidden = true
        }else{
            sender.isSelected = !sender.isSelected
            playButton.isSelected = sender.isSelected
            buttonPlayButtom.isSelected = sender.isSelected
            isPlaying ? player.pause():player.playImmediately(atRate: self.speedType.rawValue)
            isPlaying = !isPlaying
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(fadeCenterPlayButton), object: sender)
            self.perform(#selector(fadeCenterPlayButton), with: sender, afterDelay: 0.2)
        }
    }
    
    @IBAction func buttonBackWordTapped(_ sender: UIButton) {
        let currentTime = CMTimeGetSeconds(player.currentTime())
        var newTime = currentTime - 10.0
        if newTime < 0{
            newTime = 0
        }
        let time:CMTime = CMTimeMake(value: Int64(newTime*1000), timescale: 1000)
        self.player.seek(to: time)
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(fadePlayerButtomView), object: sender)
        self.perform(#selector(fadePlayerButtomView), with: sender, afterDelay: 0.2)
    }
    
    @IBAction func buttonForwordTapped(_ sender: UIButton) {
        guard let duration = player.currentItem?.duration else{return}
        let currentTime = CMTimeGetSeconds(player.currentTime())
        let newTime = currentTime + 10.0
        if newTime < (CMTimeGetSeconds(duration) - 10.0){
            let time:CMTime = CMTimeMake(value: Int64(newTime*1000), timescale: 1000)
            player.seek(to: time)
        }
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(fadePlayerButtomView), object: sender)
        self.perform(#selector(fadePlayerButtomView), with: sender, afterDelay: 0.2)
    }
    
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        player.seek(to: CMTimeMake(value: Int64(sender.value*1000), timescale: 1000))
        let totalDuration = (player.currentItem?.duration.seconds.isNaN)! ? 1.0 : player.currentItem?.duration.seconds
        let findMoveValue = Float(sender.value/Float(totalDuration ?? 1.0))
        leadingConsPreviewImage.constant = CGFloat(CGFloat(findMoveValue) * sender.width)
        self.imageViewForCheck.isHidden = false
        self.imageViewForCheck.image = self.player.currentItem?.asset.getPreviewImage(for: Int(sender.value))
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(fadePlayerButtomView), object: sender)
        self.perform(#selector(fadePlayerButtomView), with: sender, afterDelay: 0.01)
        print(Int(sender.value))
    }
    
    @IBAction func buttonReplayTapped(_ sender: UIButton) {
        let time:CMTime = CMTimeMake(value: Int64(0.0*1000), timescale: 1000)
        self.player.seek(to: time)
        player.playImmediately(atRate: self.speedType.rawValue)
        playButton.isHidden = false
        playButton.isSelected = false
        buttonPlayButtom.isSelected = false
        self.isPlaying = true
        sender.isHidden = true
    }
    
    @IBAction func buttonPdfTapped(_ sender: UIButton) {
        let controller = EbookViewController.getController(storyboard: .Home)
        controller.pdfExercise = self.ebook[0]
        controller.selectedTopic = self.selectedTopic
        self.cameFromChapterScreen = false
        controller.cameFor = .showPdf
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func buttonExcelInExamTapped(_ sender: UIButton) {
        self.cameFromChapterScreen = false
        let controller = ExamStartPopUpViewController.getController(storyboard: .Question)
        controller.subjectSelected = self.subjectSelected
        controller.cameFrom = .excelInExam
        controller.selectedTopic = self.selectedTopic
        controller.exercise = self.excelInExam.first
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func buttonRotateTapped(_ sender: UIButton) {
        if UIDevice.current.orientation == .portrait{
            UIDevice.current.setValue(UIInterfaceOrientation.landscapeRight.rawValue, forKey: "orientation")
        }else if UIDevice.current.orientation == .landscapeLeft{
            UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        }else{
            UIDevice.current.setValue(UIInterfaceOrientation.landscapeRight.rawValue, forKey: "orientation")
        }
    }
    @IBAction func buttonSamplePaperTapped(_ sender: UIButton) {
        self.cameFromChapterScreen = false
        let controller = EbookViewController.getController(storyboard: .Home)
        controller.cameFor = .getSamplePaperPdf
        controller.selectedTopic = self.selectedTopic
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func videoQualityTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        sender.isSelected ? viewVideoRate.superview?.bringSubviewToFront(viewVideoRate):viewVideoRate.superview?.sendSubviewToBack(viewVideoRate)
    }
    
    //MARK:- SHOWING VIDEO PREVIEW
    @objc func onSliderValChanged(slider: UISlider, event: UIEvent) {
        if let touchEvent = event.allTouches?.first {
            switch touchEvent.phase {
            case .ended:
                self.imageViewForCheck.isHidden = true
            default:
                break
            }
        }
    }
}

//MARK:-   COLLECTIONVIEW DATASOURCES
extension VideoPlayerViewController:DataSourcesCollectionDelegate{
    func collectionView(_ collectionView: UICollectionView, data: Any?, selectedIndex: IndexPath) {
        switch collectionView {
        case exerciseSegmentCollectionView:
            guard let segmentData = data as? SegmentModel else{return}
            manageSegmentView(selectedSegment:segmentData.type?.rawValue ?? 1)
            exerciseSegmentCollectionView.reloadData()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0001){
                collectionView.scrollToItem(at: selectedIndex, at: .centeredHorizontally, animated: true)
            }
        default:
            return
        }
    }

    //MARK:- ManageSegmentViews
    func manageSegmentView(selectedSegment:Int){
        self.containerViewVideos.isHidden = selectedSegment == 0 ? false:true
        self.containerViewExercise.isHidden = selectedSegment == 1 ? false:true
        self.containerViewLTP.isHidden = selectedSegment == 2 ? false:true
        self.containerViewQuestionBank.isHidden = selectedSegment == 3 ? false:true
        switch selectedSegment {
        case 0:
            self.passDataToVideoController?.passVideoData(data: self.videoDataArray,parentController: self)
        case 1:
            self.passDataToExerciseController?.passExerciseData(data: self.videoDataArray,parentController: self)
        case 2:
            self.passDataToLTPController?.passLTPData(data: self.ltpExam, parentController: self)
        default:
            self.passDataToQuestionBankController?.passQuestionBankData(data: self.mockTest, parentController: self)
        }
    }
}

//MARK:- Life Cycle Methods
extension VideoPlayerViewController{
    //MARK:- VIEW DIDLOAD
    func didLoad(){
        setChapterName()
        timeSlider.addTarget(self, action: #selector(onSliderValChanged(slider:event:)), for: .valueChanged)
        self.statusHeight = Double(getStatusBarHeight())
        statusBarColorwithorentation(headerColor: UIColor.blue, statusView: self.statusView)
        switch cameFrom{
        case .classVideo:
            viewForChapter()
        case .demoVideo:
            return
        }
        self.initViewLoad()
        self.playVideoWithUrl(url:self.videoUrls[0])
        self.currentPlayingVideo = 0
        tableViewPlaybackSpeed.dataPassDelegate = self
    }
    
    //MARK:- VIEW DID DISAPPEAR
    func didDisappearView(){
        if cameFromChapterScreen{
            DispatchQueue.main.async {
                self.player?.pause()
                self.timeSlider.maximumValue = 1.0
                self.timeSlider.minimumValue = 0.0
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.player = nil
                    self.playerlayer.removeFromSuperlayer()
                }
            }
        }else{
            if playerItem.status.rawValue == 1{
                initialVideoLoaded = true
                player?.pause()
            }else{
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.player = nil
                    self.playerlayer.removeFromSuperlayer()
                    self.initialVideoLoaded = false
                }
            }
            player?.pause()
        }
        NotificationCenter.default.post(name: Notification.Name(videoSeen), object: nil)
//        self.videoPerformance()
        
    }
    
    //MARK:- VIEW WILLAPPEAR
    func willAppear(){
        AppUtility.lockOrientation(.all)
        if !cameFromChapterScreen{
            if initialVideoLoaded{
                player?.playImmediately(atRate: self.speedType.rawValue)
                playButton.isSelected = false
                buttonPlayButtom.isSelected = false
                isPlaying = true
            }else{
                self.playVideo(url: self.videoUrls[self.currentPlayingVideo], itemNumber: currentPlayingVideo)
            }
        }
    }
    
    //MARK:- PREPARE SEGUE FOR CONTAINER
    func prepareSegue(segue: UIStoryboardSegue){
        switch segue.identifier {
        case "videoSegue":
            let view = segue.destination as? ChapterVideoViewController
            self.passDataToVideoController = view
            view?.passSelectedVideoData = self
        case "exerciseSegue":
            let view = segue.destination as? ChapterExerciseViewController
            self.passDataToExerciseController = view
        case "liveTestPaperSegue":
            let view = segue.destination as? ChapterLTPViewController
            self.passDataToLTPController = view
        default:
            let view = segue.destination as? ChaptrerQuestionBankViewController
            self.passDataToQuestionBankController = view
        }
    }
    
    //MARK: - STUDENT VIDEO Performance
    func videoPerformance(){
        let userType = String.getString(kUserData.role)
        switch userType {
        case String.getstring(UserRole.schoolStudent.rawValue):
            self.studentVideoPerformance()
        default:
            return
        }
    }
}

//MARK:-  PASS VIDEO DATA TO SOURCES PROTOCOL
extension VideoPlayerViewController:PassVideoDataToSources{
    func passVideoData(_ tableView: UITableView, selectedIndex: Int, data: Any?) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: videoSeen), object: nil)
        let _ = self.videoDataArray.map{$0.isSelected = false}
        self.videoDataArray[selectedIndex].isSelected = true
        tableView.reloadData()
        self.currentPlayingVideo = selectedIndex
        self.playVideo(url: self.videoDataArray[selectedIndex].videoUrlString ?? "https://", itemNumber: selectedIndex)
        self.showTopicName(object: self.videoDataArray[selectedIndex])
        self.buttonReplayVideo.isHidden = true
        self.playButton.isHidden = false
        self.playButton.isSelected = false
        self.buttonPlayButtom.isSelected = false
        self.isPlaying = true
    }
}

extension VideoPlayerViewController:DataSourcesDelegate{
    func tableView(_ tableView: UITableView, selectedIndex: Int, data: Any?) {
        guard let kData = data as? PlayerSpeedModel else {return}
        self.speedModelArray.forEach{$0.isSelected = false}
        kData.isSelected = true
        tableView.reloadData()
        buttonQuality.isSelected = false
        self.speedType = kData.speedType
        viewVideoRate.superview?.sendSubviewToBack(viewVideoRate)
        player?.playImmediately(atRate: self.speedType.rawValue)
    }
}

