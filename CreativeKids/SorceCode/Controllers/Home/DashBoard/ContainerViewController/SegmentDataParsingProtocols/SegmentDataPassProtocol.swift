//
//  SegmentDataPassProtocol.swift
//  CreativeKids
//
//  Created by Creative Kids on 13/07/21.
//

import Foundation
import UIKit

//MARK:- DELEGATE FOR VIDEO CONTAINER
protocol PassDataToChapterVideoViewController{
    func passVideoData(data:Any, parentController:UIViewController)
    func reloadTableForNextPrevious()
}
protocol PassVideoDataToSources{
    func passVideoData(_ tableView:UITableView, selectedIndex: Int, data:Any?)
}

//MARK:- DELEGATE FOR EXERCISE CONTAINER
protocol PassDataToChapterExerciseViewController{
    func passExerciseData(data:Any,parentController:UIViewController)
}
//MARK:- DELEGATE FOR LTP CONTAINER
protocol PassDataToChapterLTPViewController{
    func passLTPData(data:Any,parentController:UIViewController)
}

//MARK:- DELEGATE FOR QUESTION BANK CONTAINER
protocol PassDataToQuestionBankViewController{
    func passQuestionBankData(data:Any,parentController:UIViewController)
}


//MARK:- DEFINE ALL DELEGATE METHOD
extension PassDataToChapterVideoViewController{
    func passVideoData(data:Any, parentController:UIViewController){}
    func reloadTableForNextPrevious(){}
}

extension PassVideoDataToSources{
    func passVideoData(_ tableView:UITableView, selectedIndex: Int, data:Any?){}
}

extension PassDataToChapterExerciseViewController{
    func passExerciseData(data:Any,parentController:UIViewController){}
}

extension PassDataToChapterLTPViewController{
    func passLTPData(data:Any,parentController:UIViewController){}
}

extension PassDataToQuestionBankViewController{
    func passQuestionBankData(data:Any,parentController:UIViewController){}
}
