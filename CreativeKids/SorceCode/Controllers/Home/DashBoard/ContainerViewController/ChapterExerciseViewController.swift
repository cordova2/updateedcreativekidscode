//
//  ChapterExerciseViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 12/07/21.
//

import UIKit

class ChapterExerciseViewController: UIViewController {

    @IBOutlet weak var chapterExerciseTableView: ChapterExerciseTableView!
    var parentController:UIViewController?
    var exerciseContainedTopic:[TopicModel]?
    override func viewDidLoad() {
        super.viewDidLoad()
        chapterExerciseTableView.dataPassDelegate = self
    }
       
}
extension ChapterExerciseViewController:PassDataToChapterExerciseViewController{
    func passExerciseData(data: Any, parentController: UIViewController) {
        self.parentController = parentController
        guard let exerciseData = data as? [TopicModel] else {return}
        exerciseContainedTopic = exerciseData.filter{$0.exercise.count > 0}
        chapterExerciseTableView.getData(cell: TableCellIdentifier.chapterExercisecell, data: exerciseContainedTopic ?? [], language: (self.parentController as! VideoPlayerViewController).subjectSelected?.subjectName ?? "English")
    }
}

extension ChapterExerciseViewController: DataSourcesDelegate{
    func tableViewCallBack(_ tableView: UITableView, selectedIndex: Int, data: Any?, cell: UITableViewCell) {
        guard let cellObject = data as? ExerciseModel  else {return}
        pushCondition(exercise:cellObject.exerciseType ?? "",data:cellObject,selectedTopic:exerciseContainedTopic?[selectedIndex])
    }

    //MARK:- CONDITION FOR EXERCISE TYPE
    func pushCondition(exercise type:String,data:ExerciseModel,selectedTopic:TopicModel?){
        switch type {
        case "MCQ":
            (parentController as! VideoPlayerViewController).cameFromChapterScreen = false
            let controller = MCQViewController.getController(storyboard: .Question)
            controller.selectedTopic = selectedTopic
            controller.exercise = data
            self.navigationController?.pushViewController(controller, animated: true)
        case "DND": 
            (parentController as! VideoPlayerViewController).cameFromChapterScreen = false
            let controller = DNDViewController.getController(storyboard: .Question)
            controller.selectedTopic = selectedTopic
            controller.exercise = data
            self.navigationController?.pushViewController(controller, animated: true)
        default:
            (parentController as! VideoPlayerViewController).cameFromChapterScreen = false
            let controller = VeryShortQuestionViewController.getController(storyboard: .Question)
            controller.selectedTopic = selectedTopic
            controller.exercise = data
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}



