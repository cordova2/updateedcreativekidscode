//
//  SubjectBuyViewController.swift
//  CreativeKids
//
//  Created by Rakesh jha on 28/05/21.
//

import UIKit

class SubjectBuyViewController: UIViewController {
    
    @IBOutlet weak var subjectCollectionView: SubjectCollectionView!
    @IBOutlet weak var labelSelectedClass: UILabel!
    var subjectViewModel:SubjectBuyViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        initialViewLoad()
        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        statusBarColor(headerColor: UIColor.blue)
    }
    func initialViewLoad(){
        labelSelectedClass.text = kUserData.className ?? ""
        subjectCollectionView.dataSourcesCollection = self
        let className = kUserData.className ?? "Class-1"
        let classType = ["Class-9","Class-10"].contains(kUserData.className ?? "") ? ClassType.ncert : ClassType.cordova
        subjectViewModel = SubjectBuyViewModel(viewController: self,forClass: className, classType: classType){ data in
            let unSubscribeData = data.filter{$0.isSubscribed == false}
            self.subjectCollectionView.getData(cell: CollectionCellIdentifier.subject, data: unSubscribeData, language: "English")
            DispatchQueue.main.async {
                self.subjectCollectionView.reloadData()
            }
        }
        
    }
    @IBAction func buttonChangedClass(_ sender: UIButton) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            let controller = ClassSelectionViewController.getController(storyboard: .Registration)
            controller.cameFor = .classSelectForSubScribe
            controller.selectClassCallBack = { (selectedClass,classType) in
                self.subjectViewModel?.getSubjectApi(selectedClass: selectedClass,classType: classType){
                    data in
                    let unSubscribeData = data.filter{$0.isSubscribed == false}
                    self.subjectCollectionView.getData(cell: CollectionCellIdentifier.subject, data: unSubscribeData, language: "English")
                    self.labelSelectedClass.text = selectedClass
                    DispatchQueue.main.async {
                        self.subjectCollectionView.reloadData()
                    }
                }
            }
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @IBAction func buttonBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
//https://creativekidssolutions.com/api/student/?eid=demo@gmail.com&clssa=Class-7
//https://creativekidssolutions.com/api/student/?eid=demo@gmail.com&clssa=Class-10&NCERT
//https://creativekidssolutions.com/api/student/?eid=demo@gmail.com&clssa=Class-10NCERT
// MARK:- CollectionViewViewDataSources
extension SubjectBuyViewController:DataSourcesCollectionDelegate{
    func collectionView(_ collectionView: UICollectionView, height: CGFloat) {
        
    }
    func collectionView(_ collectionView: UICollectionView, data: Any?) {
        guard let selectedSubject = data as? SubjectModel else {return}
        let controller = ChapterViewController.getController(storyboard: .Home)
        controller.subjectSelected = selectedSubject
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
}
