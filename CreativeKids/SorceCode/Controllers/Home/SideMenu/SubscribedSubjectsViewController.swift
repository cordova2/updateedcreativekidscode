//
//  SubscribedSubjectsViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 02/06/21.
//

import UIKit

class SubscribedSubjectsViewController: UIViewController {
    
    @IBOutlet weak var subscribeTableView: UITableView!
    var subscribeSubjectModel = [SubscribeSubjectModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        subscribeTableView.delegate = self
        subscribeTableView.dataSource = self
        subscribeTableView.register(UINib(nibName: TableCellIdentifier.subscribe, bundle: nil), forCellReuseIdentifier: TableCellIdentifier.subscribe)
        setDataForTable()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        statusBarColor(headerColor: UIColor.blue)
    }
    
    func setDataForTable(){
        let totalSubScribeData = kUserData.subscriptionData
        for subjectSubscribe in totalSubScribeData{
            var objectExist = false
            for (index,subject) in subscribeSubjectModel.enumerated(){
                print(index)
                if subject.className == subjectSubscribe.className{
                    objectExist = true
                    subject.subjectSubscribe.append(subjectSubscribe)
                    break
                }
            }
            if !objectExist{
                self.subscribeSubjectModel.append(SubscribeSubjectModel(data: subjectSubscribe))
            }
        }
        self.subscribeTableView.reloadData()
    }
    @IBAction func buttonBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension SubscribedSubjectsViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        subscribeSubjectModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableCellIdentifier.subscribe, for: indexPath) as! SubscribeTableViewCell
        cell.navigationReference = self.navigationController
        cell.configCell(data: subscribeSubjectModel[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(Int(tableView.width) / 3) + CGFloat(65)
    }
}
