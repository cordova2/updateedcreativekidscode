//
//  EditProfileViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 22/03/21.
//

import UIKit
import SkyFloatingLabelTextField

class EditProfileViewController: UIViewController {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var textFieldName: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldEmailID: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldMobileNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet weak var buttonSaveChanges: UIButton!
    @IBOutlet weak var buttonChangePassword: UIButton!
    
    
    //MARK: - Variables
    var changeProfileViewModel:ChangeProfileViewModel?
    
    //MARK: - LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        setGradient()
        let tapOnLearn = UITapGestureRecognizer(target: self, action: #selector(profileChange))
        imageProfile.addGestureRecognizer(tapOnLearn)
        imageProfile.isUserInteractionEnabled = true
        initialSetUp()
        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        statusBarColor(headerColor: UIColor.blue)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppUtility.lockOrientation(.portrait)
        labelUserName.text = kUserData.userName ?? ""
    }
    
    func initialSetUp(){
        buttonSaveChanges.cornerRadius = buttonSaveChanges.height/2
        buttonChangePassword.cornerRadius = buttonChangePassword.height/2
        textFieldName.isUserInteractionEnabled = false
        textFieldName.text = kUserData.userName ?? ""
        textFieldEmailID.text = kUserData.userEmail ?? ""
        textFieldMobileNumber.text = kUserData.mobileNumber ?? ""
        self.imageProfile.downlodeImage(serviceurl: kUserData.profileImage, placeHolder: #imageLiteral(resourceName: "kid_icon"))
        
    }
    func setGradient() {
        DispatchQueue.main.async {
            let gradient: CAGradientLayer = CAGradientLayer()
            gradient.colors = [UIColor.blue.cgColor, UIColor.systemIndigo.cgColor]
            gradient.locations = [0.0 , 1.0]
            gradient.startPoint = CGPoint(x: 0.0, y: 0.3)
            gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
            gradient.frame = self.headerImage.bounds
            self.headerImage.layer.insertSublayer(gradient, at: 0)
        }
    }
    
    //MARK: - Actions
    @IBAction func buttonChangePasswordTapped(_ sender: UIButton) {
        let controller = ResetPasswordViewController.getController(storyboard: .Home)
        self.navigationController?.pushViewController(controller, animated: false)
    }
    @IBAction func buttonBackTapped(_ sender: UIButton) {
        //        kSharedAppManager.moveToHome()
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonEditNameTapped(_ sender: UIButton) {
        textFieldName.isUserInteractionEnabled = true
        textFieldName.becomeFirstResponder()
    }
    @IBAction func buttonSaveChangesTapped(_ sender: UIButton) {
        self.changeProfileViewModel = ChangeProfileViewModel(viewController: self)
    }
    @IBAction func buttonChangeProfileImage(_ sender: UIButton) {
        changeProfile()
    }
    
    
    //MARK:-  Move To Home Screen
    @objc func profileChange(gesture:UITapGestureRecognizer){
        changeProfile()
    }
    
    func changeProfile(){
        print("Change Profile")
        if UIDevice.isPhone{
            ImagePickerHelper.shared.showPickerController { [unowned self] (image) -> (Void) in
                self.imageProfile.image = image
            }
            
        }else{
            Common.showActionSheet(vc: self, firstTitle: "Camera", secondTitle: "Photo Library", sourceView: imageProfile) { selectedIndex in
                if selectedIndex == 0 {
                    ImagePickerHelper.shared.picForiPad(forCamera: true) { (image) -> (Void) in
                        DispatchQueue.main.async {
                            self.imageProfile.image = image
                        }
                    }
                } else {
                    ImagePickerHelper.shared.picForiPad(forCamera: false) { (image) -> (Void) in
                        self.imageProfile.image = image
                    }
                }
            }
        }
    }
}
