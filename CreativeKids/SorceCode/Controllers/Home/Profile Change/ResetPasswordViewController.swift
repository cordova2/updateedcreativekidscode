//
//  ResetPasswordViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 22/03/21.
//

import UIKit
import SkyFloatingLabelTextField
class ResetPasswordViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var textFieldOldPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldNewPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldConfirmPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet weak var buttonSaveChamges: UIButton!
    
    var resetPasswordViewModel:ResetPasswordViewModel?
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setGradient()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        buttonSaveChamges.cornerRadius = buttonSaveChamges.height/2
        AppUtility.lockOrientation(.portrait)
        self.profileImage.sd_setImage(with: URL(string: kUserData.profileImage ?? ""), placeholderImage: #imageLiteral(resourceName: "kid_icon"), options: .continueInBackground, completed: nil)
        labelUserName.text = kUserData.userName ?? ""
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        statusBarColor(headerColor: UIColor.blue)
    }
    func setGradient(){
        DispatchQueue.main.async {
            let gradient: CAGradientLayer = CAGradientLayer()
            gradient.colors = [UIColor.blue.cgColor, UIColor.systemIndigo.cgColor]
            gradient.locations = [0.0 , 1.0]
            gradient.startPoint = CGPoint(x: 0.0, y: 0.3)
            gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
            gradient.frame = self.headerImage.bounds
            self.headerImage.layer.insertSublayer(gradient, at: 0)
        }
    }
    
    //MARK: - IBActions
    @IBAction func buttonEyeOldPasswordTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        textFieldOldPassword.isSecureTextEntry = !sender.isSelected
    }
    @IBAction func buttonEyeNewPasswordTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        textFieldNewPassword.isSecureTextEntry = !sender.isSelected
    }
    @IBAction func buttonBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func buttonEyeConfirmPasswordTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        textFieldConfirmPassword.isSecureTextEntry = !sender.isSelected
    }
    @IBAction func buttonSaveChangesTapped(_ sender: UIButton) {
        resetPasswordViewModel = ResetPasswordViewModel(viewController: self)
    }
    
}
