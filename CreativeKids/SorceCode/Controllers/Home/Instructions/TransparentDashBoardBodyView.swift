//
//  TransparentDashBoardBodyView.swift
//  CreativeKids
//
//  Created by Creative Kids on 13/09/21.
//

import Foundation
import UIKit
import Instructions

// Transparent coach mark (text without background, cool arrow)
internal class TransparentDashboardBodyView: UIControl, CoachMarkBodyView {
    // MARK: - Internal properties
    var nextControl: UIControl? { return self }

    weak var highlightArrowDelegate: CoachMarkBodyHighlightArrowDelegate?

    var hintLabel = UITextView()

    // MARK: - Initialization
    override init (frame: CGRect) {
        super.init(frame: frame)

        self.setupInnerViewHierarchy()
    }

    convenience init() {
        self.init(frame: CGRect.zero)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("This class does not support NSCoding.")
    }

    // MARK: - Private methods
    private func setupInnerViewHierarchy() {
        self.translatesAutoresizingMaskIntoConstraints = false

        hintLabel.backgroundColor = UIColor.clear
        hintLabel.textColor = UIColor.white
        hintLabel.font = UIFont.systemFont(ofSize: 15.0)
        hintLabel.isScrollEnabled = false
        hintLabel.textAlignment = .justified
        #if targetEnvironment(macCatalyst)
        #else
        hintLabel.layoutManager.hyphenationFactor = 1.0
        #endif
        hintLabel.isEditable = false

        hintLabel.translatesAutoresizingMaskIntoConstraints = false
        hintLabel.isUserInteractionEnabled = false

        self.addSubview(hintLabel)
        hintLabel.fillSuperview()
    }
}

extension UIView {
    internal func fillSuperview() {
        fillSuperviewVertically()
        fillSuperviewHorizontally()
    }

    internal func fillSuperviewVertically() {
        guard let superview = superview else { return }

        self.topAnchor.constraint(equalTo: superview.topAnchor).isActive = true
        self.bottomAnchor.constraint(equalTo: superview.bottomAnchor).isActive = true
    }

    internal func fillSuperviewHorizontally() {
        guard let superview = superview else { return }

        self.leadingAnchor.constraint(equalTo: superview.leadingAnchor).isActive = true
        self.trailingAnchor.constraint(equalTo: superview.trailingAnchor).isActive = true
    }
}
