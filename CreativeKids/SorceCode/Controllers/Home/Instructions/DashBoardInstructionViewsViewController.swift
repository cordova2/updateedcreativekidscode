//
//  DashBoardInstructionViewsViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 13/09/21.
//

import Foundation
import UIKit
import Instructions


// This class mix different kind of coach marks together.
internal class DashBoradInstructionViewController: DashboardViewController {
    // MARK: - IBOutlet
//    @IBOutlet weak var answersLabel: UILabel!
//
//    // MARK: - Private properties
//    private let swipeImage = UIImage(named: "swipe")
//
//    private let answersText = "That's the number of answers you gave."

    // MARK: - View Lifecycle
    var numberOfTour = 4
    override func viewDidLoad() {
        super.viewDidLoad()
        self.coachMarksController.dataSource = self
        self.coachMarksController.overlay.backgroundColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.7)
    }
}

// MARK: - Protocol Conformance | CoachMarksControllerDataSource
extension DashBoradInstructionViewController: CoachMarksControllerDataSource {
    func numberOfCoachMarks(for coachMarksController: CoachMarksController) -> Int {
        return numberOfTour
    }

    func coachMarksController(_ coachMarksController: CoachMarksController,
                              coachMarkAt index: Int) -> CoachMark {

        var coachMark: CoachMark

        switch index {
        case 0:
            coachMark = coachMarksController.helper.makeCoachMark(for: viewClass)
            coachMark.arrowOrientation = .top
        case 1:
            coachMark = coachMarksController.helper.makeCoachMark(for: viewGetProgress)
            coachMark.arrowOrientation = .top
        case 2:
            coachMark = coachMarksController.helper.makeCoachMark(for: buttonSideMenu)
            coachMark.arrowOrientation = .top
        case 3:
            coachMark = coachMarksController.helper.makeCoachMark(for: self.firstSubjectCell)
            coachMark.arrowOrientation = .bottom
            
        case 4:
          self.upperScrollView.scrollToBottomScrollView()
            coachMark = coachMarksController.helper.makeCoachMark(for: (self.navigationController as! HomeNavigationController).voiceView?.viewInner)
            coachMark.arrowOrientation = .bottom
                
//        case 3:
//            // A frame rectangle can also be passed.
//            coachMark = coachMarksController.helper.makeCoachMark(forFrame: answersLabel.frame,
//                                                                  in: answersLabel.superview)
//        case 4:
//            coachMark = coachMarksController.helper.makeCoachMark(for: reputationLabel)
        default:
            coachMark = coachMarksController.helper.makeCoachMark()
        }
        coachMark.gapBetweenCoachMarkAndCutoutPath = 6.0
        return coachMark
    }

    func coachMarksController(
        _ coachMarksController: CoachMarksController,
        coachMarkViewsAt index: Int,
        madeFrom coachMark: CoachMark
    ) -> (bodyView: (UIView & CoachMarkBodyView), arrowView: (UIView & CoachMarkArrowView)?) {

        var bodyView: UIView & CoachMarkBodyView
        var arrowView: (UIView & CoachMarkArrowView)?

        switch index {
        case 0: (bodyView, arrowView) = createViews4(from: coachMark,Message: "You can change your class by clicking on \"select class\".",index: index)
        case 1: (bodyView, arrowView) = createViews4(from: coachMark,Message: "You can see you progress report in detail by click on it.",index: index)
        case 2: (bodyView, arrowView) = createViews4(from: coachMark,Message: "You can quick access and change you account detail by click on \"side menu\".",index: index)
        case 3: (bodyView, arrowView) = createViews4(from: coachMark,Message: "You can choose any of following subject which you want to learn.",index: index)
        case 4: (bodyView, arrowView) = createViews4(from: coachMark,Message: "You can get any chapter by voice search",index: index)
//      case 3: (bodyView, arrowView) = createViews4(from: coachMark)
//      case 4: (bodyView, arrowView) = createViews4(from: coachMark)
        default: (bodyView, arrowView) = createDefaultViews(from: coachMark)
        }
        return (bodyView: bodyView, arrowView: arrowView)
    }

    private func createViews4(
        from coachMark: CoachMark
        ,Message:String,index:Int) -> (bodyView: (UIView & CoachMarkBodyView), arrowView: (UIView & CoachMarkArrowView)?) {
        let coachMarkBodyView = TransparentDashboardBodyView()
        var coachMarkArrowView: TransparentDashBoardArrowView?

        coachMarkBodyView.hintLabel.text = Message

        if let arrowOrientation = coachMark.arrowOrientation {
//            arrowOrientation = .bottom
            coachMarkArrowView = TransparentDashBoardArrowView(orientation: arrowOrientation)
        }
        if index == numberOfTour - 1{
        kSharedUserDefaults.setAppTourDone(tourDone: true)
        }
        return (coachMarkBodyView, coachMarkArrowView)
    }

    private func createDefaultViews(
        from coachMark: CoachMark
    ) -> (bodyView: (UIView & CoachMarkBodyView), arrowView: (UIView & CoachMarkArrowView)?) {
        let coachViews = coachMarksController.helper.makeDefaultCoachViews(
            withArrow: true,
            arrowOrientation: coachMark.arrowOrientation
        )

        return (coachViews.bodyView, coachViews.arrowView)
    }
}

private extension DashBoradInstructionViewController {
    var borderColor: UIColor {
        let defaultColor = #colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1)

        if #available(iOS 13.0, *) {
            return UIColor { (traits) -> UIColor in
                if traits.userInterfaceStyle == .dark {
                    return #colorLiteral(red: 0.3176470697, green: 0.07450980693, blue: 0.02745098062, alpha: 1)
                } else {
                    return defaultColor
                }
            }
        } else {
            return defaultColor
        }
    }

    var highlightedBorderColor: UIColor {
        let defaultColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)

        if #available(iOS 13.0, *) {
            return UIColor { (traits) -> UIColor in
                if traits.userInterfaceStyle == .dark {
                    return #colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1)
                } else {
                    return defaultColor
                }
            }
        } else {
            return defaultColor
        }
    }

    var innerColor: UIColor {
        let defaultColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)

        if #available(iOS 13.0, *) {
            return UIColor { (traits) -> UIColor in
                if traits.userInterfaceStyle == .dark {
                    return #colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1)
                } else {
                    return defaultColor
                }
            }
        } else {
            return defaultColor
        }
    }

    var highlightedInnerColor: UIColor {
        let defaultColor = #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1)

        if #available(iOS 13.0, *) {
            return UIColor { (traits) -> UIColor in
                if traits.userInterfaceStyle == .dark {
                    return #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
                } else {
                    return defaultColor
                }
            }
        } else {
            return defaultColor
        }
    }
}


extension UIScrollView {

    // Scroll to a specific view so that it's top is at the top our scrollview
    func scrollToView(view:UIView, animated: Bool) {
        if let origin = view.superview {
            // Get the Y position of your child view
            let childStartPoint = origin.convert(view.frame.origin, to: self)
            // Scroll to a rectangle starting at the Y of your subview, with a height of the scrollview
            self.scrollRectToVisible(CGRect(x:0, y:childStartPoint.y,width: 1,height: self.frame.height), animated: animated)
        }
    }

    // Bonus: Scroll to top
    func scrollToTopOfScrollView(animated: Bool) {
        let topOffset = CGPoint(x: 0, y: -contentInset.top)
        setContentOffset(topOffset, animated: animated)
    }

    // Bonus: Scroll to bottom
    func scrollToBottomScrollView() {
        let bottomOffset = CGPoint(x: 0, y: contentSize.height - bounds.size.height + contentInset.bottom)
        if(bottomOffset.y > 0) {
            setContentOffset(bottomOffset, animated: true)
        }
    }

}
