//
//  ExcelExamViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 30/03/21.
//

import UIKit

class ExcelExamViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var questionCountLable: UILabel!
    @IBOutlet weak var questionCollectionView: UICollectionView!
    @IBOutlet weak var labelHeader: UILabel!
    
    //MARK: - Variables
    var excelExamModel = [ExcelExamQuestionModel]()
    var excelViewModel: ExcelExamViewModel?
    var subjectSelected:SubjectModel?
    var exercise:ExerciseModel?
    var selectedTopic:TopicSearchModel?
    var timer: Timer?
    var targetTime = Date()
    var count = 0
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitial()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppUtility.lockOrientation(.portrait)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        statusBarColor(headerColor: UIColor.blue)
    }
    
    //MARK: - Initial Setup
    func setupInitial() {
        labelHeader.text = self.selectedTopic?.chapterName
        questionCollectionView.delegate = self
        questionCollectionView.dataSource = self
        
        self.excelViewModel = ExcelExamViewModel(viewController: self, completionHandeler: {
            if self.excelExamModel.count == 0 {
                self.timerStop()
            } else {
                self.setTarget()
            }
            self.questionCountLable.text = "\(self.count + 1)/\(self.excelExamModel.count)"
            self.questionCollectionView.reloadData()
        })
       
    }
    
    //MARK: - SetTargetForTime
    func setTarget(forSeconds seconds: Int = 0) {
        self.startTimer()
        targetTime = Calendar.current.date(byAdding: .second, value: seconds, to: Date()) ?? Date()
    }
    
    //MARK: - Start Timer
    func startTimer () {
        timer?.invalidate()
        var sec = Calendar.current.dateComponents([.second], from: Date(), to: self.targetTime).second!
        if sec == 0 {
            timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (timer) in
                sec += 1
                self.timeLabel.text = CommonUtils.timeString(time: TimeInterval(sec))
            }
        }
    }
    
    //MARK: - StopTimer
    func timerStop() {
        timer?.invalidate()
    }
    
    //MARK: - QuestionsCount
    func setQuestionCountsWithScroll(questions: IndexPath) {
        self.count = questions.item + 1
        self.questionCountLable.text = "\(self.count + 1)/\(self.excelExamModel.count)"
        let scrollIndexPath = IndexPath(item: questions.item + 1, section: 0)
        self.questionCollectionView.scrollToItem(at: scrollIndexPath, at: .centeredHorizontally, animated: true)
    }
    
    //MARK: - Action
    @IBAction func buttonBackTapped(_ sender: UIButton) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: VideoPlayerViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
}

//MARK: - UICollectionViewDelegateFlowLayout, UICollectionViewDataSource
extension ExcelExamViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return excelExamModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionCellIdentifier.excelExam, for: indexPath) as! ExcelExamCollectionViewCell
        cell.selectedTopic = self.selectedTopic
        cell.configCell(ExcelExamModel: excelExamModel[indexPath.row])
        cell.submitButton.setTitle(indexPath.item == self.excelExamModel.count - 1 ? "Done":"Submit", for: .normal)
        cell.callBackSubmit = {
            if indexPath.item == self.excelExamModel.count - 1 {
                self.questionCollectionView.isScrollEnabled = false
                self.timerStop()
                let controller = GetPDFViewController.getController(storyboard: .Question)
                controller.selectedTopic = self.selectedTopic
                controller.exercise = self.exercise
                controller.isAllQuestionCompleted = true
                self.navigationController?.pushViewController(controller, animated: true)
            } else {
                self.setQuestionCountsWithScroll(questions: indexPath)
            }
        }
        cell.getPdfSubmit = {
            let controller = GetPDFViewController.getController(storyboard: .Question)
            controller.selectedTopic = self.selectedTopic
            controller.exercise = self.exercise
            controller.isAllQuestionCompleted = false
            self.navigationController?.pushViewController(controller, animated: true)
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
}
