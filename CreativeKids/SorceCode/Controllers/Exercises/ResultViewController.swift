//
//  ResultViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 26/03/21.
//

import UIKit

class ResultViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var questionView: UIView!
    @IBOutlet weak var scoreView: UIView!
    @IBOutlet weak var labelScore: UILabel!
    @IBOutlet weak var labeltotalQuestion: UILabel!
    @IBOutlet weak var labeltotalTime: UILabel!
    @IBOutlet weak var labelResultType: UILabel!
    @IBOutlet weak var viewPdf: UIView!
    
    @IBOutlet weak var labelStudentName: UILabel!
    @IBOutlet weak var labelClass: UILabel!
    @IBOutlet weak var labelSubjectName: UILabel!
    
    @IBOutlet weak var labelChapterName: UILabel!
    @IBOutlet weak var labelExerciseName: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    
    //MARK: - variables
    var modelArr = [VSAQQuestionModel]()
    var subjectSelected:SubjectModel?
    var exercise:ExerciseModel?
    var totalQuestion = 0
    var marksObtained = 0
    var totalMarks = 0
    var totlaTime: String?
    var setId: String?
    var correctCount = 0
    
    var resultExercise: SendExerciseResultView?
    var selectedTopic:TopicSearchModel?
    var name = "jashdbfjhsdbfjhsd"
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configInitial()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        statusBarColor(headerColor: UIColor.blue)
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.questionView.layer.addBorder(edge: UIRectEdge.right, color: .black, thickness: 1.5)
        self.scoreView.layer.addBorder(edge: UIRectEdge.right, color: .black, thickness: 1.5)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppUtility.lockOrientation(.portrait)
    }
    
    //MARK: - Initial Setup
    func configInitial() {
        let correctAnswer = modelArr.filter{$0.rightAns == $0.userSelectedOption}
        totalMarks = self.modelArr.reduce(0){$0 + Int.getIntValue($1.Marks)}
        marksObtained = correctAnswer.reduce(0){$0 + Int.getIntValue($1.Marks)}
        totalQuestion = modelArr.count
        correctCount = correctAnswer.count
        self.setId = self.modelArr.first?.setid
        
        self.labelScore.text = "\(correctAnswer.count)"
        self.labeltotalQuestion.text = "\(modelArr.count)"
        self.labeltotalTime.text = kSharedUserDefaults.string(forKey: "time_count")
        self.totlaTime = String.getstring(kSharedUserDefaults.string(forKey: "time_count_Background_Exercise"))
        let percentage = Float(correctAnswer.count * 100) / Float(modelArr.count)
        let convertInt = Int(percentage)
        labelStudentName.text = "Name: \((kUserData.userName != "" ? kUserData.userName : "NA") ?? "NA")"
        labelClass.text =  subjectSelected?.className != "" ? subjectSelected?.className : "NA"
        labelSubjectName.text = "Subject: \((subjectSelected?.subjectName != "" ? subjectSelected?.subjectName : "NA") ?? "NA")"
        labelChapterName.text = "Chapter: \((modelArr.first?.chapter != "" ? modelArr.first?.chapter : "NA") ?? "NA")"
        labelExerciseName.text = exercise?.exerciseType != "" ? "\(exercise?.exerciseType ?? "") \(exercise?.exerciseNumber ?? 1)" : "NA"
        labelDate.text = "Date: \(Date().dateString())"
        if convertInt < 33 {
            self.labelResultType.text = "Not Passed\n Try again"
        } else if convertInt >= 33 && convertInt <= 44 {
            self.labelResultType.text = "Passed\n3rd Divison"
        } else if convertInt >= 45 && convertInt <= 59 {
            self.labelResultType.text = "Passed\n2nd Divison"
        } else if convertInt >= 60 && convertInt <= 74 {
            self.labelResultType.text = "Passed\n1st Divison"
        } else {
            self.labelResultType.text = "Passed\nDistinction"
        }
        if isHindiSubject(subejct: subjectSelected?.subjectName ?? "") {
            labelChapterName.setfont(font: 15, fontFamily: chanakyaFont)
            labelChapterName.text =  ConstantHindiText.paath + " \((modelArr.first?.chapter != "" ? modelArr.first?.chapter : "NA") ?? "NA")"
        } else {
            labelChapterName.text = "Chapter: \((modelArr.first?.chapter != "" ? modelArr.first?.chapter : "NA") ?? "NA")"
        }
        
        studentPerformance()
    }
    
    //MARK: - Button Action
    @IBAction func buttonReviewTapped(_ sender: UIButton) {
        print(viewPdf.exportAsPdfFromView())
        let controller = ReviewViewController.getController(storyboard: .Question)
        controller.reviewModel = self.modelArr
        controller.subjectSelected = self.subjectSelected
        controller.exercise = self.exercise
        controller.ltpResultPdf = viewPdf.exportAsPdfFromView()
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func buttonNextTapped(_ sender: UIButton) {
       
        self.resultExercise = SendExerciseResultView(viewController: self, completionHandler: {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: VideoPlayerViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        })
    }
    
    //MARK: Exercise Performance For Student
    
    func studentPerformance(){
        let userType = String.getString(kUserData.role)
        switch userType {
        case String.getstring(UserRole.schoolStudent.rawValue):
            let exeType = String.getString(exercise?.exerciseType)
            let setid = String.getString(self.setId)
            let totalQue = String.getString(self.totalQuestion)
            let marks = String.getString(self.totalMarks)
            let scoMarks = String.getString(self.marksObtained)
            let correctAnswer = String.getString(self.correctCount)
            let totalTimeSpend = String.getString(self.totlaTime)
            //        print("\(exeType)--\(setid)--\(totalQue)--\(marks)--\(scoMarks)--\(correctAnswer)--\(totalTimeSpend)")
            let performanceData = PerformanceData(exerciseType: exeType, setId: setid, totalQuestion: totalQue, totalMarks: marks, scoredMarks: scoMarks, totalCorrectAnswer: correctAnswer, totalTimeSpend: totalTimeSpend)
            let performanceModel = StudentPerformance()
            performanceModel.LtpMockPerforManceApi(data: selectedTopic!,performanceData: performanceData)
        default:
            return
        }
    }

}

