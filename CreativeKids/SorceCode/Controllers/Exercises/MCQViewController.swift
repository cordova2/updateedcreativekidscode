//
//  MCQViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 23/03/21.
//

import UIKit
import KDCircularProgress

class MCQViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var questionsCollectionView: UICollectionView!
    @IBOutlet weak var labelQuestionCount: UILabel!
    @IBOutlet weak var timeView: KDCircularProgress!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelMarks: UILabel!
    @IBOutlet weak var labelHeader: UILabel!
    @IBOutlet weak var labelHeaderAbhyaas: UILabel!
    @IBOutlet weak var labelMaximumMarks: UILabel!
    //MARK: - Variables
    var count = 0
    var countdown = 0
    var timer: Timer?
    var targetTime = Date()
    var checkAns = 0
    var obtainedMarks = 0
    var maximumMarks = 0
    var abhyaasCheck = ""
    var bcakgroundTimer: Timer?
    var targetTimeBack = Date()
    
    var mcqDataModel: MCQModelView?
    var mcqQuestionsModel = [MCQQuestionModel]()
    var selectedTopic:TopicModel?
    var exercise:ExerciseModel?
    
    
    //MARK: - LifeCycles
    override func viewDidLoad() {
        super.viewDidLoad()
        let controller = StartTimeoutViewController.getController(storyboard: .Question)
        controller.modalTransitionStyle = .crossDissolve
        controller.modalPresentationStyle = .overCurrentContext
        controller.changeButtonText = "Start"
        controller.dismissCallBack = {
            self.setInitialSetup()
        }
        self.present(controller, animated: true, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timerStop()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        statusBarColor(headerColor: UIColor.blue)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppUtility.lockOrientation(.portrait)
    }
    
    //MARK: - InitialSetup
    func setInitialSetup() {
        questionsCollectionView.delegate = self
        questionsCollectionView.dataSource = self
        animatedViewSetup()
        
        self.mcqDataModel = MCQModelView(vc: self, completionHandler: {
            if self.mcqQuestionsModel.count == 0 {
                self.timerStop()
                self.timerStopBackground()
            } else {
                self.setTarget()
                self.setTargetBackground()
            }
            self.labelQuestionCount.text = "1/\(self.mcqQuestionsModel.count)"
            self.maximumMarks = self.mcqQuestionsModel.reduce(0){$0 + Int.getIntValue($1.Marks)}
            self.labelMaximumMarks.text = "MM:\(self.maximumMarks)"
            self.setHeaderLabel()
            self.questionsCollectionView.reloadData()
        })
    }
    
    //MARK: - SET HEADER LABEL
    func setHeaderLabel() {
        if isHindiSubject(subejct:selectedTopic?.subjectName ?? ""){
            self.labelHeader.setfont(font: 18, fontFamily: chanakyaFont)
            if isContainNumber(checkString:  self.selectedTopic?.topic ?? ""){
            let myQuestionNumber = "\(getNumericFromString(stringWithNumber: self.selectedTopic?.topic ?? "") ?? 0)"
            let myNumberAttribute = [ NSAttributedString.Key.font: UIFont(name: futuraFont, size: 11.0)!]
            let myAttrNumberString = NSAttributedString(string: myQuestionNumber, attributes: myNumberAttribute)
            let removedNumber = removeNumberFromString(stringWithNumber: self.selectedTopic?.topic ?? "")
            let myAttribute = [ NSAttributedString.Key.font: UIFont(name: chanakyaFont, size: 18.0)!]
            let myAttrQuestionString = NSAttributedString(string: removedNumber, attributes: myAttribute)
            self.labelHeader.attributedText =  myAttrQuestionString + myAttrNumberString
                
            }else{
                self.labelHeader.text = (self.selectedTopic?.topic ?? "")
            }
            self.labelHeaderAbhyaas.text = "( अभ्यास \(exercise?.exerciseNumber ?? 0))"
        } else {
            self.labelHeader.setfont(font: 18, fontFamily: futuraFont)
            self.labelHeader.text = (self.selectedTopic?.topic ?? "") + "(\(self.mcqQuestionsModel.first?.exercise ?? ""))".replacingOccurrences(of: "()", with: "")
        }
        
    }
    
    //MARK: - Animated CircularView Setup
    func animatedViewSetup() {
        timeView.startAngle = -90
        timeView.progressThickness = 0.3
        timeView.trackThickness = 0.3
        timeView.clockwise = true
        timeView.gradientRotateSpeed = 2
        timeView.roundedCorners = false
        timeView.glowMode = .forward
        timeView.glowAmount = 0.5
        timeView.set(colors: .systemGreen)
        timeView.center = CGPoint(x: view.center.x + 10, y: view.center.y + 15)
    }
    
    //MARK: - setTargetForTime
    func setTarget(forSeconds seconds: Int = 20) {
        targetTime = Calendar.current.date(byAdding: .second, value: seconds, to: Date()) ?? Date()
        self.startTimer()
        self.animatedView()
    }
    
    func startTimer () {
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (timer) in
            let sec = Calendar.current.dateComponents([.second], from: Date(), to: self.targetTime).second!
            let secondFormat = sec < 10 ? "0\(sec)" : "\(sec)"
            self.labelTime.text = "00:\(secondFormat)"
            if sec == 0 {
                timer.invalidate()
                let controller = StartTimeoutViewController.getController(storyboard: .Question)
                controller.modalTransitionStyle = .crossDissolve
                controller.modalPresentationStyle = .overCurrentContext
                controller.changeButtonText = "TimeOut"
                controller.dismissCallBack = {
                    self.setTarget(forSeconds: 20)
                    let scrollIndexPath = IndexPath(item: self.count, section: 0)
                    self.setQuestionCountsWithScroll(questions: scrollIndexPath)
                }
                self.present(controller, animated: true, completion: nil)
            }
        }
    }
    
    //MARK: - StopTimer
    func timerStop() {
        timer?.invalidate()
        self.timeView.pauseAnimation()
    }
    //MARK: - SetTargetForTime
    func setTargetBackground(forSeconds seconds: Int = 0) {
        targetTimeBack = Calendar.current.date(byAdding: .second, value: seconds, to: Date()) ?? Date()
        startTimerBackground()
    }
    //MARK: - Start Timer
    func startTimerBackground() {
        var sec = Calendar.current.dateComponents([.second], from: Date(), to: self.targetTimeBack).second!
        if sec == 0 {
            bcakgroundTimer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (timer) in
                sec += 1
                //self.timeLabel.text = CommonUtils.timeString(time: TimeInterval(sec))
                kSharedUserDefaults.setValue(sec, forKey: "time_count_Background_Exercise")
            }
        }
        
    }
    //MARK: - StopTimer
    func timerStopBackground() {
        bcakgroundTimer?.invalidate()
    }
    
    //MARK: - QuestionsCount
    func setQuestionCountsWithScroll(questions: IndexPath) {
        if questions.item == self.mcqQuestionsModel.count - 1 {
            self.moveToResultOnLast()
        } else {
        self.count = questions.item + 1
        let currentQuestion = questions.item + 2
        self.labelQuestionCount.text = "\(currentQuestion)/\(self.mcqQuestionsModel.count)"
        let scrollIndexPath = IndexPath(item: questions.item + 1, section: 0)
        self.questionsCollectionView.scrollToItem(at: scrollIndexPath, at: .centeredHorizontally, animated: true)
        }
    }
    func moveToResultOnLast() {
        self.timerStopBackground()
        let controller = DNDResultViewController.getController(storyboard: .Question)
        controller.MCQmodelArr = self.mcqQuestionsModel
        controller.correctMcq = self.checkAns
        controller.marksObtained = self.obtainedMarks
        controller.totalMarks = self.maximumMarks
        controller.camefrom = .MCQType
        controller.exercise = self.exercise
        controller.selectedTopic = self.selectedTopic
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    //MARK: - AnimatedCircularView
    func animatedView() {
        self.timeView.animate(fromAngle: 360, toAngle: 0, duration: 20) { completed in
            if completed {
                
                print("animation stopped, completed")
            } else {
                print("animation stopped, was interrupted")
            }
        }
    }
    
    //MARK: -Action
    @IBAction func buttonBacktapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: - UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension MCQViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.mcqQuestionsModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionCellIdentifier.mcq, for: indexPath) as! McqCollectionCell
        let object = mcqQuestionsModel[indexPath.item]
        cell.selectedTopic = self.selectedTopic
        cell.mcqModelArr = mcqQuestionsModel
        cell.configCollectionCell(questions: object,index: indexPath)
        cell.checkAns = self.checkAns
        cell.nextCallback = {
            if indexPath.item == self.mcqQuestionsModel.count - 1 {
                self.moveToResultOnLast()
            } else {
                self.setQuestionCountsWithScroll(questions: indexPath)
                cell.buttonConfirmNext.setTitle("Confirm", for: .normal)
                self.setTarget(forSeconds: 20)
            }
        }
        cell.selectedOptionCallback = {
            if object.rightAns == cell.buttonOptionsA.tag && cell.buttonOptionsA.isSelected {
                self.obtainedMarks = self.obtainedMarks + Int.getIntValue(object.Marks)
                self.checkAns = self.checkAns + 1
                self.labelMarks.text = "Score: \(self.obtainedMarks)"
            } else if object.rightAns == cell.buttonOptionsB.tag && cell.buttonOptionsB.isSelected  {
                self.obtainedMarks = self.obtainedMarks + Int.getIntValue(object.Marks)
                self.checkAns = self.checkAns + 1
                self.labelMarks.text = "Score: \(self.obtainedMarks)"
            } else if object.rightAns == cell.buttonOptionsC.tag && cell.buttonOptionsC.isSelected  {
                self.obtainedMarks = self.obtainedMarks + Int.getIntValue(object.Marks)
                self.checkAns = self.checkAns + 1
                self.labelMarks.text = "Score: \(self.obtainedMarks)"
            } else if object.rightAns == cell.buttonOptionsD.tag && cell.buttonOptionsD.isSelected  {
                self.obtainedMarks = self.obtainedMarks + Int.getIntValue(object.Marks)
                self.checkAns = self.checkAns + 1
                self.labelMarks.text = "Score: \(self.obtainedMarks)"
            }
            if cell.buttonOptionsA.isSelected || cell.buttonOptionsB.isSelected || cell.buttonOptionsC.isSelected || cell.buttonOptionsD.isSelected {
                self.timerStop()
            }
        }
        
        return cell
    }
    
    //MARK:- COLLECTION HEIGHT
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
}

