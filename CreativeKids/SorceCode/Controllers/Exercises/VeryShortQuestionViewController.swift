//
//  VeryShortQuestionViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 23/03/21.
//

import UIKit

class VeryShortQuestionViewController: UIViewController {
    //MARK: - Outlets
    @IBOutlet weak var questionsCollectionView: UICollectionView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var chapterLabel: UILabel!
    @IBOutlet weak var labelAbyaas: UILabel!
    //MARK: - Variables
    var VSAQDataModel: VSAQViewModel?
    var VSAQQuestionsModel = [VSAQQuestionModel]()
    var selectedTopic:TopicModel?
    var exercise:ExerciseModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setInitialSetup()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        statusBarColor(headerColor: UIColor.blue)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppUtility.lockOrientation(.portrait)
    }
    //MARK: - Initial Setup
    func setInitialSetup() {
        initialLoad()
        questionsCollectionView.delegate = self
        questionsCollectionView.dataSource = self
        
        self.VSAQDataModel = VSAQViewModel(viewController: self, completionHandler: {
            DispatchQueue.main.async {
                if isHindiSubject(subejct:self.selectedTopic?.subjectName ?? ""){
                    self.chapterLabel.setfont(font: 18, fontFamily: chanakyaFont)
                    if isContainNumber(checkString: self.selectedTopic?.topic ?? ""){
                        self.chapterLabel.attributedText = makeNewNumericString(StringWithNumber: self.selectedTopic?.topic ?? "")
                    }else{
                        self.chapterLabel.text = self.selectedTopic?.topic ?? ""
                    }
                    self.labelAbyaas.text = "( अभ्यास \(getNumericFromString(stringWithNumber: self.VSAQQuestionsModel.first?.exercise ?? "") ?? 0) )"
                } else {
                    self.chapterLabel.setfont(font: 18, fontFamily: futuraFont)
                    self.chapterLabel.text = (self.selectedTopic?.topic ?? "") + "(\(self.VSAQQuestionsModel.first?.exercise ?? ""))".replacingOccurrences(of: "()", with: "")
                }
                self.questionsCollectionView.reloadData()
            }
        })
    }
    
    
    fileprivate func initialLoad(){
        if isHindiSubject(subejct:selectedTopic?.subjectName ?? ""){
            headerLabel.setfont(font: 16, fontFamily: chanakyaFont)
            self.chapterLabel.setfont(font: 16, fontFamily: chanakyaFont)
            forHindi()
        }else{
            headerLabel.setfont(font: 16, fontFamily: futuraFont)
            self.chapterLabel.setfont(font: 16, fontFamily: futuraFont)
            forEnglish()
        }
    }
    //MARK: - FONT SET FOR ENGLISH
    func forEnglish(){
        switch exercise?.exerciseType {
        case "VSAQ":
            self.headerLabel.text = "Very Short Answer Question"
        case "SAQ":
            self.headerLabel.text = "Short Answer Question"
        case "LAQ":
            self.headerLabel.text = "Long Answer Question"
        default:
            return
        }
    }
    
    //MARK: - FONT SET FOR HINDI
    func forHindi(){
        switch exercise?.exerciseType {
        case "VSAQ":
            self.headerLabel.text = "लघु उत्तरीय प्रश्न और उत्तर"
        case "SAQ":
            self.headerLabel.text = "लघु उत्तरीय प्रश्न"
        case "LAQ":
            self.headerLabel.text = "दीर्घ उत्तरीय प्रश्न"
        default:
            return
        }
    }
    
    //MARK: - Action
    @IBAction func buttonBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: - UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension VeryShortQuestionViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return VSAQQuestionsModel.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionCellIdentifier.shortAnswer, for: indexPath) as! ShortAnswerCell
        let object = VSAQQuestionsModel[indexPath.row]
        cell.nextButton.setTitle(indexPath.item == self.VSAQQuestionsModel.count - 1 ? "Done": "Next", for: .normal)
        cell.nextBtnCallback = {
            if indexPath.item == self.VSAQQuestionsModel.count - 1 {
                self.questionsCollectionView.isScrollEnabled = false
                self.navigationController?.popViewController(animated: true)
            } else {
                let scrollIndexPath = IndexPath(item: indexPath.item + 1, section: 0)
                self.questionsCollectionView.scrollToItem(at: scrollIndexPath, at: .centeredHorizontally, animated: true)
            }
        }
        cell.selectedTopic = self.selectedTopic
        cell.configCollectionCell(VSAQQuestion: object,index: indexPath)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
}

