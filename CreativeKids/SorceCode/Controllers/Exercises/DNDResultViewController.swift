//
//  DNDResultViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 31/03/21.
//

import UIKit

enum CameFromOnResult {
    case DNDType
    case MCQType
    case MockTest
}

class DNDResultViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var questionView: UIView!
    @IBOutlet weak var scoreView: UIView!
    @IBOutlet weak var labelScore: UILabel!
    @IBOutlet weak var labeltotalQuestion: UILabel!
    @IBOutlet weak var labeltotalMarks: UILabel!
    @IBOutlet weak var labelType: UILabel!
    @IBOutlet weak var viewPdf: UIView!
    
    @IBOutlet weak var labelStudentName: UILabel!
    @IBOutlet weak var labelClass: UILabel!
    @IBOutlet weak var labelSubjectName: UILabel!
    
    @IBOutlet weak var labelChapterName: UILabel!
    @IBOutlet weak var labelExerciseName: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    
    //MARK: - Variables
    var modelArr = [VSAQQuestionModel]()
    var MCQmodelArr = [MCQQuestionModel]()
    var camefrom: CameFromOnResult!
    var totalQuestion = 0
    var marksObtained = 0
    var totalMarks = 0
    var correctMcq = 0
    var totlaTime: String?
    var setId: String?
    var exercise: ExerciseModel?
    var resultExercise: SendExerciseResultView?
    var mockTestModel: VSAQQuestionModel?
    var selectedTopic:TopicModel?
    var selectedMockTopic:TopicSearchModel?
    var subjectSelected:SubjectModel?
    
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configInitial()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppUtility.lockOrientation(.portrait)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        statusBarColor(headerColor: UIColor.blue)
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.questionView.layer.addBorder(edge: UIRectEdge.right, color: .black, thickness: 1.5)
        self.scoreView.layer.addBorder(edge: UIRectEdge.right, color: .black, thickness: 1.5)
    }
    
    //MARK: - Initial Setup
    func configInitial() {
        labelStudentName.text = "Name: \((kUserData.userName != "" ? kUserData.userName : "NA") ?? "NA")"
        labelClass.text = selectedTopic?.className != "" ? selectedTopic?.className : "NA"
        labelSubjectName.text = "Subject: \((selectedTopic?.subjectName != "" ? selectedTopic?.subjectName : "NA") ?? "NA")"
        labelExerciseName.text = exercise?.exerciseType != "" ? "\(exercise?.exerciseType ?? "") \(exercise?.exerciseNumber ?? 1)" : "NA"
        labelDate.text = "Date: \(Date().dateString())"
        switch camefrom {
            //MARK: - DND Type
        case .DNDType:
            let correctAnswer = modelArr.filter{$0.rightAns == $0.userSelectedOption}
            labelChapterName.text = "Chapter: \((modelArr.first?.chapter != "" ? modelArr.first?.chapter : "NA") ?? "NA")"
            totalQuestion = modelArr.count
            correctMcq = correctAnswer.count
            totalMarks = modelArr.reduce(0){$0 + Int.getIntValue($1.Marks)}
            marksObtained = correctAnswer.reduce(0){$0 + Int.getIntValue($1.Marks)}
            setUI(object:modelArr)
            self.setId = self.modelArr.first?.setid
            studentPerformance(topic: selectedTopic)
            //MARK: - MCQ Type
        case .MCQType:
            labelChapterName.text = "Chapter: \((MCQmodelArr.first?.chapter != "" ? MCQmodelArr.first?.chapter : "NA") ?? "NA")"
            totalQuestion = MCQmodelArr.count
            setUI(object:MCQmodelArr)
            self.setId = self.MCQmodelArr.first?.setid
            studentPerformance(topic: selectedTopic)
            
            //MARK: - MOCK TEST
        case .MockTest:
            labelChapterName.text = "Chapter: \((modelArr.first?.chapter != "" ? modelArr.first?.chapter : "NA") ?? "NA")"
            labelClass.text = selectedMockTopic?.className != "" ? selectedMockTopic?.className : "NA"
            labelSubjectName.text = "Subject: \((selectedMockTopic?.subjectName != "" ? selectedMockTopic?.subjectName : "NA") ?? "NA")"
            totalQuestion = modelArr.count
            setUI(object:modelArr)
            self.setId = self.modelArr.first?.setid
            studentPerformance(topic: selectedMockTopic)
            if isHindiSubject(subejct: subjectSelected?.subjectName ?? "") {
                labelChapterName.setfont(font: 15, fontFamily: chanakyaFont)
                labelChapterName.text =  ConstantHindiText.paath + "% \(modelArr.first?.chapter != "" ? String(modelArr.first?.chapter ?? "NA") : "NA")"
            } else {
                labelChapterName.text = "Chapter: \((modelArr.first?.chapter != "" ? modelArr.first?.chapter : "NA") ?? "NA")"
            }
        default:
            break
        }
    }
    
    func setUI(object:[Any]){
        self.labeltotalQuestion.text = "\(object.count)"
        let percentage = Double(marksObtained * 100) / Double(totalMarks)
        let convertInt = Int(percentage)
        self.labeltotalMarks.text = "\(convertInt) %"
        self.labelScore.text = "\(correctMcq)"
        self.totlaTime = String.getString(kSharedUserDefaults.object(forKey: "time_count_Background_Exercise"))
        if convertInt <= 33 {
            self.labelType.text = "Need to work hard"
        } else if convertInt > 33 && convertInt <= 60 {
            self.labelType.text = "Good"
        }
        else if convertInt > 60 && convertInt <= 80 {
            self.labelType.text = "Awesome"
        } else if convertInt > 80 && convertInt <= 100 {
            self.labelType.text = "Victory"
        }
    }
    
    //MARK: - Action
    @IBAction func nextButtonPressend(_ sender: UIButton) {
        switch self.camefrom{
        case .MockTest:
            let controller = ReviewViewController.getController(storyboard: .Question)
            controller.cameFrom = .mockTest
            controller.exercise = self.exercise
            controller.reviewModel = modelArr
            controller.subjectSelected = self.subjectSelected
            controller.ltpResultPdf = viewPdf.exportAsPdfFromView()
            self.navigationController?.pushViewController(controller, animated: true)
        default:
            self.resultExercise = SendExerciseResultView(viewController: self, completionHandler: {
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: VideoPlayerViewController.self) {
                        self.navigationController!.popToViewController(controller, animated: true)
                        break
                    }
                }
            })
        }
    }
    
    func sendResultPdf(){
        let _ = SentPDFViewModel.init(vc:self, exricsePdfPath: viewPdf.exportAsPdfFromView())
    }
    
    //MARK: Exercise Performance For Student
    
    func studentPerformance(topic:Any?){
        let userType = String.getString(kUserData.role)
        switch userType {
        case String.getstring(UserRole.schoolStudent.rawValue):
            let exeType = String.getString(exercise?.exerciseType)
            let setid = String.getString(self.setId)
            let totalQue = String.getString(self.totalQuestion)
            let marks = String.getString(self.totalMarks)
            let scoMarks = String.getString(self.marksObtained)
            let correctAnswer = String.getString(self.correctMcq)
            let totalTimeSpend = String.getString(self.totlaTime)
            //        print("\(exeType)--\(setid)--\(totalQue)--\(marks)--\(scoMarks)--\(correctAnswer)--\(totalTimeSpend)")
            let performanceData = PerformanceData(exerciseType: exeType, setId: setid, totalQuestion: totalQue, totalMarks: marks, scoredMarks: scoMarks, totalCorrectAnswer: correctAnswer, totalTimeSpend: totalTimeSpend)
            let performanceModel = StudentPerformance()
            if let topicExe = topic as? TopicModel{
                performanceModel.exercisePerforManceApi(data: topicExe,performanceData: performanceData)
            }else if let mockTopic = topic as? TopicSearchModel{
                performanceModel.LtpMockPerforManceApi(data: mockTopic, performanceData: performanceData)
            }
            
        default:
            return
        }
    }
}
