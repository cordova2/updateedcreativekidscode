//
//  pdfViewController.swift
//  CreativeKids
//
//  Created by Rakesh jha on 10/05/21.
//

import UIKit
import PDFKit


class pdfViewController: UIViewController {
    var secureView: UIView!
    var path = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Add PDFView to view controller.
        let pdfView = PDFView(frame: self.view.bounds)
        
        self.view.addSubview(pdfView)
        
        // Fit content in PDFView.
        pdfView.autoScales = true
        
        // Load Sample.pdf file.
        
        let fileURL = Bundle.main.url(forResource: "", withExtension: "pdf")
        pdfView.document = PDFDocument(url: fileURL!)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        statusBarColor(headerColor: UIColor.blue)
    }
    
    @objc func didTakeScreenshot() -> Void {
        self.showSimpleAlert(message: "Can't take screenShot")
    }
}
