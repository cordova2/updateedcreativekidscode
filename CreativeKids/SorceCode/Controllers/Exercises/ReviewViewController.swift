//
//  ReviewViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 27/03/21.
//

import UIKit

class ReviewViewController: UIViewController {
    
    enum CameFrom{
        case ltp,mockTest
    }
    //MARK: - Outlets
    @IBOutlet weak var tableReviewQuestion: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var heightConsTable: NSLayoutConstraint!
    var isMailSend = false
    var cameFrom:CameFrom = .ltp
    var reviewModel = [VSAQQuestionModel]()
    var subjectSelected:SubjectModel?
    var resultExercise:SendExerciseResultView?
    
    var ltpResultPdf = ""
    var exercise:ExerciseModel?
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        switch self.cameFrom{
        case .ltp:
            setupInitialLtp()
        case .mockTest:
            setupInitialMockTest()
        }
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        statusBarColor(headerColor: UIColor.blue)
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        headerView.layer.addBorder(edge: UIRectEdge.bottom, color: .black, thickness: 1.5)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppUtility.lockOrientation(.portrait)
    }
    //MARK: - Initial Setup
    func setupInitialLtp() {
        self.tableReviewQuestion.delegate = self
        self.tableReviewQuestion.dataSource = self
        heightConsTable.constant = 25000
        self.tableReviewQuestion.register(UINib(nibName: TableCellIdentifier.review, bundle: nil), forCellReuseIdentifier: TableCellIdentifier.review)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) { [self] in
            self.heightConsTable.constant = self.tableReviewQuestion.contentSize.height
            isMailSend = true
            let previewlastPath = tableReviewQuestion.exportAsPdfFromTable()
            let _ = SentPDFViewModel.init(vc:self, previewPdfPath: previewlastPath,resultPdfPath: ltpResultPdf)
        }
    }
    func setupInitialMockTest() {
        self.tableReviewQuestion.delegate = self
        self.tableReviewQuestion.dataSource = self
        heightConsTable.constant = 25000
        self.tableReviewQuestion.register(UINib(nibName: TableCellIdentifier.mockTestReviewCell, bundle: nil), forCellReuseIdentifier: TableCellIdentifier.mockTestReviewCell)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) { [self] in
            self.heightConsTable.constant = self.tableReviewQuestion.contentSize.height
            isMailSend = true
            let previewlastPath = tableReviewQuestion.exportAsPdfFromTable()
            let _ = SentPDFViewModel.init(vc:self, previewPdfPath: previewlastPath,resultPdfPath: ltpResultPdf)
        }
    }
    @IBAction func buttonBackTapped(_ sender: UIButton) {
        if !isMailSend{
            return
        }
        self.resultExercise = SendExerciseResultView(viewController: self, completionHandler: {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: VideoPlayerViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        })
    }
}

//MARK: - UITableViewDelegate, UITableViewDataSource
extension ReviewViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reviewModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch self.cameFrom {
        case .ltp:
            let cell = tableView.dequeueReusableCell(withIdentifier: TableCellIdentifier.review, for: indexPath) as! ReviewTableViewCell
            cell.subjectSelected = self.subjectSelected
            cell.configTableCell(questionCheck: reviewModel[indexPath.row],index: indexPath)
            return cell
        case .mockTest:
            let cell = tableView.dequeueReusableCell(withIdentifier: TableCellIdentifier.mockTestReviewCell, for: indexPath) as! MockTestReviewTableViewCell
            cell.subjectSelected = self.subjectSelected
            cell.configTableCell(questionCheck: reviewModel[indexPath.row],index: indexPath)
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension                                                                              
    }
    
}
extension UITableView {
     
    // Export pdf from UITableView and save pdf in drectory and return pdf file path
    func exportAsPdfFromTable() -> String {

        let originalBounds = self.bounds
        self.bounds = CGRect(x:originalBounds.origin.x, y: originalBounds.origin.y, width: self.contentSize.width, height: self.contentSize.height)
        let pdfPageFrame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.contentSize.height)

        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, pdfPageFrame, nil)
        UIGraphicsBeginPDFPageWithInfo(pdfPageFrame, nil)
        guard let pdfContext = UIGraphicsGetCurrentContext() else { return "" }
        self.layer.render(in: pdfContext)
        UIGraphicsEndPDFContext()
        self.bounds = originalBounds
        // Save pdf data
        return self.saveTablePdf(data: pdfData)

    }

    // Save pdf file in document directory
    func saveTablePdf(data: NSMutableData) -> String {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let docDirectoryPath = paths[0]
        let pdfPath = docDirectoryPath.appendingPathComponent("resultPdf.pdf")
        if data.write(to: pdfPath, atomically: true) {
            return pdfPath.path
        } else {
            return ""
        }
    }
}
