//
//  MathsMockTestViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 11/10/21.
//

import UIKit

class MathsMockTestViewController: UIViewController {
    
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var questionsCollectionView: UICollectionView!
    @IBOutlet weak var labelQuestionCount: UILabel!
    @IBOutlet weak var labelHeader: UILabel!
    @IBOutlet weak var labelHeaderAbhyaas: UILabel!
    
    
    var timer: Timer?
    var targetTime = Date()
    var checkAns = 0
    var count = 0
    var obtainedMarks = 0
    var maximumMarks = 0
    var mockTestModelData = [VSAQQuestionModel]()
    var mockTest:ExerciseModel?
    var selectedTopic:TopicSearchModel?
    var mockTestViewModel: MathsMockTestViewModel?
    var subjectSelected:SubjectModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialLoad()
        questionsCollectionView.delegate = self
        questionsCollectionView.dataSource = self
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppUtility.lockOrientation(.portrait)
        
        self.mockTestViewModel = MathsMockTestViewModel(viewController: self, completionHandler: {
            if self.mockTestModelData.count == 0 {
                self.timerStop()
            } else {
                self.setTarget()
            }
            self.maximumMarks = self.mockTestModelData.reduce(0){$0 + Int.getIntValue($1.Marks)}
            self.labelQuestionCount.text = "1/\(self.mockTestModelData.count)"
            //            self.setHeaderLabel()
            self.questionsCollectionView.reloadData()
        })
    }
    
    let navigation = self
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        statusBarColor(headerColor: UIColor.blue)
    }
    
    fileprivate func initialLoad(){
        if  isHindiSubject(subejct:selectedTopic?.subjectName ?? ""){
            self.labelHeader.setfont(font: 16, fontFamily: chanakyaFont)
            self.labelHeader.attributedText = makeNewNumericStringFor3And7(StringWithNumber:selectedTopic?.chapterName ?? "")
        }else{
            self.labelHeader.setfont(font: 16, fontFamily: futuraFont)
            self.labelHeader.text = selectedTopic?.chapterName ?? ""
        }
    }
    
    //MARK: - SetTargetForTime
    func setTarget(forSeconds seconds: Int = 0) {
        targetTime = Calendar.current.date(byAdding: .second, value: seconds, to: Date()) ?? Date()
        startTimer() 
    }
    //MARK: - Start Timer
    func startTimer() {
        timer?.invalidate()
        var sec = Calendar.current.dateComponents([.second], from: Date(), to: self.targetTime).second!
        if sec == 0 {
            timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (timer) in
                sec += 1
                kSharedUserDefaults.setValue(sec, forKey: "time_count_Background_Exercise")
                self.labelTime.text = CommonUtils.timeString(time: TimeInterval(sec))
            }
        }
    }
    //MARK: - StopTimer
    func timerStop() {
        timer?.invalidate()
    }
    //MARK: - QuestionsCount + Scroll Collection
    func setQuestionCountsWithScroll(questions: IndexPath) {
        if questions.item == (self.mockTestModelData.count) - 1 {
            self.moveToResultOnLast()
        } else {
            self.count = questions.item + 1
            self.labelQuestionCount.text = "\(self.count + 1)/\(self.mockTestModelData.count)"
            let scrollIndexPath = IndexPath(item: questions.item + 1, section: 0)
            self.questionsCollectionView.scrollToItem(at: scrollIndexPath, at: .centeredHorizontally, animated: true)
        }
    }
    //MARK: -Actiona
    @IBAction func buttonBacktapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    func moveToResultOnLast() {
        self.timerStop()
        let controller = DNDResultViewController.getController(storyboard: .Question)
        //        controler.subjectSelected =
        controller.modelArr = self.mockTestModelData
        controller.correctMcq = self.checkAns
        controller.camefrom = .MockTest
        controller.marksObtained = self.obtainedMarks
        controller.totalMarks = self.maximumMarks
        controller.exercise = self.mockTest
        controller.selectedMockTopic = self.selectedTopic
        controller.subjectSelected = self.subjectSelected
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
//MARK: - UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension MathsMockTestViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    //MARK:- COLLECTION numberOfItemsInSection
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionView.getNumberOfCell(numberofCell: self.mockTestModelData.count, message: "")
    }
    
    //MARK:- COLLECTION cellForItemAt
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionCellIdentifier.questionBankMath, for: indexPath) as! MathsMockTestCollectionViewCell
        let object = self.mockTestModelData[indexPath.item]
        cell.configCell(modelData: object, indexPath: indexPath)
        cell.mockTestModel = self.mockTestModelData
        cell.nextCallback = {
            if indexPath.item == (self.mockTestModelData.count) - 1 {
                self.moveToResultOnLast()
            } else {
                self.setQuestionCountsWithScroll(questions: indexPath)
                cell.buttonConfirmNext.setTitle("Confirm", for: .normal)
                //self.setTarget(forSeconds: 20)
            }
        }
        
        cell.selectedOptionCallback = {
            if object.rightAns == cell.buttonOptions[0].tag && cell.buttonOptions[0].isSelected {
                self.checkAns = self.checkAns + 1
                self.obtainedMarks = self.obtainedMarks + Int.getIntValue(object.Marks)
            } else if object.rightAns == cell.buttonOptions[1].tag && cell.buttonOptions[1].isSelected  {
                self.checkAns = self.checkAns + 1
                self.obtainedMarks = self.obtainedMarks + Int.getIntValue(object.Marks)
            } else if object.rightAns == cell.buttonOptions[2].tag && cell.buttonOptions[2].isSelected  {
                self.checkAns = self.checkAns + 1
                self.obtainedMarks = self.obtainedMarks + Int.getIntValue(object.Marks)
            } else if object.rightAns == cell.buttonOptions[3].tag && cell.buttonOptions[3].isSelected  {
                self.checkAns = self.checkAns + 1
                self.obtainedMarks = self.obtainedMarks + Int.getIntValue(object.Marks)
            }
        }
        return cell
    }
    
    //MARK:- COLLECTION HEIGHT
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
}
