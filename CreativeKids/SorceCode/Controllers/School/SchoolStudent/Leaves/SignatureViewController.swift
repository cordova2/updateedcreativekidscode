//
//  SignatureViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 13/04/22.
//

import Foundation
import UIKit

protocol SignatureDatasource {
    func getSignature(_ signature: UIImage)
}

class SignatureViewController: UIViewController {
    
    @IBOutlet weak var signView: Canvas!
    
    var signatureDatasource: SignatureDatasource? = nil
    

    override func viewDidLoad() {
        super.viewDidLoad()
        statusBarColor(headerColor: UIColor.blue)
    }
    @IBAction func clearSignature(_ sender: UIButton) {
        signView.clear()
    }
    @IBAction func buttonSaveTapped(_ sender : UIButton) {
        if signView.isEmpty {
            showAlertMessage.alert(message: "Please Sign First!!!")
            return
        }
        if let image = signView.getDesign {
            print(image)
            self.signatureDatasource?.getSignature(image)
        }
        self.navigationController?.popViewController(animated: true)
    }
}
