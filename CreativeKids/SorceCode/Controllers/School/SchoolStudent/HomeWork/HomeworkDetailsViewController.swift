//
//  HomeworkDetailsViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 24/03/22.
//

import UIKit

class HomeworkDetailsViewController: UIViewController {
    enum CameFor{
        case Add
        case Update
    }
    var work:HomeWork?
    var assignment:AssignmentModel?
    var cameFor: CameFor = .Add
    var viewModel:HomeWorkViewModel?
    var reloadBackScreen:ReloadDataDataSources?
    @IBOutlet weak var labelChapter: UILabel!
    @IBOutlet weak var labelSubject: UILabel!
    @IBOutlet weak var labelQuestion: UILabel!
    @IBOutlet weak var buttonSubmitWork: UIButton!
    @IBOutlet weak var collectionViewHomework: HomeworkDetailsCollectionView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        statusBarColor(headerColor: UIColor.blue)
        viewModel = HomeWorkViewModel()
        initialsetUp()
    }
    
    func initialsetUp(){
        collectionViewHomework.dataPassDelegate = self
        viewModel?.dataPassDelegate = self
        buttonSubmitWork.setTitle(cameFor == .Add ? "Submit your homework":"Update your homework", for: .normal)
        switch cameFor {
        case .Add:
            viewModel?.workQuestionApi(assignmentId: work?.assignmentId ?? "")
        case .Update:
            viewModel?.submittedWorkApi(assignmentId: work?.assignmentId ?? "")
        }
        
    }
    
    @IBAction func buttonSubmitHomeworkTapped(_ sender: UIButton) {
        let uploadHomework = UploadHomeworkViewController.getController(storyboard: .StudentDashboard)
        uploadHomework.cameFor = self.cameFor == .Add ? .Add : .Update
        uploadHomework.assignment = self.assignment
        uploadHomework.reloadBackScreen = self.reloadBackScreen
        uploadHomework.work = self.work
        self.navigationController?.pushViewController(uploadHomework, animated: true)
    }
    @IBAction func buttonBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension HomeworkDetailsViewController:HomeWorkDelegate{
    func onSuccess(_ data: Any) {
        guard let assignmentData = data as? AssignmentModel else{return}
        self.assignment = assignmentData
        labelSubject.attributedText = createAttributedText(firstText: "Subject: ", secondText: "\(work?.subjectName ?? "")")
        labelChapter.attributedText = createAttributedText(firstText: "Chapter Name: ", secondText: "\(assignmentData.chapterName ?? "")")
        if assignmentData.question?.isEmpty ?? true{
            labelQuestion.isHidden = true
        }else{
            labelQuestion.isHidden = false
            labelQuestion.attributedText = createAttributedText(firstText: "\(assignmentData.question ?? "")", secondText: "")
        }
        collectionViewHomework.getData(cell: "", data: assignmentData.questionImages)
    }
}

extension HomeworkDetailsViewController:DataSourcesCollectionDelegate{
    func collectionView(_ collectionView: UICollectionView, data: Any?, selectedIndex: IndexPath) {
        let previewVC = ImagePreviewViewController.getController(storyboard: .TeacherDashboard)
        previewVC.imageArr = self.collectionViewHomework.item
        previewVC.selcetedIndex = selectedIndex.item
        previewVC.cameFor = .Student
        self.navigationController?.pushViewController(previewVC, animated: true)
    }
}
