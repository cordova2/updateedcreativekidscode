//
//  UploadHomeworkViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 26/03/22.
//

import UIKit

class UploadHomeworkViewController: UIViewController {
    
    enum CameFor{
        case Add
        case Update
    }
    
    @IBOutlet weak var uploadImageCollectionView: UICollectionView!
    @IBOutlet weak var heightCollectionView: NSLayoutConstraint!
    
    @IBOutlet weak var buttonSubmit: UIButton!
    var reloadBackScreen:ReloadDataDataSources?
    var cameFor:CameFor = .Add
    var assignment:AssignmentModel?
    var work:HomeWork?
    var viewModel:WorkSubmitViewModel?
    var uploadImageArr:[Any] = [UIImage(systemName: "plus.circle") ?? UIImage()]{
        didSet{
            buttonSubmit?.isUserInteractionEnabled = uploadImageArr.count > 1
            buttonSubmit?.isHighlighted = !(uploadImageArr.count > 1)
            
        }
    }
    var forCollectionViewHeight:Int{
        get{
            let count = uploadImageArr.count
            return (count % 4) == 0 ? count/4 : (count/4) + 1
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        statusBarColor(headerColor: UIColor.blue)
        initialSetUp()
    }
    
    func initialSetUp(){
        viewModel = WorkSubmitViewModel(controller: self)
        viewModel?.dataPassDelegate = self
        switch cameFor{
        case .Add:
            buttonSubmit.setTitle("Submit", for: .normal)
        case .Update:
            buttonSubmit.setTitle("Update", for: .normal)
            uploadImageArr = assignment!.answerImages + uploadImageArr
            uploadImageCollectionView.reloadData()
        }
    }
    
    @IBAction func buttonBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonSubmitTapped(_ sender: UIButton) {
        var answerImage = uploadImageArr
        answerImage.remove(at: uploadImageArr.count - 1)
        switch cameFor{
        case .Add:
            viewModel?.assignmentSubmitByStudentApi(answerImageArray: answerImage)
        case .Update:
            viewModel?.assignmentUpdatedByStudentApi(answerImageArray: answerImage)
        }
        
    }
}



extension UploadHomeworkViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.heightCollectionView.constant = CGFloat(forCollectionViewHeight) * (uploadImageCollectionView.frame.width/4) + 10
        return uploadImageArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UploadHomeworkCollectionCell", for: indexPath) as! UploadHomeworkCollectionCell
        if let imageData = uploadImageArr[indexPath.item] as? UIImage{
            cell.imageHomework.image = imageData
        }else{
            if let imageData = uploadImageArr[indexPath.item] as? String{
                cell.imageHomework.downloadImageFromURL(urlString: imageData){ image in
                    if let img = image{
                        self.uploadImageArr[indexPath.item] = img
                    }
                }
            }
        }
        cell.buttonCross.isHidden = indexPath.item == self.uploadImageArr.count - 1 ? true : false

        cell.removeImageCallBack = {
            self.uploadImageArr.remove(at: indexPath.item)
            self.uploadImageCollectionView.reloadData()
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.item == self.uploadImageArr.count - 1 {
            getPicture(controller: self, sourceView: collectionView){ image in
                guard let returnedImage = image else{return}
                self.uploadImageArr.insert(returnedImage, at: self.uploadImageArr.count - 1)
                self.uploadImageCollectionView.reloadData()
            }
        } else {
            let previewVC = ImagePreviewViewController.getController(storyboard: .TeacherDashboard)
            var images = self.uploadImageArr
//            let selectedImage = images[indexPath.item]
//            images.remove(at: indexPath.item)
            images.removeLast()
//            images.insert(selectedImage, at: 0)
            previewVC.imageArr = images
            previewVC.selcetedIndex = indexPath.item
            previewVC.cameFor = .Student
            self.navigationController?.pushViewController(previewVC, animated: true)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 4, height: collectionView.frame.width / 4)
    }
}

extension UploadHomeworkViewController:WorkSubmitDelegate{
    func onSuccess(_ data: Any) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: HomeworkViewController.self) {
                self.reloadBackScreen?.reload()
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
}
