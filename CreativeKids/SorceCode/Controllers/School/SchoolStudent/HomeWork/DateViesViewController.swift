//
//  DateViesViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 14/03/22.
//

import UIKit

class DateViesViewController: UIViewController {

    var viewModel:HomeWorkDatesViewModel?
    @IBOutlet weak var tableViewDates: UITableView!
    var datesArray = [HomeWorkDate]()
    override func viewDidLoad() {
        super.viewDidLoad()
        statusBarColor(headerColor: UIColor.blue)
        viewModel = HomeWorkDatesViewModel()
        initialsetUp()
    }
    
    func initialsetUp(){
        viewModel?.dataPassDelegate = self
        viewModel?.homeWorkDatesApi()
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension DateViesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        datesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DateViesTableCell", for: indexPath) as! DateViesTableCell
        cell.configCell(data: datesArray[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let homeworkVc = HomeworkViewController.getController(storyboard: .StudentDashboard)
        homeworkVc.selectedDate = self.datesArray[indexPath.row]
        self.navigationController?.pushViewController(homeworkVc, animated: true)
    }
}


class DateViesTableCell: UITableViewCell {
    
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var viewBackground: UIView! {
        didSet {
            viewBackground.drawShadowOnCell()
        }
    }
    
    func configCell(data:Any){
        guard let datesObject = data as? HomeWorkDate else{return}
        labelDate.text = datesObject.date
    }
}

extension DateViesViewController:HomeWorkDatesDelegate{
    func onSuccess(_ data: Any) {
        datesArray = data as! [HomeWorkDate]
        tableViewDates.reloadData()
    }
}
