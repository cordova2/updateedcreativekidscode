//
//  CalenderViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 08/06/22.
//

import UIKit
import FSCalendar



protocol SelectedDateDelegate{
    func selectedDate(fromDate:Date?, toDate:Date?)
}

class CalenderViewController: UIViewController {

    enum SelectionType{
        case single
        case multiple
    }
    
    @IBOutlet weak var calender: FSCalendar!
    @IBOutlet weak var buttonDateSelected: UIButton!
    
    var delegate:SelectedDateDelegate?
    // first date in the range
    private var firstDate: Date?
    // last date in the range
    private var lastDate: Date?
    private var datesRange: [Date]?
    var selectionType:SelectionType = .multiple
    override func viewDidLoad() {
        super.viewDidLoad()
        setConfigureCalender()
    }
    func setConfigureCalender(){
        calender.delegate = self
        calender.dataSource = self
        switch selectionType {
        case .single:
            calender.allowsMultipleSelection = false
        case .multiple:
            calender.allowsMultipleSelection = true
        }
        
    }

    @IBAction func buttonBackgroundAreaTapped(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
    
    @IBAction func buttonDoneTapped(_ sender: UIButton) {
        if self.firstDate != nil{
            self.dismiss(animated: true){
                self.delegate?.selectedDate(fromDate: self.firstDate, toDate: self.lastDate)
            }
        }else{
            CommonUtils.showToast(message: "Please select any date for proceed.")
        }
    }
}

extension CalenderViewController: FSCalendarDelegate,FSCalendarDataSource {
    func maximumDate(for calendar: FSCalendar) -> Date {
        return Date()
    }
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        if firstDate == nil {
                firstDate = date
                datesRange = [firstDate!]
                print("datesRange contains: \(datesRange!)")
                return
            }

            // only first date is selected:
            if firstDate != nil && lastDate == nil {
                // handle the case of if the last date is less than the first date:
                if date <= firstDate! {
                    calendar.deselect(firstDate!)
                    firstDate = date
                    datesRange = [firstDate!]

                    print("datesRange contains: \(datesRange!)")

                    return
                }

                let range = datesRange(from: firstDate!, to: date)

                lastDate = range.last

                for d in range {
                    calendar.select(d)
                }

                datesRange = range

                print("datesRange contains: \(datesRange!)")
                return
            }

            // both are selected:
            if firstDate != nil && lastDate != nil {
                for d in calendar.selectedDates {
                    calendar.deselect(d)
                }

                lastDate = nil
                firstDate = nil

                datesRange = []

                print("datesRange contains: \(datesRange!)")
            }
    }
    func calendar(_ calendar: FSCalendar, didDeselect date: Date, at monthPosition: FSCalendarMonthPosition) {
        // both are selected:

            // NOTE: the is a REDUANDENT CODE:
            if firstDate != nil && lastDate != nil {
                for d in calendar.selectedDates {
                    calendar.deselect(d)
                }

                lastDate = nil
                firstDate = nil

                datesRange = []
                print("datesRange contains: \(datesRange!)")
            }
    }
    
    func datesRange(from: Date, to: Date) -> [Date] {
        // in case of the "from" date is more than "to" date,
        // it should returns an empty array:
        if from > to { return [Date]() }

        var tempDate = from
        var array = [tempDate]

        while tempDate < to {
            tempDate = Calendar.current.date(byAdding: .day, value: 1, to: tempDate)!
            array.append(tempDate)
        }

        return array
    }
    
}
