//
//  NoticeViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 07/03/22.
//

import UIKit
import FSCalendar

class NoticeViewController: UIViewController {

    @IBOutlet weak var noticeTableView: UITableView!
    @IBOutlet weak var viewCalender: UIView!
    var viewModel:NoticeViewModel?
    var noticeArray = [NoticeModel](){
        didSet{
            noticeTableView.reloadData()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showTabbar()
        statusBarColor(headerColor: UIColor.blue)
        initialView()
        viewCalender.drawShadow()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showTabbar()
        viewModel = NoticeViewModel.init{ [weak self] noticeArray in
            self?.noticeArray = noticeArray
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.hideTabbar()
    }
    
    
    func initialView(){
        noticeTableView.register(UINib(nibName: TableCellIdentifier.noticeCell, bundle: nil), forCellReuseIdentifier: TableCellIdentifier.noticeCell)
        noticeTableView.delegate = self
        noticeTableView.dataSource = self
    }
    
    @IBAction func buttonCalenderTapped(_ sender: UIButton) {
        let controller = CalenderViewController.getController(storyboard: .StudentDashboard)
        controller.modalTransitionStyle = .crossDissolve
        controller.modalPresentationStyle = .overCurrentContext
        controller.selectionType = .single
        controller.delegate = self
        self.navigationController?.present(controller, animated: true, completion: nil)
    }
}
extension NoticeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableView.getNumberOfRow(numberofRow: noticeArray.count, message: "No notice found", messageColor: .black)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableCellIdentifier.noticeCell, for: indexPath) as! NoticeTableViewCell
        cell.cellConfig(data: noticeArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedIndex = noticeArray[indexPath.row]
        let controller = NoticeViewPopUpViewController.getController(storyboard: .StudentDashboard)
        controller.modalTransitionStyle = .crossDissolve
        controller.modalPresentationStyle = .overCurrentContext
        controller.notice = selectedIndex
        self.navigationController?.present(controller, animated: true)
    }
}

extension NoticeViewController: SelectedDateDelegate{
    func selectedDate(fromDate: Date?, toDate: Date?) {
        guard let fromDt = fromDate else{return}
        viewModel?.getNotice(date: fromDt, usertype: .Student)
    }
}
