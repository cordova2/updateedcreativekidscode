//
//  StudentTabBarController.swift
//  CreativeKids
//
//  Created by Prashant Swain on 18/06/22.
//

import UIKit
import Alamofire

enum StudentBarItem:String{
    case Home
    case Attendance
    case Notice
    case Chat
    case Profile
}


class StudentTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        createControllerObject()
    }
    
    func createControllerObject(){
        self.viewControllers = [homeViewController(),attendanceViewController(),noticeViewController(),chatController(),profileViewController()]
    }
    
    func homeViewController() -> UIViewController{
        let homeController = StudentDashboardVC.getController(storyboard: .StudentDashboard)
        let homeBarItem = UITabBarItem(title: StudentBarItem.Home.rawValue, image: UIImage(named: "HomeUnselected"), selectedImage: UIImage(named: "HomeSelected"))
        homeController.tabBarItem = homeBarItem
        return homeController
    }
    
    func attendanceViewController() -> UIViewController{
        let attendanceController = StudentAttendanceViewController.getController(storyboard: .StudentDashboard)
        let homeBarItem = UITabBarItem(title: StudentBarItem.Attendance.rawValue, image: UIImage(named: "AttendanceUnselected"), selectedImage: UIImage(named: "AttendanceSelected"))
        attendanceController.tabBarItem = homeBarItem
        return attendanceController
    }
    
    func noticeViewController() -> UIViewController{
        let noticeController = NoticeViewController.getController(storyboard: .StudentDashboard)
        let homeBarItem = UITabBarItem(title: StudentBarItem.Notice.rawValue, image: UIImage(named: "NoticeUnselected"), selectedImage: UIImage(named: "NoticeSelected"))
        noticeController.tabBarItem = homeBarItem
        return noticeController
    }
    
    func chatController() -> UIViewController{
        let chatController = RecentChatsViewController.getController(storyboard: .Chat)
        let chatBarItem = UITabBarItem(title: StudentBarItem.Chat.rawValue, image: UIImage(named: "ChatUnselected"), selectedImage: UIImage(named: "ChatSelected"))
        receivedMessageCount(){ unreadCount in
            if unreadCount != 0{
                chatBarItem.badgeValue = String.getstring(unreadCount)
            }else{
                chatBarItem.badgeValue = nil
            }
        }
        chatController.tabBarItem = chatBarItem
        return chatController
    }
    
    func profileViewController() -> UIViewController{
        let profileController = StudentSideMenuViewController.getController(storyboard: .StudentDashboard)
        let homeBarItem = UITabBarItem(title: StudentBarItem.Profile.rawValue, image: UIImage(named: "ProfileUnselected"), selectedImage: UIImage(named: "ProfileSelected"))
        profileController.tabBarItem = homeBarItem
        return profileController
    }

}

extension StudentTabBarController{
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        guard let tabItem = item.title else{return}
        switch tabItem{
        case StudentBarItem.Chat.rawValue:
            if !(NetworkReachabilityManager()?.isReachable ?? false){
                showAlertMessage.alert(message:AlertMessage.knoNetwork)
                return
            }
        default:
            return
        }
    }
    
    func receivedMessageCount(completion: @escaping ((_ :Int)->Void)){
        var recentUser = Array<ResentUsers>(){
            didSet{
                let count = recentUser.reduce(0, { (sum, user) in
                     sum + Int.getint(user.readCount)
                })
                completion(count)
            }
        }
            ChatHalper.shared.receiveResentUsers { users in
                recentUser = users ?? []
             }
            
    }
}







