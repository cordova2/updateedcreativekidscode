//
//  StudentDashboardVC.swift
//  CreativeKids
//
//  Created by Creative Kids on 22/02/22.
//

import UIKit
import KYDrawerController
import AVFoundation

class StudentDashboardVC: UIViewController {
    
    @IBOutlet weak var typesCollectionHeight: NSLayoutConstraint!
    @IBOutlet weak var labelStudentName: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var collectionViewTypes: TypesCollectionView!
    @IBOutlet weak var heightEventCollection: NSLayoutConstraint!
    @IBOutlet weak var viewProgress: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var schoolEventsCollectionView: DashBoardEventCollectionView!
    
    @IBOutlet weak var circularProgress: CircularProgressView!
    
    @IBOutlet weak var labelChapterRead: UILabel!
    @IBOutlet weak var labelExerciseDone: UILabel!
    @IBOutlet weak var labelTestDone: UILabel!
    
    @IBOutlet weak var progressChapterRead: UIProgressView!
    @IBOutlet weak var progressExerciseDone: UIProgressView!
    @IBOutlet weak var progressTestDone: UIProgressView!
    
    
    var updateProfileViewModel:UpdateProfilePictureViewModel?
    var eventImagesCount:Int?
    var currentIndex = 0
    var timer: Timer?
    
    var subjects = [SubjectModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showTabbar()
        initialSetUp()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showTabbar()
        startTimer()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.hideTabbar()
        self.timer?.invalidate()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setGradient()
        profileImage.layer.cornerRadius = profileImage.height/2
        statusBarColor(headerColor: UIColor.blue)
    }
    
    func initialSetUp(){
        if !isUserGreetDone{
            getCurrentTimeForGreetUser()
        }
        StudentPerformanceViewModel().getPerformanceApi { studentPerformanceDict in
            self.setProgressView(value:studentPerformanceDict)
        }
        labelStudentName.text = kUserData.userName
        profileImage.downlodeImage(serviceurl: kUserData.profileImage, placeHolder: UIImage(named: "kid_icon"))
        schoolEventsCollectionView.dataPassDataSources = self
        self.collectionViewTypes.dataPassDelegate = self
        self.collectionViewTypes.getData(cell: CollectionCellIdentifier.typesCollection, data: self.collectionViewTypes.modelData)
    }
    func startTimer() {
        self.timer =  Timer.scheduledTimer(timeInterval: 8.0, target: self, selector: #selector(self.autoScroll), userInfo: nil, repeats: true)
    }
    
    //MARK: -  Auto Scroll Methods
    @objc func autoScroll() {
         if (eventImagesCount ?? 0) != 0{
            if self.currentIndex < (eventImagesCount ?? 0) - 1 {
                self.currentIndex += 1
                let indexPath = IndexPath(item: currentIndex, section: 0)
                self.schoolEventsCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
                self.pageControl.currentPage = self.currentIndex
            }else{
                self.currentIndex = 0
                self.pageControl.currentPage = self.currentIndex
                self.schoolEventsCollectionView.scrollToItem(at: IndexPath(item: self.currentIndex, section: 0), at: .centeredHorizontally, animated: true)
            }
        }
    }
    
    //MARK:- SET VALUE IN PROGRESS VIEW
    func setProgressView(value:Dict){
        let chapterRead = Int.getInt(String.getString(value["chapter"]))
        let exerciseDone = Int.getInt(String.getString(value["exe"]))
        let testDone = Int.getInt(String.getString(value["ltp"]))
        let overAllPerformance = Int.getInt(String.getString(value["performance"]))
        self.labelChapterRead.text = "\(chapterRead)%"
        self.labelExerciseDone.text = "\(exerciseDone)%"
        self.labelTestDone.text = "\(testDone)%"
        self.progressChapterRead.progress = Float(chapterRead)/100
        self.progressExerciseDone.progress = Float(exerciseDone)/100
        self.progressTestDone.progress = Float(testDone)/100
        let progressInPercentage = Float(overAllPerformance)/100
        //        let totalProgress = Float(chapterRead + exerciseDone + testDone)/300
        let progress = (Double(progressInPercentage))
        //        let twoDecimalPointString = String(format: "%.2f", progress)
        //        print(twoDecimalPointString)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.circularProgress.setProgressWithAnimation(duration: 1.0, value: Float(progress))
        }
    }
    
    //MARK:- Set Gradient
    func setGradient() {
        DispatchQueue.main.async {
            self.viewProgress.setgradient(color1: .blue, color2: .systemIndigo)
        }
    }
    
    @IBAction func buttonSideDrawer(_ sender: UIButton) {
        if let kyDrawer = self.parent as? KYDrawerController{
            kyDrawer.drawerWidth = UIDevice.isPhone ? self.view.frame.width - 100 : self.view.frame.width - 250
            kyDrawer.setDrawerState(.opened, animated: true)
        }
    }
    @IBAction func buttonEditProfileTapped(_ sender: UIButton) {
        changeProfile()
    }
    @IBAction func buttonLeaves(_ sender: UIButton) {
        let leaveVc = LeaveListViewController.getController(storyboard: .StudentDashboard)
        self.navigationController?.pushViewController(leaveVc, animated: true)
    }
    
    @IBAction func buttonPerformanceTapped(_ sender: UIButton) {

        //  self.showSimpleAlert(message: "Under Development")
        SubjectProgressViewModel().getStuPerformanceApi()
        
//        if kSharedUserDefaults.isUserLoggedIn() &&  self.subjects.isEmpty{
//            let _ = SubjectViewModel(complition: { subjectArray in
//                self.subjects = subjectArray
//                let controller = ProgressReportViewController.getController(storyboard: .Home)
//                controller.subjectListArray = self.subjects
//                controller.performanceFor = .student
//                self.navigationController?.pushViewController(controller, animated: true)
//            })
//        }else{
//            if !self.subjects.isEmpty{
//                let controller = ProgressReportViewController.getController(storyboard: .Home)
//                controller.subjectListArray = self.subjects
//                controller.performanceFor = .student
//                self.navigationController?.pushViewController(controller, animated: true)
//            }
//        }
    }
    
}
extension StudentDashboardVC: DataSourcesCollectionDelegate {
    func collectionView(_ collectionView: UICollectionView, data: Any?, selectedIndex: Int) {
        switch selectedIndex {
            case 0:
            let homeworkVc = DateViesViewController.getController(storyboard: .StudentDashboard)
            self.navigationController?.pushViewController(homeworkVc, animated: true)
            case 1:
            let syllabusVc = SyllabusViewController.getController(storyboard: .TeacherDashboard)
            syllabusVc.userType = .Student
            self.navigationController?.pushViewController(syllabusVc, animated: true)
            case 2:
                let feesVc = StudentFeesViewController.getController(storyboard: .StudentDashboard)
                self.navigationController?.pushViewController(feesVc, animated: true)
            case 3:
                let learningVc = DashBoradInstructionViewController.getController(storyboard: .Home)
                moduleFor = .student
                self.navigationController?.pushViewController(learningVc, animated: true)
            default:
                return
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, height: CGFloat) {
        switch collectionView {
        case collectionViewTypes:
            self.typesCollectionHeight.constant = height
        default:
            break
        }
    }
}

extension StudentDashboardVC:EventCollectionDataSources{
    func height(_: UICollectionView, height: CGFloat) {
        heightEventCollection.constant = height
    }
    
    func numberOfCell(_: UICollectionView, _ numberOfCell: Int) {
        eventImagesCount = numberOfCell
        pageControl.numberOfPages = eventImagesCount ?? 0
    }
    func selectedCell(_: UICollectionView, selectedData: Any) {
        if selectedData is EventModel{
            let controller = ScoolEventViewController.getController(storyboard: .TeacherDashboard)
            guard let eventData = selectedData as? EventModel else{return}
            controller.webUrl = eventData.videoURL
            controller.title = eventData.videoTitle
            self.navigationController?.pushViewController(controller, animated: true)
        }else{
            guard let youtubeData = selectedData as? YoutubeDataModel else{return}
            CommonUtils.playInYoutube(youtubeId: youtubeData.videoCode ?? "")
        }
    }
    func scrollAt(_ indexPath: IndexPath) {
        schoolEventsCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        pageControl.currentPage = indexPath.item
        currentIndex = indexPath.item
    }
}

extension StudentDashboardVC: UploadProfileDelegate{
    func onSuccessWithImage(_ status: Bool, _ imageUrl: String, _ image: UIImage) {
        if status{
            profileImage.image = image
            kUserData.profileImage = imageUrl
        }
    }
    
    func changeProfile(){
        print("Change Profile")
        if UIDevice.isPhone{
            ImagePickerHelper.shared.showPickerController { [unowned self] (image) -> (Void) in
                self.updateProfileViewModel = UpdateProfilePictureViewModel()
                self.updateProfileViewModel?.dataPassDelegate = self
                self.updateProfileViewModel?.updateProfilePic(userType: .Student, image: (image ?? self.profileImage.image!))
            }
            
        }else{
            Common.showActionSheet(vc: self, firstTitle: "Camera", secondTitle: "Photo Library", sourceView: profileImage) { selectedIndex in
                if selectedIndex == 0 {
                    ImagePickerHelper.shared.picForiPad(forCamera: true) { (image) -> (Void) in
                        DispatchQueue.main.async {
                            self.updateProfileViewModel = UpdateProfilePictureViewModel()
                            self.updateProfileViewModel?.dataPassDelegate = self
                            self.updateProfileViewModel?.updateProfilePic(userType: .Student, image: (image ?? self.profileImage.image!))
                        }
                    }
                } else {
                    ImagePickerHelper.shared.picForiPad(forCamera: false) { (image) -> (Void) in
                        self.updateProfileViewModel = UpdateProfilePictureViewModel()
                        self.updateProfileViewModel?.dataPassDelegate = self
                        self.updateProfileViewModel?.updateProfilePic(userType: .Student, image: (image ?? self.profileImage.image!))
                    }
                }
            }
        }
    }
}
