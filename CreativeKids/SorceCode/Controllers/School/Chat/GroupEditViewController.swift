//
//  GroupEdirViewController.swift
//  Chat Demo
//
//  Created by Creative Kids on 26/03/22.
//

import UIKit

protocol UpdateGroupDelegate{
    func updateGroupDetail()
}
extension UpdateGroupDelegate{
    func updateGroupDetail(){}
}

class GroupEditViewController: UIViewController {
    @IBOutlet weak var tableViewGroupMember: UITableView!
    @IBOutlet weak var groupImage: UIImageView!
    @IBOutlet weak var textFieldGroupName: UITextField!
    @IBOutlet weak var buttonEdit: UIButton!
    var updateGroupDelegate:UpdateGroupDelegate?
    var groupMembers = [UsersState]()
    var blockedUsers = [String]()
    var editCallBack:(()->Void)?
    var groupDetail:GroupModel?
    var navigationReference:UINavigationController?
    let userDetails = UsersState(userdata: chatUserDefault.getuserDetails())
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewGroupMember.register(UINib(nibName: "NewChatTableViewCell", bundle: nil), forCellReuseIdentifier: "NewChatTableViewCell")
        tableViewGroupMember.delegate = self
        tableViewGroupMember.dataSource = self
        initialView()
        
    }
    func initialView(){
        self.textFieldGroupName.text = String.getstring(groupDetail?.name)
        self.groupImage.downlodeImage(serviceurl: String.getstring(groupDetail?.imageUrl), placeHolder: UIImage(systemName: "person.3"))
            ChatHalper.shared.receiveAllUsers{ [weak self] (allUser) in
                let allMember = allUser ?? []
                let groupExistMember = chatSharedInstanse.getStringArray(self?.groupDetail?.user)
                self?.blockedUsers = chatSharedInstanse.getStringArray(self?.groupDetail?.blockedUser)
                self?.groupMembers = allMember.filter{groupExistMember.contains(String.getstring($0.userId)) && String.getstring(self?.userDetails.userId) != String.getstring($0.userId)}
                self?.blockedUsers(users:self?.blockedUsers ?? [])
                
            }
    }
    
    func blockedUsers(users:[String]){
        self.groupMembers.forEach{
            if self.blockedUsers.contains(String.getstring($0.userId)){
                $0.isSelected = true
            }
        }
        self.tableViewGroupMember.reloadData()
    }
    
    @IBAction func textFieldGroupName(_ sender: UITextField) {
        manageCreateButton(textFieldGroupName: sender)
    }
    
    @IBAction func buttonCloseTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func buttonEditTapped(_ sender: UIButton) {
        guard let group = groupDetail else{return}
        group.name = String.getstring(self.textFieldGroupName.text)
        ChatHalper.shared.updateGroupDetails(group: group){
            self.dismiss(animated: true){
                self.editCallBack?()
            }
        }
    }
    @IBAction func buttonGroupImage(_ sender: UIButton) {
        CommonUtils.showHudWithNoInteraction(show: true)
        ChatHalper.shared.uploadGroupProfileImage { groupImage in
            self.groupDetail?.imageUrl = String.getstring(groupImage)
            CommonUtils.showHudWithNoInteraction(show: false)
        }
    }
    @IBAction func buttonAddParticipate(_ sender: UIButton) {
        self.dismiss(animated: false){
            let controller = AddGroupMemberViewController.getController(storyboard: .Chat)
            controller.navigationReference = self.navigationReference
            controller.addMemberFor = .GroupUpdate
            controller.updateGroupDelegate = self.updateGroupDelegate
            controller.groupDetail = self.groupDetail
            controller.existanceUser = chatSharedInstanse.getStringArray(self.groupDetail?.user)
            self.navigationReference?.present(controller, animated: true, completion: nil)
        }
    }
    
    func manageCreateButton(textFieldGroupName:UITextField){
        if  String.getstring(textFieldGroupName.text).isEmpty{
            buttonEdit.isUserInteractionEnabled = false
            buttonEdit.setTitleColor(UIColor.lightGray, for: .normal)
        }else{
            buttonEdit.isUserInteractionEnabled = true
            buttonEdit.setTitleColor(UIColor.link, for: .normal)
        }
    }
}

extension GroupEditViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return groupMembers.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewChatTableViewCell", for: indexPath) as! NewChatTableViewCell
        let object = groupMembers[indexPath.row]
        cell.accessoryType = .disclosureIndicator
        cell.suspendView.isHidden = !object.isSelected
        cell.selectCallBack = {
            if object.isSelected{
                self.showAlertSheet(title: "Continue \(object.name ?? "")"){
                    ChatHalper.shared.unblockMemberInGroup(groupCreatedBy: String.getstring(self.groupDetail?.createdBy), groupId: String.getstring(self.groupDetail?.userid), unblockUsers: [String.getstring(object.userId)]){
                        object.isSelected = !object.isSelected
                        self.tableViewGroupMember.reloadData()
                    }
                }
                
            }else{
                self.showAlertSheet(title: "Suspend \(object.name ?? "")"){
                    ChatHalper.shared.blockMemberInGroup(groupCreatedBy: String.getstring(self.groupDetail?.createdBy), groupId: String.getstring(self.groupDetail?.userid), blockUsers: [String.getstring(object.userId)]){
                        object.isSelected = !object.isSelected
                        self.tableViewGroupMember.reloadData()
                    }
                }
                
            }
            print("selected cell \(indexPath.row)")
        }
        cell.configCell(object: object)
        return cell
        
    }

}


extension GroupEditViewController{
    // To show alertSheet controller with two button and call back to receiver.
        func showAlertSheet(title: String,
                          completion: @escaping() -> ()) -> Void {
        
        let alertController = UIAlertController(title: nil,
                                                message: nil,
                                                preferredStyle: .actionSheet)
        
        let action = UIAlertAction(title: title,
                                        style: .default,
                                        handler: { (alert) in
                                            print("Alert button tapped")
                                            completion()
        })
        
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .destructive,
                                         handler: nil)
        
        alertController.addAction(action)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
