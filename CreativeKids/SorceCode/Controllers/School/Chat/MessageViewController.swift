//
//  ReceiveMessageViewController.swift
//  Chat Demo
//
//  Created by Creative Kids on 01/03/22.
//

import UIKit
import Foundation
import MobileCoreServices
import IQKeyboardManagerSwift

enum ChatType{
    case oneToOne
    case group
}

class MessageViewController: UIViewController {

    
    @IBOutlet weak var receiveMessageTableView: UITableView!
    @IBOutlet weak var viewNoParticipation: UIView!
    @IBOutlet weak var buttonEdit: UIButton!
    @IBOutlet weak var labelChatName: UILabel!
    @IBOutlet weak var imageViewChatPerson: UIImageView!
    @IBOutlet weak var textViewMessage: IQTextView!
    
    @IBOutlet weak var buttonSendImage: UIButton!
    @IBOutlet weak var topHeaderConstraint: NSLayoutConstraint!
    
    var messages = [MessageClass]()
    var senderId:String = ""
    var reloadInitially = true
    var messageSection = [MessageGroup]()
    var recentUser:ResentUsers?
    var chatType:ChatType = .oneToOne
    var groupDetail:GroupModel?
    let userDetails = UsersState(userdata: chatUserDefault.getuserDetails())
    override func viewDidLoad() { 
        super.viewDidLoad()
        textViewMessage.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
       
        statusBarColor(headerColor: UIColor.blue)
        CommonUtils.showHudWithNoInteraction(show: true)
        receiveMessageTableView.delegate = self
        receiveMessageTableView.dataSource = self
        receiveMessageTableView.register(UINib(nibName: "STextCell", bundle: nil), forCellReuseIdentifier: "STextCell")
        receiveMessageTableView.register(UINib(nibName: "RTextCell", bundle: nil), forCellReuseIdentifier: "RTextCell")
        receiveMessageTableView.register(UINib(nibName: "RImageCell", bundle: nil), forCellReuseIdentifier: "RImageCell")
        receiveMessageTableView.register(UINib(nibName: "SImageCell", bundle: nil), forCellReuseIdentifier: "SImageCell")
        receiveMessageTableView.register(UINib(nibName: "EventCell", bundle: nil), forCellReuseIdentifier: "EventCell")
        receiveMessageTableView.register(UINib(nibName: "SDocumentCell", bundle: nil), forCellReuseIdentifier: "SDocumentCell")
        receiveMessageTableView.register(UINib(nibName: "RDocumentCell", bundle: nil), forCellReuseIdentifier: "RDocumentCell")
        initialView()
        switch chatType{
        case .oneToOne:
            ChatHalper.shared.receiveMessage(receiverId: senderId) { [weak self] (message, backupMessage) in
                if !(message?.isEmpty ?? true){
                    self?.messages.removeAll()
                    self?.messages = message ?? []
                    ChatHalper.shared.readMessage(senderId: String.getstring(self?.recentUser?.senderId), receiverId: String.getstring(self?.recentUser?.receiverId), messages: self?.messages)
                    self?.filterMessageArray()
                }
            }
        case .group:
            ChatHalper.shared.receivceMessagefromGroup(groupid: String.getstring(self.recentUser?.receiverId),receiverId: String.getstring(userDetails.userId)){ [weak self] (message, backupMessage) in
                if !(message?.isEmpty ?? true){
                    self?.messages.removeAll()
                    self?.messages = message ?? []
                    ChatHalper.shared.readGroupMessage(groupid: String.getstring(self?.recentUser?.receiverId), receiverId: String.getstring(self?.userDetails.userId), messages: self?.messages)
                    self?.filterMessageArray()
                }
            }
            getGroupDetails()
        }
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
//    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.textViewMessage.resignFirstResponder()
    }
    @objc func keyboardWillShow(notification: NSNotification) {
//        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            print(keyboardSize.height)
//            if self.view.frame.origin.y == 0{
//                self.view.frame.origin.y -= keyboardSize.height
//
//            }
//        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
//        topHeaderConstraint.constant = 0.0
//        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            if self.view.frame.origin.y != 0{
//                self.view.frame.origin.y += keyboardSize.height
//            }
//        }
    }
    
    func initialView(){
        imageViewChatPerson.downlodeImage(serviceurl: String.getstring(self.recentUser?.imageUrl), placeHolder: UIImage(named: chatType == .oneToOne ? "individual_chat 1" : "Group_Chat 1"))
        labelChatName.text = String.getstring(self.recentUser?.receiverName)
        buttonEdit.isHidden = chatType == .oneToOne ? true : false
    }
    
    func getGroupDetails(){
        ChatHalper.shared.getGroupDetail(groupCreatedBy: String.getstring(self.recentUser?.createdBy), groupId: String.getstring(self.recentUser?.receiverId)) { [weak self] group in
            self?.groupDetail = group
            self?.buttonEdit.isHidden = String.getstring(group.adminId) == String.getstring(self?.userDetails.userId)  ? false : true
            if chatSharedInstanse.getStringArray(group.blockedUser).contains(String.getstring(self?.userDetails.userId)){
                self?.viewNoParticipation.isHidden = false
            }else{
                self?.viewNoParticipation.isHidden = true
            }
        }
    }
    
    @IBAction func buttonBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func buttonMessageTapeed(_ sender: UIButton) {
        if (textViewMessage.text?.isEmpty ?? true) {return}
        switch chatType{
        case .oneToOne:
            let message = MessageClass()
            message.receiverId = senderId
            message.mediaType = .text
            message.receiverName = self.recentUser?.receiverName ?? ""
            message.message = textViewMessage.text ?? "Hii"
            ChatHalper.shared.sendMessage(messageDic: message)
            textViewMessage.text = ""
        case .group:
            ChatHalper.shared.getGroupDetail(groupCreatedBy: String.getstring(self.recentUser?.createdBy), groupId: String.getstring(self.recentUser?.receiverId)) { [weak self] group in
                let message = MessageClass()
                message.receiverId = String.getstring(group.userid)
                message.senderId = String.getstring(self?.userDetails.userId)
                message.senderName = String.getstring(self?.userDetails.name)
                message.mediaType = .text
                message.message =  String.getstring(self?.textViewMessage.text ?? "Hii")
                message.receiverName = String.getstring(group.name)
                ChatHalper.shared.sendMessageToGroup(messageDic:message, senderid :String.getstring(self?.userDetails.userId)  , groupid: String.getstring(group.userid),users: chatSharedInstanse.getStringArray(group.user))
                self?.textViewMessage.text = ""
            }
        }
    }
    
    @IBAction func buttonDocumentTapped(_ sender: UIButton) {
//        self.openDocumentPicker(UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF),String(kUTTypeText),String(kUTTypeContent),String(kUTTypeItem),String(kUTTypeData)], in: .import))
        self.openDocumentPicker(UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF),String(kUTTypeText)], in: .import))
    }
    
    
    @IBAction func buttonImageTapped(_ sender: UIButton) {
        getImage(sourceView: sender){[weak self] geImage in
            guard let uploadImage = geImage else{return}
            switch self?.chatType{
            case .oneToOne:
                let message = MessageClass()
                message.receiverId = self?.senderId
                message.mediaType = .photos
                message.receiverName = self?.recentUser?.receiverName ?? ""
                ChatHalper.shared.uploadImage(uploadImage, messageDic: message)
            case .group:
                ChatHalper.shared.getGroupDetail(groupCreatedBy: String.getstring(self?.recentUser?.createdBy), groupId: String.getstring(self?.recentUser?.receiverId)) { [weak self] group in
                    let message = MessageClass()
                    message.receiverId = String.getstring(group.userid)
                    message.senderId = String.getstring(self?.userDetails.userId)
                    message.senderName = String.getstring(self?.userDetails.name)
                    message.mediaType = .photos
                    message.receiverName = String.getstring(group.name)
                    ChatHalper.shared.uploadGroupImage(uploadImage,messageDic: message, groupUsers: chatSharedInstanse.getStringArray(group.user))
                }
            default:
                return
            }
            self?.textViewMessage.resignFirstResponder()
        }
    }
    
    func getImage(sourceView:UIView, completion: @escaping ((UIImage?)->Void)){
        if UIDevice.isPhone{
            ImagePickerHelper.shared.showPickerController { [unowned self] (image) -> (Void) in
                completion(image)
            }
            
        }else{
            Common.showActionSheet(vc: self, firstTitle: "Camera", secondTitle: "Photo Library", sourceView: sourceView) { selectedIndex in
                if selectedIndex == 0 {
                    ImagePickerHelper.shared.picForiPad(forCamera: true) { (image) -> (Void) in
                        DispatchQueue.main.async {
                            completion(image)
                        }
                    }
                } else {
                    ImagePickerHelper.shared.picForiPad(forCamera: false) { (image) -> (Void) in
                        completion(image)
                    }
                }
            }
        }
    }
    
    @IBAction func buttonEditGroup(_ sender: UIButton) {
        let controller = GroupEditViewController.getController(storyboard: .Chat)
        controller.navigationReference = self.navigationController
        controller.groupDetail = self.groupDetail
        controller.updateGroupDelegate = self
        controller.editCallBack = {
            self.labelChatName.text = self.groupDetail?.name
        }
        self.navigationController?.present(controller, animated: true, completion: nil)
    }
    
    
    func filterMessageArray() {
        self.messageSection.removeAll()
        var timeStampArray = [String]()
        self.messages.forEach { (message) in
            let date = message.sendingTime.dateFromTimeStamp()
            let dateStr  = date.toString(withFormat: "MM/dd/yyyy")
            timeStampArray.append(dateStr)
        }
        let arr = Array(Set(timeStampArray))
        timeStampArray = arr
        for time in timeStampArray {
            let date1 = Date.dateFromString(date: time, withCurrentFormat: "MM/dd/yyyy")
            let timestamp = date1.toMillis() ?? 0
            var tmpArr = [MessageClass]()
            self.messages.forEach { (message) in
                let date1 = message.sendingTime.dateFromTimeStamp()
                let dateStr1  = date1.toString(withFormat: "MM/dd/yyyy")
                if time == dateStr1 {
                    tmpArr.append(message)
                }
            }
            self.messageSection.append(MessageGroup(time: timestamp, message: tmpArr))
            self.messageSection.sort{ ($0.sendingTime ?? 0 ) < ($1.sendingTime ?? 0) }
            
            
        }
        CommonUtils.showHudWithNoInteraction(show: false)
        self.receiveMessageTableView.layoutIfNeeded()
        self.receiveMessageTableView.reloadData()
        scrollToBottom()
    }
    func scrollToBottom() {
        DispatchQueue.main.async {
            if Int.getint(self.messageSection.count) != 0  {
                let arr = self.messageSection.last?.message
                let indexPath = IndexPath(row: (arr?.count ?? 0) - 1, section: (self.messageSection.count)-1)
                self.receiveMessageTableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
            }
        }
    }
}

extension MessageViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        messageSection.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.messageSection[section].message?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if messageSection.indices.contains(indexPath.section) == false { return UITableViewCell() }
        let messageObj = self.messageSection[indexPath.section]
        if messageObj.message?.indices.contains(indexPath.row) == false { return UITableViewCell() }
        guard let message = messageObj.message?[indexPath.row] else {return UITableViewCell() }
        
        return tableView.returnMessageCell(messageObject: message, indexPath: indexPath,navigation: self.navigationController)
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        40
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 40))
        view.backgroundColor = .clear
        let headerLabel = UILabel.init(frame: CGRect(x: (UIScreen.main.bounds.width-100)/2, y: 0, width: 100, height: 25))
        headerLabel.backgroundColor = .darkGray
        headerLabel.layer.cornerRadius = 3
        headerLabel.clipsToBounds = true
        headerLabel.textAlignment = .center
        headerLabel.textColor = .white
        headerLabel.font = UIFont.systemFont(ofSize: 10.0)//UIFont(name:"Calibri", size: 10.0)
        view.addSubview(headerLabel)
        if !(section >= self.messageSection.count){
        let timeStamp = self.messageSection[section].sendingTime
        let date1 =  timeStamp?.dateFromTimeStamp()
        if date1?.isToday() ?? false {
            headerLabel.text = "Today"
        }else if date1?.isYesterday() ?? false {
            headerLabel.text = "Yesterday"
        }else {
            let dateStr1  = date1?.toString(withFormat: "dd MMM YYYY")
            headerLabel.text = dateStr1
        }
    }
        return view
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
}


// MARK:- func for initial cellSetup() or updateCell
extension UITableView {
    func returnMessageCell(messageObject:MessageClass? , indexPath: IndexPath, navigation:UINavigationController?) -> UITableViewCell {
//        let parent = self.parentViewController as! ChatVC
        guard let message = messageObject else {return UITableViewCell()}
        // MARK:- switch For handel case for messages cell
        switch message.messageFrom {
        case .sender:
            switch message.mediaType {
            case .text:
                let text = self.dequeueReusableCell(withIdentifier: "STextCell", for: indexPath) as! STextCell
                text.message = message
                return text
            case .create,.join,.kickOutByAdmin,.leave:
                let text = self.dequeueReusableCell(withIdentifier: "EventCell", for: indexPath) as! EventCell
                text.message = message
                return text
//            case .audio:
//                let audio = self.dequeueReusableCell(withIdentifier: "SenderAudioCell", for: indexPath) as! SenderAudioCell
//                audio.updateCell(controller: parent, message: message, indexPath: indexPath)
//                return audio
            case .photos:
                let image = self.dequeueReusableCell(withIdentifier: "SImageCell", for: indexPath) as! SImageCell
                image.message = message
                return image
            case .pdf:
                let doc = self.dequeueReusableCell(withIdentifier: "SDocumentCell", for: indexPath) as! SDocumentCell
                doc.message = message
                doc.didTapCallBack = {
                    let controller = DocumentShowViewController.getController(storyboard: .Chat)
                    controller.headerName = doc.message?.fileName ?? ""
                    controller.webUrl = doc.message?.message ?? ""
                    navigation?.pushViewController(controller, animated: true)
                }
                return doc
//            case .video:
//                let video = self.dequeueReusableCell(withIdentifier: "SVideoCell", for: indexPath) as! SVideoCell
//                video.message = message
//                return video
//            case .leave , .join , .create , .anousement :
//                let text = self.dequeueReusableCell(withIdentifier: "EventCell", for: indexPath) as! EventCell
//                text.message = messageObject
//                return text
            default:
                return UITableViewCell()
            }
        case .receiver :
            switch message.mediaType {
            case .text:
                let text = self.dequeueReusableCell(withIdentifier: "RTextCell", for: indexPath) as! RTextCell
                text.message = messageObject
                return text
//            case .audio:
//                let audio = self.dequeueReusableCell(withIdentifier: "ReceiverAudioCell", for: indexPath) as! ReceiverAudioCell
//                audio.updateCell(controller: parent, message: message, indexPath: indexPath)
//                return audio
            case .create,.join,.kickOutByAdmin,.leave:
                let text = self.dequeueReusableCell(withIdentifier: "EventCell", for: indexPath) as! EventCell
                text.message = message
                return text
            case .photos:
                let image = self.dequeueReusableCell(withIdentifier: "RImageCell", for: indexPath) as! RImageCell
                image.message = message
                return image
            case .pdf:
                let doc = self.dequeueReusableCell(withIdentifier: "RDocumentCell", for: indexPath) as! RDocumentCell
                doc.message = message
                doc.didTapCallBack = {
                    let controller = DocumentShowViewController.getController(storyboard: .Chat)
                    controller.headerName = doc.message?.fileName ?? ""
                    controller.webUrl = doc.message?.message ?? ""
                    navigation?.pushViewController(controller, animated: true)
                }
                return doc
//            case .leave , .join , .create , .anousement:
//                let text = self.dequeueReusableCell(withIdentifier: "EventCell", for: indexPath) as! EventCell
//                text.message = messageObject
//                return text
//            case .video:
//                let video = self.dequeueReusableCell(withIdentifier: "RVideoCell", for: indexPath) as! RVideoCell
//                video.message = message
//                return video
            default:
                return UITableViewCell()
            }
        default:
            return UITableViewCell()
        }
    }
}

extension MessageViewController:UpdateGroupDelegate{
    func updateGroupDetail() {
        self.getGroupDetails()
    }
}

// MARK:-UIDocumentPickerExtendedDelegate
extension MessageViewController:UIDocumentPickerExtendedDelegate{
    func docPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let fileName = url.lastPathComponent
        let pdfUrl = url
        switch self.chatType{
        case .oneToOne:
            let message = MessageClass()
            message.receiverId = self.senderId
            message.mediaType = .pdf
            message.receiverName = self.recentUser?.receiverName ?? ""
            message.fileName = fileName
            ChatHalper.shared.uploadPdfFile(pdfUrl, messageDic: message)
        case .group:
            ChatHalper.shared.getGroupDetail(groupCreatedBy: String.getstring(self.recentUser?.createdBy), groupId: String.getstring(self.recentUser?.receiverId)) { [weak self] group in
                let message = MessageClass()
                message.receiverId = String.getstring(group.userid)
                message.senderId = String.getstring(self?.userDetails.userId)
                message.senderName = String.getstring(self?.userDetails.name)
                message.fileName = fileName
                message.mediaType = .pdf
                message.receiverName = String.getstring(group.name)
                ChatHalper.shared.uploadGroupPdfFile(pdfUrl, messageDic: message, groupUsers: chatSharedInstanse.getStringArray(group.user))
            }
        }
        textViewMessage.resignFirstResponder()
    }
    
}

extension MessageViewController: UITextViewDelegate{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if let getText = textView.text,
                  let textRange = Range(range, in: getText) {
                  let updatedText = getText.replacingCharacters(in: textRange,
                                                    with: text)
            if updatedText.isEmpty{
                buttonSendImage.isHidden = false
            }else{
                buttonSendImage.isHidden = true
            }
      }
        return true
    }
}



