//
//  CreateGroupViewController.swift
//  Chat Demo
//
//  Created by Creative Kids on 25/03/22.
//

import UIKit

class CreateGroupViewController: UIViewController {

    
    @IBOutlet weak var imageGroup: UIImageView!
    @IBOutlet weak var buttonGroupCreated: UIButton!
    @IBOutlet weak var textFieldGroupName: UITextField!
    
    @IBOutlet weak var collectionViewMembers: UICollectionView!
    var groupImage:String?
    var items = [Any]()
    var navigationReference:UINavigationController?
    let userDetails = UsersState(userdata: chatUserDefault.getuserDetails())
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionViewMembers.delegate = self
        collectionViewMembers.dataSource = self
        buttonGroupCreated.isUserInteractionEnabled = false
        buttonGroupCreated.setTitleColor(UIColor.lightGray, for: .normal)
    }

    @IBAction func textFieldGroupName(_ sender: UITextField) {
        manageCreateButton(textFieldGroupName: sender)
    }
    @IBAction func buttonCreateGroupTapped(_ sender: UIButton) {
        var groupUser = items.map{String.getstring(($0 as? UsersState)?.userId)}
        groupUser.append(String.getstring(userDetails.userId))
        let group = GroupModel(adminId: String.getstring(userDetails.userId), adminName: String.getstring(userDetails.name), userid: "", name: String.getstring(textFieldGroupName.text), imageUrl: self.groupImage, user: groupUser, createdBy: String.getstring(userDetails.name))
            ChatHalper.shared.createGroup(group: group)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelTapped(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
    @IBAction func buttonGroupImageTapped(_ sender: UIButton) {
        CommonUtils.showHudWithNoInteraction(show: true)
        ChatHalper.shared.uploadGroupProfileImage { groupImage in
            self.groupImage = String.getstring(groupImage)
            self.imageGroup.downlodeImage(serviceurl: String.getstring(groupImage), placeHolder: nil)
            CommonUtils.showHudWithNoInteraction(show: false)
        }
    }
    
    func manageCreateButton(textFieldGroupName:UITextField){
        if items.isEmpty || String.getstring(textFieldGroupName.text).isEmpty{
            buttonGroupCreated.isUserInteractionEnabled = false
            buttonGroupCreated.setTitleColor(UIColor.lightGray, for: .normal)
        }else{
            buttonGroupCreated.isUserInteractionEnabled = true
            buttonGroupCreated.setTitleColor(UIColor.link, for: .normal)
        }
    }
}

extension CreateGroupViewController: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GroupMemberCollectionViewCell", for: indexPath) as! GroupMemberCollectionViewCell
        cell.configureCell(data: items[indexPath.item])
        cell.deleteCallBACK = {
            self.items.remove(at: indexPath.item)
            self.collectionViewMembers.reloadData()
            self.manageCreateButton(textFieldGroupName:self.textFieldGroupName)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/3, height:  collectionView.frame.width/3)
    }
    
}
