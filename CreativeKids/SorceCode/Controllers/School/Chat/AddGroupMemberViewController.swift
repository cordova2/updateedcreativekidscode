//
//  AddGroupMemberViewController.swift
//  Chat Demo
//
//  Created by Creative Kids on 25/03/22.
//

import UIKit
enum AddMemberFor{
    case GroupCreate
    case GroupUpdate
}

class AddGroupMemberViewController: UIViewController {

    @IBOutlet weak var tableViewNewChat: AddMemberTableView!{
        didSet{
            self.tableViewNewChat.addMemberFor = self.addMemberFor
            self.tableViewNewChat.existanceUser = self.existanceUser
        }
    }
    
    @IBOutlet weak var uiSearchBar: UISearchBar!
    @IBOutlet weak var buttonGroupCreated: UIButton!
    var addMemberFor:AddMemberFor = .GroupCreate
    var updateGroupDelegate:UpdateGroupDelegate?
    var groupDetail:GroupModel?
    var existanceUser = [String]()
    var navigationReference:UINavigationController?
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonGroupCreated.isUserInteractionEnabled = false
        buttonGroupCreated.setTitleColor(UIColor.lightGray, for: .normal)
        uiSearchBar.delegate = self
        tableViewNewChat.dataPassDelegate = self
        initilaView()
    }
    
    @IBAction func textFieldSearch(_ sender: UITextField) {
        tableViewNewChat.filterDataUsingText(searchBy: String.getstring(sender.text?.lowercased()))
    }
    
    @IBAction func buttonCancelTapped(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
    
    @IBAction func buttonCreateTapped(_ sender: UIButton) {
        switch addMemberFor{
        case .GroupCreate:
            let selectedMember = tableViewNewChat.item.filter{($0 as? UsersState)?.isSelected ?? false}
            self.dismiss(animated: false){
                let controller = CreateGroupViewController.getController(storyboard: .Chat)
                controller.items = selectedMember
                controller.navigationReference = self.navigationReference
                self.navigationReference?.present(controller, animated: true, completion: nil)
            }
        case .GroupUpdate:
            let selectedMember = tableViewNewChat.item.filter{($0 as? UsersState)?.isSelected ?? false && !(tableViewNewChat.existanceUser.contains(String.getstring(($0 as? UsersState)?.userId)))}
            ChatHalper.shared.addMemberToGroup(groupCreatedBy: String.getstring(groupDetail?.createdBy), groupId: String.getstring(groupDetail?.userid), addUsers: selectedMember.map{String.getstring(($0 as? UsersState)?.userId)}){
                self.dismiss(animated: false){
                    self.updateGroupDelegate?.updateGroupDetail()
                }
            }
        }
        
    }
    func initilaView(){
        switch addMemberFor{
        case .GroupCreate:
            buttonGroupCreated.setTitle("Create", for: .normal)
        case .GroupUpdate:
            buttonGroupCreated.setTitle("Add", for: .normal)
        }
    }
}

extension AddGroupMemberViewController:DataSourcesDelegate,UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        tableViewNewChat.filterDataUsingText(searchBy: String.getstring(searchText.lowercased()))
    }
    
    func tableView(_ tableView: UITableView, selectedIndex: Int, data: Any?) {
        switch addMemberFor{
        case .GroupCreate:
            guard let kData = data as? UsersState else{return}
            for value in tableViewNewChat.item{
                if String.getstring((value as? UsersState)?.userId) ==  String.getstring((kData.userId)){
                    guard let object = value as? UsersState else{return}
                    object.isSelected = !object.isSelected
                    break
                }
            }
            tableViewNewChat.reloadData()
            manageCreateButton()
        case .GroupUpdate:
            guard let kData = data as? UsersState else{return}
            for value in tableViewNewChat.item{
                if String.getstring((value as? UsersState)?.userId) ==  String.getstring((kData.userId)){
                    guard let object = value as? UsersState else{return}
                    object.isSelected = !object.isSelected
                    break
                }
            }
            tableViewNewChat.reloadData()
            mangeCreateButtonForGroupEdit()
        }
    }
    
    func manageCreateButton(){
        let selectedMember = tableViewNewChat.item.filter{($0 as? UsersState)?.isSelected ?? false}
        buttonGroupCreated.isUserInteractionEnabled = selectedMember.isEmpty ? false : true
        buttonGroupCreated.setTitleColor(selectedMember.isEmpty ? UIColor.lightGray : UIColor.link, for: .normal)
    }
    
    func mangeCreateButtonForGroupEdit(){
        let selectedMember = tableViewNewChat.item.filter{($0 as? UsersState)?.isSelected ?? false && !(tableViewNewChat.existanceUser.contains(String.getstring(($0 as? UsersState)?.userId)))}
        buttonGroupCreated.isUserInteractionEnabled = selectedMember.isEmpty ? false : true
        buttonGroupCreated.setTitleColor(selectedMember.isEmpty ? UIColor.lightGray : UIColor.link, for: .normal)
    }
}
