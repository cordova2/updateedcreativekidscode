//
//  NewChatViewController.swift
//  Chat Demo
//
//  Created by Creative Kids on 24/03/22.
//

import UIKit

class NewChatViewController: UIViewController {

    @IBOutlet weak var tableViewNewChat: TableViewNewChat!
    var navigationReference:UINavigationController?
    @IBOutlet weak var uiSearchBar: UISearchBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        uiSearchBar.delegate = self
        tableViewNewChat.dataPassDelegate = self
    }

    
    @IBAction func buttonCreateNewGroup(_ sender: UIButton) {
        self.dismiss(animated: false){
            let controller = AddGroupMemberViewController.getController(storyboard: .Chat)
            controller.navigationReference = self.navigationReference
            self.navigationReference?.present(controller, animated: true, completion: nil)
        }
    }
    
    @IBAction func buttonCancelTapped(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
    
}
extension NewChatViewController:DataSourcesDelegate, UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        tableViewNewChat.filterDataUsingText(searchBy: String.getstring(searchText.lowercased()))
    }
    func tableView(_ tableView: UITableView, selectedIndex: Int, data: Any?) {
        guard let kData = data as? UsersState else{return}
        self.dismiss(animated: false){
            let controller = MessageViewController.getController(storyboard: .Chat)
            var receiverDetail = ResentUsers()
            receiverDetail.receiverId = String.getstring(kUserData.userId)
            receiverDetail.senderId = String.getstring(kData.userId)
            receiverDetail.receiverName = String.getstring(kData.name)
            receiverDetail.imageUrl = String.getstring(kData.profile_image)
            controller.recentUser = receiverDetail
            controller.senderId = kData.userId ?? ""
            controller.chatType = .oneToOne
            self.navigationReference?.pushViewController(controller, animated: true)
        }
        
    }
}
