//
//  DocumentShowViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 21/06/22.
//

import UIKit
import WebKit

class DocumentShowViewController: UIViewController {

    @IBOutlet weak var webView:WKWebView!
    @IBOutlet weak var labelHeaderName: UILabel!
    @IBOutlet weak var imageDoc: UIImageView!
    var webUrl:String = ""
    var headerName = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.uiDelegate = self
        webView.navigationDelegate = self
        statusBarColor(headerColor: UIColor.blue)
        labelHeaderName.text = headerName
        imageDoc.isHidden = true
        saveFile(url: webUrl, fileName: headerName) { filePath in
            if let filPath = filePath{
                DispatchQueue.main.async {
                    let fileURL = URL(fileURLWithPath: filPath)
                    self.webView.loadFileURL(fileURL, allowingReadAccessTo: fileURL)
                    }
            }
        }
    }
    
    func pushFile(_ destination: URL) {
        let finalURL = destination.absoluteString.replacingOccurrences(of: "file://", with: "shareddocuments://")
           DispatchQueue.main.async {
            if let url = URL(string: finalURL) {
                    if #available(iOS 10, *){
                        UIApplication.shared.open(url)
                    }else{
                        UIApplication.shared.openURL(url)
                    }

            }
           }
    }
    
    
    @IBAction func buttonBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
     func saveFile(url: String, fileName: String, completion: @escaping(_ path: String?) -> ()) -> Void {
            if url.isEmpty {
                return
            }
            DispatchQueue.global().async {
                let profileurl = String(describing: url.replacingOccurrences(of: " ", with: "%20")).replacingOccurrences(of: "\\", with: "/")
                do {
                    let fileData = try Data(contentsOf: URL(string: profileurl)! as URL)
                    let fileManager = FileManager.default
                    let filePath = CommonUtils.getDocumentDirectoryPath() + "/\(fileName)"
                    let isDownloaded = fileManager.createFile(atPath: filePath as String, contents: fileData, attributes: nil)
                    if isDownloaded {
                        completion(filePath)
                    }else {
                        completion("")
                    }
                } catch {
                    print("Unable to load data: \(error)")
                    completion("")
                }
            }
        }
    
    
}



extension DocumentShowViewController: WKUIDelegate,WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        CommonUtils.showHud(show: false)
    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        CommonUtils.showHud(show: true)
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        CommonUtils.showHud(show: false)
    }
}
