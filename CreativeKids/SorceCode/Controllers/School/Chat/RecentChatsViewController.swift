//
//  ChatContactsViewController.swift
//  Chat Demo
//
//  Created by Creative Kids on 09/03/22.
//

import UIKit
import Foundation
import WebKit

    class RecentChatsViewController: UIViewController {
        
        @IBOutlet weak var contactTableView: TableViewChatContact!
        var item = [Any]()
        override func viewDidLoad() {
            super.viewDidLoad()
            statusBarColor(headerColor: UIColor.blue)
            contactTableView.dataPassDelegate = self
        }
        
        @IBAction func buttonBackTapped(_ sender: UIButton) {
            self.navigationController?.popViewController(animated: true)
        }
        
        @IBAction func buttonNewChatTapped(_ sender: UIButton) {
            let controller = NewChatViewController.getController(storyboard: .Chat)
            controller.navigationReference = self.navigationController
            self.navigationController?.present(controller, animated: true, completion: nil)
        }
        
}

extension RecentChatsViewController:DataSourcesDelegate{
    func tableView(_ tableView: UITableView, selectedIndex: Int, data: Any?) {
        guard let kData = data as? ResentUsers else{return}
        let controller = MessageViewController.getController(storyboard: .Chat)
        var receiverDetail = ResentUsers()
        receiverDetail.receiverId = String.getstring(kData.receiverId)
        receiverDetail.senderId = String.getstring(kData.senderId)
        receiverDetail.receiverName = String.getstring(kData.receiverName)
        receiverDetail.imageUrl = String.getstring(kData.imageUrl)
        receiverDetail.createdBy = String.getstring(kData.createdBy)
        controller.senderId = receiverDetail.receiverId ?? ""
        controller.chatType = String.getstring(kData.createdBy).isEmpty ? .oneToOne : .group
        controller.recentUser = receiverDetail
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
}
