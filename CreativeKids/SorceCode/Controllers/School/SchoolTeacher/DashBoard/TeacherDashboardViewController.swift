//
//  TeacherDashboardViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 11/02/22.
//

import UIKit
import KYDrawerController

class TeacherDashboardViewController: UIViewController {
    
    @IBOutlet weak var schoolEventsCollectionView: DashBoardEventCollectionView!
    @IBOutlet weak var viewAssignHomework: UIView!
    @IBOutlet weak var viewSyllabus: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var collectionViewPeriods:PeriodsCollectionView!
    @IBOutlet weak var heightEventCollection: NSLayoutConstraint!
    @IBOutlet weak var labelTeacherName: UILabel!
    @IBOutlet weak var viewTeacherDetail: UIView!
    @IBOutlet weak var heightconstraintsSchedule: NSLayoutConstraint!
    @IBOutlet weak var viewDigitalLearning: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    var eventImagesCount:Int?
    var timer: Timer?
    var currentIndex = 0
    var viewModel:TeacherHomeViewModel?
    var updateProfileViewModel:UpdateProfilePictureViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showTabbar()
        setGradient()
        initialView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showTabbar()
        startTimer()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.hideTabbar()
        self.timer?.invalidate()
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        setGradient()
    }
    
    private func initialView(){
        if !isUserGreetDone{
            getCurrentTimeForGreetUser()
        }
        schoolEventsCollectionView.dataPassDataSources = self
//        heightconstraintsSchedule.constant = kScreenHeight * 0.2
        labelTeacherName.text = String.getstring(kUserData.userName).isEmpty ? "Teacher name" : kUserData.userName
        statusBarColor(headerColor: UIColor.blue)
        self.collectionViewPeriods.dataPassDelegate = self
        viewModel = TeacherHomeViewModel()
        viewModel?.dataPassDataSources = self
        let tapAssignHomework = UITapGestureRecognizer(target: self, action: #selector(assignHomeworkTapped(_:)))
        self.viewAssignHomework.isUserInteractionEnabled = true
        self.viewAssignHomework.addGestureRecognizer(tapAssignHomework)
        
        let tapSyllabus = UITapGestureRecognizer(target: self, action: #selector(syllabusTapped(_:)))
        self.viewSyllabus.isUserInteractionEnabled = true
        self.viewSyllabus.addGestureRecognizer(tapSyllabus)
    }
    
    @IBAction func buttonEditTapped(_ sender: UIButton) {
        changeProfile()
    }
    @IBAction func buttonLeavesTapped(_ sender: UIButton) {
        let leaveVc = LeaveListViewController.getController(storyboard: .StudentDashboard)
        leaveVc.cameFrom = .Teacher
        self.navigationController?.pushViewController(leaveVc, animated: true)
    }
    func startTimer() {
        self.timer =  Timer.scheduledTimer(timeInterval: 8.0, target: self, selector: #selector(self.autoScroll), userInfo: nil, repeats: true)
    }
    
    //MARK: -  Auto Scroll Methods
    @objc func autoScroll() {
        if (eventImagesCount ?? 0) != 0{
            if self.currentIndex < (eventImagesCount ?? 0) - 1 {
                self.currentIndex += 1
                let indexPath = IndexPath(item: currentIndex, section: 0)
                self.schoolEventsCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
                self.pageControl.currentPage = self.currentIndex
            }else{
                self.currentIndex = 0
                self.pageControl.currentPage = self.currentIndex
                self.schoolEventsCollectionView.scrollToItem(at: IndexPath(item: self.currentIndex, section: 0), at: .centeredHorizontally, animated: true)
            }
        }
    }
    
    @objc func assignHomeworkTapped(_ sender: UITapGestureRecognizer) {
        let controller = AssignHomeworkDateViewController.getController(storyboard: .TeacherDashboard)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc func syllabusTapped(_ sender: UITapGestureRecognizer) {
        let controller = SyllabusViewController.getController(storyboard: .TeacherDashboard)
        controller.userType = .Teacher
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    //MARK:- Set Gradient
    func setGradient() {
        DispatchQueue.main.async {
            self.viewTeacherDetail.setgradient(color1: .blue, color2: .systemIndigo)
        }
    }
    
    func matchDate(date:String)->Bool{
        let currentTime = String.getCurrentDate(format: "HH:mm a")
        return currentTime == date
    }
}

extension TeacherDashboardViewController: DataSourcesCollectionDelegate {
    func collectionView(_ collectionView: UICollectionView, data: Any?, selectedIndex: IndexPath) {
//        guard let cell = collectionView.cellForItem(at: selectedIndex) as? PeriodsCollectionViewCell else {return}
        guard let kdata = data as? PeriodModel else{return}
//        if !kdata.isFree{
//            if !(kdata.isOver) && !(kdata.isAttandanceMarked){
//                kdata.isAttandanceMarked = true
//                collectionView.reloadData()
//            }else if !(kdata.isOver) && kdata.isAttandanceMarked{
//                kdata.isAttandanceMarked = true
//                collectionView.reloadData()
//            }else if kdata.isOver && !(kdata.isAttandanceMarked){
//                self.showSimpleAlert(message: "Period has been over. Please contact to your administrator.")
//            }else{
//                return
//            }
//
//        }
        if !kdata.isFree{
            if !(checkTimeForAttendance(data: kdata.startTime ?? "", format: "hh:mm a")){
                self.showSimpleAlert(message: "You cannot mark attendance before class start.")
                return
            }
            else if !(kdata.isAttandanceMarked){
                CommonUtils.showAlert(title: kAppName, message: "Have you take this period.", firstTitle: "Cancel", secondTitle: "Yes") { buttonTitle in
                    if buttonTitle == "Cancel"{
                        return
                    }else if buttonTitle == "Yes"{
                        self.viewModel?.markAttendance(periodId: kdata.id ?? "")
                    }
                }
                
            }
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, height: CGFloat) {
        heightconstraintsSchedule.constant = height
        viewDigitalLearning.isHidden = height == 0.0 ? true : false
    }
}

extension TeacherDashboardViewController: TeacherHomeDataSources{
    func markAttendance(status: Bool, message: String) {
        if status{
            showSimpleAlert(message: message)
            viewModel?.periodApi()
        }else{
            showSimpleAlert(message: message)
        }
    }
    
    func periodData(data: [PeriodModel]) {
        self.collectionViewPeriods.getData(cell: CollectionCellIdentifier.periods, data: data)
    }
}

extension TeacherDashboardViewController:EventCollectionDataSources{
    func height(_: UICollectionView, height: CGFloat) {
        heightEventCollection.constant = height
    }
    func numberOfCell(_: UICollectionView, _ numberOfCell: Int) {
        eventImagesCount = numberOfCell
        pageControl.numberOfPages = eventImagesCount ?? 0
    }
    func selectedCell(_: UICollectionView, selectedData: Any) {
        if selectedData is EventModel{
            let controller = ScoolEventViewController.getController(storyboard: .TeacherDashboard)
            guard let eventData = selectedData as? EventModel else{return}
            controller.webUrl = eventData.videoURL
            controller.title = eventData.videoTitle
            self.navigationController?.pushViewController(controller, animated: true)
        }else{
            guard let youtubeData = selectedData as? YoutubeDataModel else{return}
            CommonUtils.playInYoutube(youtubeId: youtubeData.videoCode ?? "")
        }
    }
    func scrollAt(_ indexPath: IndexPath) {
        schoolEventsCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        pageControl.currentPage = indexPath.item
        currentIndex = indexPath.item
    }
}

extension TeacherDashboardViewController: UploadProfileDelegate{
    func onSuccessWithImage(_ status: Bool, _ imageUrl: String, _ image: UIImage) {
        if status{
            profileImage.image = image
            kUserData.profileImage = imageUrl
        }
    }
    
    func changeProfile(){
        print("Change Profile")
        if UIDevice.isPhone{
            ImagePickerHelper.shared.showPickerController { [unowned self] (image) -> (Void) in
                self.updateProfileViewModel = UpdateProfilePictureViewModel()
                self.updateProfileViewModel?.dataPassDelegate = self
                self.updateProfileViewModel?.updateProfilePic(userType: .Teacher, image: (image ?? self.profileImage.image!))
            }
            
        }else{
            Common.showActionSheet(vc: self, firstTitle: "Camera", secondTitle: "Photo Library", sourceView: profileImage) { selectedIndex in
                if selectedIndex == 0 {
                    ImagePickerHelper.shared.picForiPad(forCamera: true) { (image) -> (Void) in
                        DispatchQueue.main.async {
                            self.updateProfileViewModel = UpdateProfilePictureViewModel()
                            self.updateProfileViewModel?.dataPassDelegate = self
                            self.updateProfileViewModel?.updateProfilePic(userType: .Teacher, image: (image ?? self.profileImage.image!))
                        }
                    }
                } else {
                    ImagePickerHelper.shared.picForiPad(forCamera: false) { (image) -> (Void) in
                        self.updateProfileViewModel = UpdateProfilePictureViewModel()
                        self.updateProfileViewModel?.dataPassDelegate = self
                        self.updateProfileViewModel?.updateProfilePic(userType: .Teacher, image: (image ?? self.profileImage.image!))
                    }
                }
            }
        }
    }
}
