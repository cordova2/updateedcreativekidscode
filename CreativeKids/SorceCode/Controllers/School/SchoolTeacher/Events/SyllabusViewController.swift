//
//  SyllabusViewController.swift
//  CreativeKids
//
//  Created by creativekids solutions on 11/10/22.
//

import UIKit

class SyllabusViewController: UIViewController {
    
    @IBOutlet weak var textFieldClass: UITextField!
    
    @IBOutlet weak var viewClassSelection: UIView!
    var classesArray = [ClassesModel]()
    var assignClassViewModel: AssignChapterViewModel?
    var viewModel:GetSyllabusViewModel?
    var userType:UserType!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        statusBarColor(headerColor: UIColor.blue)
        setupUI()
        viewModel = GetSyllabusViewModel()
        initialsetUp()
    }
    
    func initialsetUp(){
        viewModel?.dataPassDelegate = self
        switch userType{
        case .Student:
            textFieldClass.text = kUserData.className ?? ""
            viewModel?.getSyllabusForStudentApi()
        default:
            return
        }
    }
    
    func setupUI() {
        self.viewClassSelection.isHidden = userType == .Student
       let tapClass = UITapGestureRecognizer(target: self, action: #selector(textfieldClassTapped(_:)))
        self.textFieldClass.isUserInteractionEnabled = true
        self.textFieldClass.addGestureRecognizer(tapClass)
    }
    
    @objc func textfieldClassTapped(_ sender: UITapGestureRecognizer) {
        if !classesArray.isEmpty{
            showClasses()
        }else{
            self.assignClassViewModel = AssignChapterViewModel.init(getClasscompletion: { classData in
                self.classesArray = classData
                showClasses()
            })
        }
        
        //MARK: - Show Class Data
        func showClasses(){
            if !classesArray.isEmpty{
                let controller = SelectClassSubjectViewController.getController(storyboard: .TeacherDashboard)
                controller.modalTransitionStyle = .crossDissolve
                controller.modalPresentationStyle = .overCurrentContext
                controller.data = classesArray
                controller.selectedDelegate = self
                controller.field = self.textFieldClass
                self.present(controller, animated: true, completion: nil)
            }
        }
    }
    @IBAction func buttonBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension SyllabusViewController: SelectedDataDelegate {
    func selecetd(textField: UITextField, item: SelectionDelegate) {
        self.textFieldClass.text = item.name
        viewModel?.getSyllabusForTeacherApi()
    }
}

extension SyllabusViewController:GetSyllabusDelegate{
    func onSuccess(_ data: Any) {
        print(data)
    }
    
    
}
