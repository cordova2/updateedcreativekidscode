//
//  ScoolEventViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 23/09/22.
//

import UIKit
import WebKit

class ScoolEventViewController: UIViewController {

    @IBOutlet weak var labelEventName:UILabel!
    @IBOutlet weak var webView:WKWebView!
    var webUrl:String?
    var eventTitle:String?
    override func viewDidLoad() {
        super.viewDidLoad()
        loadWebView()
    }
    
    func loadWebView(){
        labelEventName.text = eventTitle
        guard let url = webUrl else{return}
        let webURL = URL(string: url)
        let request = URLRequest(url: webURL!)
        webView.load(request)
    }
    
    @IBAction func buttonBackTapped(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
}
