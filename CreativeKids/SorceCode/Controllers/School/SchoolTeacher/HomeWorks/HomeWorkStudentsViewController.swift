//
//  HomeWorkStudentsViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 15/09/22.
//

import UIKit

class HomeWorkStudentsViewController: UIViewController {

    @IBOutlet weak var collectionViewStudents: UICollectionView!
    var viewModel:StudentsViewModel?
    var assignedWork:HomeWorkModel?
    var students = [StudentWork]()
    var selectedIndex:Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        initialView()
    }
    
    private func initialView(){
        statusBarColor(headerColor: UIColor.blue)
        collectionViewStudents.register(UINib(nibName: CollectionCellIdentifier.studentWorkList, bundle: nil), forCellWithReuseIdentifier: CollectionCellIdentifier.studentWorkList)
        collectionViewStudents.dataSource = self
        collectionViewStudents.delegate = self
        viewModel = StudentsViewModel(assiedWork: assignedWork)
        viewModel?.dataPassDataSources = self
    }

    @IBAction func buttonBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension HomeWorkStudentsViewController:UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        collectionView.getNumberOfCell(numberofCell: students.count, message: "No Student Submit Work Yet!")
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionCellIdentifier.studentWorkList, for: indexPath) as! StudentWorkCollectionViewCell
        cell.configCell(data: students[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let object = students[indexPath.item]
        selectedIndex = indexPath.item
        self.viewModel?.getStudentHomeWork(studentData: object)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.width/3, height: collectionView.width/3)
    }
}

extension HomeWorkStudentsViewController:StudentsWorkSubmitDataSources{
    func studentWorked(data: Any) {
        guard let studentWork = data as? String else{return}
        print(studentWork)
        let controller = HomeWorkWithQuestionViewController.getController(storyboard: .TeacherDashboard)
        controller.questionImageArr = ["https://picsum.photos/200","https://picsum.photos/200/300","https://picsum.photos/200","https://picsum.photos/200/300","https://picsum.photos/200","https://picsum.photos/200/300","https://picsum.photos/200","https://picsum.photos/200/300","https://picsum.photos/200","https://picsum.photos/200/300"]
        controller.answerImageArr = ["https://picsum.photos/200","https://picsum.photos/200/300"]
        controller.studentsDetails = students[selectedIndex]
        controller.reloadData = self
//        controller.cameFor = .Teacher(1)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func getAllStudent(data: [StudentWork]) {
        students = data
        collectionViewStudents.reloadData()
    }
}

extension HomeWorkStudentsViewController:ReloadDataDataSources{
    func reload() {
        self.students[selectedIndex].workDetail.isChecked = true
        self.collectionViewStudents.reloadItems(at: [IndexPath(item: selectedIndex, section: 0)])
    }
}
