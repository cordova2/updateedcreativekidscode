//
//  StudentHomeworkViewController.swift
//  CreativeKids
//
//  Created by Creative Kids on 16/03/22.
//

import UIKit
import IQKeyboardManagerSwift


class StudentHomeworkViewController: UIViewController {
    
    enum ScreenFor{
        case Assiged
        case Update
    }
    @IBOutlet weak var fileImageCollectionView: UICollectionView!
    @IBOutlet weak var heightCollectionView: NSLayoutConstraint!
    
    @IBOutlet weak var labelClass: UILabel!
    @IBOutlet weak var labelSubject: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var textFieldChapterName: UITextField!
    @IBOutlet weak var textViewQuestion: IQTextView!
    @IBOutlet weak var buttonSubmit: UIButton!
    @IBOutlet weak var buttonAttechedFile: UIButton!
    var viewModel:AssignWorkViewModel?
    var reloadBackData:ReloadDataDataSources?
    var cameFor:ScreenFor = .Assiged
    var assignedData:HomeWorkModel?
    var imageArr:[Any?] = []{
        didSet{
            buttonAttechedFile?.setTitle(imageArr.isEmpty ? "Add Picture" : "Add More Pictures", for: .normal)
        }
    }
    var forCollectionViewHeight:Int{
        get{
            let count = imageArr.count
            return (count % 4) == 0 ? count/4 : (count/4) + 1
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        statusBarColor(headerColor: UIColor.blue)
        
        initialView()
        
    }
    
    private func initialView(){
        self.labelClass.text = "\(assignedData?.className ?? "") \(assignedData?.section ?? "")"
        self.textFieldChapterName.delegate = self
        self.labelDate.text = assignedData?.date ?? ""
        self.labelSubject.text = assignedData?.subjectName ?? ""
        viewModel = AssignWorkViewModel(controller: self)
        viewModel?.dataPassDataSources = self
        switch cameFor{
        case .Update:
            buttonAttechedFile?.setTitle(imageArr.isEmpty ? "Add Picture" : "Add More Pictures", for: .normal)
            buttonSubmit.setTitle("Update", for: .normal)
        case .Assiged:
            buttonSubmit.setTitle("Submit", for: .normal)
        }
    }
    
    @IBAction func buttonAttachFileTapped(_ sender: UIButton) {
        if UIDevice.isPhone{
            ImagePickerHelper.shared.showPickerController { [unowned self] (image) -> (Void) in
                self.imageArr.insert(image ?? UIImage(), at: 0)
                self.fileImageCollectionView.reloadData()
            }
            
        }else{
            Common.showActionSheet(vc: self, firstTitle: "Camera", secondTitle: "Photo Library", sourceView: fileImageCollectionView) { selectedIndex in
                if selectedIndex == 0 {
                    ImagePickerHelper.shared.picForiPad(forCamera: true) { (image) -> (Void) in
                        DispatchQueue.main.async {
                            self.imageArr.insert(image ?? UIImage(), at: 0)
                            self.fileImageCollectionView.reloadData()
                        }
                    }
                } else {
                    ImagePickerHelper.shared.picForiPad(forCamera: false) { (image) -> (Void) in
                        self.imageArr.insert(image ?? UIImage(), at: 0)
                        self.fileImageCollectionView.reloadData()
                    }
                }
            }
        }
        
    }
    @IBAction func buttonBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonSubmitTapped(_ sender: UIButton) {
        switch cameFor{
        case .Update:
            viewModel?.updateHomeWork()
        case .Assiged:
            viewModel?.assignHomeWork()
        }
    }
}

extension StudentHomeworkViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.heightCollectionView.constant = CGFloat(forCollectionViewHeight) * (fileImageCollectionView.frame.width/4) + 10
        return imageArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeworkImageCollectionCell", for: indexPath) as! HomeworkImageCollectionCell
        if let imageData = imageArr[indexPath.item] as? UIImage{
            cell.imageViewFile.image = imageData
        }else{
            if let imageData = imageArr[indexPath.item] as? String{
                cell.imageViewFile.downloadImageFromURL(urlString: imageData){ image in
                    self.imageArr[indexPath.item] = image
                }
            }
        }
        
        cell.removeImageButtonCallback = {
            self.imageArr.remove(at: indexPath.item)
            self.fileImageCollectionView.reloadData()
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 4, height: collectionView.frame.width / 4)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let previewVC = ImagePreviewViewController.getController(storyboard: .TeacherDashboard)
        let images = self.imageArr
        previewVC.imageArr = images
        previewVC.cameFor = .Teacher
        previewVC.selcetedIndex = indexPath.item
        self.navigationController?.pushViewController(previewVC, animated: true)
    }
}

extension StudentHomeworkViewController:AddHomeWorkDataSources{
    func onSuccess(_ status: Bool) {
        reloadBackData?.reload()
        self.navigationController?.popViewController(animated: true)
    }
}

extension StudentHomeworkViewController:UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        viewModel?.getChapter(subid: assignedData?.subjectId ?? ""){ chaptersArray in
            var datasource = chaptersArray
           // datasource = [TopicSearchModel(data: ["chapterName":"Kuch bhi data dikh raha hai"])]
            if !datasource.isEmpty {
                CommonUtils.showDropDown(sender: textField, dataSources: datasource.map{$0.chapterName ?? ""}){ (index,value) in
                    textField.text = value
                }
            }
        }
    }
}
