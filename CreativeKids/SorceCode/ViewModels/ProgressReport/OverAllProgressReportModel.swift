//
//  OverAllProgressReportModel.swift
//  CreativeKids
//
//  Created by Creative Kids on 17/07/21.
//

import Foundation
import UIKit

class OverAllProgressViewModel {
    var vc:OverAllProgressViewController?
    init(viewController:UIViewController,complitionHandler: @escaping ((Dict)->())){
        self.vc = viewController as? OverAllProgressViewController
        getPerformanceApi(){ data in
            complitionHandler(data)
        }
    }
    
    func getPerformanceApi(complition: @escaping ((Dict)->())){
        let className = ""
        let serviceUrl = "?Email=\(kUserData.userEmail ?? "")&=abc&cls=\(className)".replacingOccurrences(of: " ", with: "%20")
        BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { (response, statusCode) in
            if statusCode == 200 {
                print("response: ------\(response)")
                let performanceDetailArray = kSharedInstance.getArray(withDictionary: response[getResponce])
                let performanceDetailInDict = kSharedInstance.getDictionary(performanceDetailArray.first)
                print(performanceDetailInDict)
                complition(performanceDetailInDict)
            } else {
                showAlertMessage.alert(message: "NO Data Found")
            }
        }
    }
}
