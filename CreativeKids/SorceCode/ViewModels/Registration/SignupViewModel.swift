//
//  SignupViewModel.swift
//  CreativeKids
//
//  Created by Creative Kids on 20/03/21.
//

import Foundation
import UIKit

class SignupViewModel{
    
    var vc:SignupViewController?
    init(viewController:SignupViewController){
        self.vc = viewController
        setModel()
        
    }
    //gfhfg@gmail.com
    //432453
    
    func setModel(){
        validationField()
    }
    
    //MARK: validationField
    func validationField(){
        if String.getString(vc?.textFieldName.text).isEmpty {
            showAlertMessage.alert(message: Notifications.kName)
            return
        }else if String.getString(vc?.textFieldMobileNumber.text).isEmpty{
            showAlertMessage.alert(message: Notifications.kEnterMobileNumber)
            return
        }else if !String.getString(vc?.textFieldMobileNumber.text).isPhoneNumber(){
            showAlertMessage.alert(message: Notifications.kEnterValidMobileNumber)
            return
        }else if String.getString(vc?.textFieldEmailID.text).isEmpty {
            showAlertMessage.alert(message: Notifications.kEmailId)
            return
        }
        else if !String.getString(vc?.textFieldEmailID.text).isEmail() {
            showAlertMessage.alert(message: Notifications.kEnterValidEmailId)
            return
        }
        else if String.getString(vc?.textFieldPassword.text).isEmpty {
            showAlertMessage.alert(message: Notifications.kEnterPassword)
            return
        }else if String.getString(vc?.textFieldPassword.text).count < 8 {
            showAlertMessage.alert(message: Notifications.kPasswordLength)
            return
        }
        else if String.getString(vc?.textFieldSchoolName.text).isEmpty {
            showAlertMessage.alert(message: Notifications.kSchoolName)
            return
        }
        else if String.getString(vc?.textFieldCity.text).isEmpty {
            showAlertMessage.alert(message: Notifications.kAddress)
            return
        }else if String.getString(vc?.textFieldClass.text).isEmpty {
            showAlertMessage.alert(message: Notifications.kEnterClass)
            return
        }
        else if !(vc?.btnTermsCondition.isSelected ?? true) {
            showAlertMessage.alert(message: Notifications.kTermsCondition)
            return
        }
        vc?.view.endEditing(true)
        signupApi()
    }
    
    //MARK: signupApi
    func signupApi(){
        let serviceName = "?register=xyz".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let params:Dict = [ApiParameters.kname:vc?.textFieldName.text ?? "",
                           ApiParameters.kpassword:vc?.textFieldPassword.text ?? "",
                           ApiParameters.kmobileNumber:vc?.textFieldMobileNumber.text ?? "",
                           ApiParameters.kemail:vc?.textFieldEmailID.text ?? "",
                           ApiParameters.kcity:vc?.cityName ?? "None",
                           ApiParameters.kState:vc?.stateName ?? "None",
                           ApiParameters.studentClass:vc?.textFieldClass.text ?? "",
                           ApiParameters.parentEmail:vc?.textFieldGuardianEmailId.text ?? "",
                           ApiParameters.teacherEmail:vc?.textFieldTeacherEmailId.text ?? "",
                           ApiParameters.schoolName:vc?.textFieldSchoolName.text ?? "",
                           ApiParameters.termAndCondition:"1"]
    
        BaseController.shared.postToServerAPI(url: serviceName, params: params, type: .POST) { (response, statusCode) in
            if statusCode == 200 {
                let responceMessage = String.getString(response[getResponce])
                if responceMessage == "Email Id already registered"{
                    showAlertMessage.alert(message: responceMessage)
                    return
                }else{
                CommonUtils.showToast(message: responceMessage)
                kSharedAppManager.moveToLogin()
                }
                //  self.vc?.navigationController?.popViewController(animated: true)
            } else {
                showAlertMessage.alert(message: "User is Already registered")
            }
        }
    }
}
