//
//  ApiRouter.swift
//  CreativeKids
//
//  Created by Creative Kids on 20/03/21.
//  simulator iphone 8 udid = EF5FA907-9A5C-43CD-BA04-AE70E47CF5CA
//  ipad = 566742A7-CF56-4153-881B-3369C17C7A3C

import Foundation
import UIKit

enum UserRole:Int{
    case schoolStudent = 1
    case schoolTeacher
    case normalUser
}

class LoginViewModel{
    var vc:LoginViewController?
    init(viewController:LoginViewController){
        self.vc = viewController
        setModel()
        
    }
    
    func setModel(){
        validationField()
    }
    
    //MARK: validationField
    func validationField(){
        if String.getString(vc?.textFieldMailID.text).isEmpty {
            showAlertMessage.alert(message: Notifications.kEmailId)
            return
        }else if String.getString(vc?.textFieldPassword.text).isEmpty {
            showAlertMessage.alert(message: Notifications.kEnterPassword)
            return
        }
        vc?.view.endEditing(true)
        loginApi()
    }
    
    //MARK: loginApi
    func loginApi(){
        let serviceName = "?email=\(vc?.textFieldMailID.text ?? "")&uniqno=\(getUDID.getDeviceIdFromKeyChain())&pass=\(vc?.textFieldPassword.text ?? "")&img=xyz&ios=ios"
        BaseController.shared.postToServerAPI(url: serviceName, params: [:], type: .GET) { (response, statusCode) in
            if statusCode == 200 {
                print("response: ------\(response)")
                let data = kSharedInstance.getDictionary(response[getResponce])
                if String.getString(data["uniqno"]) == getUDID.getDeviceIdFromKeyChain() || String.getString(data["uniqno"]) == ""{
                    kUserData.dataSave(data: data)
                    let  loginData = kSharedUserDefaults.getLoggedInUserDetails()["role"] as? String
                    DispatchQueue.main.async {
                        switch loginData {
                        case String.getstring(UserRole.schoolStudent.rawValue):
                            self.accessTokenApi(){
                                cameFrom = .login
                                kSharedUserDefaults.setUserLoggedIn(userLoggedIn: true)
                                kSharedAppManager.moveToStudentDashboard()
                            }
                        case String.getstring(UserRole.schoolTeacher.rawValue):
                            self.accessTokenApi{
                                cameFrom = .login
                                kSharedUserDefaults.setUserLoggedIn(userLoggedIn: true)
                                kSharedAppManager.moveToTeacherDashboard()
                            }
                        default:
                            if ["Class-9","Class-10"].contains(String.getString(data["classname"])){
                                kSharedUserDefaults.setUserClass(userClass: ["className":String.getString(data["classname"]),"classType":ClassType.ncert])
                            }else{
                                kSharedUserDefaults.setUserClass(userClass: ["className":String.getString(data["classname"]),"classType":ClassType.cordova])
                            }
                            self.userSubscribeApi()
                        }
                    }
                    
                }else{
                    self.showPopUpForLogout(roleId: String.getstring(data["role"]))
                }
            } else {
                showAlertMessage.alert(message: "Invalid Credential")
            }
        }
    }
    
    //MARK: get user AccessToken
    func accessTokenApi(completion: @escaping (()->Void)){
        CommonUtils.showHudWithNoInteraction(show: true)
        let serviceName = "https://api.cordovalearningsolutions.com/Auth/Authenticate"
        let params:Dict = ["Userid":vc?.textFieldMailID.text ?? "","password":vc?.textFieldPassword.text ?? ""]
        
        guard let url = URL(string: serviceName) else {return}
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        guard let httpbody = try? JSONSerialization.data(withJSONObject: params, options:[]) else {return}
        request.httpBody = httpbody
        
        let session = URLSession.shared
        session.dataTask(with: request) {data, response, error  in
            CommonUtils.showHudWithNoInteraction(show: false)
            DispatchQueue.main.async {
                if let response = response {
                    print(response)
                }
                if let data = data{
                    let mdata = String(data: data, encoding: .utf8)
                    print("This is data: \(mdata!)")
                    kSharedUserDefaults.setLoggedInAccessToken(loggedInAccessToken: String.getstring(mdata))
                    completion()
                }
            }
        }.resume()
        //                BaseController.shared.postToServerAPI(url: serviceName, params: params, type: .POST) { (response, statusCode) in
        //                    if statusCode == 200 {
        //                        print("response: ------\(response)")
        //                        let data = kSharedInstance.getDictionary(response[getResponce])
        //                        kSharedUserDefaults.setLoggedInAccessToken(loggedInAccessToken: String.getstring(data["token"]))
        //                        completion()
        //                    } else {
        //                        showAlertMessage.alert(message: "User Not Authenticate.")
        //                    }
        //                }
    }
    
    //MARK: User Subscribed Data Api
    func userSubscribeApi(){
        let serviceName = "?Email=\(vc?.textFieldMailID.text ?? "")&abc=xyz"
        BaseController.shared.postToServerAPI(url: serviceName, params: [:], type: .GET) { (response, statusCode) in
            if statusCode == 200 {
                print("response: ------\(response)")
                let data = kSharedInstance.getArray(withDictionary: response[getResponce])
                let saveDataToUserDefalut:[String:Any] = ["data":data]
                kUserData.subscriptionData = data.map{SubjectModel(data: $0)}
                kSharedUserDefaults.setUserSubscribtionDetail(subscribtionDetail: saveDataToUserDefalut)
                cameFrom = .login
                moduleFor = .normal
                kSharedUserDefaults.setUserLoggedIn(userLoggedIn: true)
                kSharedAppManager.moveToHome()
                
            } else {
                showAlertMessage.alert(message: "Unable to get user subscription Data")
            }
        }
    }
    
    //MARK: Modify Device Id
    func changeUniqueId(roleId:String){
        var serviceURL:String = ""
        switch roleId{
        case String.getstring(UserRole.schoolStudent.rawValue),String.getstring(UserRole.schoolTeacher.rawValue):
            serviceURL = "?ERPuniqNO=\(getUDID.getDeviceIdFromKeyChain())&mail=\(vc?.textFieldMailID.text ?? "")".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        case String.getstring(UserRole.normalUser.rawValue):
            serviceURL = "?uniqNO=\(getUDID.getDeviceIdFromKeyChain())&Email=\(vc?.textFieldMailID.text ?? "")".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        default:
            return
        }
        BaseController.shared.postToServerAPI(url: serviceURL, params: [:], type: .GET) { (response, statusCode) in
            if statusCode == 200 {
                kSharedAppManager.moveToLogin()
            } else {
                showAlertMessage.alert(message: "Something error")
            }
        }
    }
    
    //MARK: AlertView For Logout
    func showPopUpForLogout(roleId:String){
        let alert = UIAlertController.init(title: "Creative Kids", message: "Already logged in some other device, Do you want to logout.", preferredStyle: .alert)
        let cancel = UIAlertAction.init(title: "Cancel", style: .destructive){ _ in
            alert.dismiss(animated: true, completion: nil)
        }
        let Logout = UIAlertAction.init(title: "Logout", style: .default){ _ in
            alert.dismiss(animated: true){
                self.changeUniqueId(roleId: roleId)
            }
        }
        alert.addAction(cancel)
        alert.addAction(Logout)
        self.vc?.present(alert, animated: true, completion: nil)
    }
}//DGA21261110666538313074

// CHECK API USING URL SESSION MANAGER
//        func loginApiCheck(){
//            let session = URLSession.shared
//            let url = URL(string: "https://creativekidssolutions.com/api/student/?Email=prashantcoolboy234@gmail.com&clsa=L.K.G (B)")!
//            var url1 = URLRequest(url: url)
//            url1.httpMethod = "GET"
//            url1.setValue("application/json", forHTTPHeaderField: "Content-Type")
//            let task1 = session.dataTask(with: url1) { (data, urlResponce, error) in
//                print(data)
//                if error != nil || data == nil{
//                    print("client error")
//                }
//                print((urlResponce as? HTTPURLResponse)?.statusCode)
//                guard let response = urlResponce as? HTTPURLResponse, (200...399).contains(response.statusCode) else {
//                        print("Server error!")
//                        return
//                    }
//
//                    guard let mime = response.mimeType, mime == "application/json" else {
//                        print("Wrong MIME type!")
//                        return
//                    }
//
//                    do {
//                        let json = try JSONSerialization.jsonObject(with: data!, options: [])
//                        print(json)
//                    } catch {
//                        print("JSON error: \(error.localizedDescription)")
//                    }
//            }
//            task1.resume()
//    //        let task = session.dataTask(with: url) { (data, urlResponce, error) in
//    //            print(data)
//    //            if error != nil || data == nil{
//    //                print("client error")
//    //            }
//    //            print((urlResponce as? HTTPURLResponse)?.statusCode)
//    //            guard let response = urlResponce as? HTTPURLResponse, (200...500).contains(response.statusCode) else {
//    //                    print("Server error!")
//    //                    return
//    //                }
//    //
//    //                guard let mime = response.mimeType, mime == "application/json" else {
//    //                    print("Wrong MIME type!")
//    //                    return
//    //                }
//    //
//    //                do {
//    //                    let json = try JSONSerialization.jsonObject(with: data!, options: [])
//    //                    print(json)
//    //                } catch {
//    //                    print("JSON error: \(error.localizedDescription)")
//    //                }
//    //        }
//    //        task.resume()
//        }



