//
//  AdditionalDetailModel.swift
//  CreativeKids
//
//  Created by Creative Kids on 26/08/21.
//
import Foundation
import UIKit

class AdditionalDetailViewModel{
    
    var vc:AdditionalDetailViewController?
    init(viewController:AdditionalDetailViewController){
        self.vc = viewController
        setModel()
    }
    
    func setModel(){
        validationField()
    }
    
    //MARK: validationField
    func validationField(){
        if String.getString(vc?.textFieldTeacherEmailId.text).isEmpty &&  String.getString(vc?.textFieldGuardianEmailId.text).isEmpty{
            showAlertMessage.alert(message: "Please enter Email Id",controller: vc!)
        return
        }else if !(String.getString(vc?.textFieldGuardianEmailId.text).isEmpty) && !(String.getString(vc?.textFieldGuardianEmailId.text)).isEmail() {
            showAlertMessage.alert(message: "Please enter valid parents email Id",controller: vc!)
            return
        }else if !(String.getString(vc?.textFieldTeacherEmailId.text).isEmpty) && !(String.getString(vc?.textFieldTeacherEmailId.text)).isEmail(){
            showAlertMessage.alert(message: "Please enter valid teacher email Id",controller: vc!)
            return
        }
        vc?.view.endEditing(true)
        additionalDetailApi()
    }
    
    //MARK: loginApi
    func additionalDetailApi(){
        let serviceName = "?email=\(kUserData.userEmail ?? "")&t_email=\(vc?.textFieldTeacherEmailId.text ?? "")&p_email=\(vc?.textFieldGuardianEmailId.text ?? "")".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        BaseController.shared.postToServerAPI(url: serviceName, params: [:], type: .GET) { (response, statusCode) in
            if statusCode == 200 {
                print("response: ------\(response)")
                let data = String.getString(response[getResponce])
                var userData:[String:Any] = kSharedUserDefaults.getLoggedInUserDetails()
                userData["parentEmail"] = self.vc?.textFieldGuardianEmailId.text ?? ""
                userData["teacherEmail"] = self.vc?.textFieldTeacherEmailId.text ?? ""
                kUserData.parentEmail = self.vc?.textFieldGuardianEmailId.text ?? ""
                kUserData.teacherEmail = self.vc?.textFieldTeacherEmailId.text ?? ""
                kSharedUserDefaults.setLoggedInUserDetails(loggedInUserDetails: userData)
                self.vc?.dismiss(animated: true, completion: nil)
            } else {
                showAlertMessage.alert(message: "Invalid Credential")
            }
        }
    }              
}
