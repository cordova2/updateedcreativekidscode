//
//  ForgotViewModel.swift
//  CreativeKids
//
//  Created by Creative Kids on 20/03/21.
//

import Foundation
import UIKit

class ForgotViewModel{
    
    //MARK: VARIABLE
    var vc:ForgotPasswordViewController?
    init(viewController:ForgotPasswordViewController){
        self.vc = viewController
        setModel()
    }
    
    func setModel(){
        validationField()
    }
    
    //MARK: validation
    func validationField(){
        if String.getString(vc?.textFieldEmailID.text).isEmpty {
            showAlertMessage.alert(message: Notifications.kEmailId)
            return
        }else if !String.getString(vc?.textFieldEmailID.text).isEmail() {
            showAlertMessage.alert(message: Notifications.kEnterValidEmailId)
            return
        }
        vc?.view.endEditing(true)
        forgotApi()
    }
    
    //MARK: forgotApi
    func forgotApi(){
        let serviceName = "?email=\(vc?.textFieldEmailID.text ?? "")"
        BaseController.shared.postToServerAPI(url: serviceName, params: [:], type: .GET) { (response, statusCode) in
            if statusCode == 200 {
                let responceMessage = String.getString(response[getResponce])
                if responceMessage == "This mail id is not registered"{
                    showAlertMessage.alert(message: responceMessage)
                }else{
                    CommonUtils.showToast(message: responceMessage)
                    AppManager.shared.moveToLogin()
                }
            } else {
                showAlertMessage.alert(message: "Wrong email id")
            }
        }
    }
}
