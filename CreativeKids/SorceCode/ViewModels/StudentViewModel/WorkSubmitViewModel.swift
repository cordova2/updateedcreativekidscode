//
//  WorkSubmitViewModel.swift
//  CreativeKids
//
//  Created by Prashant Swain on 14/10/22.
//

import Foundation
import UIKit

protocol WorkSubmitDelegate{
    func onSuccess(_ data:Any)
}

class WorkSubmitViewModel{
    
    var dataPassDelegate:WorkSubmitDelegate?
    var vc:UploadHomeworkViewController?
    init(controller:UploadHomeworkViewController){
        vc = controller
    }
    //MARK: Assignment Checked BY USER
    
    func assignmentSubmitByStudentApi(answerImageArray:[Any]){
        let serviceName = kStudentBaseUrl + "SubmitHomework"
        let params:Dict =  [ApiParameters.studentID:"\(kUserData.userId ?? "")",ApiParameters.subjectId:"\(vc?.work?.subjectId ?? "")",ApiParameters.homeWorkID:"\(vc?.work?.assignmentId ?? "")"]
        let imageArray = answerImageArray.compactMap{["imageName":ApiParameters.imageName,"image":($0 as! UIImage)]}
        BaseController.shared.postToServerRequestMultiPart(serviceName, params: params, imageParams: imageArray) { (response, statusCode) in
            if statusCode == 200 {
                print("response: ------\(response)")
                let data = kSharedInstance.getDictionary(response[getResponce])
                self.dataPassDelegate?.onSuccess(true)
                showAlertMessage.alert(message: String.getstring(data["message"]))
            }
            else {
                showAlertMessage.alert(message: "Something went wrong Upload Again." )
            }
        }
    }
    
    func assignmentUpdatedByStudentApi(answerImageArray:[Any]){
        let serviceName = kStudentBaseUrl + "UpdateHomeWorkFile"
        let params:Dict =  [ApiParameters.studentID:"\(kUserData.userId ?? "")",ApiParameters.studentSubmittedId:"\(vc?.work?.studentHomeworkDetails?.workSubmittedId ?? "")"]
        let imageArray = answerImageArray.compactMap{["imageName":ApiParameters.imageName,"image":($0 as! UIImage)]}
        BaseController.shared.postToServerRequestMultiPart(serviceName, params: params, imageParams: imageArray) { (response, statusCode) in
            if statusCode == 200 {
                print("response: ------\(response)")
                let data = kSharedInstance.getDictionary(response[getResponce])
                self.dataPassDelegate?.onSuccess(true)
                showAlertMessage.alert(message: String.getstring(data["message"]))
            }
            else {
                showAlertMessage.alert(message: "Something went wrong Upload Again." )
            }
        }
    }
}
