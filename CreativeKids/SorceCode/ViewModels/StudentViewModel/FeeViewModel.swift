//
//  FeeViewModel.swift
//  CreativeKids
//
//  Created by Creative Kids on 19/04/22.
//

import Foundation

//MARK: FEES MODEL FOR API
class FeesModel{
    var reciptNo:String
    var amountToPay:String
    var receivedBy:String
    var dueAmount:String
    var addmissionFee:String
    var tutionFee:String
    var transportChagres:String
    var annualChagres:String
    var activityChagres:String
    var fineAmount:String
    var cautionAmount:String
    var prospectusAmount:String
    var paidAmount:String
    var paidDate:String
    var month:String
    var paymentMode:String
    var isSelected: Bool = false
    
    init(data:Dict){
        reciptNo = String.getstring(data["ReciptNo"])
        amountToPay = String.getstring(data["payable"])
        receivedBy = String.getstring(data["recivedby"])
        dueAmount = String.getstring(data["due"])
        addmissionFee = String.getstring(data["Admissionfee"])
        tutionFee = String.getstring(data["TutionFee"])
        transportChagres = String.getstring(data["TransportCharges"])
        annualChagres = String.getstring(data["AnnualCharges"])
        activityChagres = String.getstring(data["ActivityCharges"])
        fineAmount = String.getstring(data["fine"])
        cautionAmount = String.getstring(data["CautionMoney"])
        prospectusAmount = String.getstring(data["Prospectus"])
        paidAmount = String.getstring(data["paid"])
        paidDate = String.getstring(data["CreatedDate"])
        month = String.getstring(data["month"])
        paymentMode = String.getstring(data["Paymode"])
    }
}

class FeeViewModel{
    var dataCompletion:(([FeesModel])->Void)?
    init(completion:@escaping (([FeesModel])->Void)){
        getFeesDetails()
        self.dataCompletion = completion
    }
    
    //MARK: GET STUDENT FEES DETAILS
    func getFeesDetails(){
        let serviceUrl = "?AdmissionNo=\(String.getstring(kUserData.admissionNo))"
        BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { response, statusCode in
            let returnData = kSharedInstance.getArray(withDictionary: response[getResponce])
            let dataArray = returnData.map{FeesModel(data: $0)}
            self.dataCompletion?(dataArray)
            print(dataArray)
     }
   }
}
