//
//  StudentAttendanceViewModel.swift
//  CreativeKids
//
//  Created by Creative Kids on 24/06/22.
//

import Foundation

struct StudentAttendanceDetailModel{
    var totalAttendance:String
    var totalPresent:String
    var totalAbsent:String
    var totalLeave:String
    var date:String
    var status:String
    init(data:Dict) {
        totalAttendance = String.getstring(data["totaldays"])
        totalPresent = String.getstring(data["present"])
        totalAbsent = String.getstring(data["absent"])
        totalLeave = String.getstring(data["leave"])
        date = String.getstring(data["dates"])
        status = String.getstring(data["status"])
    }
}

class StudentAttendViewModel{
    
    var viewController:StudentAttendanceViewController?
    var callBackDateAccording:(([StudentAttendanceDetailModel])->Void)?
    init(vc:StudentAttendanceViewController){
        self.viewController = vc
    }
    
    func studentAttendanceDetail(completion: @escaping ((StudentAttendanceDetailModel)->Void)){
        let serviceUrl = "?stuid=\(String.getstring(kUserData.userId))&schid=\(String.getstring(kUserData.schid))".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { response, statusCode in
            let returnData = kSharedInstance.getArray(withDictionary: response[getResponce])
            let data = StudentAttendanceDetailModel(data: returnData.first ?? [:])
            completion(data)
     }
    }
    //2022-05-28T00:00:00
    func studentAttendanceDateAcc(){
        let serviceUrl = "?fromdt=\(String.getstring(viewController?.textFieldFromDate.text))&todt=\(String.getstring(viewController?.textFieldToDate.text))&stuid=\(String.getstring(kUserData.userId))&schid=\(String.getstring(kUserData.schid))".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
//        let serviceUrl = "?fromdt=05/28/2022&todt=06/02/2022&stuid=721&schid=3".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { response, statusCode in
            let returnData = kSharedInstance.getArray(withDictionary: response[getResponce])
            let dataArray =  returnData.map{StudentAttendanceDetailModel(data:$0)}
            self.callBackDateAccording?(dataArray)
     }
    }
}

