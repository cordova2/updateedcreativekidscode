//
//  StudentPerformance.swift
//  CreativeKids
//
//  Created by Prashant Swain on 20/10/22.
//

import Foundation

struct PerformanceData{
    var exerciseType:String
    var setId:String
    var totalQuestion:String
    var totalMarks:String
    var scoredMarks:String
    var totalCorrectAnswer:String
    var totalTimeSpend:String
}

class StudentPerformance{
    init(){}
    func videoSeenForPerforManceApi(totalVideoTime:Int,seenTime:Int,data:TopicModel){
          let dispatchQueue = DispatchQueue(label: "VideoSeen", qos: .background)
        dispatchQueue.async{
            var serviceName = "http://api.cordovalearningsolutions.com/Students/UpdateVideoSeenStatus".replacingOccurrences(of: " ", with: "%20")
            let params:Dict = ["studentId":String.getString(kUserData.userId),"SchoolId":String.getString(kUserData.schid),"ChapterId":String.getString(data.chapterId),"TopicId":String.getString(data.topicId),"VideoTime":String.getString(totalVideoTime),"WatchTime":String.getString(seenTime)]
            BaseController.shared.postToServerAPI(url: serviceName, params: params, type: .POST,showHud: false) { (response, statusCode) in
                if statusCode == 200 {
                    let responceMessage = String.getString(response[getResponce])
                    print(responceMessage)
                } else {
                    print("wrror in responce")
                }
            }
        }
        
    }
    
    func exercisePerforManceApi(data:TopicModel,performanceData:PerformanceData){
        let dispatchQueue = DispatchQueue(label: "QueueIdentification", qos: .background)
        dispatchQueue.async{
            var serviceName =  kStudentBaseUrl + "UpdateExercisePerformance".replacingOccurrences(of: " ", with: "%20")
            let params:Dict = [ApiParameters.studentID:String.getstring(kUserData.userId),
                               ApiParameters.schId:String.getstring(kUserData.schid),
                               ApiParameters.chapterId:String.getstring(data.chapterId),
                               ApiParameters.setId:performanceData.setId,
                               ApiParameters.exerciseType:performanceData.exerciseType,
                               ApiParameters.totalQuestionCount:performanceData.totalQuestion,
                               ApiParameters.totalMarks:performanceData.totalMarks,
                               ApiParameters.scoredMarks:performanceData.scoredMarks,
                               ApiParameters.correctAnswerCount:performanceData.totalCorrectAnswer,
                               ApiParameters.totalSpendTime:performanceData.totalTimeSpend,
                               ApiParameters.subId:String.getstring(data.subjectId)]
            BaseController.shared.postToServerAPI(url: serviceName, params: params, type: .POST,showHud: false) { (response, statusCode) in
                if statusCode == 200 {
                    let responceMessage = String.getString(response[getResponce])
                    print(responceMessage)
                } else {
                    print("wrror in responce")
                }
            }
        }
    }
    
    func LtpMockPerforManceApi(data:TopicSearchModel,performanceData:PerformanceData){
        let dispatchQueue = DispatchQueue(label: "QueueIdentification", qos: .background)
        dispatchQueue.async{
            var serviceName =  kStudentBaseUrl + "UpdateExercisePerformance".replacingOccurrences(of: " ", with: "%20")
            let params:Dict = [ApiParameters.studentID:String.getstring(kUserData.userId),
                               ApiParameters.schId:String.getstring(kUserData.schid),
                               ApiParameters.chapterId:String.getstring(data.chapterNumber),
                               ApiParameters.setId:performanceData.setId,
                               ApiParameters.exerciseType:performanceData.exerciseType,
                               ApiParameters.totalQuestionCount:performanceData.totalQuestion,
                               ApiParameters.totalMarks:performanceData.totalMarks,
                               ApiParameters.scoredMarks:performanceData.scoredMarks,
                               ApiParameters.correctAnswerCount:performanceData.totalCorrectAnswer,
                               ApiParameters.totalSpendTime:performanceData.totalTimeSpend,
                               ApiParameters.subId:String.getstring(data.sunjectId)]
            BaseController.shared.postToServerAPI(url: serviceName, params: params, type: .POST,showHud: false) { (response, statusCode) in
                if statusCode == 200 {
                    let responceMessage = String.getString(response[getResponce])
                    print(responceMessage)
                } else {
                    print("wrror in responce")
                }
            }
        }
    }
}
