//
//  HomeWorkViewModel.swift
//  CreativeKids
//
//  Created by Prashant Swain on 14/10/22.
//

import Foundation

protocol HomeWorkDelegate{
    func onSuccess(_ data:Any)
}
class HomeWorkViewModel{
    
    var dataPassDelegate:HomeWorkDelegate?
    func workQuestionApi(assignmentId:String){
        let serviceUrl = kStudentBaseUrl + "GetHomeWorkBySubject/\(assignmentId)"
            BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { (response, statusCode) in
                if statusCode == 200 {
                    print("response: ------\(response)")
                    let data = kSharedInstance.getDictionary(response[getResponce])
                    let homeWorkData = AssignmentModel(data: data)
                    print(homeWorkData)
                    self.dataPassDelegate?.onSuccess(homeWorkData)
                } else {
                    showAlertMessage.alert(message: "No HomeWork Question are found.")
                }
            }
    }
    
    func submittedWorkApi(assignmentId:String){
        let serviceUrl = kStudentBaseUrl + "GetSubmittedHomeWork/\(kUserData.userId ?? "")/\(assignmentId)"
            BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { (response, statusCode) in
                if statusCode == 200 {
                    print("response: ------\(response)")
                    let data = kSharedInstance.getDictionary(response[getResponce])
                    let homeWorkData = AssignmentModel(data: data)
                    print(homeWorkData)
                    self.dataPassDelegate?.onSuccess(homeWorkData)
                } else {
                    showAlertMessage.alert(message: "No HomeWork Question are found.")
                }
            }
    }
}
