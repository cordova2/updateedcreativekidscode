//
//  ResetPassword.swift
//  CreativeKids
//
//  Created by Creative Kids on 27/03/21.
//

import Foundation
import UIKit

class ResetPasswordViewModel{
    
    var vc:ResetPasswordViewController?
    init(viewController:ResetPasswordViewController){
        self.vc = viewController
        setModel()
    }
    func setModel(){
        validationField()
    }
    
    func validationField(){
        if String.getString(vc?.textFieldOldPassword.text).isEmpty {
            showAlertMessage.alert(message: Notifications.kEnterOldPassword)
            return
        }else if String.getString(vc?.textFieldNewPassword.text).isEmpty {
            showAlertMessage.alert(message: Notifications.kEnterNewPassword)
            return
        }else if String.getString(vc?.textFieldNewPassword.text).count < 8 {
            showAlertMessage.alert(message: Notifications.kPasswordLength)
            return
        }else if String.getString(vc?.textFieldNewPassword.text) != String.getString(vc?.textFieldConfirmPassword.text) {
            showAlertMessage.alert(message: Notifications.kConfirmPasswordMatch)
            return
        }
        self.vc?.view.endEditing(true)
        resetPasswordApi()
    }
    
    func resetPasswordApi(){
        let serviceName = "?userid=\(kUserData.userId ?? "")&Oldpass=\(vc?.textFieldOldPassword.text ?? "")&Newpass=\(vc?.textFieldNewPassword.text ?? "")".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        BaseController.shared.postToServerAPI(url: serviceName, params: [:], type: .GET) { (response, statusCode) in
            if statusCode == 200 {
                let responceMessage = String.getString(response["returnData"])
                if responceMessage == "Record Not Found...!!!"{
                    showAlertMessage.alert(message: responceMessage)
                }else{
                    CommonUtils.showToast(message: responceMessage)
                    kSharedAppManager.moveToHome()
                }
            } else {
                showAlertMessage.alert(message: "Bad request")
            }
        }
    }
}
