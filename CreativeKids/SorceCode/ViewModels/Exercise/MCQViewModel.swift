//
//  MCQViewModel.swift
//  CreativeKids
//
//  Created by Creative Kids on 24/03/21.
//

import Foundation
import UIKit

class MCQModelView {
    var vc: MCQViewController?
    
    init(vc: MCQViewController, completionHandler: @escaping (() -> Void)) {
        self.vc = vc
        mcqApi {
            completionHandler()
        }
    }
    public func mcqApi(completion: @escaping (() -> Void)) {
        
        let serviceUrl = "?subid=\(vc?.selectedTopic?.subjectId ?? "")&Chapid=\(vc?.selectedTopic?.chapterId ?? "")&topicid=\(vc?.selectedTopic?.topicId ?? "")&cls=\(vc?.selectedTopic?.className ?? "")&Exercise=\(vc?.exercise?.exercise ?? "")"
        guard let urlString = serviceUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {return}
        BaseController.shared.postToServerAPI(url: urlString, params: [:], type: .GET) { (response, _) in
            let data = kSharedInstance.getArray(withDictionary: response[getResponce] )
            self.vc?.mcqQuestionsModel = data.map{MCQQuestionModel.init(MCQData: $0)}
            completion()
        }
    }
}

class VSAQViewModel {
    var vc: VeryShortQuestionViewController?
    
    init(viewController: VeryShortQuestionViewController, completionHandler: @escaping (() -> Void)) {
        self.vc = viewController
        VSAQQuestionsApi {
            completionHandler()
        }
    }
    public func VSAQQuestionsApi(completion: @escaping (() -> Void)) {
        
        let serviceUrl = "?subid=\(vc?.selectedTopic?.subjectId ?? "")&chapid=\(vc?.selectedTopic?.chapterId ?? "")&topicid=\(vc?.selectedTopic?.topicId ?? "")&cls=\(vc?.selectedTopic?.className ?? "")&Exercise=\(vc?.exercise?.exercise ?? "")&type=\(vc?.exercise?.exerciseType ?? "")"
        guard let urlString = serviceUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
        BaseController.shared.postToServerAPI(url: urlString, params: [:], type: .GET) { (response, _) in
            let data = kSharedInstance.getArray(withDictionary: response[getResponce] )
            self.vc?.VSAQQuestionsModel = data.map{VSAQQuestionModel.init(VSAQData: $0)}
            completion()
        }
    }
}

class LTPViewModel {
    var vc: LTPViewController?
    
    init(viewController: LTPViewController, completionHandler: @escaping (() -> Void)) {
        self.vc = viewController
        LTPQuestionsApi {
            completionHandler()
        }
    }
    public func LTPQuestionsApi(completion: @escaping (() -> Void)) {
        let serviceUrl = "?chapid=\(vc?.selectedTopic?.chapterNumber ?? "")&sid=\(vc?.selectedTopic?.sunjectId ?? "")&type=\(vc?.exercise?.exerciseType ?? "")&ltp=\(vc?.exercise?.exercise ?? "")&cls=\(vc?.selectedTopic?.className ?? "")"
        guard let urlString = serviceUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {return}
        BaseController.shared.postToServerAPI(url: urlString, params: [:], type: .GET) { (response, _) in
            let data = kSharedInstance.getArray(withDictionary: response[getResponce] )
            self.vc?.LTPQuestionsModel = data.map{VSAQQuestionModel.init(VSAQData: $0)}
            completion()
        }
    }
}

class ExcelExamViewModel {
    
    var vc:ExcelExamViewController?
    
    init(viewController: ExcelExamViewController, completionHandeler: @escaping (() -> Void)) {
        self.vc = viewController
        ExcelExamApi {
            completionHandeler()
        }
    }
    public func ExcelExamApi(completion: @escaping (() -> Void)) {
        let serviceUrl = "?chapid=\(vc?.selectedTopic?.chapterNumber ?? "")&sid=\(vc?.selectedTopic?.sunjectId ?? "")&type=\(vc?.exercise?.exerciseType ?? "")&cls=\(vc?.selectedTopic?.className ?? "")"
        BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { (response, _) in
            let data = kSharedInstance.getArray(withDictionary: response[getResponce])
            self.vc?.excelExamModel = data.map{ExcelExamQuestionModel.init(ExcelExam: $0)}
            completion()
        }
    }
}
class DNDViewModel {
    var vc: DNDViewController?
    
    init(viewController: DNDViewController, completionHandler: @escaping (() -> Void)) {
        self.vc = viewController
        DNDQuestionsApi {
            completionHandler()
        }
    }
    public func DNDQuestionsApi(completion: @escaping (() -> Void)) {
        //?subid=267&chapid=3&topicid=1&cls=Class-7&Exercise=vH;kl 4&type=DND
        //"?subid=245&chapid=1&topicid=1&cls=Class-5&Exercise=Exercise 2&type=DND"
        let serviceUrl = "?subid=\(vc?.selectedTopic?.subjectId ?? "")&chapid=\(vc?.selectedTopic?.chapterId ?? "")&topicid=\(vc?.selectedTopic?.topicId ?? "")&cls=\(vc?.selectedTopic?.className ?? "")&Exercise=\(vc?.exercise?.exercise ?? "")&type=\(vc?.exercise?.exerciseType ?? "")"
        guard let urlString = serviceUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {return}
        BaseController.shared.postToServerAPI(url: urlString, params: [:], type: .GET) { (response, _) in
            let data = kSharedInstance.getArray(withDictionary: response[getResponce] )
            self.vc?.DNDQuestionsModel = data.map{VSAQQuestionModel.init(VSAQData: $0)}
            completion()
        }
    }
}

class PDFDataViewModel {
    var vc: GetPDFViewController?
    
    init(viewController: GetPDFViewController, completionHandler: @escaping (() -> Void)) {
        self.vc = viewController
        getPDFApi {
            completionHandler()
        }
    }
    public func getPDFApi(completion: @escaping (() -> Void)) {
        let serviceUrl = "?chapid=\(vc?.selectedTopic?.chapterNumber ?? "")&sid=\(vc?.selectedTopic?.sunjectId ?? "")&ltp=\(vc?.exercise?.exerciseType ?? "")&cls=\(vc?.selectedTopic?.className ?? "")"
        BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { (response, _) in
            let data = kSharedInstance.getArray(withDictionary: response[getResponce] )
            self.vc?.pdfModelData  = PDFModel.init(data: kSharedInstance.getDictionary(data))
            completion()
        }
    }
}

class SendExerciseResultView {
    var vc: UIViewController?
    
    init(viewController: UIViewController, completionHandler: @escaping (() -> Void)) {
        self.vc = viewController
        getexerciseResult {
            completionHandler()
        }
    }
    public func getexerciseResult(completion: @escaping (() -> Void)) {
        var serviceUrl = ""
        if let DNDcontroller = self.vc as? DNDResultViewController {
            serviceUrl = "?setid=\(DNDcontroller.setId ?? "")&totalques=\(DNDcontroller.labeltotalQuestion.text ?? "")&right=\(DNDcontroller.labelScore.text ?? "")&tim=\(DNDcontroller.totlaTime ?? "")&email=\(kUserData.userEmail ?? "")&type=\(DNDcontroller.exercise?.exerciseType ?? "")"
        } else {
            if let LTPcontroller = self.vc as? ResultViewController {
                serviceUrl = "?setid=\(LTPcontroller.modelArr.first?.setid ?? "")&totalques=\(LTPcontroller.labeltotalQuestion.text ?? "")&right=\(LTPcontroller.labelScore.text ?? "")&tim=\(kSharedUserDefaults.object(forKey: "time_count_LTP") ?? "")&email=\(kUserData.userEmail ?? "")&type=\(LTPcontroller.exercise?.exerciseType ?? "")"
            }
        }
       
        BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { (response, _) in
            completion()
        }
    }
}

class MockTestViewModel {
    var vc: QuestionBankViewController?
    
    init(viewController: QuestionBankViewController, completionHandler: @escaping (() -> Void)) {
        self.vc = viewController
        getMockTestApi {
            completionHandler()
        }
    }
    public func getMockTestApi(completion: @escaping (() -> Void)) {
        let serviceUrl = "?sid=\(vc?.selectedTopic?.sunjectId ?? "")&chapid=\(vc?.selectedTopic?.chapterNumber ?? "")&type=\(vc?.mockTest?.exerciseType ?? "")&ltp=\(vc?.mockTest?.exercise ?? "")&cls=\(vc?.selectedTopic?.className ?? "")&MT=mt".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { (response, _) in
            let data = kSharedInstance.getArray(withDictionary: response[getResponce] )
           // self.vc?.DNDQuestionsModel = data.map{VSAQQuestionModel.init(VSAQData: $0)}
            self.vc?.mockTestModelData = data.map{VSAQQuestionModel.init(VSAQData: $0)}
            print(data)
            completion()
        }
    }
}

class MathsMockTestViewModel {
    var vc: MathsMockTestViewController?
    
    init(viewController: MathsMockTestViewController, completionHandler: @escaping (() -> Void)) {
        self.vc = viewController
        getMockTestApi {
            completionHandler()
        }
    }
    public func getMockTestApi(completion: @escaping (() -> Void)) {
        let serviceUrl = "?sid=\(vc?.selectedTopic?.sunjectId ?? "")&chapid=\(vc?.selectedTopic?.chapterNumber ?? "")&type=\(vc?.mockTest?.exerciseType ?? "")&ltp=\(vc?.mockTest?.exercise ?? "")&cls=\(vc?.selectedTopic?.className ?? "")&MT=mt".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { (response, _) in
            let data = kSharedInstance.getArray(withDictionary: response[getResponce] )
           // self.vc?.DNDQuestionsModel = data.map{VSAQQuestionModel.init(VSAQData: $0)}
            self.vc?.mockTestModelData = data.map{VSAQQuestionModel.init(VSAQData: $0)}
            print(data)
            completion()
        }
    }
}
