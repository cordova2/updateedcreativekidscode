//
//  AssignWorkViewModel.swift
//  CreativeKids
//
//  Created by Prashant Swain on 07/10/22.
//

import Foundation
import UIKit

protocol AddHomeWorkDataSources{
    func onSuccess(_ status:Bool)
}

class AssignWorkViewModel{
    var vc:StudentHomeworkViewController?
    var dataPassDataSources:AddHomeWorkDataSources?
    init(controller:StudentHomeworkViewController){
        vc = controller
    }
    
    func isValidated()->Bool{
        if (vc?.textViewQuestion.text.isEmpty ?? true) && (vc?.imageArr.isEmpty ?? true){
            showAlertMessage.alert(message: Notifications.kQuestionImageEmpty)
            return false
        }else{
            return true
        }
    }
    
    func assignHomeWork(){
        if isValidated(){
            let serviceName = kTeacherBaseUrl + "SubmitHomework"
            let params:Dict =  [ApiParameters.teacherId:"\(kUserData.userId ?? "")",ApiParameters.periodId:"\(vc?.assignedData?.periodID ?? "")",ApiParameters.date:"\(String.getCurrentDate(format: "yyyy-MM-dd"))",ApiParameters.question:"\(vc?.textViewQuestion.text ?? "")", ApiParameters.chapter: vc?.textFieldChapterName.text ?? ""]
            //        let imageArray = vc?.imageArr.map{["imageName":ApiParameters.imageName,"image":$0]}
            let imageArray = vc?.imageArr.compactMap{["imageName":ApiParameters.imageName,"image":($0 as! UIImage)]}
            BaseController.shared.postToServerRequestMultiPart(serviceName, params: params, imageParams: imageArray!) { (response, statusCode) in
                if statusCode == 200 {
                    print("response: ------\(response)")
                    let data = kSharedInstance.getDictionary(response[getResponce])
                    self.dataPassDataSources?.onSuccess(true)
                    showAlertMessage.alert(message: String.getstring(data["message"]))
                    
                }else if statusCode == 404{
                    print("data not found")
                }
                else {
                    showAlertMessage.alert(message: "Something went wrong")
                }
            }
        }
    }
    
    //MARK: Update HomeWork
    
    func updateHomeWork(){
        if isValidated(){
            let serviceName = kTeacherBaseUrl + "UpdateHomework"
            let params:Dict =  [ApiParameters.homeWorkID:"\(vc?.assignedData?.id ?? "")",ApiParameters.question:"\(vc?.textViewQuestion.text ?? "")", ApiParameters.chapter: vc?.textFieldChapterName.text ?? ""]
            let imageArray = vc?.imageArr.compactMap{["imageName":ApiParameters.imageName,"image":($0 as! UIImage)]}
            BaseController.shared.postToServerRequestMultiPart(serviceName, params: params, imageParams: imageArray!) { (response, statusCode) in
                let data = kSharedInstance.getDictionary(response[getResponce])
                if statusCode == 200 {
                    print("response: ------\(response)")
                    self.dataPassDataSources?.onSuccess(true)
                    showAlertMessage.alert(message: String.getstring(data["message"]))
                }else if statusCode == 404{
                    showAlertMessage.alert(message: String.getstring(data["title"]))
                }
                else {
                    showAlertMessage.alert(message: "Something went wrong")
                }
            }
        }
    }
    
    //MARK: getSubject
    func getChapter(subid: String,completion: @escaping (([TopicSearchModel])->())) {
        BaseController.shared.postToServerAPI(url: "?subid=\(subid)", params: [:], type: .GET) { response, _ in
            let returnData = kSharedInstance.getArray(withDictionary: response[getResponce])
            let chapterData = returnData.map{TopicSearchModel.init(data: $0)}
            completion(chapterData)
        }
    }
}
