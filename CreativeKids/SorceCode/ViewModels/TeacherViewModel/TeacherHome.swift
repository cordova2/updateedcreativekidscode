//
//  TeacherHome.swift
//  CreativeKids
//
//  Created by Prashant Swain on 17/09/22.
//

import Foundation

protocol TeacherHomeDataSources{
    func periodData(data:[PeriodModel])
    func markAttendance(status:Bool, message:String)
}
class TeacherHomeViewModel{
    
    var dataPassDataSources:TeacherHomeDataSources?
    
    init(){
        periodApi()
    }
    
// MARK: GET SCHEDULE
    func periodApi(){
        let serviceName = kTeacherBaseUrl + "TeacherSchedule/\(kUserData.userId ?? "")/\(kUserData.schid ?? "")/?_date=\(String.getCurrentDate(format: "yyyy-MM-dd"))"
        BaseController.shared.postToServerAPI(url: serviceName, params: [:], type: .GET) { (response, statusCode) in
            if statusCode == 200 {
                print("response: ------\(response)")
                let data = kSharedInstance.getArray(withDictionary: response[getResponce])
                let periodDataArray = data.map{PeriodModel(data: $0)}.sorted{$0.number < $1.number}
                self.dataPassDataSources?.periodData(data: periodDataArray)
            }else if statusCode == 404{
                print("data not found")
                self.dataPassDataSources?.periodData(data: [])
            }
            else {
                showAlertMessage.alert(message: "Something went wrong")
            }
        }
    }
   
    // MARK: MARK ATTENDANCE
    func markAttendance(periodId:String){
        let serviceName = kTeacherBaseUrl + "MarkAttendanceForPeriod/\(kUserData.userId ?? "")/\(periodId)/?_date=\(String.getCurrentDate(format: "yyyy-MM-dd"))"
        BaseController.shared.postToServerAPI(url: serviceName, params: [:], type: .GET) { (response, statusCode) in
            if statusCode == 200 {
                let data = kSharedInstance.getDictionary(response[getResponce])
                self.dataPassDataSources?.markAttendance(status: true, message: data["message"] as? String ?? "")
                print("response: ------\(response)")
            }else if statusCode == 404{
                print("data not found")
                self.dataPassDataSources?.markAttendance(status: false, message: "Attendance not marked try again.")
            }
            else {
                showAlertMessage.alert(message: "Something went wrong")
            }
        }
    }

}
