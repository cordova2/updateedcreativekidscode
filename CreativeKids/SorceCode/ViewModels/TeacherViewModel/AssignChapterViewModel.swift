//
//  AssignChapterViewModel.swift
//  CreativeKids
//
//  Created by Creative Kids on 29/03/22.
//

import Foundation
import UIKit

class  AssignChapterViewModel {
    var vc: AssignChaptersViewController?
    
    var classDataCallback: (([ClassesModel])->())?
    
    init(viewController: AssignChaptersViewController, completionHandler: @escaping (([ClassesModel])->())) {
        self.vc = viewController
        self.classDataCallback = completionHandler
        self.getClass()
    }
    
    init(getClasscompletion: @escaping (([ClassesModel])->())){
        self.classDataCallback = getClasscompletion
        self.getClass()
    }
    
    func getClass() {
        BaseController.shared.postToServerAPI(url: "?clss=abc", params: [:], type: .GET) { response, statusCode in
            let returnData = kSharedInstance.getArray(withDictionary: response[getResponce])
            print(returnData)
            let classData = returnData.map{String.getString($0["classname"])}
            let classDataArray = returnData.map{ClassesModel(data: $0)}
            self.classDataCallback?(classDataArray)
        }
    }
    func getSubject(completion: @escaping (([SubjectByChapterModel])->())) {
        let serviceUrl = "?clssa=\(vc?.textfieldSelectClass.text ?? "")".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { response, statusCode in
            let returnData = kSharedInstance.getArray(withDictionary: response[getResponce])
            self.vc?.subjectModel = returnData.map{SubjectByChapterModel.init(subjectData: $0)}
            let subjectArray = returnData.map{SubjectByChapterModel.init(subjectData: $0)}
//            let subjectData = returnData.map{String.getString($0["titlename"])}
            completion(subjectArray)
            
        }
    }
    func getChapter(subid: String,completion: @escaping (([TopicSearchModel])->())) {
//        let serviceUrl = "?subid=\(subid)&schoolId=\(kUserData.schid ?? "")"
        let serviceUrl = "?subid=\(subid)"
        BaseController.shared.postToServerAPI(url: "\(serviceUrl)", params: [:], type: .GET) { response, _ in
            let returnData = kSharedInstance.getArray(withDictionary: response[getResponce])
            let chapterData = returnData.map{TopicSearchModel.init(data: $0)}
            self.vc?.tableViewChapter.reloadData()
            completion(chapterData)
        }
    }
//    func assignChapter(chapter:TopicSearchModel,completion:@escaping(()->Void)) {
//        let chapterArr = vc?.chapterModel?.filter{$0.isSelected}
//        let chapterIds = chapterArr?.compactMap{String.getString($0.chapterUniqueIdId)}
//        let chapteridString = chapterIds?.joined(separator: ",")
//        let subId = vc?.chapterModel?.filter{$0.isSelected}
//        let classname = vc?.chapterModel?.filter{$0.isSelected}
//
//        let serviceUrl = "?subid=\(subId?.first?.sunjectId ?? "")&teachrid=\(kUserData.userId ?? "")&chapid=\(chapteridString ?? "")&clas=\(classname?.first?.className ?? "")&schid=\(kUserData.schid ?? "")"
//
//        BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { responseData, _ in
//            showAlertMessage.alert(message: "Chapter Enabled Successfully!")
//            completion()
//        }
//    }
    
    
    func assignChapter(chapter:TopicSearchModel,completion:@escaping(()->Void)) {

        let chapteridString = String.getString(chapter.chapterUniqueIdId)
        let subId = String.getString(chapter.sunjectId)
        let classname = String.getString(chapter.className)
        
        let serviceUrl = "?subid=\(subId)&teachrid=\(kUserData.userId ?? "")&chapid=\(chapteridString)&clas=\(classname)&schid=\(kUserData.schid ?? "")"
        
        BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { responseData, _ in
            showAlertMessage.alert(message: "Chapter Enabled Successfully!")
            completion()
        }
    }

}
