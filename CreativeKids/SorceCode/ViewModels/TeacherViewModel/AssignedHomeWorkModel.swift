//
//  AssignedHomeWorkModel.swift
//  CreativeKids
//
//  Created by Prashant Swain on 07/10/22.
//

import Foundation

protocol AssignedHomeWorkDelegate{
    func periodData(data:[HomeWorkModel])
}

class AssignedHomeWorkModel{
    
    init(){
        
    }
    
    var dataPassDataSources:AssignedHomeWorkDelegate?
    func getPeriodListToAssignHomeWork(forDate:String = ""){
        //let serviceName = "http://api.cordovalearningsolutions.com/Teacher/TeacherSchedule/\(kUserData.userId ?? "")/\(kUserData.schid ?? "")/?_date=\(String.getCurrentDate(format: "yyyy-MM-dd"))"
        let serviceName = kTeacherBaseUrl + "GetPeriodListToAssignHomeWork/\(kUserData.userId ?? "")/\(kUserData.schid ?? "")\(forDate.isEmpty ? "" : "/?date=\(forDate)")"
        BaseController.shared.postToServerAPI(url: serviceName, params: [:], type: .GET) { (response, statusCode) in
            if statusCode == 200 {
                print("response: ------\(response)")
                let data = kSharedInstance.getArray(withDictionary: response[getResponce])
                let periodDataArray = data.map{HomeWorkModel(data: $0)}.sorted{$0.periodID < $1.periodID}
                self.dataPassDataSources?.periodData(data: periodDataArray)
            }else if statusCode == 404{
                print("data not found")
                self.dataPassDataSources?.periodData(data: [])
            }
            else {
                showAlertMessage.alert(message: "Something went wrong")
            }
        }

    }
}
