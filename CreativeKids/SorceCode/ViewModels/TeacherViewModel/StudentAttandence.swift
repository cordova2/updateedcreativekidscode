//
//  StudentAttandence.swift
//  CreativeKids
//
//  Created by Creative Kids on 26/05/22.
//

import Foundation

class StudentAttendanceViewModel{
    var studentAttendanceCallback: (([StudentAttendance])->())?
    var attendanceSubmitCallback:  ((Bool)->())?
    
    init(selectedClass:String,selectedSection: String, attendanceType:AttandanceType, completionHandler: @escaping (([StudentAttendance])->())){
        self.studentAttendanceCallback = completionHandler
        getAttandenceDetail(selectedClass: selectedClass,selectedSection:selectedSection, attendanceType: attendanceType)
    }
    init(studentArrendanceList:[StudentAttendance],attendanceFor:AttandanceType, completionHandler: @escaping ((Bool)->())){
        self.attendanceSubmitCallback = completionHandler
        let dict = StudentAttendance.getAttandanceDictionery(studentObjects: studentArrendanceList, attendanceFor: attendanceFor)
        markAttendance(attendanceListDict: dict)
    }
    
    func getAttandenceDetail(selectedClass: String, selectedSection: String, attendanceType:AttandanceType) {
        let serviceUrl = "?schid=\(kUserData.schid ?? "")&teachid=\(kUserData.userId ?? "")&clas=\(selectedClass)&dt=\(String.getCurrentDate(format: "MM/dd/yyyy"))&secid=\(selectedSection)&periods=\(attendanceType.rawValue)"
        BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { response, statusCode in
            if statusCode == 200{
                let returnData = kSharedInstance.getArray(withDictionary: response[getResponce])
                let attandenceData = returnData.map{StudentAttendance(data: $0)}
                self.studentAttendanceCallback?(attandenceData)
            }else{
                showAlertMessage.alert(message: "Something went wrong")
            }
        }
    }
    
    func markAttendance(attendanceListDict:[Dict]){
        CommonUtils.showHudWithNoInteraction(show: true)
        let serviceUrl = "?attend=stu&dt=\(String.getCurrentDate(format: "MM/dd/yyyy"))"
        let session = URLSession.shared
        let url = URL(string: kBaseUrl + serviceUrl)!
        print(url)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-type")
        let postData = json(from: attendanceListDict)
        guard let data = postData?.data(using: .utf8) else{return}
        urlRequest.httpBody = data
        session.dataTask(with: urlRequest) { (data, urlResponce, error) in
            CommonUtils.showHudWithNoInteraction(show: false)
            if error != nil || data == nil{
                print("client error")
            }
            guard let response = urlResponce as? HTTPURLResponse, (200...399).contains(response.statusCode) else {
                print("Server error!")
                return
            }
            
            guard let mime = response.mimeType, mime == "application/json" else {
                print("Wrong MIME type!")
                return
            }
            if response.statusCode == 200{
            do
            {
                let responseData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as? String
                print("Success with JSON: \(String(describing: responseData))")
                self.attendanceSubmitCallback?(true)
            }
            catch let error
            {
                print_debug(items: "json error: \(error.localizedDescription)")
            }
            }else{
                showAlertMessage.alert(message: "Something went wrong")
            }
        }.resume()
        
        
    }
    
    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
}
