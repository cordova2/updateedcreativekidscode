//
//  NoticeViewModel.swift
//  CreativeKids
//
//  Created by Creative Kids on 17/06/22.
//

import Foundation


class NoticeViewModel{
    
    var dataCompletion:(([NoticeModel])->Void)?
    
    init(completion:@escaping (([NoticeModel])->Void)){
        self.dataCompletion = completion
        self.getNotice(usertype: kSharedUserDefaults.getLoggedInUserDetails()["role"] as? String == String.getstring(UserRole.schoolStudent.rawValue) ? .Student : .Teacher)
    }
    
    func getNotice(date:Date = Date(), usertype:UserType = .Teacher){
//        let serviceUrl = "?date=\(String.getDateToString(date: date, format: "MM/dd/yyyy"))&schid=\(kUserData.schid ?? "")&clas=\(kUserData.classId ?? "")"
        let serviceUrl = "?date=\("05/17/2022")&schid=\(kUserData.schid ?? "")&clas=\(kUserData.classId ?? "")&role=\(usertype.rawValue)"
        BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { response, statusCode in
            let returnData = kSharedInstance.getArray(withDictionary: response[getResponce])
            let dataArray = returnData.map{NoticeModel(data: $0)}
            self.dataCompletion?(dataArray)
     }
   }
}
