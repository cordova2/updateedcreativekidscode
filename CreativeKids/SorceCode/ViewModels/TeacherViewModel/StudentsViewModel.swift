//
//  StudentsViewModel.swift
//  CreativeKids
//
//  Created by Prashant Swain on 07/10/22.
//

import Foundation

protocol StudentsWorkSubmitDataSources{
    func getAllStudent(data:[StudentWork])
    func studentWorked(data:Any)
}

class StudentsViewModel{
    var workData:HomeWorkModel?
    var dataPassDataSources:StudentsWorkSubmitDataSources?
    init(assiedWork:HomeWorkModel?){
        self.workData = assiedWork
        getAllStudentApi()
    }
    
    //MARK: Get All Student
    
    func getAllStudentApi(){
        let serviceName = kTeacherBaseUrl + "StudentListOfClassWork"
        //"roleID":kUserData.role ?? ""
        let params:Dict = [ApiParameters.schoolID:kUserData.schid ?? "",ApiParameters.classID:workData?.classId ?? "",ApiParameters.periodId:workData?.periodID ?? ""]
//        let params:Dict = [ApiParameters.schoolID:"1",ApiParameters.classID:"4",ApiParameters.periodId:"4"]
        BaseController.shared.postToServerRequestMultiPart(serviceName, params: params, imageParams: []){ (response, statusCode) in
            if statusCode == 200 {
                print("response: ------\(response)")
                let data = kSharedInstance.getArray(withDictionary: response[getResponce])
                let allStudent = data.map{StudentWork(data: $0)}
                self.dataPassDataSources?.getAllStudent(data: allStudent)
            } else {
                showAlertMessage.alert(message: "Unable to get user subscription Data")
            }
        }
    }
    
    
    func getStudentHomeWork(studentData:StudentWork){
        let serviceName = "http://api.cordovalearningsolutions.com/teacher/GetHomeWorkByStudentId/\(studentData.studentId)/\(studentData.workDetail.workId)"
        self.dataPassDataSources?.studentWorked(data: "")
        BaseController.shared.postToServerAPI(url: serviceName, params: [:], type: .GET) { (response, statusCode) in
            print("response: ------\(response)")
            let data = kSharedInstance.getArray(withDictionary: response[getResponce])
//            let data = kSharedInstance.getDictionary(response[getResponce])
            if statusCode == 200 {
                self.dataPassDataSources?.studentWorked(data: data)
            }else if statusCode == 404{
//                showAlertMessage.alert(message: String.getstring(data["title"]))
            }
            else {
                showAlertMessage.alert(message: "Something went wrong")
            }
        }

    }
    
}
