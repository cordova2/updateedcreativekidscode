//
//  AssignmentCheckViewModel.swift
//  CreativeKids
//
//  Created by Prashant Swain on 07/10/22.
//

import Foundation

protocol AssignmentCheckedDataSources{
    func checkedStatus(status:Bool)
}

class AssignmentCheckViewModel{
    var studentWorkedData:HomeWorkModel?
    var studentDetail:StudentWork?
    var dataPassDataSources:AssignmentCheckedDataSources?
    init(worked:HomeWorkModel){
        studentWorkedData = worked
    }
    
    init(studentDetail detail:StudentWork?){
        self.studentDetail = detail
        getStudentAssignment()
    }
    
    
    //MARK: Assignment Checked BY USER
    
    func assignmentCheckedByTeacherApi(){
        self.dataPassDataSources?.checkedStatus(status: true)
        let serviceName = kTeacherBaseUrl + "SubmitHomeWork"
        let params:Dict = [ApiParameters.studentID:kUserData.schid ?? "",ApiParameters.teacherId:studentWorkedData?.classId ?? "",ApiParameters.assigmentIdByStudent:studentWorkedData?.periodID ?? "",ApiParameters.remark:"A+"]
        BaseController.shared.postToServerAPI(url: serviceName, params: params, type: .POST) { (response, statusCode) in
            if statusCode == 200 {
                print("response: ------\(response)")
                let data = kSharedInstance.getDictionary(response[getResponce])
                self.dataPassDataSources?.checkedStatus(status: true)
            } else {
                showAlertMessage.alert(message: "Failed to submit try again")
            }
        }
    }
    
    
    func getStudentAssignment(){
        let serviceName = kTeacherBaseUrl + "GetHomeWorkByStudentId/\(studentDetail?.studentId ?? "")/\(studentDetail?.workDetail.workId ?? "")"
        let params:Dict = [:]
        BaseController.shared.postToServerAPI(url: serviceName, params: params, type: .GET) { (response, statusCode) in
            if statusCode == 200 {
                print("response: ------\(response)")
                let data = kSharedInstance.getDictionary(response[getResponce])
//                self.dataPassDataSources?.checkedStatus(status: true)
            } else {
                showAlertMessage.alert(message: "Failed to submit try again")
            }
        }
    }
}
