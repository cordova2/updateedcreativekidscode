//
//  UpdateProfilePic.swift
//  CreativeKids
//
//  Created by Prashant Swain on 13/10/22.
//

import Foundation
import UIKit

protocol UploadProfileDelegate{
    func onSuccess(_ status:Bool)
    func onSuccessWithImage(_ status:Bool,_ imageUrl:String,_ image:UIImage)
}

extension UploadProfileDelegate{
    func onSuccess(_ status:Bool){}
    func onSuccessWithImage(_ status:Bool,_ imageUrl:String,image:UIImage){}
}


class UpdateProfilePictureViewModel{
    
    var dataPassDelegate:UploadProfileDelegate?
    func updateProfilePic(userType:UserType, image:UIImage){
        var serviceName = ""
        var params:Dict = [:]
        switch userType{
        case .Teacher:
             serviceName = "http://api.cordovalearningsolutions.com/Teachers/UpdateProfilePic"
             params = [ApiParameters.teacherId:"\(kUserData.userId ?? "")"]
        case .Student:
             serviceName = "http://api.cordovalearningsolutions.com/Students/UpdateProfilePic"
             params = [ApiParameters.studentID:"\(kUserData.userId ?? "")"]
        }
        // let imageArray = vc?.imageArr.map{["imageName":ApiParameters.imageName,"image":$0]}
        let imageDict:Dict = ["imageName":ApiParameters.imageName,"image":image]
            BaseController.shared.postToServerRequestMultiPart(serviceName, params: params, imageParams: [imageDict]) { (response, statusCode) in
                if statusCode == 200 {
                    print("response: ------\(response)")
                    let data = kSharedInstance.getDictionary(response[getResponce])
                    self.dataPassDelegate?.onSuccessWithImage(true, String.getstring(data["message"]), image)
                }else if statusCode == 404{
                    print("data not found")
                }
                else {
                    showAlertMessage.alert(message: "Something went wrong")
                }
            }
        
    }
}
