//
//  HomeWorkDatesViewModel.swift
//  CreativeKids
//
//  Created by Prashant Swain on 14/10/22.
//

import Foundation

protocol HomeWorkDatesDelegate{
    func onSuccess(_ data:Any)
}

class HomeWorkDatesViewModel{
    var dataPassDelegate:HomeWorkDatesDelegate?
    func homeWorkDatesApi(){
        let serviceUrl = kStudentBaseUrl + "HomeWorkDates/\(kUserData.ClassSecid ?? "")"
        BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { (response, statusCode) in
            if statusCode == 200 {
                print("response: ------\(response)")
                let data = kSharedInstance.getArray(withDictionary: response[getResponce])
                let datesArray = data.map{HomeWorkDate(data: $0)}
                self.dataPassDelegate?.onSuccess(datesArray)
            } else {
                showAlertMessage.alert(message: "No HomeWork Found")
            }
        }
    }
    func homeWorkDatesApiForTeacher(){
        let serviceUrl = kTeacherBaseUrl + "HomeworkDates/\(kUserData.schid ?? "")/\(kUserData.teacherId ?? "")"
        BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { (response, statusCode) in
            if statusCode == 200 {
                print("response: ------\(response)")
                let data = kSharedInstance.getArray(withDictionary: response[getResponce])
                let datesArray = data.map{HomeWorkDate(data: $0)}
                self.dataPassDelegate?.onSuccess(datesArray)
            } else {
                showAlertMessage.alert(message: "No HomeWork Found")
            }
        }
    }
}
