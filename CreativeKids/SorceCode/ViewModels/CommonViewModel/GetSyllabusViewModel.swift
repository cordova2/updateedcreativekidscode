//
//  GetSyllabusViewModel.swift
//  CreativeKids
//
//  Created by Prashant Swain on 14/10/22.
//

import Foundation

protocol GetSyllabusDelegate{
    func onSuccess(_ data:Any)
}

class GetSyllabusViewModel{
    
    var dataPassDelegate:GetSyllabusDelegate?
    func getSyllabusForStudentApi(){
        let serviceUrl = "http://api.cordovalearningsolutions.com/Students/GetSyllabus/\(kUserData.classId ?? "")/\(kUserData.schid ?? "")"
            BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { (response, statusCode) in
                if statusCode == 200 {
                    print("response: ------\(response)")
                    let data = kSharedInstance.getArray(withDictionary: response[getResponce])
                    print(data)
                    self.dataPassDelegate?.onSuccess(data)
                } else {
                    showAlertMessage.alert(message: "No Syllabus found")
                }
            }
    }
    
    func getSyllabusForTeacherApi(){
        let serviceUrl = "http://api.cordovalearningsolutions.com/Students/GetSyllabus/\(kUserData.classId ?? "")/\(kUserData.schid ?? "")"
            BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { (response, statusCode) in
                if statusCode == 200 {
                    print("response: ------\(response)")
                    let data = kSharedInstance.getDictionary(response[getResponce])
                    print(data)
                    self.dataPassDelegate?.onSuccess(data)
                } else {
                    showAlertMessage.alert(message: "No HSyllabus found")
                }
            }
    }
}
