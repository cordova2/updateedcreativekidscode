//
//  YoutubeListViewModel.swift
//  CreativeKids
//
//  Created by Creative Kids on 15/04/21.
//

import Foundation

class YouTubeListViewModel{
    
    init(complitionHandler: @escaping (([YoutubeDataModel])->())){
        getYouTubeListApi(){ data in
            complitionHandler(data)
        }
    }
    
    func getYouTubeListApi(complition: @escaping (([YoutubeDataModel])->())){
        let serviceUrl = "?fmd=abc".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { (response, statusCode) in
            if statusCode == 200 {
                print("response: ------\(response)")
                let youTubeDataArray = kSharedInstance.getArray(withDictionary: response[getResponce])
                let fillYoutubetModel = youTubeDataArray.map{YoutubeDataModel(data: $0)}
                complition(fillYoutubetModel)
            } else {
                showAlertMessage.alert(message: "NO Data Found")
            }
        }
    }
}
