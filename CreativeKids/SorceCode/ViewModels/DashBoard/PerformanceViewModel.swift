//
//  PerformanceViewModel.swift
//  CreativeKids
//
//  Created by Creative Kids on 04/06/21.
//

import Foundation

class PerformanceViewModel{
    var vc:DashboardViewController?
    init(viewController:DashboardViewController,complitionHandler: @escaping ((Dict)->())){
        self.vc = viewController
        getPerformanceApi(){ data in
            complitionHandler(data)
        }
    }
    
    func getPerformanceApi(complition: @escaping ((Dict)->())){
        var className = ""
        switch cameFrom {
        case .login, .sideMenu:
            if kSharedUserDefaults.isUserLoggedIn(){
                className = kUserData.className ?? ""
            }else{
                className = String.getString(kSharedUserDefaults.getUserClass()["className"])
            }
        case .changedClass:
            className = String.getString(kSharedUserDefaults.getUserClass()["className"])
        }
        let serviceUrl = "?Email=\(kUserData.userEmail ?? "")&dash=abc&cls=\(className)".replacingOccurrences(of: " ", with: "%20")
        BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { (response, statusCode) in
            if statusCode == 200 {
                print("response: ------\(response)")
                let performanceDetailArray = kSharedInstance.getArray(withDictionary: response[getResponce])
                let performanceDetailInDict = kSharedInstance.getDictionary(performanceDetailArray.first)
                complition(performanceDetailInDict)
            } else {
                showAlertMessage.alert(message: "NO Performance Data Found")
            }
        }
    }
}
