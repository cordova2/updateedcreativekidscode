//
//  TopicSearchViewModel.swift
//  CreativeKids
//
//  Created by Creative Kids on 15/04/21.
//

import Foundation

class TopicSearchViewModel{
    var vc:DashboardViewController?
    init(viewController:DashboardViewController,complitionHandler: @escaping (([TopicSearchModel])->())){
        self.vc = viewController
        getTopicSearchListApi(){ data in
            complitionHandler(data)
        }
    }
    
    func getTopicSearchListApi(complition: @escaping (([TopicSearchModel])->())){
        var className = ""
        switch cameFrom {
        case .login, .sideMenu:
            if kSharedUserDefaults.isUserLoggedIn(){
                className = kUserData.className ?? ""
            }else{
                className = String.getString(kSharedUserDefaults.getUserClass()["className"])
            }
        case .changedClass:
            className = String.getString(kSharedUserDefaults.getUserClass()["className"])
        }
        let serviceUrl = "?cls=\(className)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        DispatchQueue.global(qos: .userInteractive).async {
            BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { (response, statusCode) in
                if statusCode == 200 {
                    print("response: ------\(response)")
                    let searchListArray = kSharedInstance.getArray(withDictionary: response[getResponce])
                    print(searchListArray.count)
                    let fillTopicSeachModel = searchListArray.map{TopicSearchModel(data: $0)}
                    complition(fillTopicSeachModel)
                } else {
                    showAlertMessage.alert(message: "NO Data Found")
                }
            }
        }
    }
}
