//
//  DashboardVeiwModel.swift
//  CreativeKids
//
//  Created by Creative Kids on 14/04/21.
//

import Foundation


class SubjectViewModel{
    var vc:DashboardViewController?
    
    //MARK: Initializer For Normal User
    init(viewController:DashboardViewController,complitionHandler: @escaping (([SubjectModel])->())){
        self.vc = viewController
        getSubjectApi(){ data in
            complitionHandler(data)
        }
    }
    
    //MARK: Initializer For School Student
    init(complition: @escaping (([SubjectModel])->())){
        getSubjectforSchoolStudent(){ data in
            complition(data)
        }
    }
    
    //MARK:- GET SUBJECT
    func getSubjectApi(complition: @escaping (([SubjectModel])->())){
        var className = ""
        var classType = ""
        switch cameFrom {
        case .login, .sideMenu:
            if kSharedUserDefaults.isUserLoggedIn(){
                className = kUserData.className ?? "Class-1"
                classType = ["Class-9","Class-10"].contains(kUserData.className ?? "") ? ClassType.ncert : ClassType.cordova
            }else{
                className = String.getString(kSharedUserDefaults.getUserClass()["className"]) == "" ? "Class-1" : String.getString(kSharedUserDefaults.getUserClass()["className"])
                classType = String.getString(kSharedUserDefaults.getUserClass()["classType"])
            }
            
        case .changedClass:
            className = String.getString(kSharedUserDefaults.getUserClass()["className"])
            classType = String.getString(kSharedUserDefaults.getUserClass()["classType"])
        }
        
        let serviceUrl = "?eid=\(kUserData.userEmail ?? "")&clssa=\(className)&\(classType)".replacingOccurrences(of: " ", with: "%20")
        BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { (response, statusCode) in
            if statusCode == 200 {
                print("response: ------\(response)")
                let subjectArray = kSharedInstance.getArray(withDictionary: response[getResponce])
                let fillSubjectModel = subjectArray.map{SubjectModel(data: $0)}
                complition(fillSubjectModel)
            } else {
                showAlertMessage.alert(message: "NO Data Found")
            }
        }
    }
    
    func getSubjectforSchoolStudent(complition: @escaping (([SubjectModel])->())){
        let serviceUrl = "?clssa=\(kUserData.className ?? "Class-1")".replacingOccurrences(of: " ", with: "%20")
        BaseController.shared.postToServerAPI(url: serviceUrl, params: [:], type: .GET) { (response, statusCode) in
            if statusCode == 200 {
                print("response: ------\(response)")
                let subjectArray = kSharedInstance.getArray(withDictionary: response[getResponce])
                let fillSubjectModel = subjectArray.map{SubjectModel(data: $0)}
                complition(fillSubjectModel)
            } else {
                showAlertMessage.alert(message: "NO Data Found")
            }
        }
    }
}
