//
//  TeacherModels.swift
//  CreativeKids
//
//  Created by Creative Kids on 25/05/22.
//

import Foundation

class StudentAttendance{
    var classId:String
    var studentId:String
    var section:String
    var attendanceType:String
    var isPresent:Bool
    var isOnLeave:Bool
    var className:String
    var teacherName:String
    var status:String
    var leaveSubject:String
    var studentName:String
    var message:String
    var date:String
    var isSelected:Bool = false
    
    init(data:Dict) {
        classId = String.getstring(data["classid"])
        studentId = String.getstring(data["stuid"])
        className = String.getstring(data["classname"])
        teacherName = String.getstring(data["teachername"])
        status = String.getstring(data["status"])
        leaveSubject = String.getstring(data["message"])
        studentName = String.getstring(data["stuname"])
        section = String.getstring(data["section"])
        isPresent = String.getstring(data["status"]) == "2" ? false : true
        isOnLeave = String.getstring(data["leave"]) == "0" ? false : true
        message = String.getstring(data["message"])
        date    = String.getstring(data["dt"])
        attendanceType = String.getstring(data["Period"])
    }
    
    static func getAttandanceDictionery(studentObjects:[StudentAttendance],attendanceFor:AttandanceType) -> [Dict]{
        var attendanceDict:[Dict] = []
        for student in studentObjects{
            let date = String.convertDateString(dateString: student.date, fromFormat: "yyyy-MM-ddThh:mm:ss", toFormat: "MM/dd/yyyy")
            let leave = student.isOnLeave ? "1" : "0"
            let status = student.isPresent ? "1" : "2"
            let createDictionery:Dict = ["stuid":"\(student.studentId)","dt":"\(date)","leave":"\(leave)","status":"\(status)","period":"\(attendanceFor.rawValue)"]
            attendanceDict.append(createDictionery)
            print(createDictionery)
        }
        return attendanceDict
    }
}


class LeaveModel{
    
    enum LeaveFor{
        case student
        case teacherForApproval
        case teacher
    }
    var leaveFor:LeaveFor = .student
    var period:String?
    var classId:String?
    var className:String?
    var date:String?
    var fromDate:String?
    var toDate:String?
    var leave:String?
    var id:String?
    var message:String?
    var section:String?
    var status:String?
    var userId:String?
    var UserName:String?
    var applicationSubject:String?
    var signatureImage:String?
    var isApproved:Bool = false
    var isDeclined:Bool = false
    
    init(data:Dict){
        self.leaveFor = .teacherForApproval
        self.period = String.getstring(data["Period"])
        self.classId = String.getstring(data["classid"])
        self.className = String.getstring(data["classname"])
        self.date = String.getstring(data["dt"])
        self.fromDate = String.getstring(data["fromdt"])
        self.toDate = String.getstring(data["todt"])
        self.leave = String.getstring(data["message"])
        self.id = String.getstring(data["leaveid"])
        self.message = String.getstring(data["message"])
        self.section = String.getstring(data["section"])
        self.status = String.getstring(data["sts"])
        self.userId = String.getstring(data["stuid"])
        self.UserName = String.getstring(data["stuname"])
        self.applicationSubject = String.getstring(data["subj"])
        self.signatureImage = String.getstring(data["signimg"])
    }
    
    init(Kdata:Dict){
        self.leaveFor = .student
        self.period = String.getstring(Kdata["Period"])
        self.classId = String.getstring(Kdata["classid"])
        self.className = String.getstring(Kdata["classname"])
        self.date = String.getstring(Kdata["date"])
        self.fromDate = String.getstring(Kdata["fromDate"])
        self.toDate = String.getstring(Kdata["toDate"])
        self.leave = String.getstring(Kdata["leave_Body"])
        self.id = String.getstring(Kdata["leaveid"])
        self.message = String.getstring(Kdata["leave_Body"])
        self.section = String.getstring(Kdata["section"])
        self.status = String.getstring(Kdata["sts"])
        self.userId = String.getstring(Kdata["stuid"])
        self.UserName = String.getstring(Kdata["studentName"])
        self.applicationSubject = String.getstring(Kdata["subject"])
        self.signatureImage = String.getstring(Kdata["signimg"])
        isApproved = String.getstring(Kdata["isApproved"]) == "1"
        isDeclined = String.getstring(Kdata["isDecline"]) == "1"
    }
    
    init(teacherdata:Dict){
        self.leaveFor = .teacher
        self.period = String.getstring(teacherdata["Period"])
        self.classId = String.getstring(teacherdata["classid"])
        self.className = String.getstring(teacherdata["classname"])
        self.date = String.getstring(teacherdata["date"])
        self.fromDate = String.getstring(teacherdata["fromDate"])
        self.toDate = String.getstring(teacherdata["toDate"])
        self.leave = String.getstring(teacherdata["leave_Body"])
        self.id = String.getstring(teacherdata["leaveid"])
        self.message = String.getstring(teacherdata["leave_Body"])
        self.section = String.getstring(teacherdata["section"])
        self.status = String.getstring(teacherdata["sts"])
        self.userId = String.getstring(teacherdata["stuid"])
        self.UserName = String.getstring(teacherdata["name"])
        self.applicationSubject = String.getstring(teacherdata["subject"])
        self.signatureImage = String.getstring(teacherdata["signimg"])
        isApproved = String.getstring(teacherdata["isApproved"]) == "1"
        isDeclined = String.getstring(teacherdata["isDecline"]) == "1"
    }
    
}

struct NoticeModel{
    var date:String
    var message:String
    var name:String
    var designation:String
    
        init(data:Dict) {
            self.date = String.getstring(data["dates"])
            self.message = String.getstring(data["msg"])
            self.name = String.getstring(data["name"])
            self.designation = String.getstring(data["designation"])
    }
}


//MARK: period model
class PeriodModel {
    var id:String?
    var number:String?
    var day:String?
    var className:String?
    var section:String?
    var subject:String?
    var isOver:Bool = false
    var isFree:Bool = false
    var isAttandanceMarked = false
    var startTime:String?
    var endTime:String?
    
    init(data:Dict){
        id = String.getstring(data["periodID"])
        number = String.getstring(data["periodNo"])
        day = String.getstring(data["day"])
        let classSec = seperateClassSection(string:String.getstring(data["className"]))
        className = "\(classSec.0) \(classSec.1)"
        section = classSec.2
        subject = String.getstring(data["subjectName"])
        isOver = String.getstring(data["isOver"]) == "1" ? true : false
        isFree = String.getstring(data["freePeriod"]) == "1" ? true : false
        isAttandanceMarked = String.getstring(data["isAttendanceMarked"]) == "1" ? true : false
        endTime = String.getstring(data["endTime"])
        startTime = String.getstring(data["startTime"])
    }
    
    private func seperateClassSection(string:String)->(String,String,String){
        let seperatedString = string.components(separatedBy: "-")
        if seperatedString.count == 3{
            return (seperatedString[0],seperatedString[1],seperatedString[2])
        }else if seperatedString.count == 2{
            return ("",seperatedString[0],seperatedString[1])
        }else if  seperatedString.count == 1{
            return ("",seperatedString[0],"")
        }else{
            return ("","","")
        }
    }
}


//MARK: School Event

class EventModel{
    enum MediaType{
        case Image
        case Video
    }
    var id:String
    var schoolId:String
    var imageURL:String
    var mediaType:MediaType
    var description:String
    var videoURL:String
    var videoTitle:String
    init(data:Dict){
        id = String.getstring(data["eventId"])
        schoolId = String.getstring(data["schoolId"])
        imageURL = String.getstring(data["imageUrl"])
        mediaType = String.getstring(data["title"]) == "Image" ? .Image : .Video
        description = String.getstring(data["description"])
        videoURL = String.getstring(data["videoLink"])
        videoTitle = String.getstring(data["videoTitle"])
    }
}

//MARK:- Assigned HomeWork

struct HomeWorkModel{
    var id:String
    var periodID:String
    var periodNo:String
    var date:String
    var className:String
    var section:String
    var classId:String
    var subjectName:String
    var subjectId:String
    var isWorkedAssigned:Bool = false
    var canAssignWork:Bool = false
    var isWorkedChecked:Bool = false
    
    init(data:Dict){
        id = String.getstring(data["homeWorkId"])
        periodID = String.getstring(data["periodID"])
        periodNo = String.getstring(data["periodNo"])
        date = String.getstring(data["date"])
        className = ""
        section = ""
        classId = String.getstring(data["classId"])
        subjectName = String.getstring(data["subjectName"])
        subjectId = String.getstring(data["subjectId"])
        isWorkedAssigned = String.getstring(data["isWorkedAssigned"]) == "1"
        canAssignWork = String.getstring(data["canAssignWork"]) == "1"
        isWorkedChecked = String.getstring(data["isHomeWorkCheckedByTeacher"]) == "1"
        let classSec = seperateClassSection(string:String.getstring(data["className"]))
        className = "\(classSec.0) \(classSec.1)"
        section = classSec.2
    }
    
     mutating func seperateClassSection(string:String)->(String,String,String){
        let seperatedString = string.components(separatedBy: "-")
        if seperatedString.count == 3{
            return (seperatedString[0],seperatedString[1],seperatedString[2])
        }else if seperatedString.count == 2{
            return ("",seperatedString[0],seperatedString[1])
        }else if  seperatedString.count == 1{
            return ("",seperatedString[0],"")
        }else{
            return ("","","")
        }
    }
}

//MARK: Student Work Model

class StudentWork{
    var studentId:String
    var studentName:String
    var classId:String
    var className:String
    var section:String
    var profileImage:String
    var workDetail:WorkDetail
    
    init(data:Dict){
        studentId = String.getstring(data["studentId"])
        studentName = String.getstring(data["studentName"])
        classId = String.getstring(data["classId"])
        className = String.getstring(data["className"])
        section = String.getstring(data["section"])
        profileImage = String.getstring(data["profileImage"])
        workDetail = WorkDetail(data: kSharedInstance.getDictionary(data["homeworkDetail"]))
    }
}

struct WorkDetail{
    var workId:String
    var remark:String
    var isChecked:Bool = false
    
    init(data:Dict){
        workId = String.getstring(data["homeworkId"])
        remark = String.getstring(data["remark"])
        isChecked = String.getstring(data["_checked"]) == "1"
    }
}
