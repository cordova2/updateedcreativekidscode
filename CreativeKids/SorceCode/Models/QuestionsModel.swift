
//  QuestionsModel.swift
//  CreativeKids
//
//  Created by Creative Kids on 24/03/21.
//

import Foundation

class MCQQuestionModel {
    var qid: String?
    var chapter: String?
    var chapid: String?
    var topic: String?
    var topicid: String?
    var Questions: String?
    var Marks: String?
    var type: String?
    var option_A: String?
    var option_B: String?
    var option_C: String?
    var option_D: String?
    var rightAns: Int?
    var subid: String?
    var subname: String?
    var exercise: String?
    var setid: String
    
    init(MCQData: [String: Any]) {
        self.qid = String.getString(MCQData["qid"])
        self.chapter = String.getString(MCQData["chapter"])
        self.topic = String.getString(MCQData["topic"])
        self.topicid = String.getString(MCQData["topicid"])
        self.Questions = String.getString(MCQData["Questions"])
        //self.Marks = String.getString(MCQData["Marks"])
        self.Marks = String.getString("3")
        self.type = String.getString(MCQData["type"])
        self.option_A = String.getString(MCQData["option_A"])
        self.option_B = String.getString(MCQData["option_B"])
        self.option_C = String.getString(MCQData["option_C"])
        self.option_D = String.getString(MCQData["option_D"])
        self.rightAns = Int.getInt(MCQData["rightAns"])
        self.subid = String.getString(MCQData["subid"])
        self.subname = String.getString(MCQData["subname"])
        self.exercise = String.getString(MCQData["exercise"])
        self.setid = String.getString(MCQData["setid"])
    }
}

class VSAQQuestionModel {
    var qid: String?
    var subid: String?
    var chapter: String?
    var classname: String?
    var chapid: String?
    var exercise: String?
    var topicid: String?
    var Questions: String?
    var option_A: String?
    var option_B: String?
    var option_C: String?
    var option_D: String?
    var img: String?
    var setid: String?
    var rightAns: Int?
    var Marks: String?
    var topic: String?
    var userSelectedOption = 0
    var textArrSplit = [String]()
    var type: String?
    var subname: String?
    var textfieldFirst:String?
    var texrFieldSecond:String?
    var questionNumber = 0
    var quesimg: String?
    
    init(VSAQData: [String: Any]) {
        self.qid = String.getString(VSAQData["qid"])
        self.subid = String.getString(VSAQData["subid"])
        self.chapter = String.getString(VSAQData["chapter"])
        self.classname = String.getString(VSAQData["classname"])
        self.chapid = String.getString(VSAQData["chapid"])
        self.exercise = String.getString(VSAQData["exercise"])
        self.topicid = String.getString(VSAQData["topicid"])
        self.Questions = String.getString(VSAQData["Questions"])
        self.option_A = String.getString(VSAQData["option_A"])
        self.option_B = String.getString(VSAQData["option_B"])
        self.option_C = String.getString(VSAQData["option_C"])
        self.option_D = String.getString(VSAQData["option_D"])
        self.img = String.getString(VSAQData["img"])
        self.setid = String.getString(VSAQData["setid"])
        self.rightAns = Int.getInt(VSAQData["rightAns"])
        self.Marks = String.getString(VSAQData["Marks"])
        self.topic = String.getString(VSAQData["topic"])
        self.textArrSplit  = (self.Questions?.contains("<span>...</span>") ?? true) ? (self.Questions?.components(separatedBy: "<span>...</span>") ?? []) : (self.Questions?.components(separatedBy: "<span>…</span>") ?? [])
        self.type = String.getString(VSAQData["type"])
        self.subname = String.getString(VSAQData["subname"])
        self.quesimg = String.getString(VSAQData["quesimg"])
   }
}

class ExcelExamQuestionModel {
    var qid: String?
    var subid: String?
    var chapter: String?
    var classname: String?
    var chapid: String?
    var exercise: String?
    var topicid: String?
    var Questions: String?
    var option_A: String?
    var option_B: String?
    var option_C: String?
    var option_D: String?
    var img: String?
    var setid: String?
    var rightAns: Int?
    var Marks: String?
    var topic: String?
    var LTP: String?
    var userTextAns: String?
    var subname: String?
    
    init(ExcelExam: [String: Any]) {
        self.qid = String.getString(ExcelExam["qid"])
        self.subid = String.getString(ExcelExam["subid"])
        self.chapter = String.getString(ExcelExam["chapter"])
        self.classname = String.getString(ExcelExam["classname"])
        self.chapid = String.getString(ExcelExam["chapid"])
        self.exercise = String.getString(ExcelExam["exercise"])
        self.topicid = String.getString(ExcelExam["topicid"])
        self.Questions = String.getString(ExcelExam["Questions"])
        self.option_A = String.getString(ExcelExam["option_A"])
        self.option_B = String.getString(ExcelExam["option_B"])
        self.option_C = String.getString(ExcelExam["option_C"])
        self.option_D = String.getString(ExcelExam["option_D"])
        self.img = String.getString(ExcelExam["img"])
        self.setid = String.getString(ExcelExam["setid"])
        self.rightAns = Int.getInt(ExcelExam["rightAns"])
        self.Marks = String.getString(ExcelExam["Marks"])
        self.topic = String.getString(ExcelExam["topic"])
        self.LTP = String.getString(ExcelExam["LTP"])
        self.subname = String.getString(ExcelExam["subname"])
    }
}

class PDFModel {
    var pdf: String?
    var subname: String?
    var classname: String?
    
    init(data: [String: Any]) {
        self.pdf = String.getString(data["pdf"])
        self.subname = String.getString(data["subname"])
        self.classname = String.getString(data["classname"])
    }
}
