//
//  UserDataModel.swift
//  CreativeKids
//
//  Created by Creative Kids on 20/03/21.
//

import Foundation
import UIKit
let kUserData = UserData.shared

class UserData {
    
    static var shared = UserData()
    var profileImage:String?
    var userId:String?
    var schoolName:String?
    var mobileNumber:String?
    var className:String?
    var uniqueNumber:String?
    var state:String?
    var userName:String?
    var userAge:String?
    var userEmail:String?
    var city:String?
    var parentEmail:String?
    var teacherEmail:String?
    var subscriptionData = [SubjectModel]()
    var role: String?
    var stuid: String?
    var username: String?
    var schid: String?
    var schname: String?
    var gender:String?
    var admissionNo:String?
    var teacherId:String?
    var classId:String?
    var ClassSecid: String?
    
    init(){
        let data = kSharedUserDefaults.getLoggedInUserDetails()
        dataSave(data: data)
        let subscribeData = kSharedUserDefaults.getUserSubscribtionDetail()
        let subscribeArray = kSharedInstance.getArray(subscribeData["data"])
        self.subscriptionData = subscribeArray.map{SubjectModel(data: $0 as! Dict)}
    }
    
    func dataSave(data:Dict){
        userId           = String.getString(data["Id"])
        parentEmail      = String.getString(data["parentEmail"])
        admissionNo      = String.getstring(data["AdmissionNo"])
        teacherEmail     = String.getString(data["teacherEmail"])
        schoolName       = String.getString(data["schoolname"])
        mobileNumber     = String.getString(data["mobile"])
        className        = String.getString(data["classname"])
        uniqueNumber     = String.getString(data["uniqno"])
        state            = String.getString(data["state"])
        userName         = String.getString(data["Name"])
        userAge          = String.getString(data["age"])
        userEmail        = String.getString(data["Email"])
        city             = String.getString(data["city"])
        role             = String.getString(data["role"])
        stuid            = String.getString(data["stuid"])
        username         = String.getString(data["username"])
        schid            = String.getString(data["schid"])
        schname          = String.getString(data["schoolname"])
        gender           = String.getString(data["Gender"])
        teacherId        = String.getString(data["teacherid"])
        classId          = String.getString(data["clsid"])
        ClassSecid       = String.getString(data["ClassSecid"])
        forProfileImage(data:data)
        kSharedUserDefaults.setLoggedInUserDetails(loggedInUserDetails: data)
        AppManager.shared.saveDatatoChatHalper()
    }
    
    func forProfileImage(data:Dict){
        switch String.getstring(self.role){
        case String.getstring(UserRole.schoolStudent.rawValue),String.getstring(UserRole.schoolTeacher.rawValue):
            profileImage = String.getString(data["images"])
        case String.getstring(UserRole.normalUser.rawValue):
//            profileImage = "https://creativekidssolutions.com/content/assets/images/" + (userId ?? "")  + String.getString(data["path"])
            profileImage = String.getString(data["images"])
        default:
            return
        }
    }
}

class SubscrptionDetails{
    var isTrialPeriod:String?
    var purchaseDate:String?
    var productId:String?
    var quantity:String?
    var transationId:String?
    init(data:Dict){
        self.isTrialPeriod  = String.getString(data["is_trial_period"])
        self.purchaseDate   = String.getString(data["original_purchase_date_ms"])
        self.productId      = String.getString(data["product_id"])
        self.quantity       = String.getString(data["quantity"])
        self.transationId   = String.getString(data["transaction_id"])
    }
}
