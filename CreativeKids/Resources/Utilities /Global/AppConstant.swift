//
//  AppConstant.swift
//  CommonCode
//
//  Created by Shubham Kaliyar on 6/10/17.
//  Copyright © 2017 Shubham Kaliyar. All rights reserved.
//

import Foundation
import UIKit



// MARK: - Structure

//NotificationObserverKeys
let videoSeen = "videoSeenByUser"

//typealias  JSON = [String:Any]?

let kAppName                                   = "Creative Kids"
let kIsTutorialAlreadyShown                    = "isTutorialAlreadyShown"
let kIsUserLoggedIn                            = "isUserLoggedIn"
let kLoggedInAccessToken                       = "access_token"
let kLoggedInUserDetails                       = "loggedInUserDetails"
let kRecentChapterSeen                         = "recentChapterSeen"
let kUserSubscriptionDetails                   = "userSubscriptionDetails "
let kLoggedInUserId                            = "loggedInUserId"
let kLatitude                                  = "latitude"
let kLongitude                                 = "longitude"
let kIsOtpVerified                             = "is_mobile_verified"
let kIsProfileCreated                          = "is_profile_create"
let kIs_Active                                 = "is_active"
let kIs_Notification                           = "is_notification"
let kIsAppInstalled                            = "isAppInstalled"
let kAccessToken                               = "access_token"
let kDeviceToken                               = "device_token"
let kUserClass                                 = "class"
let iosDeviceType                              = "1"
let iosDeviceTokan                             = "123456789"
let kIsTourDone                                = "isTourDone"
let kchatUserDetails                           = "chatUserDetails"



struct APIUrl {
    static let kBaseUrl                        = "http://dailygoto.fluper.in:3000/user/"
    static let videoUrl                        = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"
}

struct Keys {
    static let kDeviceToken                    = "deviceToken"
    static let kAccessToken                    = "access_token"
    static let kFirebaseId                     = "firebaseId"
    static let kMobileVerified                 = "isMobileVerified"
    static let kUserName                       = "username"
    static let alphabet                        = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
    
}

struct ServiceName {
    static let register                        = "signUp"
    static let verifyotp                       = "verifyOtp"
    static let logout                          = "api/user/signout"
    static let login                           = "signIn"
    static let forgotPassword                  = "forgotPassword"
    static let resetPassword                   = "resetPassword"
    static let resendOtp                       = "resendOtp"
    static let socialSignup                    = "socialsignUp"
    
    // HOME
    static let getAllGroundType                = "getAllGroundType"
    static let getGrounds                      = "getGroundbyfavorite"
    
    //    static let verifiedOtpForgot             = "api/user/verifiedOtpForgot"
    //    static let uploadDocuments               = "api/user/uploadDocuments"
    //    static let driverMode                    = "api/user/dutyMode"
    
        
}


struct ApiParameters {
    
    static let teacherId                       = "teacherId"
    static let periodId                        = "periodId"
    static let date                            = "_date"
    static let question                        = "question"
    static let imageName                       = "files"
    static let homeWorkID                      = "homeWorkId"
    static let studentSubmittedId              = "workSubmittedId"
    static let chapter                         = "chapter"
    
    static let schoolID                        = "schoolId"
    static let classID                        = "classId"
    
    static let studentID                       = "studentId"
    static let assigmentIdByStudent            = "workSubmittedIdByStudent"
    static let remark                         = "remarks"
    
    static let subjectId                     = "subjectId"
    
    
    static let schId                           = "SchoolId"
    static let chapterId                       = "ChapterId"
    static let setId                           = "setid"
    static let exerciseType                    = "exerciseType"
    static let totalQuestionCount              = "totalQuestionCount"
    static let totalMarks                      = "totalMarks"
    static let scoredMarks                     = "scoredMarks"
    static let correctAnswerCount              = "correctAnswerCount"
    static let totalSpendTime                  = "totalSpendTimeInSec"
    static let subId                           = "SubjectId"
    
    static let kname                           = "Name"
    static let kemail                          = "Email"
    static let kforgotpasswordEmail            = "email"
    static let kmobileNumber                   = "mobile"
    static let kcountryCode                    = "country_code"
    static let KcountryName                    = "country"
    static let kpassword                       = "pass"
    static let klatitude                       = "latitude"
    static let klongitude                      = "longitude"
    static let kcredantials                    = "credantials"
    static let kNewPassword                    = "newpassword"
    static let kmobileEmail                    = "mobile_email"
    static let ksocialId                       = "social_id"
    static let ksocialType                     = "social_type"
    static let schoolName                      = "schoolname"
    
    static let kConfirmPassword                = "password_confirmation"
    static let kdeviceType                     = "device_type"
    static let kdeviceToken                    = "device_token"
    static let kfirst_name                     = "first_name"
    static let klast_name                      = "last_name"
    static let kcountry                        = "country"
    static let kcity                           = "city"
    static let kotp                            = "verification_code"
    static let kLocation                       = "location"
    static let kPostalCode                     = "postal_code"
    static let kState                          = "state"
    static let kprofilepic                     = "img"
    static let klogin_type                     = "login_type"
    static let studentClass                    = "classname"
    static let parentEmail                     = "parentEmail"
    static let teacherEmail                    = "teacherEmail"
    static let termAndCondition                = "privacy"
    static let previewPdf                      = "Preview"
    static let resultPdf                       = "Result"
    static let signature                       = "sign"
    
    
    
    static let message                         = "message"
    static let user_id                         = "user_id"
    static let id                              = "id"
    static let age                             = "age"
    static let vehicle_number                  = "vehicle_number"
    static let vehicle_insurance               = "vehicle_insurance"
    static let vehicle_type                    = "vehicle_type"
    static let vehicle_model                   = "vehicle_model"
    static let idproof_image_1                 = "idproof_image_1"
    static let idproof_image_2                 = "idproof_image_2"
    static let license_image_1                 = "license_image_1"
    static let license_image_2                 = "license_image_2"
    static let passportvisa_image_1            = "passportvisa_image_1"
    static let passportvisa_image_2            = "passportvisa_image_2"
    static let vehicleproof_image_1            = "vehicleproof_image_1"
    static let vehicleproof_image_2            = "vehicleproof_image_2"
    static let is_document_uploaded            = "is_document_uploaded"
    static let is_otpverify                    = "is_otpverify"
    static let on_duty                         = "on_duty"
}
typealias Dict = [String:Any]


struct NumberContants {
    static let kMinPasswordLength = 8
}


struct  AlertMessage {
    static let kDefaultError                   = "Something went wrong. Please try again."
    static let knoNetwork                      = "Please check your internet connection !"
    static let kSessionExpired                 = "Your session has expired. Please login again."
    static let kNoInternet                     = "Unable to connect to the Internet. Please try again."
    static let kInvalidUser                    = "Oops something went wrong. Please try again later."
    static let knoData                         = "No Data Found"
    static let noName                          = "Empty name"
    static let Under_Development               = "Under Development"
    static let logout                          = "Are you sure you want to logout?"
    static let signin                          = "Please sign in first."
    static let currentPagealert                = "you are already on this page"
}


struct Identifiers {
    static let kLoginInController              = "LoginViewController"
    static let kSignUpController               = "SignUpViewController"
    static let kOTPController                  = "PhoneVerificationViewController"
    static let kForgotPasswordController       = "ForgotPasswordViewController"
    static let kResetPasswordController        = "ResetPasswordViewController"
    static let kHomeController                 = "HomeViewController"
    static let kPasswordPopUpController        = "PasswordChangedPopUpViewController"
    static let kSettingController              = "SettingsViewController"
    static let kServiceDeatilController        = "ServiceDetailViewController"
    static let kUserProfileController          = "UserProfileViewController"
    static let kBookingsController             = "BookingsViewController"
    static let kResumeListController           = "ResumeListViewController"
    static let kResumeDetailController         = "ResumeDetailViewController"
    static let kResumeImageController          = "ResumeImageViewController"
    static let kBookServiceController          = "BookServiceViewController"
    static let kPaymentController              = "PaymentViewController"
    
    
}

struct Storyboards {
    static let kMain                           = "Main"
    static let kHome                           = "Home"
}

enum HasCameFrom{
    case signup,forgotpassword,login,none
}
struct Notifications {
    static let kEnterMobileNumber               = "Please enter mobile number"
    static let kEnterValidMobileNumber          = "Please enter valid mobile number"
    static let kEnterPassword                   = "Please enter password"
    static let kEnterNewPassword                = "Please enter new password"
    static let kEnterValidPassword              = "Please enter valid password"
    static let kName                            = "Please enter name"
    static let kFullName                        = "Please enter full name"
    static let kFirstName                       = "Please enter first name"
    static let kLastName                        = "Please enter Last name"
    static let kEnterValidEmailId               = "Please enter valid email ID"
    static let kEnterValidName                  = "Please enter valid name"
    static let kEmailId                         = "Please enter email ID"
    static let kEmailIdForForgotPassword        = "Please enter email ID to reset your password"
    static let kAcceptTerms                     = "Please accept terms & conditions"
    static let kPasswordLength                  = "Password should be minimum 8 character long"
    static let kSchoolName                      = "Please enter school name"
    static let kPasswordMatch                   = "Password & confirm password should be same"
    static let kEnterEmailOrMobile              = "Please enter email or mobile number"
    static let kEnterValidEmailOrMobile         = "Please enter valid email or mobile number"
    static let KOtp                             = "Please enter OTP"
    static let KPhoneCode                       = "Please enter code send to your email ID"
    static let kCity                            = "Please enter City"
    static let kAddress                         = "Please enter your address"
    static let kEnterClass                      = "Please select your Class"
    static let kTermsCondition                  = "Please Select Terms And Conditions"
    static let kEnterOldPassword                = "Please enter old password"
    static let kConfirmPasswordMatch            = "New password & confirm password should be same"
    static let kQuestionImageEmpty            = "Question or QuestionImages cannot be empty"
    
}


struct AlertTitle {
    static let kOk                              = "OK"
    static let kCancel                          = "Cancel"
    static let kDone                            = "Done"
    static let ChooseDate                       = "Choose Date"
    static let SelectCountry                    = "Select Country"
    static let logout                           = "Logout"
    
}


struct Cellidentifier {
    
    static let IntroductionCell                 = "IntroductionCell"
    static let SidebarMenuCell                  = "SidebarMenuCell"
    
    
}



struct OtherConstant {
    static let kAppDelegate                     = UIApplication.shared.delegate as? AppDelegate
    // static let kRootVC                          = UIApplication.shared.keyWindow?.rootViewController
    static let kBundleID                        = Bundle.main.bundleIdentifier!
    static let kGenders: [String]               = ["Male", "Female", "Other"]
    static let kReviewsSortBy: [String]         = ["Recent", "Last Month", "Last Year"]
}
let kSharedAppDelegate                          = OtherConstant.kAppDelegate

func Localised(_ aString:String) -> String {
    
    return NSLocalizedString(aString, comment: aString)
}



struct Indicator {
    
    static func showToast(message aMessage: String)
    {
        DispatchQueue.main.async
            {
                showAlertMessage.alert(message: aMessage)
        }
    }
}

// Enums
enum PhotoSource {
    case library
    case camera
}

enum MessageType {
    case photo
    case text
    case video
    case audio
}

enum MessageOwner {
    case sender
    case receiver
}

enum BottomOptions: Int {
    case search = 0
    case match
    case message
    case post
}

//enum HasCameFrom{
//    case Forgot // forgot Password flow
//    case SignUp
//    case ResetPassword
//}

enum AppColor {
    case Blue, Red
    var color : UIColor {
        switch self {
        case .Blue:
            return UIColor.blue
        case .Red:
            return UIColor.init(hexString: "#C7003B")
        }
    }
}


enum OpenMediaType: Int {
    case camera = 0
    case photoLibrary = 1
    case videoCamera = 2
    case videoLibrary = 3
}

enum DashboardType {
    case NormalUsers
    case Student
    case Teacher
    var userType:String{
        switch self{
        case .NormalUsers:
            return "3"
        case .Student:
            return "1"
        case .Teacher:
            return "2"
        }
    }
}


enum AppFonts {
    case bold(CGFloat),regular(CGFloat)
    var font:UIFont {
        switch self {
        case .bold(let size):
            return UIFont (name: "System", size: size)!
        case .regular(let size):
            return UIFont.systemFont(ofSize: size)
        }
    }
}



//// MARK: ---------Color Constants---------

let appThemeUp               = UIColor.init(red: 231/255, green: 76/255, blue: 60/255, alpha: 1)
let appThemeDown             = UIColor.init(red: 251/255, green: 136/255, blue: 51/255, alpha: 1)

//MARK: ---------Method Constants---------
let kSharedInstance             = SharedClass.sharedInstance
let kSharedSceneDelegate        = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate
let kSharedAppManager           = AppManager.shared
let kSharedUserDefaults         = UserDefaults.standard
let kScreenWidth                = UIScreen.main.bounds.size.width
let kScreenHeight               = UIScreen.main.bounds.size.height

func print_debug(items: Any) {
    print(items)
}

func print_debug_fake(items: Any) {
}
