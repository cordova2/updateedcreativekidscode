//
//  BaseController.swift
//  Nihao
//
//  Created by Anurag Chauhan on 20/11/18.
//  Copyright © 2018 fluper. All rights reserved.
//

import UIKit

protocol APIResponseDelegate {
    func getAPiResponse(dicResponce : [String : Any],requestTitle : String)
}

class BaseController: UIViewController {
    static let shared = BaseController()
    var apiResponseDelegate : APIResponseDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func postToServerAPI(url : String,params : [String : Any],type : kHTTPMethod,guestApi:Bool = false, showHud:Bool = true, completionHandler : @escaping (_ params : [String : Any],_ statusCode : Int)->Void) {
        
        CommonUtils.showHudWithNoInteraction(show: showHud)
        
        TANetworkManager.sharedInstance.requestApi(withServiceName: url,
                                                   requestMethod: type,
                                                   requestParameters: params, withProgressHUD: false)
        { (result: Any?, error: Error?, errorType: ErrorType, statusCode: Int?) in
            
            CommonUtils.showHudWithNoInteraction(show: false)
            if errorType == .requestSuccess {
                let dictResult = kSharedInstance.getDictionary(result)
                
                switch Int.getInt(statusCode) {
                    
                case 200:
//                    print(dictResult)
                    completionHandler(dictResult, Int.getInt(statusCode))
                case 400:
                    completionHandler(dictResult, Int.getInt(statusCode))
                case 403:
                    completionHandler(dictResult, Int.getInt(statusCode))
                case 404:
                    completionHandler(dictResult, Int.getInt(statusCode))
                case 500:
                    completionHandler(dictResult, Int.getInt(statusCode))
                default:
//                    CommonUtils.showToast(message: String.getString(dictResult["message"]))
                    showAlertMessage.alert(message:String.getString(dictResult["message"]))
                }
                
            } else if errorType == .noNetwork {
//                CommonUtils.showToast(message: AlertMessage.knoNetwork)
                showAlertMessage.alert(message:AlertMessage.knoNetwork)
                
            } else {
//                CommonUtils.showToast(message: AlertMessage.kDefaultError)
                showAlertMessage.alert(message:AlertMessage.kDefaultError)
            }
        }
    }
    
    func postToChatServerAPI(url : String,params : [String : Any],type : kHTTPMethod,completionHandler : @escaping (_ params : [String : Any],_ statusCode : Int)->Void) {
        
        TANetworkManager.sharedInstance.requestApi(withServiceName: url,
                                                   requestMethod: type,
                                                   requestParameters: params ,
                                                   withProgressHUD: false)
        { (result: Any?, error: Error?, errorType: ErrorType, statusCode: Int?) in
            
            if errorType == .requestSuccess {
                
                let dictResult = kSharedInstance.getDictionary(result)
                
                switch Int.getInt(statusCode) {
                    
                case 200:
                    
                    print(dictResult)
                    completionHandler(dictResult, Int.getInt(statusCode))
                    
                default:
//                      CommonUtils.showToast(message: String.getString(dictResult["message"]))
                      self.showSimpleAlert(message:String.getString(dictResult["message"]))
                    return
                }
            } else if errorType == .noNetwork {
//                CommonUtils.showToast(message: AlertMessage.knoNetwork)
                self.showSimpleAlert(message:AlertMessage.knoNetwork)
            } else {
//                CommonUtils.showToast(message: AlertMessage.kDefaultError)
                self.showSimpleAlert(message:AlertMessage.kDefaultError)
            }
        }
    }
    
    //MARK:- Multipart
    func postToServerRequestMultiPart(_ url : String, params : [String:Any],imageParams : [[String : Any]],showHud:Bool = true ,completionHandler : @escaping (_ params : [String : Any],_ statusCode : Int)->Void) {
        
        CommonUtils.showHudWithNoInteraction(show: showHud)
        
        TANetworkManager.sharedInstance.requestMultiPart(withServiceName: url,
                                                         requestMethod: .post,
                                                         requestImages: imageParams,
                                                         requestVideos: [:],
                                                         requestData: params)
        { (result: Any?, error: Error?, errorType: ErrorType, statusCode: Int?) in
            
            CommonUtils.showHudWithNoInteraction(show: false)
            
            if errorType == .requestSuccess {
                
                let dictResult = kSharedInstance.getDictionary(result)
                
                switch Int.getInt(statusCode) {
                    
                case 200:
                    completionHandler(dictResult, Int.getInt(statusCode))
                case 404:
                    CommonUtils.showToast(message: String.getString(dictResult["title"]))
                default:
                    CommonUtils.showToast(message: String.getString(kSharedInstance.getDictionary(dictResult[getResponce])["title"]))
                }
            } else if errorType == .noNetwork {
                CommonUtils.showToast(message: AlertMessage.knoNetwork)
            } else {
                CommonUtils.showToast(message: AlertMessage.kDefaultError)
            }
        }
    }
    
    //MARK:- Function For Calling
    func Calling (phoneNumber:String) {
        if let url = URL(string: "tel://\(phoneNumber)"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        }
    }
    
    //    //MARK:- Func for Clock View
    //    func FunctionforClock(callback:@escaping (_ result: String) -> ()) {
    //
    //        DatePickerDialog().show("Choose Time", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .time) {(time) -> Void in
    //            if let TimeFormet = time {
    //                let formatter = DateFormatter()
    //                formatter.timeStyle = .short
    //                let picTime = formatter.string(from: TimeFormet)
    //                callback(picTime)
    //            }
    //        }
    //    }
}
