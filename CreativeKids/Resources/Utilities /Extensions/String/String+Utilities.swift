//
//  String+Utilities.swift
//  HealthTotal
//
//  Created by Office on 23/05/16.
//  Copyright © 2016 Collabroo. All rights reserved.
//

import Foundation

extension String {
    // To Check Whether Email is valid
    func isEmail() -> Bool {
        let emailRegex = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,4})$" as String
        let emailText = NSPredicate(format: "SELF MATCHES %@",emailRegex)
        let isValid  = emailText.evaluate(with: self) as Bool
        return isValid
    }
    // To Check Whether name is valid
    func isValidName() -> Bool {
        //self.trim
        let replaced = self.replacingOccurrences(of: "", with: "")
        let RegEx = "^[([A-Za-z]+[\\s]*[A-Za-z]*)+]{1,30}$"
        let Test = NSPredicate(format:"SELF MATCHES %@", RegEx)
        return Test.evaluate(with: replaced)
    }
    
    // To Check Whether Email is valid
    func isValidString() -> Bool {
        if self == "<null>" || self == "(null)" {
            return false
        }
        return true
    }
    
    func isValidPassword(text: String) -> Bool {
        
        let letters = CharacterSet.letters
        
        let phrase = text
        let range = phrase.rangeOfCharacter(from: letters)
        
        // range will be nil if no letters is found
        if range != nil {
            return false
        }
        else {
            return true
        }
    }
    
    // To Check Whether Phone Number is valid
    
    func isPhoneNumber() -> Bool {
        if self.isStringEmpty() {
            return false
        }
        let phoneRegex = "^\\d{8,15}$"
        let phoneText = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        let isValid = phoneText.evaluate(with: self) as Bool
        return isValid
    }
    
    func isNumber() -> Bool {
        if self.isStringEmpty() {
            return false
        }
        let phoneRegex = "^\\d{1,15}$"
        let phoneText = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        let isValid = phoneText.evaluate(with: self) as Bool
        return isValid
    }
    // Password_Validation
    
    func isPasswordValidate() -> Bool {
        let passwordRegix = "[A-Za-z0-9.@#$%*?:!+-/]{8,25}"
        let passwordText  = NSPredicate(format:"SELF MATCHES %@",passwordRegix)
        
        return passwordText.evaluate(with:self)
    }
    
    
    // To Check Whether URL is valid
    
    func isURL() -> Bool {
        let urlRegex = "(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+" as String
        let urlText = NSPredicate(format: "SELF MATCHES %@", urlRegex)
        let isValid = urlText.evaluate(with: self) as Bool
        return isValid
    }
    
    // To Check Whether Image URL is valid
    
    func isImageURL() -> Bool {
        if self.isURL() {
            if self.range(of: ".png") != nil || self.range(of: ".jpg") != nil || self.range(of: ".jpeg") != nil {
                return true
            } else {
                return false
            }
        } else {
            return false
        }
    }
    
    func toInt() -> Int
    {
        return Int(self) ?? 0
    }
    
    func trimAll()->String
    {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    static func getString(_ message: Any?) -> String {
        guard let strMessage = message as? String else {
            guard let doubleValue = message as? Double else {
                guard let intValue = message as? Int else {
                    guard let int64Value = message as? Int64 else{
                        return ""
                    }
                    return String(int64Value)
                }
                return String(intValue)
            }
            let formatter = NumberFormatter()
            formatter.minimumFractionDigits = 0
            formatter.maximumFractionDigits = 20
            formatter.minimumIntegerDigits = 1
            guard let formattedNumber = formatter.string(from: NSNumber(value: doubleValue)) else {
                return ""
            }
            return formattedNumber
        }
        return strMessage.stringByTrimmingWhiteSpaceAndNewLine()
    }
    
    static func getLength(_ message: Any?) -> Int {
        return String.getString(message).stringByTrimmingWhiteSpaceAndNewLine().count
    }
    
    static func checkForValidNumericString(_ message: AnyObject?) -> Bool {
        guard let strMessage = message as? String else {
            return true
        }
        
        if strMessage == "" || strMessage == "0" {
            return true
        }
        return false
    }
    
    
    // To Check Whether String is empty
    
    func isStringEmpty() -> Bool {
        return self.stringByTrimmingWhiteSpace().count == 0 ? true : false
    }
    
    mutating func removeSubString(subString: String) -> String {
        if self.contains(subString) {
            guard let stringRange = self.range(of: subString) else { return self }
            return self.replacingCharacters(in: stringRange, with: "")
        }
        return self
    }
    

    
    // Get string by removing White Space & New Line
    
    func stringByTrimmingWhiteSpaceAndNewLine() -> String {
        return self.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
    }
    
    // Get string by removing White Space
    
    func stringByTrimmingWhiteSpace() -> String {
        return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
    }
    
    func getSubStringFrom(begin: NSInteger, to end: NSInteger) -> String {
        // var strRange = begin..<end
        // let str = self.substringWithRange(strRange)
        return ""
    }
    
    var html2Attributed: NSAttributedString? {
            do {
                guard let data = data(using: String.Encoding.utf8) else {
                    return nil
                }
                return try NSAttributedString(data: data,
                                              options: [.documentType: NSAttributedString.DocumentType.html,
                                                        .characterEncoding: String.Encoding.utf8.rawValue],
                                              documentAttributes: nil)
            } catch {
                print("error: ", error)
                return nil
            }
        }
     
    //Mark:- getTime
    static func secondsToHoursMinutesSeconds (seconds : Int) -> (String, String, String) {
        let hour = seconds / 3600 == 0 ? "00" : String(seconds / 3600)
        let minute = (seconds % 3600) / 60 == 0 ? "00" : String((seconds % 3600) / 60)
        let second = (seconds % 3600) % 60 == 0 ? "00" : String((seconds % 3600) % 60)
      return (hour, minute,second)
    }
    
    static func convertDateString(dateString : String, fromFormat sourceFormat : String, toFormat desFormat : String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = sourceFormat
        let date = dateFormatter.date(from: dateString) ?? Date()
        dateFormatter.dateFormat = desFormat
        return dateFormatter.string(from: date)
    }
    static func getCurrentDate(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        let date = formatter.date(from: format) ?? Date()
        return formatter.string(from: date)
    }
    
    static func getDateToString(date:Date, format:String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: date)
    }
}



//extension NSAttributedString {
//    var html2Attributed: NSAttributedString? {
//        do {
//                let htmlData = try self.data(from: .init(location: 0, length: self.length),
//                                                       documentAttributes: [.documentType: NSAttributedString.DocumentType.html])
//            return try NSAttributedString(data: htmlData,
//                                          options: [.documentType: NSAttributedString.DocumentType.html,
//                                                    .characterEncoding: String.Encoding.utf8.rawValue],
//                                          documentAttributes: nil)
//
//            } catch {
//                print("error: ", error)
//                return nil
//            }
//        }
//}

extension String{
    func getVideoThumbnailYouTube(completion:@escaping ((String?)->Void)){
        do {
        let regex = try NSRegularExpression(pattern: "(?<=v(=|/))([-a-zA-Z0-9_]+)|(?<=youtu.be/)([-a-zA-Z0-9_]+)|(?<=youtu.com/)([-a-zA-Z0-9_]+)|(?<=embed/)([-a-zA-Z0-9_]+)", options: .caseInsensitive)
        let match = regex.firstMatch(in: self, options: .reportProgress, range: NSMakeRange(0, self.lengthOfBytes(using: String.Encoding.utf8)))
        guard let range = match?.range(at: 0)else{
            completion(nil)
            return
        }
        let youTubeID = (self as NSString).substring(with: range)
            print(youTubeID)
            genarateThumbnailFromYouTubeID(youTubeID: youTubeID)
        } catch {
        completion(nil)
        print(error)
        }
        //From YouTubeID
        func genarateThumbnailFromYouTubeID(youTubeID: String) {
        let urlString = "http://img.youtube.com/vi/\(youTubeID)/1.jpg"
    //    let image = try! (UIImage(withContentsOfUrl: urlString))!
        completion(urlString)
        print(urlString)
        }
    }
}
