//
//  UIViewController.swift
//  Happy Event Demo
//
//  Created by fluper on 21/02/20.
//  Copyright © 2020 Manoj. All rights reserved.
//

import Foundation
import UIKit

var isUserGreetDone = false
enum Storyboard: String{
    case Registration = "Main"
    case Home = "Home"
    case Question = "Question"
    case TeacherDashboard = "SchoolTeacher"
    case StudentDashboard = "SchoolStudent"
    case Chat = "Chat"
}

extension UIViewController{
    var isRootController: Bool {
        return (self.navigationController?.viewControllers.count ?? 0) == 1
    }
    
    static var storyboardID:String {
        return String(describing: self)
    }
    
    static func getControllerObject<T : UIViewController>(storyboard: String, type: T.Type ) -> T {
        let storyboard = UIStoryboard(name: storyboard, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: self.storyboardID) as! T
        return controller
    }
    
    static func getController(storyboard: Storyboard) -> Self {
        return getControllerObject(storyboard: storyboard.rawValue, type: self)
    }
    
    //MARK:- get time For Greet User
    func getCurrentTimeForGreetUser(){
        var greet = ""
        let date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        print(hour)
        print(minutes)
        if hour < 12{
            greet = "GoodMorning"
        }else if hour >= 12 && hour < 16{
            greet = "GoodAfterNoon"
        }else {
            greet = "GoodEvening"
        }
        CommonUtils.greedVoice(voice: greet + (kUserData.userName ?? ""))
        isUserGreetDone = true
    }
    
    //    static func topController() -> UIViewController? {
    //        return topController((UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController)
    //    }
    
    //    static func topController(of controller: UIViewController?) -> UIViewController? {
    //        guard let nav = controller as? UINavigationController else {
    //            guard let tabbar = controller as? UITabBarController else {
    //                guard let drawer = controller as? KYDrawerController else {
    //                    if let presentedController = controller?.presentedViewController {
    //                        return topController(of: presentedController)
    //                    } else {
    //                        return controller
    //                    }
    //                }
    //                return topController(of: drawer.mainViewController)
    //            }
    //            return topController(of:tabbar.selectedViewController)
    //        }
    //        return topController(of: nav.topViewController)
    //    }
    
    //    static func getVisibleController() -> UIViewController? {
    //        let controller = (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController
    //        return visibleController(rootController: controller)
    //    }
    
    //    static func visibleController(rootController: UIViewController?) -> UIViewController? {
    //        return topController(of: rootController)
    //    }
    func pushController(storyboard: Storyboard, controllerType type: UIViewController.Type) {
        let controller = type.self.getController(storyboard: storyboard)
        self.pushController(controller: controller)
    }
    func shareApp(appUrl urlString:String, sorceView: UIView, sender: UIButton? = nil){
        if let urlStr = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
            let objectsToShare = [urlStr]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            //if iPhone
            if UIDevice.isPhone {
                self.present(activityVC, animated: true, completion: nil)
            }
            else {
                //In iPad Change Rect to position Popover
                if let btn = sender{
                    activityVC.popoverPresentationController?.sourceRect = btn.frame
                    activityVC.popoverPresentationController?.sourceView = sorceView
                }
                self.present(activityVC, animated: true, completion: nil)
            }
            
            
        }
    }
    func showPopUp(title: String, message: String, optionTypeCancel: String, optionTypeOther: String, complitationHandler: @escaping ((Int) -> ())){
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        let cancel = UIAlertAction.init(title: optionTypeCancel, style: .default){ _ in
            alert.dismiss(animated: true, completion: nil)
        }
        let Logout = UIAlertAction.init(title: optionTypeOther, style: .destructive){ _ in
            alert.dismiss(animated: true){
                //self.logoutApi()
                complitationHandler(1)
            }
        }
        alert.addAction(cancel)
        alert.addAction(Logout)
        self.navigationController?.present(alert, animated: true, completion: nil)
    }
    
    //    func pushController(storyboard: Storyboard, controllerType type: UIViewController.Type) {
    //        let controller = type.self.getController(storyboard: storyboard)
    //        self.pushController(controller: controller)
    //    }
    
    func pushController(controller: UIViewController) {
        //        let transition              = CATransition()
        //        transition.duration         = 0.35
        //        transition.timingFunction   = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        //        transition.type             = CATransitionType.push
        //        transition.subtype          = AppSharedModel.shared.language == .arabic ? CATransitionSubtype.fromLeft : .fromRight
        //        self.navigationController?.view.layer.removeAllAnimations()
        //        self.navigationController?.view.layer.add(transition, forKey: kCATransition)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func popToController<T: UIViewController>(controllerType: T.Type) {
        let controllers = (self.navigationController?.viewControllers ?? []).reversed()
        for controller in controllers {
            if controller.isKind(of: controllerType) {
                self.navigationController?.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    func showPopUp(storyboard: Storyboard, controllerType type: UIViewController.Type) {
        let controller = type.self.getController(storyboard: storyboard)
        self.showPopUp(controller)
    }
    
    func showPopUp(_ controller: UIViewController) {
        controller.modalTransitionStyle     = UIModalTransitionStyle.crossDissolve
        controller.modalPresentationStyle   = UIModalPresentationStyle.overFullScreen
        controller.view.backgroundColor     = UIColor.darkGray.withAlphaComponent(0.5)
        self.present(controller, animated: true, completion: nil)
    }
    
    //   MARK:- Chamge Status Bar Color
    func statusBarColor(headerColor:UIColor){
        let statusBarView = UIView()
        if UIDevice.current.orientation.isLandscape {
            statusBarView.frame = CGRect(x: 0, y: 0, width: 10, height: 10)
            self.view.addSubview(statusBarView)
        }else{
            if #available(iOS 13.0, *){
                let height = (UIApplication.shared.connectedScenes.first?.delegate as! SceneDelegate).window?.windowScene?.statusBarManager?.statusBarFrame.height ?? CGFloat()
                let width = kScreenHeight > kScreenWidth ? kScreenHeight : kScreenWidth
                if checkDevice(){
                    statusBarView.frame = CGRect(x: 0, y: 0, width: width, height: height + 5)
                }else{
                    statusBarView.frame = CGRect(x: 0, y: 0, width: width, height: height)
                }
                statusBarView.backgroundColor = headerColor
            }else{
                statusBarView.frame = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as! CGRect
                statusBarView.backgroundColor = headerColor
            }
            self.view.addSubview(statusBarView)
        }
        func checkDevice()->Bool{
            (UIDevice().name.lowercased()).contains("iPhone 14 Pro".lowercased())
        }
    }
    
    
    func getStatusBarHeight() -> CGFloat{
        if #available(iOS 13.0, *){
            let height = (UIApplication.shared.connectedScenes.first?.delegate as! SceneDelegate).window?.windowScene?.statusBarManager?.statusBarFrame.height ?? CGFloat()
            if UIDevice().name == "iPhone 14 Pro" || UIDevice().name == "iPhone 14 Pro MAX"{
                return height + 5
            }else{
                return height
            }
            
        }else{
            let rect = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as! CGRect
            return(rect.height)
        }
    }
    
}

protocol UIDocumentPickerExtendedDelegate {
    func docPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL)
}

     
extension UIViewController: UIDocumentPickerDelegate {
    func openDocumentPicker(_ documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        self.present(documentPicker, animated: true, completion: {
            documentPicker.delegate = self
        })
    }
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        controller.dismiss(animated: true, completion: nil)
        guard let viewC = self as? UIDocumentPickerExtendedDelegate else { return }
        viewC.docPicker(controller, didPickDocumentAt: url)
    }
    
    public func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}
extension UITableViewCell: UIDocumentPickerDelegate {
    func openDocumentPicker(_ documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        UIApplication.shared.windows.first?.rootViewController?.present(documentPicker,animated: true,completion: {
            documentPicker.delegate = self
        })
    }
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        controller.dismiss(animated: true, completion: nil)
        guard let viewC = self as? UIDocumentPickerExtendedDelegate else { return }
        viewC.docPicker(controller, didPickDocumentAt: url)
    }
    
    public func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
}

extension UIViewController{
    func hideTabbar(){
        for controller in self.tabBarController?.viewControllers ?? []{
            if controller.isKind(of: Self.self){
                self.tabBarController?.tabBar.isHidden = false
                return
            }
            self.tabBarController?.tabBar.isHidden = true
        }
        
    }
    func showTabbar(){
        self.tabBarController?.tabBar.isHidden = false
    }
}
