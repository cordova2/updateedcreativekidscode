//
//  AppManager.swift
//  CreativeKids
//
//  Created by Creative Kids on 19/03/21.
//

import Foundation
import UIKit
import KYDrawerController


class AppManager:NSObject{
 static let shared = AppManager()
    var application:UIApplication?

    override init() {
        super.init()
    }
    
    func saveDatatoChatHalper(){
        let localDetails = chatUserDefault.getLoggedInUserDetails()
        let userType = String.getstring(localDetails["role"])
        switch userType {
        case "1":
            let userDetails = UsersState()
            userDetails.name = String.getstring(localDetails["Name"])
            userDetails.class = String.getstring(localDetails["classname"])
            userDetails.schoolId = String.getstring(localDetails["schid"])
            userDetails.schoolName = String.getstring(localDetails["schname"])
            userDetails.userId = String.getstring(localDetails["Id"])
            userDetails.userType = String.getstring(UserType.Student.rawValue)
            userDetails.profile_image = String.getString(localDetails["images"])
            userDetails.mobile_number = String.getstring(localDetails["mobile"])
            chatUserDefault.setuserDetials(loggedInUserDetails: userDetails)
            ChatHalper.shared.userReference.child(userDetails.userId ?? "").setValue(userDetails.createDictonary(objects: userDetails))
        case "2":
            let userDetails = UsersState()
            userDetails.name = String.getstring(localDetails["Name"])
            userDetails.class = String.getstring(localDetails["classname"])
            userDetails.schoolId = String.getstring(localDetails["schid"])
            userDetails.schoolName = String.getstring(localDetails["schname"])
            userDetails.userType = String.getstring(UserType.Teacher.rawValue)
            userDetails.userId = String.getstring(localDetails["Id"])
            userDetails.profile_image = String.getString(localDetails["images"])
            userDetails.mobile_number = String.getstring(localDetails["mobile"])
            chatUserDefault.setuserDetials(loggedInUserDetails: userDetails)
            ChatHalper.shared.userReference.child(userDetails.userId ?? "").setValue(userDetails.createDictonary(objects: userDetails))
        default:
            return;
        }
    }
  
    func moveToHome() {
        let viewController = KYDrawerController.getController(storyboard: .Home)
//            let storyboard = UIStoryboard.init(name: "Home", bundle: nil)
//            guard let viewController  = storyboard.instantiateViewController(withIdentifier: "KYDrawerController") as? KYDrawerController else {return}
        //let navigationController: =  UINavigationController(rootViewController: viewController)
        let navigationController = HomeNavigationController(rootViewController: viewController)
            navigationController.isNavigationBarHidden = true
            self.application?.windows.first?.rootViewController  = navigationController
            self.application?.windows.first?.makeKeyAndVisible()
        }
    func moveToTeacherDashboard() {
        let viewController = TeacherTabbarController.getController(storyboard: .TeacherDashboard)
            let navigationController =  UINavigationController(rootViewController: viewController)
            navigationController.isNavigationBarHidden = true
            self.application?.windows.first?.rootViewController  = navigationController
            self.application?.windows.first?.makeKeyAndVisible()
        
        }
    func moveToStudentDashboard() {
        let viewController = StudentTabBarController.getController(storyboard: .StudentDashboard)
            let navigationController =  UINavigationController(rootViewController: viewController)
            navigationController.isNavigationBarHidden = true
            self.application?.windows.first?.rootViewController  = navigationController
            self.application?.windows.first?.makeKeyAndVisible()
        }
    func moveToLogin() {
        let viewController = LoginViewController.getController(storyboard: .Registration)
            let navigationController =  UINavigationController(rootViewController: viewController)
            navigationController.isNavigationBarHidden = true
            self.application?.windows.first?.rootViewController  = navigationController
            self.application?.windows.first?.makeKeyAndVisible()
        }
    func moveToSignup() {
        let viewController = SignupViewController.getController(storyboard: .Registration)
            let navigationController =  UINavigationController(rootViewController: viewController)
            navigationController.isNavigationBarHidden = true
            self.application?.windows.first?.rootViewController  = navigationController
            self.application?.windows.first?.makeKeyAndVisible()
        }
    func moveToWlkThrough() {
        let viewController = WalkThroughViewController.getController(storyboard: .Registration)
            let navigationController =  UINavigationController(rootViewController: viewController)
            navigationController.isNavigationBarHidden = true
            self.application?.windows.first?.rootViewController  = navigationController
            self.application?.windows.first?.makeKeyAndVisible()
        }
    
    func profileEdit() {
        let viewController = EditProfileViewController.getController(storyboard: .Home)
            let navigationController =  UINavigationController(rootViewController: viewController)
            navigationController.isNavigationBarHidden = true
            self.application?.windows.first?.rootViewController  = navigationController
            self.application?.windows.first?.makeKeyAndVisible()
        }
      
    func logout(){
        kSharedUserDefaults.setLoggedInUserDetails(loggedInUserDetails: [:])
        kSharedUserDefaults.setLoggedInAccessToken(loggedInAccessToken: "")
        kSharedUserDefaults.setUserLoggedIn(userLoggedIn: false)
        isUserGreetDone = false
        kSharedAppManager.moveToLogin()
    }
}
