//
//  AppDelegate.swift
//  CreativeKids
//
//  Created by Creative Kids on 17/03/21.
// com.apps.Cordova.CreativeKids
// com.Cordova.CreativeKids
// 9A09D1B0-CCEC-44C5-AD60-31C8217CA6CD

import Foundation
import UIKit
import KYDrawerController
import IQKeyboardManagerSwift
import FirebaseCore

// https://www.getpostman.com/collections/607c5d47db9b9e9b4e5b
@main

class AppDelegate: UIResponder, UIApplicationDelegate {
    
//    let screenProtector = ScreenProtector()
    var window: UIWindow?
    var application:UIApplication?
    var orientationLock = UIInterfaceOrientationMask.portrait
   
    var screenProtector = ScreenProtector()
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.application = application
        AppManager.shared.application = application
        IQKeyboardManager.shared.enable = true
        // Override point for customization after application launch.
        self.window = UIWindow(frame: UIScreen.main.bounds)
        FirebaseApp.configure()
        self.screenProtector.startPreventingRecording()
        AppManager.shared.saveDatatoChatHalper()
        //self.screenProtector.startPreventingScreenshot()
        return true

    }
    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
            return self.orientationLock
    }
}


